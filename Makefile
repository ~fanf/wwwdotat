rsync = rsync -cilpr

it:: site search sync

site::
	mkdir -p target/site/
	rsync -a --delete other/ target/site/
	cargo run --bin generate

search::
	pagefind
	sed -i~ 's/append(a, t1);/a.innerHTML = t1_value;/' \
		target/site/pagefind/pagefind-ui.js
	rsync -clpr --del target/site/pagefind/ chiark:public-html/pagefind/

sync::
	git pull --rebase
	git push
	ssh chiark 'cd wwwdotat && git pull --rebase'
	${rsync} cgi/ chiark:public-cgi/
	${rsync} target/site/ chiark:public-html/
	if [ $$(uname -m) = x86_64 ]; then make bin; fi

bin::
	cargo build --release
	${rsync} target/release/cgi-links chiark:public-cgi/
	${rsync} target/release/karma-farma chiark:bin/
	${rsync} target/release/links-from-mail chiark:bin/

svg::
	perl -nE 'say $$& if m{[a-z0-9-]+[.]svg}' hbs/* md/* | \
	(cd bootstrap-icons-1.10.2; xargs -J% cp -v % ../other/svg/)

dev::
	cargo fmt
	cargo clippy
	cargo build
	cargo run --bin generate
