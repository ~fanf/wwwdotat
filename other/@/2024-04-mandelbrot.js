let shader = {};
let gl = null;

async function fetch_text(url) {
    const r = await fetch(url);
    return await r.text();
}

async function fetch_shader(slug, type) {
    const url = "https://dotat.at/@/2024-04-" + slug + ".glsl";
    const text = await fetch_text(url);
    shader[type] = text;
    shader.status++;
}

function compile_shader(gl, program, type, source) {
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
	throw gl.getShaderInfoLog(shader);
    gl.attachShader(program, shader);
}

function link_shaders(gl, program, vertex, fragment) {
    compile_shader(gl, program, gl.VERTEX_SHADER, vertex);
    compile_shader(gl, program, gl.FRAGMENT_SHADER, fragment);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS))
	throw gl.getProgramInfoLog(program);
    gl.useProgram(program);
}

function shader_parameters(gl, program) {
    shader.parameter = {};
    function attribute(name) {
	shader.parameter[name] = gl.getAttribLocation(program, name);
	if (shader.parameter[name] < 0)
	    throw `could not find ${name} parameter`;
    }
    function uniform(name) {
	shader.parameter[name] = gl.getUniformLocation(program, name);
	if (shader.parameter[name] == null)
	    throw `could not find ${name} parameter`;
    }
    attribute("vertex");
    uniform("transform");
    uniform("marker");
}

function build_shaders() {
    gl = canvas.getContext("webgl");
    if (gl === null) {
	alert("WebGL is not available");
	return;
    }

    shader.program = gl.createProgram();
    link_shaders(gl, shader.program, shader.vertex, shader.fragment);
    shader_parameters(gl, shader.program);

    // clockwise triangle fan order
    const vertices = new Float32Array([
	+1.0, +1.0, 0.0, 1.0,
	+1.0, -1.0, 0.0, 1.0,
	-1.0, -1.0, 0.0, 1.0,
	-1.0, +1.0, 0.0, 1.0,
    ]);
    const buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    gl.vertexAttribPointer(shader.parameter.vertex, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shader.parameter.vertex);

    window.requestAnimationFrame(draw);
}

function vadd(a, b) {
    return { x: a.x + b.x, y: a.y + b.y };
}

function vsub(a, b) {
    return { x: a.x - b.x, y: a.y - b.y };
}

function vscale(s, v) {
    return { x: s * v.x, y: s * v.y };
}

let walk = {};
const marksize = 2048;
const steps = 256;
const grid = 65536;
const radius = 64;
const maxiter = 64;
const infinity = 65536;
const alpha = 1 - 1/64;

function mandelbrot(point) {
    const a = point.x;
    const b = point.y;
    let y = 0.0;
    let x = 0.0;
    let y2 = 0.0;
    let x2 = 0.0;
    for (let i = 0; i < maxiter; i++) {
	y = 2.0 * x * y + b;
	x = x2 - y2 + a;
	y2 = y * y;
	x2 = x * x;
	if (x2 + y2 > infinity) {
	    return false;
	}
    }
    return true;
}

function walk_start(step) {
    // to trace the boundary of the mandelbrot set, we walk a triangle
    // around keeping the rule that one point must be inside the set, one
    // must be outside, and the third point is probed for membership.
    walk = {
	outside: { x: 0, y: 0 },
	inside: { x: 0, y: 0 },
	place: { x: 0, y: 0 },
	smooth: { x: 0, y: 0 },
    };
    // search for the cusp, at roughly +0.25 +0i -- our level set boundary
    // will be slightly offset, so we need to search if the step is small.
    while(mandelbrot(walk.place)) {
	walk.place.x += step;
    }
    // set up a baseline that crosses the boundary at the cusp
    walk.outside.x = walk.place.x;
    walk.inside.x = walk.place.x - step;
    // the first probe point off the real axis sets the shape of
    // the triangular grid, in this case an equilateral grid
    walk.place.x = (walk.inside.x + walk.outside.x) / 2;
    walk.place.y = (3 ** 0.5) * step / 2;
    // initial smoothed position
    walk.smooth.x = 0.25;
}

function walk_step(swap) {
    // rename the points of the current triangle by swapping the
    // current place with the previous inside or outside point,
    // according to the result of the iteration. this gives us the
    // new baseline that crosses the boundary, and an old place
    // that's the third point of the current triangle.
    [ walk.place, walk[swap] ] = [ walk[swap], walk.place ];
    // reflect the old place across the baseline to find a new
    // place that's the third point of a new triangle
    walk.place = vsub(vadd(walk.inside, walk.outside), walk.place);
}

function walk_next() {
    for (let i = 0; i < steps; i++) {
	// find a new place that's outside the mandelbrot set
	while(mandelbrot(walk.place)) {
	    walk_step("inside");
	}
	walk_step("outside");
    }
    // update smoothed position
    walk.smooth = vadd(vscale(alpha, walk.smooth),
		       vscale(1 - alpha, walk.outside));
}

function main() {
    shader.status = 0;
    Promise.all([
	fetch_shader("canvas", "vertex"),
	fetch_shader("mandelbrot", "fragment"),
    ]).then(build_shaders)
    walk_start(1 / grid)
}

function draw() {
    const width = canvas.width = window.innerWidth;
    const height = canvas.height = window.innerHeight;
    const size = width < height ? width : height;
    gl.viewport(0, 0, width, height);

    walk_next();
    const x_pos = walk.smooth.x;
    const y_pos = walk.smooth.y;
    const x_mag = (1 / radius) * (width / size);
    const y_mag = (1 / radius) * (height / size);
    gl.uniformMatrix4fv(shader.parameter.transform,
			false, [
			    x_mag, 0.0, 0.0, 0.0,
			    0.0, y_mag, 0.0, 0.0,
			    0.0, 0.0, 0.0, 0.0,
			    x_pos, y_pos, 0.0, 1.0,
			]);

    const mark_radius = 1 / marksize;
    const mark_thick = 2 / (radius * size);
    const x_mark = walk.inside.x;
    const y_mark = walk.inside.y;
    const mark_min = (mark_radius - mark_thick)**2;
    const mark_max = (mark_radius + mark_thick)**2;
    gl.uniform4fv(shader.parameter.marker,
		  [ x_mark, y_mark, mark_min, mark_max ]);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

    window.requestAnimationFrame(draw);
}
