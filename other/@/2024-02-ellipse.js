const abs = Math.abs
const r2 = Math.sqrt(2);
const pi = Math.PI;
const tau = pi * 2;
const sin = Math.sin;
const cos = Math.cos;
const asin = Math.asin;
const acos = Math.acos;
const atan2 = Math.atan2;

// scale positions but not line thickness or font size
const scale = 180;

let A = 6;
let B = 4;
let X = A - 1;
let Y = B - 1;

function draw() {
    let c = canvas.getContext("2d")
    c.save();
    c.clearRect(0,0, canvas.width, canvas.height);
    c.translate(scale, scale);

    c.font = 'lighter 20px sans-serif';

    function style(w, s, f) {
	c.lineWidth = w;
	c.strokeStyle = s;
	c.fillStyle = f;
    }

    function print(x, y, msg) {
	c.fillText(msg, x * scale, y * scale);
    }

    function line(x1, y1, x2, y2) {
	c.beginPath();
	c.moveTo(x1 * scale, y1 * scale);
	c.lineTo(x2 * scale, y2 * scale);
	c.stroke();
    }
    function line_by(x, y, w, h) {
	c.beginPath();
	c.moveTo(x * scale, y * scale);
	c.lineTo((x + w) * scale, (y + h) * scale);
	c.stroke();
    }

    function rect(x, y, w, h, r) {
	r ??= 0;
	c.beginPath();
	c.roundRect(x * scale, y * scale,
		    w * scale, h * scale, r * scale);
	c.fill();
	c.stroke();
    }

    function spot(x, y, r) {
	c.beginPath();
	c.roundRect(x * scale - r, y * scale - r,
		    r * 2, r * 2, r);
	c.fill();
	c.stroke();
    }

    style(1, "#ccc", "#333");
    spot(A, 0, 25);
    spot(0, B, 25);
    spot(X, Y, 25);
    style(1, "#8888", "#0000");
    rect(0,0,A,B);
    rect(0,0,X,Y);
    style(1, "#880", "#0000");
    line_by(A,0,-100,200);
    line_by(A,0,+200,200);
    style(1, "#088", "#0000");
    line_by(0,B,200,-100);
    line_by(0,B,200,+200);

    function corner(x, y, a, b) {
	let aa = a*a;
	let bb = b*b;
	let h = (aa + bb) ** 0.5;
	let p = aa / h;
	let q = bb / h;
	return [p+x, q+y]
    }

    function ellipse(x, y, a, b) {
	let n = 360;
	for (let i = 0; i < n; i++) {
	    theta = tau * i/n;
	    spot(x + a * cos(theta), y + b * sin(theta), 4);
	}

	let [p,q] = corner(x, y, a, b);
	spot(p, q, 8);
	line_by(p, q, -1,+1);
	line_by(p, q, +1,-1);
	line_by(p, q, (p-x)*b*b, (q-y)*a*a);
    }

    style(1, "#808", "#8088");
    ellipse(0,0,A,B);

    function dist(p, q) {
	return (X - p)**2 + (Y - q)**2;
    }

    let [o,a,b] = [0,A,B];
    for (let i = 0; i < 10000; i++) {
	let [p,q] = corner(o, 0, a, b);
	b += Y - q;
	o += X - p;
	a = A - o;
	if (dist(p,q) < 0.00001) {
	    style(1, "#880", "#8808");
	    ellipse(o,0,a,b);
	    break;
	}
    }

    [o,a,b] = [0,A,B];
    for (let i = 0; i < 10000; i++) {
	let [p,q] = corner(0, o, a, b);
	a += X - p;
	o += Y - q;
	b = B - o;
	if (dist(p,q) < 0.00001) {
	    style(1, "#088", "#0888");
	    ellipse(0,o,a,b);
	    break;
	}
    }

    c.restore();
}

function main() {
    console.log(canvas.width = window.innerWidth);
    console.log(canvas.height = window.innerHeight);
    draw();
}

function reposition(ev) {
    let x = (ev.pageX - scale) / scale;
    let y = (ev.pageY - scale) / scale;

    function dist(a, b) {
	return (x - a)**2 + (y - b)**2;
    }

    let Dxy = dist(X, Y);
    let Daa = dist(A, 0);
    let Dbb = dist(0, B);

    if (Daa < 1 && Daa < Dbb && Daa < Dxy) {
	A = x;
    }
    if (Dbb < 1 && Dbb < Daa && Dbb < Dxy) {
	B = y;
    }
    if (Dxy < 1 && Dxy < Daa && Dxy < Dbb) {
	X = x;
	Y = y;
    }
    draw();
}

function moused(ev) {
    if (ev.buttons != 0) {
	reposition(ev);
    }
}

function touched(ev) {
    if (ev.touches.length > 0) {
	reposition(ev.touches.item(0));
    }
}
