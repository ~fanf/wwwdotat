Tony Finch – pgp keys
=====================

[My current PGP key](fanf.gpg) fingerprint is:

        pub   rsa4096 2017-04-04 [SC]
              D9B6 599A 03AA 1D93 8DC5  A820 72F3 EE0B 78D9 305F
        uid   Tony Finch <dot@dotat.at>
        uid   Tony Finch <fanf@FreeBSD.org>
        uid   Tony Finch <fanf@apache.org>
        uid   Tony Finch <fanf2@cam.ac.uk>
        uid   Tony Finch <fanf@exim.org>
        sub   rsa4096 2017-04-04 [E]

[My old PGP key](fanf-old.gpg) fingerprint is:

        pub   dsa1024 2002-05-03 [SC]
              199C F25B 2679 6D04 63C5  2159 FFC0 F14C 84C7 1B6E
        uid   Tony Finch <dot@dotat.at>
        uid   Tony Finch <fanf@FreeBSD.org>
        uid   Tony Finch <fanf@apache.org>
        uid   Tony Finch <fanf2@cam.ac.uk>
        uid   Tony Finch <fanf@exim.org>
        sub   elg2048 2002-05-03 [E]
