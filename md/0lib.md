A zero-conditions libre software license
========================================


the 0lib license
----------------

    Written by Tony Finch <dot@dotat.at> in Cambridge.

    Permission is hereby granted to use, copy, modify, and/or
    distribute this software for any purpose with or without fee.

    This software is provided 'as is', without warranty of any kind.
    In no event shall the authors be liable for any damages arising
    from the use of this software.

    SPDX-License-Identifier: 0BSD OR MIT-0


because
-------

In the past I used [the CC0 public domain dedication][CC0] on my code, but
it is [problematic][] as a permissive open source license because it
explicitly excludes a grant of patent rights. By contrast, the broad grants
in the [BSD][], [ISC][], and [MIT][] licenses are conventionally understood
to include all patent and copyright permissions necessary for using the
software. So I wanted to replace [CC0][] with something effectively as
permissive as a public domain dedication, but less wordy than conventional
licenses.


words
-----

The license above uses the permission clause from [the 0BSD license][0BSD]
(which is actually a trimmed-down [ISC][] license), and the disclaimer of
warranty from [the Zlib license][zlib]. I adjusted the wording in a few
places to match [the MIT-0 license][MIT-0] where the MIT wording is better.
In particular,

  * Move "is hereby granted" to the start of the sentence, because it reads
    better when there is a following clause (like the clause deleted from
    the [ISC][] license to make the [0BSD][] license).

  * Change the disclaimer to "without warranty of any kind" because
    that is more clearly all-encompassing than the [Zlib][] wording.

  * Remove "held" because that implies a judgment, whereas we are
    making a straightforward assertion.


excuse
------

This is a non-standard license, which is generally a bad idea. However I
intend it to be an abbreviated summary of either [0BSD][] or [MIT-0][],
either of which are fine for my purposes. So the [SPDX line][SPDX] refers
to them instead of using a custom `LicenseRef` tag.


name
----

The name is pronounced "zero lib", taking the `0` from [0BSD][] and the
`lib` from [Zlib][], and [retconned][retcon] to be short for "a
zero-conditions libre software license".


[0BSD]: https://opensource.org/licenses/0BSD
[BSD]: https://opensource.org/licenses/BSD-2-Clause
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
[ISC]: https://opensource.org/licenses/ISC
[MIT-0]: https://opensource.org/licenses/MIT-0
[MIT]: https://opensource.org/licenses/MIT
[Zlib]: https://opensource.org/licenses/Zlib

[retcon]: https://en.wiktionary.org/wiki/retcon
[SPDX]: https://spdx.github.io/spdx-spec/v2.3/using-SPDX-short-identifiers-in-source-files/
[problematic]: http://lists.opensource.org/pipermail/license-review_lists.opensource.org/2012-February/thread.html
