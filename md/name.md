Tony Finch's name
=================

People sometimes ask why my username is `fanf` (e.g. `fanf@FreeBSD.org` or
`fanf@apache.org` etc.). It is because my full name is "Frederick Anthony
Nicholas Finch" so my initials are "F.A.N.F.". It is conveniently
unique (ish). Note that `fanf` is pronounced as one syllable (i.e. not
"fan-eff").

My first name is "Frederick" because it's a family name: for seven
generations the first child in my family has been a boy called
Frederick Finch (though the middle names have varied). One of my
forenames is "Nicholas" in memory of my father's half-brother, Nick
Tomalin, who was killed in the Yom Kippur War when working as a war
correspondent. (Nick was the first husband of the biographer Clare
Tomalin.) "Anthony" is my given name, though I usually abbreviate it
to "Tony".

Sometimes my username is `fanf2` instead of `fanf`. This is my
[Cambridge University
username](https://fanf2.user.srcf.net/hermes/doc/misc/crsids.pdf): the
current Cambridge policy is 5 to 7 characters comprising your initials
plus a distinguishing number allocated first-come-first-served, and
the number starts with a digit greater than 1 to avoid confusion with
letters. (The policy has been different in the past, but the current
one is good enough to have remained stable for many years.) I use
`fanf2` elsewhere when `fanf` is already taken, e.g. on
[GitHub](https://github.com/fanf2) and [Hacker
News](https://news.ycombinator.com/user?id=fanf2).
