---
head_title: "Search – Tony Finch"
body_title: "Search this site"
format: html
...

<link rel="stylesheet" type="text/css" href="/search.css">
<script type="text/javascript" src="/pagefind/pagefind-ui.js"></script>
<script>
    window.addEventListener('DOMContentLoaded', (event) => {
        new PagefindUI({
            element: "#search",
            pageSize: 25,
            resetStyles: false,
        });
    });
    function waitForElement(selector) {
        return new Promise(resolve => {
            if (document.querySelector(selector)) {
                return resolve(document.querySelector(selector));
            }
            const observer = new MutationObserver(mutations => {
                if (document.querySelector(selector)) {
                    resolve(document.querySelector(selector));
                    observer.disconnect();
                }
            });
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        });
    }
    /* autofocus on the search box */
    waitForElement('.pagefind-ui__search-input').then((elem) => {
        elem.focus();
    });
    waitForElement('#sorry').then((elem) => {
        elem.remove();
    });
</script>

<section>
    At the moment, this search index covers <a href="/@/">my blog</a> but
    does not cover <a href="/prog/">my software pages</a> or
    <a href="/:/">my link log</a> (which has its own search facility).
</section>
<section id="search">
    <p id="sorry">Sorry, this page requires JavaScript</p>
</section>
