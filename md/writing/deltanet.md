Deltanet: a clean slate network architecture
============================================




why the name?
-------------

The "delta" in Deltanet refers to a river delta. The way topology
information flows from the core to the edges of the Deltanet is like
the way a river delta has channels that split and rejoin as the water
flows towards the sea.


layers
------

Like the Internet, the Deltanet's layers are not strictly
self-contained. For instance, in both casees layer 3 depends on
auxiliary application-layer protocols: BGP in the Internet, and DNTDP
in the Deltanet. The layering model fits more comfortably as a guide
to packet structure: each layer adds a header to the packet.

The OSI layers do not fit the Internet particularly well, and they fit
the Deltanet worse. For instance, layers 4 and 5 correspond to QUIC,
which has the opposite layering to TLS-over-TCP. As an attempt to
reduce confusion I have given the layers different names.

<style>
  table { margin: 1rem; }
  th, td { padding: 0rem 1rem; }
</style>

| layer | name        | functions |
|:------|:------------|:----------|
| 2     | link        | underlying network and protocols that adapt it to the Deltanet |
| 3     | node        | packet forwarding; establish FIB and local RIB; propagate RIB |
| 4     | connection  | construct paths; end-to-end authentication and encryption |
| 5     | stream      | multipath congestion control; multiplexed data transfer |
| 6     | n/a         | n/a |
| 7     | application | e.g. HTTP, DNTDP, ... |


key differences from the Internet
---------------------------------

  * no addresses: instead there are cryptographic identifiers and paths

  * paths are chosen by source routing, not by routers in the network

  * valley-free and loop-free paths are enforced with only local knowledge

  * a name lookup is necessary to discover the path to a service

  * networks, routers, hosts, and ports are unified as "nodes"

  * nodes are normally multihomed and connections are normally multipath

  * connections bypass link failures without relying on the routing protocol

  * DNTDP is a link-state protocol, not distance-vector like BGP

  * the RIB is built from the FIB instead of the other way round

  * the RIB distributes path characteristics: there's no need for MTU discovery

  * error reporting is out-of-band and aggregated, not in-band like ICMP


glossary
--------

- DNTDP: Deltanet topology distribution protocol
- FIB: forwarding information base
- RIB: routing information base
