---
dw:
  anum: 125
  eventtime: "2006-02-23T19:21:00Z"
  itemid: 202
  logtime: "2006-02-23T19:40:17Z"
  props:
    import_source: livejournal.com/fanf/51976
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/51837.html"
format: casual
lj:
  anum: 8
  can_comment: 1
  ditemid: 51976
  event_timestamp: 1140722460
  eventtime: "2006-02-23T19:21:00Z"
  itemid: 203
  logtime: "2006-02-23T19:40:17Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/51976.html"
title: More streamlining
...

I've been discussing my ideas on the <a href="http://www.ietf.org/html.charters/lemonade-charter.html">Lemonade</a> mailing list.

Unsurprisingly, the gurus there didn't like my attempt to rehabilitate smtps, mainly for political reasons. The RCPTHDR extension probably only has a future as input to the RFC 2822 or RFC 2476 updates.

People seemed fairly keen on <a href="https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-smtp-quickstart.txt">QUICKSTART</a>. I have updated it to shave off a couple of round trips from connections that use STARTTLS.

Further analysis of the <a href="http://www.ietf.org/internet-drafts/draft-ietf-lemonade-burl-04.txt">BURL</a> draft revealed that its pipelining changes aren't totally correct - see <a href="http://www1.ietf.org/mail-archive/web/lemonade/current/msg02018.html">my message on the subject</a>. The upshot is that you can't save a round trip for AUTH.

However, a closer look at the <a href="http://www.ietf.org/rfc/rfc3030.txt">CHUNKING</a> spec revealed that it has a very desirable property: it completely eliminates synchronization points once you have got the connection going and you are sending messages. With basic ESMTP, there is a synchronization point at the DATA command, which has the effect of limiting your sending speed to at most one message per round-trip. CHUNKING allows you to send at the maximum speed TCP can sustain. Nice. The CHUNKING RFC needs a little clarification about its interaction with PIPELINING, but its author has a draft update in the works which will do the trick.

I have more to say about sending email efficiently in future essays on how not to design MTAs.
