---
dw:
  anum: 0
  eventtime: "2006-01-17T12:58:00Z"
  itemid: 179
  logtime: "2006-01-17T13:22:30Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/46213
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/45824.html"
format: casual
lj:
  anum: 133
  can_comment: 1
  ditemid: 46213
  event_timestamp: 1137502680
  eventtime: "2006-01-17T12:58:00Z"
  itemid: 180
  logtime: "2006-01-17T13:22:30Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/46213.html"
title: "AGP+PCI multihead, attempts two and three"
...

Following on from &lt;http://www.livejournal.com/users/fanf/44881.html&gt;, &lt;cam user=jpk28&gt; kindly lent me a couple of non-Mac PCI graphics cards to see if I could get any further with them.

(2) 1995-vintage S3 Vision968

This was amusing. The card is only a little bit more recent than my 1993-1994 gap year at <span style="font-family: sans-serif">inmos</span>, where at the time they made palette-DAC chips for graphics cards and had a respectable share of the market. However this card has an IBM DAC, not an <strike><span style="font-family: sans-serif">inmos</span></strike> <strike>SGS-Thomson</strike> STMicroelectronics one.

<tt>Xorg</tt> <tt>-configure</tt> took rather a long time to run, and this turned out to be because it thought there were 110 S3 cards in the machine and enormous numbers of PCI buses. I edited the generated configuration file to be something more reasonable and tried starting X. The machine wasn't very happy about this: X sort of hung (I can't remember if I managed to kill it or if I had to reboot) and the ethernet card lost its interrupts. Not much success there at all. Since the card has a practially useless 2MB vRAM, I gave up fairly quickly.

(3) 1997-vintage ATI 3D Rage Pro PCI

By the time of this card, separate palette-DAC chips were a thing of the past. It has 4MB vRAM which is just barely tolerable.

The Xorg ATI driver claims that it should recognize this card as a Mach64 series card, but it doesn't, and X instead falls back to the VESA driver. DDC manages to get useful information from the monitor, which is good, but it autoconfigures with too many pixels to be able to maintain a decent refresh rate. 60Hz is nasty.

Multi-head X <em>almost</em> worked, except that when I moved the pointer to the secondary screen it disappeared. Juggling things around (virtual positions of screens, primary/secondary numbering) didn't improve matters - sometimes the VESA screen would have a corrupted display, sometimes X would get confused about where the boundary between the screens was (mouse pointer appearing 1280 pixels from the left of the 1600 pixel screen). I could run X on one screen at a time fine, but not both together.
