---
format: html
lj:
  anum: 104
  can_comment: 1
  ditemid: 106344
  event_timestamp: 1271430540
  eventtime: "2010-04-16T15:09:00Z"
  itemid: 415
  logtime: "2010-04-16T15:09:49Z"
  props:
    personifi_tags: "18:2,6:5,8:47,31:5,30:2,42:5,43:14,1:32,3:23,5:5,19:5,20:5,9:5,21:5,nterms:yes"
    verticals_list: "computers_and_software,technology"
  reply_count: 7
  url: "https://fanf.livejournal.com/106344.html"
title: User agent stats
...

<p>Number of users who sent email using different software in the last couple of days.</p>

<table>
<tr><th align="right">users</th><th align="left">platform</th></tr>
<tr><td align="right"> 6139</td><td>Windows</td></tr>
<tr><td align="right"> 2991</td><td>Macintosh</td></tr>
<tr><td align="right">  681</td><td>iPhone / iPod / iPad</td></tr>
<tr><td align="right">  609</td><td>Linux</td></tr>
<tr><td align="right">   40</td><td>EPOC</td></tr>
</table>

<table>
<tr><th></th><th align="left">mail user agent</th></tr>
<tr><td align="right">14050</td><td>webmail</td></tr>
<tr><td align="right"> 3479</td><td>Thunderbird</td></tr>
<tr><td align="right"> 2358</td><td>Apple Mail</td></tr>
<tr><td align="right"> 2213</td><td>Outlook</td></tr>
<tr><td align="right">  569</td><td>other Microsoft</td></tr>
<tr><td align="right">  411</td><td>Mulberry</td></tr>
<tr><td align="right">  237</td><td>Eudora</td></tr>
<tr><td align="right">  186</td><td>Entourage</td></tr>
<tr><td align="right">  106</td><td>Evolution</td></tr>
<tr><td align="right">   54</td><td>Alpine</td></tr>
<tr><td align="right">   30</td><td>Opera</td></tr>
<tr><td align="right">   29</td><td>other Mozilla</td></tr>
<tr><td align="right">   19</td><td>KMail</td></tr>
<tr><td align="right">   15</td><td>Pegasus</td></tr>
<tr><td align="right">   13</td><td>Claws</td></tr>
<tr><td align="right">   10</td><td>Mutt</td></tr>
</table>

<p>Number of users who logged into webmail in the last couple of days:</p>

<table>
<tr><th></th><th align="left">webmail platform</th></tr>
<tr><td align="right">  15803</td><td>Windows</td></tr>
<tr><td align="right">   3646</td><td>Macintosh</td></tr>
<tr><td align="right">    777</td><td>Linux</td></tr>
<tr><td align="right">    358</td><td>iPhone</td></tr>
<tr><td align="right">    120</td><td>Symbian</td></tr>
<tr><td align="right">     23</td><td>SunOS</td></tr>
<tr><td align="right">      3</td><td>iPod / iPad</td></tr>
</table>

<table>
<tr><th></th><th align="left">webmail browser</th></tr>
<tr><td align="right">   8108</td><td>Firefox</td></tr>
<tr><td align="right">   5250</td><td>MSIE 8</td></tr>
<tr><td align="right">   4262</td><td>MSIE 7</td></tr>
<tr><td align="right">   2960</td><td>Safari</td></tr>
<tr><td align="right">   2226</td><td>Chrome</td></tr>
<tr><td align="right">   1202</td><td>MSIE 6 or older</td></tr>
<tr><td align="right">    176</td><td>Opera</td></tr>
<tr><td align="right">     48</td><td>BlackBerry</td></tr>
<tr><td align="right">     46</td><td>SonyEricsson</td></tr>
<tr><td align="right">     26</td><td>SAMSUNG</td></tr>
</table>
