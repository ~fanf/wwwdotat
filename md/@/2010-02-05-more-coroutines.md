---
format: casual
lj:
  anum: 183
  can_comment: 1
  ditemid: 105655
  event_timestamp: 1265383260
  eventtime: "2010-02-05T15:21:00Z"
  itemid: 412
  logtime: "2010-02-05T15:21:28Z"
  props:
    personifi_tags: "8:25,31:25,23:25,1:25,32:25,20:50,9:25"
  reply_count: 2
  url: "https://fanf.livejournal.com/105655.html"
title: More coroutines
...

Following <a href="http://fanf.livejournal.com/105413.html">my previous entry</a> I have put up <a href="http://dotat.at/cgi/git?p=picoro.git">a git repository containing a few little coroutine implementations</a>. They are all pure ANSI/ISO C 89 and don't depend on magic stack pointer arithmetic, but instead maintain a special coroutine at the top of the stack which is responsible for allocating space for new coroutines.
