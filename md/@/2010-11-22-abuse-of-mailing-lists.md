---
format: html
lj:
  anum: 129
  can_comment: 1
  ditemid: 110721
  event_timestamp: 1290425940
  eventtime: "2010-11-22T11:39:00Z"
  itemid: 432
  logtime: "2010-11-22T11:39:00Z"
  props:
    personifi_tags: "15:6,17:3,8:46,31:3,27:3,32:3,1:40,3:6,25:6,19:3,26:6,20:12,9:15,21:6,22:6,nterms:yes"
    verticals_list: computers_and_software
  reply_count: 8
  url: "https://fanf.livejournal.com/110721.html"
title: Abuse of mailing lists
...

<p>On the 2nd November, one of our users had the bright idea of promoting an event by CC:ing their ad to a very large number of our internal mailing lists, almost all of which were thoroughly inappropriate for the subject of the message. This is, of course, <a href="http://www.cam.ac.uk/cs/isss/rules/other/bulkemail/general.html">not allowed</a>, and in such cases the perpetrator is usually invited to visit the office of my colleague who specializes in abuse (as it were) and/or their college authorities are asked to deal with it as a disciplinary matter.</p>

<p>Happily this kind of abuse is very rare - it's the first time it has happened to us for about ten years. However, just like last time, the first event has led to a very annoying series of copy-cat offenses, as various other thoughtless idiots have used "reply to all" to distribute their own off-topic bumf. I'm lucky that I don't have to tell them off myself, but I understand that their usual reaction is to whine "I thought it was OK because someone else did it".</p>

<p>We're trying various things to put a stop to this. Just getting the senders deaned isn't enough.</p>

<p>I added a simple clause to the Exim configuration on our mailing list system to freeze any message with too many recipients. This turned out not to be enough: one of the miscreants is a GMail user, and GMail splits a message so that there are at most 20 recipients per copy (even though the standard says 100 should work fine).</p>

<pre>
  warn
    condition = ${if >{$recipients_count}{25} }
    control   = freeze
</pre>

<p>So I added a second clause that freezes messages that have too many addresses in the To: and CC: headers. This still has the weakness of missing BCC:ed messages. (<i>Edited:</i> I came up with a more cunning regex which only counts @lists addresses, since counting all To: and CC: addresses led to far too many false positives.)</p>

<pre>
  warn
    condition = ${if >{${strlen:${sg {$h_To:,$h_CC:} \
                                     {(?s)(?!@lists[.]cam[.]ac[.]uk).} {} \
                      }}} {25} }
    control   = freeze
</pre>

<p>Freezing the messages is a fairly benign way of dealing with awkward edge cases, since we can inspect the frozen messages and either let them pass if they are OK, or bounce them or delete them if not. Messages that are frozen incorrectly just get delayed for a little while. We have used this technique for a while to detect spam from compromised accounts, so I already had the necessary support scripts.</p>

<p>I have also updated the message of the day on <a href="https://webmail.hermes.cam.ac.uk">the Hermes webmail front page</a>, to get the word out that this kind of idiocy is not tolerated. Hopefully we will not have to escalate to public floggings.</p>
