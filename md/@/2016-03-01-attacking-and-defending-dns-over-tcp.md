---
dw:
  anum: 242
  eventtime: "2016-03-01T18:01:00Z"
  itemid: 447
  logtime: "2016-03-01T18:01:21Z"
  props:
    commentalter: 1491292415
    import_source: livejournal.com/fanf/141807
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/114674.html"
format: html
lj:
  anum: 239
  can_comment: 1
  ditemid: 141807
  event_timestamp: 1456855260
  eventtime: "2016-03-01T18:01:00Z"
  itemid: 553
  logtime: "2016-03-01T18:01:21Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/141807.html"
title: Attacking and defending DNS over TCP
...

<p>A month ago I wrote about <a href="http://fanf.livejournal.com/140566.html">a denial of service attack that caused a
TCP flood on one of our externally-facing authoritative DNS
servers</a>. In that case the
mitigation was to discourage legitimate clients from using TCP;
however those servers are sadly still vulnerable to TCP flooding
attacks, and because they are authoritative servers they have to be
open to the Internet. But we get some mitigation from having off-site
anycast servers so it isn't completely hopeless.</p>

<p>This post is about our inward-facing recursive DNS servers.</p>

<p>Two weeks ago, <a href="https://googleonlinesecurity.blogspot.co.uk/2016/02/cve-2015-7547-glibc-getaddrinfo-stack.html">Google and Red Hat announced
CVE-2015-7547</a>,
a remote code execution vulnerability in glibc's <code>getaddrinfo()</code>
function. There are no good mitigations for this vulnerability so we
patched everything promptly (and I hope you did too).</p>

<p>There was some speculation about whether it is possible to exploit
CVE-2015-7547 through a recursive DNS server. No-one could demonstrate
an attack but the general opinion was that it is likely to be
possible. <a href="http://dankaminsky.com/2016/02/20/skeleton/">Dan Kaminsky described the vulnerability as "a skeleton key
of unkown strength"</a>,
citing the general rule that "attacks only get better".</p>

<p>Yesterday, Jaime Cochran and Marek Vavruša of Cloudflare described <a href="https://blog.cloudflare.com/a-tale-of-a-dns-exploit-cve-2015-7547/">a
successful cache traversal exploit of
CVE-2015-7547</a>.
Their attack relies on the behaviour of djbdns when it is flooded with
TCP connections - it drops older connections.</p>

<p>BIND's behaviour is different; it retains existing connections and
refuses new connections. This makes it trivially easy to DoS BIND's
TCP listener, and given the wider discussion about <a href="https://tools.ietf.org/html/draft-ietf-dnsop-5966bis">proper support for
DNS over TCP</a>
including <a href="https://tools.ietf.org/html/draft-ietf-dnsop-edns-tcp-keepalive">persistent
connections</a>
djbdns's behaviour appears to be superior.</p>

<p>So it's unfortunate that djbdns has better connection handling which makes it vulnerable to this attack, whereas BIND is protected by being worse!</p>

<p>Fundamentally, we need DNS server software to catch up with the best
web servers in the quality of their TCP connection handling.</p>

<p>But my immediate reaction was to realise that my servers would have a
problem if the Cloudflare attack (or something like it) became at all
common. Our recursive DNS servers were open to TCP connections from
the wider Internet, and we were relying on BIND to reject queries from
foreign clients. This clearly was not sufficiently robust. So now,
where my firewall rules used to have a comment</p>

<pre><code>    # XXX or should this be CUDN only?
</code></pre>

<p>there are now some actual filtering rules to block incoming TCP
connections from outside the University. (I am still sending DNS
REFUSED replies over UDP since that is marginally more friendly than
an ICMP rejection and not much more expensive.)</p>

<p>Although this change was inspired by CVE-2015-7547, it is <em>really</em>
about general robustness; it probably doesn't have much effect on our
ability to evade more sophisticated exploits.</p>
