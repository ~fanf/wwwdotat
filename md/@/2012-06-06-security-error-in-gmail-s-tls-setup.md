---
format: html
lj:
  anum: 23
  can_comment: 1
  ditemid: 120855
  event_timestamp: 1339015800
  eventtime: "2012-06-06T20:50:00Z"
  itemid: 472
  logtime: "2012-06-06T19:50:22Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/120855.html"
title: "Security error in GMail's TLS setup"
...

<p><a href="http://tools.ietf.org/html/rfc6186">RFC 6186</a> specifies how to use SRV records to auto-configure an MUA given an email address. There is an error with the setup of GMail's RFC 6185 SRV records which means that a correctly implemented client will fail to connect, reporting a certificate mismatch / security failure.</p>

<p>The SRV records are:
<pre>
  _submission._tcp.gmail.com. 84664 IN  SRV   5 0 587 smtp.gmail.com.
  _imaps._tcp.gmail.com.      84628 IN  SRV   5 0 993 imap.gmail.com.
  _pop3s._tcp.gmail.com.      86400 IN  SRV  20 0 995 pop.gmail.com.
</pre>
</p>

<p>These servers all present certificates that only authenticate the
corresponding SRV target host name - they do not contain any
SubjectAltName fields for gmail.com. They also do not support Server Name
Indication to vary their certificate according to the name expected by
the client. The gmail.com zone is not signed with DNSSEC.</p>

<p>RFC 6186 says that certificate verification follows the procedure set out
in RFC 6125. The relevant part is <a href="http://tools.ietf.org/html/rfc6125#section-6.2.1">RFC 6125 section 6.2.1</a>. In particular,
<blockquote>
   Each reference identifier in the list SHOULD be based on the source
   domain and SHOULD NOT be based on a derived domain (e.g., a host name
   or domain name discovered through DNS resolution of the source
   domain).
</blockquote>
That is, the only acceptable reference identifier for an RFC 6128 client that
has been given a gmail.com address is gmail.com. This does not match the
identifiers in any of the certificates.</p>

<p>This error is not a security vulnerability in itself, though it can lead
to vulnerabilities when combined with human factors, such as:
<ul>
<li>Users of RFC 6186 clients might turn off certificate verification for gmail.com in order to make connections work.</li>
<li>Authors of RFC 6186 clients might copy the same mistake for interop purposes and write code match certificates against insecurely derived identifiers (SRV target hostnames in non-DNSSEC zones).</li>
</ul>
</p>
