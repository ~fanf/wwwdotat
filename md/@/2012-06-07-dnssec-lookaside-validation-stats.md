---
format: html
lj:
  anum: 19
  can_comment: 1
  ditemid: 121107
  event_timestamp: 1339077240
  eventtime: "2012-06-07T13:54:00Z"
  itemid: 473
  logtime: "2012-06-07T12:54:27Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/121107.html"
title: DNSSEC lookaside validation stats
...

<p>DNSSEC lookaside validation can provide a chain of trust to signed zones whose parents have not yet deployed DNSSEC. DLV validators usually use a registry maintained by the ISC, <a href="http://dlv.isc.org/">dlv.isc.org</a>. The <a href="http://tools.ietf.org/html/rfc5074&quot;">DLV spec</a> uses NSEC records to identify empty spans of the registry zone, so a validator does not need to query the registry for names that fall in these spans. A side-effect of this is that you can use the NSEC records to get a list of all the entries in the registry.</p>

<pre>
  # note the hacky difference in trailing dots
  # to make starting and ending conditions different
  n=dlv.isc.org
  while [ $n != dlv.isc.org. ]
  do
    r=$(dig +short nsec $n)
    n=${r%% *}
    echo $n
  done
</pre>

<p>There are only 2664 entries in the DLV at the moment, so it is tiny compared to the number of properly delegated DNSSEC zones. The most popular TLDs in the DLV are:</p>

<pre>
 495 arpa
 436 com
 254 de
 253 org
 237 net
  64 eu
  61 uk
  57 info
  50 hu
  42 nl
  37 fr
  36 ro
  36 ch
  32 cz
  30 ru
  29 us
  24 jp
  23 br
  22 biz
  20 it
  20 co
  20 at
  19 be
  18 name
  18 au
</pre>
