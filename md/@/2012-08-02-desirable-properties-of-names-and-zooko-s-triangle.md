---
format: html
lj:
  anum: 85
  can_comment: 1
  ditemid: 122965
  event_timestamp: 1343938020
  eventtime: "2012-08-02T20:07:00Z"
  itemid: 480
  logtime: "2012-08-02T19:07:48Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/122965.html"
title: "Desirable properties of names and Zooko's Triangle"
...

<p>In <a href="http://zooko.com/distnames.html">Zooko's original essay</a> he described the trade-off as "Decentralized, Secure, Human-Meaningful: Choose Two". The problem with this is that these properties are not independent: a central naming authority can rescind or re-assign a name, which is effectively an attack on the association between the name and its referent. So not decentralised implies not secure.</p>

<p>Paul "ciphergoth" Crowley talks about <a href="http://www.lshift.net/blog/2007/11/21/squaring-zookos-triangle-part-two">Zooko's Tetrahedron</a> which is a four-way tradeoff between "Memorable, Decentralized, Specific, Transferable". In his essay he gives a weak meaning to "Specific", such that security requires decentralisation and specificity but is not implied by them. However it seems that the Tahoe-LAFS developers use "Specific" with the same meaning that others give to "Secure" - which leads to the same lack of independence as Zooko's version.</p>

<p>In <a href="http://www.skyhunter.com/marcs/petnames/IntroPetNames.html">Marc Stiegler's essay on petnames</a> the trichotomy is described as "Memorable, Global, Securely Unique" which can each be chosen independently. This is the version I prefer, though I might use different terms for the concepts - perhaps "User-friendly, Transferable, Secure".</p>
