---
dw:
  anum: 223
  eventtime: "2009-06-02T13:09:00Z"
  itemid: 386
  logtime: "2009-06-02T13:11:56Z"
  props:
    commentalter: 1491292398
    import_source: livejournal.com/fanf/100365
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/99039.html"
format: html
lj:
  anum: 13
  can_comment: 1
  ditemid: 100365
  event_timestamp: 1243948140
  eventtime: "2009-06-02T13:09:00Z"
  itemid: 392
  logtime: "2009-06-02T13:11:56Z"
  props:
    personifi_tags: "8:100,1:100"
  reply_count: 9
  url: "https://fanf.livejournal.com/100365.html"
title: CRSIDs and email addresses
...

<p>I've recently written a three page memo about the advantages of our username scheme compared to "friendly name" email addresses in large domains. I have put <a href="https://fanf2.user.srcf.net/hermes/doc/misc/crsids.pdf">a copy of the PDF on the web</a> which you can read if you like.</p>
