---
dw:
  anum: 83
  eventtime: "2016-09-09T13:04:00Z"
  itemid: 467
  logtime: "2016-09-09T12:04:48Z"
  props:
    commentalter: 1491292422
    import_source: livejournal.com/fanf/146763
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/119635.html"
format: html
lj:
  anum: 75
  can_comment: 1
  ditemid: 146763
  event_timestamp: 1473426240
  eventtime: "2016-09-09T13:04:00Z"
  itemid: 573
  logtime: "2016-09-09T12:04:48Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/146763.html"
title: "Listing zones with jq and BIND's statistics channel"
...

<p>The <a href="https://stedolan.github.io/jq/tutorial/">jq tutorial</a> demonstrates simple filtering and rearranging of JSON data, but lacks any examples of how you might combine jq's more interesting features. So I thought it would be worth writing this up.

<p>I want a list of which zones are in which views in my DNS server. I can get this information from BIND's statistics channel, with a bit of processing.

<p>It's quite easy to get a list of zones, but the zone objects returned from the statistics channel do not include the zone's view.

<pre>	$ curl -Ssf http://[::1]:8053/json |
	  jq '.views[].zones[].name' |
	  head -2
	"authors.bind"
	"hostname.bind"</pre>

<p>The view names are keys of an object further up the hierarchy.

<pre>	$ curl -Ssf http://[::1]:8053/json |
	  jq '.views | keys'
	[
	  "_bind",
	  "auth",
	  "rec"
	]</pre>

<p>I need to get hold of the view names so I can use them when processing the zone objects.

<p>The first trick is <tt>to_entries</tt> which turns the "views" object into an array of name/contents pairs.

<pre>	$ curl -Ssf http://[::1]:8053/json |
	  jq -C '.views ' | head -6
	{
	  <span style="color:blue">"_bind"</span>: {
	    <span style="color:blue">"zones"</span>: [
	      {
	        <span style="color:blue">"name"</span>: <span style="color:green">"authors.bind"</span>,
	        <span style="color:blue">"class"</span>: <span style="color:green">"CH"</span>,
	$ curl -Ssf http://[::1]:8053/json |
	  jq -C '.views | to_entries ' | head -8
	[
	  {
	    <span style="color:blue">"key"</span>: <span style="color:green">"_bind"</span>,
	    <span style="color:blue">"value"</span>: {
	      <span style="color:blue">"zones"</span>: [
	        {
	          <span style="color:blue">"name"</span>: <span style="color:green">"authors.bind"</span>,
	          <span style="color:blue">"class"</span>: <span style="color:green">"CH"</span>,</pre>

<p>The second trick is to save the view name in a variable before descending into the zone objects.

<pre>	$ curl -Ssf http://[::1]:8053/json |
	  jq -C '.views | to_entries |
		.[] | .key as $view |
		.value' | head -5
	{
	  <span style="color:blue">"zones"</span>: [
	    {
	      <span style="color:blue">"name"</span>: <span style="color:green">"authors.bind"</span>,
	      <span style="color:blue">"class"</span>: <span style="color:green">"CH"</span>,</pre>

<p>I can then use string interpolation to print the information in the format I want. (And use array indexes to prune the output so it isn't ridiculously long!)

<pre>	$ curl -Ssf http://[::1]:8053/json |
	  jq -r '.views | to_entries |
		.[] | .key as $view |
		.value.zones[0,1] |
		"\(.name) \(.class) \($view)"'
	authors.bind CH _bind
	hostname.bind CH _bind
	dotat.at IN auth
	fanf2.ucam.org IN auth
	EMPTY.AS112.ARPA IN rec
	0.IN-ADDR.ARPA IN rec</pre>

<p>And that's it!
