---
dw:
  anum: 249
  eventtime: "2015-10-07T11:43:00Z"
  itemid: 431
  logtime: "2015-10-07T10:43:23Z"
  props:
    commentalter: 1491292413
    import_source: livejournal.com/fanf/137485
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/110585.html"
format: html
lj:
  anum: 13
  can_comment: 1
  ditemid: 137485
  event_timestamp: 1444218180
  eventtime: "2015-10-07T11:43:00Z"
  itemid: 537
  logtime: "2015-10-07T10:43:23Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 7
  url: "https://fanf.livejournal.com/137485.html"
title: crit-bit tries without allocation
...

<p>Crit-bit tries have fixed-size branch nodes and a constant overhead
per leaf, which means they can be used as an embedded lookup
structure. Embedded lookup structures do not need any extra memory
allocation; it is enough to allocate the objects that are to be
indexed by the lookup structure.</p>

<p>An embedded lookup structure is a data structure in which the internal
pointers used to search for an object (such as branch nodes) are
embedded within the objects you are searching through. Each object can
be a member of at most one of any particular kind of lookup structure,
though an object can simultaneously be a member of several different
kinds of lookup structure.</p>

<p>The <a href="https://svnweb.freebsd.org/base/head/sys/sys/queue.h?view=markup">BSD <code>&lt;sys/queue.h&gt;</code>
macros</a>
are embedded linked lists. They are used frequently in the kernel, for
instance in the network stack to chain <a href="https://svnweb.freebsd.org/base/head/sys/sys/mbuf.h?view=markup#l178"><code>mbuf</code> packet
buffers</a>
together. Each mbuf can be a member of a list and a tailq. There is
also a
<a href="http://cvsweb.openbsd.org/cgi-bin/cvsweb/src/sys/sys/tree.h?rev=HEAD&amp;content-type=text/x-cvsweb-markup"><code>&lt;sys/tree.h&gt;</code></a>
which is used by <a href="http://cvsweb.openbsd.org/cgi-bin/cvsweb/src/usr.bin/ssh/monitor_mm.h?rev=HEAD&amp;content-type=text/x-cvsweb-markup">OpenSSH's privilege separation memory
manager</a>. Embedded red-black trees also appear in <a href="https://github.com/jemalloc/jemalloc/blob/HEAD/include/jemalloc/internal/rb.h">jemalloc</a>.</p>

<h2>embedded crit-bit branch node structure</h2>

<p>DJB's crit-bit branch nodes require three words: bit index, left
child, and right child; embedded crit-bit branches are the same with
an additional parent pointer.</p>

<pre><code>    struct branch {
        uint index;
        void *twig[2];
        void **parent;
    };
</code></pre>

<p>The "twig" child pointers are tagged to indicate whether they point to
a branch node or a leaf. The parent pointer normally points to the
relevant child pointer inside the parent node; it can also point at
the trie's root pointer, which means there has to be exactly one root
pointer in a fixed place.</p>

<p>(An aside about how I have been counting overhead: DJB does not
include the leaf string pointer as part of the overhead of his
crit-bit tries, and I have followed his lead by not counting the leaf
key and value pointers in my crit-bit and qp tries. So by this logic,
although an embedded branch adds four words to an object, it only
counts as three words of overhead. Perhaps it would be more honest to
count the total size of the data structure.)</p>

<h2>using embedded crit-bit tries</h2>

<p>For most purposes, embedded crit-bit tries work the same as external
crit-bit tries.</p>

<p>When searching for an object, there is a final check that the search
key matches the leaf. This check needs to know where to find the
search key inside the leaf object - it should not assume the key is at
the start.</p>

<p>When inserting a new object, you need to add a branch node to the
trie. For external crit-bit tries this new branch is allocated; for
embedded crit-bit tries you use the branch embedded in the new leaf
object.</p>

<h2>deleting objects from embedded crit-bit tries</h2>

<p>This is where the fun happens. There are four objects of interest:</p>

<ul>
<li><p>The doomed leaf object to be deleted;</p></li>
<li><p>The victim branch object which needs to remain in the trie, although
it is embedded in the doomed leaf object;</p></li>
<li><p>The parent branch object pointing at the leaf, which will be
unlinked from the trie;</p></li>
<li><p>The bystander leaf object in which the parent branch is embedded,
which remains in the trie.</p></li>
</ul>

<p>The plan is that after unlinking the parent branch from the trie, you
rescue the victim branch from the doomed leaf object by moving it into
the place vacated by the parent branch. You use the parent pointer in
the victim branch to update the twig (or root) pointer to follow the
move.</p>

<p>Note that you need to beware of the case where the parent branch
happens to be embedded in the doomed leaf object.</p>

<h2>exercise for the reader</h2>

<p>Are the parent pointers necessary?</p>

<p>Is the movement of branches constrained enough that we will always
encounter a leaf's embedded branch in the course of searching for that
leaf? If so, we can eliminate the parent pointers and save a word of
overhead.</p>

<h2>conclusion</h2>

<p>I have not implemented this idea, but following <a href="http://fanf.livejournal.com/137283.html">Simon Tatham's
encouragement</a> I have written
this description in the hope that it inspires someone else.</p>
