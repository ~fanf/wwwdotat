---
dw:
  anum: 130
  eventtime: "2006-06-09T10:44:00Z"
  itemid: 236
  logtime: "2006-06-09T10:47:27Z"
  props:
    commentalter: 1491292362
    import_source: livejournal.com/fanf/60769
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/60546.html"
format: casual
lj:
  anum: 97
  can_comment: 1
  ditemid: 60769
  event_timestamp: 1149849840
  eventtime: "2006-06-09T10:44:00Z"
  itemid: 237
  logtime: "2006-06-09T10:47:27Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/60769.html"
title: RFC 2222 bis inter alia
...

A quick note that the "simple authentication and security layer" specification has been updated as <a href="http://www.rfc-editor.org/rfc/rfc4422.txt">RFC 4422</a>.

(Confusingly, Erlang also has a SASL, but in that case it's the "system application support libraries".)

There's also been an enormous upload of new LDAP RFCs, numbered 4510..4533 (24 documents!).
