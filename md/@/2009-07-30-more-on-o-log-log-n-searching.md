---
dw:
  anum: 134
  eventtime: "2009-07-30T15:51:00Z"
  itemid: 389
  logtime: "2009-07-30T18:33:15Z"
  props:
    commentalter: 1491292366
    import_source: livejournal.com/fanf/101174
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/99718.html"
format: html
lj:
  anum: 54
  can_comment: 1
  ditemid: 101174
  event_timestamp: 1248969060
  eventtime: "2009-07-30T15:51:00Z"
  itemid: 395
  logtime: "2009-07-30T18:33:15Z"
  props:
    personifi_tags: "8:75,1:75,3:25,4:25,nterms:yes"
    verticals_list: "computers_and_software,technology"
  reply_count: 5
  url: "https://fanf.livejournal.com/101174.html"
title: More on O(log(log(n))) searching
...

<p>Thanks to everyone for the informative comments on <a href="http://fanf.livejournal.com/100975.html">my previous post</a>.</p>

<p>I found a bug in my simulation code which meant that many of the tests were performed on arrays that were half-full of zeroes (oops). This distorted the stats a little, and more for larger N because I tested fewer different arrays for large N. This bug caused the increase in s.d. for large N: the corrected code has the same s.d. for all N. Another effect was to increase the maximum number of iterations required to find an entry. After the fix the maximum number of iterations goes down to log(log(N))+12 for the pure secant method, and (log(N)+14)/2 for Junio's mixed secant/bisection method. Altogether much more well-behaved.</p>
