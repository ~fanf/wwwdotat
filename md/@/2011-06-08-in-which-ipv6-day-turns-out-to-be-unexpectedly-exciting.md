---
format: html
lj:
  anum: 76
  can_comment: 1
  ditemid: 113996
  event_timestamp: 1307553660
  eventtime: "2011-06-08T17:21:00Z"
  itemid: 445
  logtime: "2011-06-08T16:21:46Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/113996.html"
title: In which IPv6 day turns out to be unexpectedly exciting
...

<p>As part of <a href="http://www.worldipv6day.org/">IPv6 Day</a> we have enabled IPv6 on the important parts of our mail service: mx.cam (incoming mail), ppsw.cam (outgoing), and smtp/imap/pop/webmail.hermes. Most of the University is IPv4-only, apart from the Computing Service's staff network, the Institute of Astronomy, the <a href="http://www.cusu.cam.ac.uk/">CUSU</a>/<a href="http://www.srcf.ucam.org">SRCF</a> network, and a smattering elsewhere. Today has been mostly unexciting for us, which is cheerful. Mostly.</p>
<p>This afternoon I got an alarming email and phone call from a departmental sysadmin whose mail was all of a sudden being rejected by our outgoing relay. (This turned out to be one of the very few occasions where my special "accept all mail to postmaster regardless of ACLs" rule actually got used by someone to work around a problem!)</p>
<p>Looking at the logs I immediately saw that the rejected mail was arriving over IPv6 from a 6to4 address; our ACLs do not treat 6to4 addresses as being inside the University, so the mail was being rejected.</p>
<p>The quickest way to fix mail delivery was to add <tt>disable_ipv6</tt> to the Exim configuration on the sender.</p>
<p>The breakage suddenly started happening at 15:24, which immediately made me suspect a bogus IPv6 router had appeared on the server's LAN at that time. Malc has <a href="http://malc.org.uk/6doom">a good description of how Windows Internet Connection Sharing breaks connectivity</a> by issuing rogue router advertisements.</p>
<p>It is possible to identify the machine responsible for this kind of braindamage. The unwanted 6to4 address was of the form <tt>2002:836f:...</tt> where <tt>2002::/16</tt> is the 6to4 prefix and the two following hexadectets are the IPv4 address of the tunnel endpoint. <tt>836f:xxxx</tt> is hex for 131.111.ddd.ddd.</p>
<p>I trawled my logs for other 6to4 lossage and found a couple of colleges that had a few messages from their web servers rejected. One of them had at least four machines issuing rogue RAs.</p>
<p>It is going to be, um, interesting dealing with this in the future...</p>
