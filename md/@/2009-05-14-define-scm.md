---
dw:
  anum: 112
  eventtime: "2009-05-14T11:15:00Z"
  itemid: 384
  logtime: "2009-05-14T11:32:33Z"
  props:
    commentalter: 1491292365
    import_source: livejournal.com/fanf/100022
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/98416.html"
format: html
lj:
  anum: 182
  can_comment: 1
  ditemid: 100022
  event_timestamp: 1242299700
  eventtime: "2009-05-14T11:15:00Z"
  itemid: 390
  logtime: "2009-05-14T11:32:33Z"
  props:
    personifi_tags: "17:14,8:42,23:28,1:42,3:14,9:28,21:14"
  reply_count: 1
  url: "https://fanf.livejournal.com/100022.html"
title: Define SCM
...

<p>Here's an abbreviation to avoid, because its various meanings overlap so heavily.</p>
<ul>
<li>Source Code Manager: synonym for version control system, e.g. as used by the Linux crowd in the early days of git.</li>
<li>Software Configuration Manager: usually incorporates version control, build system, archival of build products, and CASE methodology baggage, e.g. ClearCASE and Vesta.</li>
<li>System Configuration Manager: for system administrators rather than developers, e.g. cfengine and Puppet.</li>
</ul>
