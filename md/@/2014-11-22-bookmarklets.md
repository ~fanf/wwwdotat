---
dw:
  anum: 51
  eventtime: "2014-11-22T16:26:00Z"
  itemid: 410
  logtime: "2014-11-22T16:25:34Z"
  props:
    commentalter: 1491292408
    import_source: livejournal.com/fanf/132277
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/105011.html"
format: html
lj:
  anum: 181
  can_comment: 1
  ditemid: 132277
  event_timestamp: 1416673560
  eventtime: "2014-11-22T16:26:00Z"
  itemid: 516
  logtime: "2014-11-22T16:25:34Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/132277.html"
title: Bookmarklets
...

<p>I recently saw <a href="https://news.ycombinator.com/item?id=8554132">FixedFixer</a> on Hacker News. This is a bookmarklet which turns off CSS position:fixed, which makes a lot of websites less annoying. Particular offenders include Wired, Huffington Post, Medium, et cetera ad nauseam. A lot of them are unreadable on my old small phone because of all the crap they clutter up the screen with, but even on my new bigger phone the clutter is annoying. Medium's bottom bar is particularly vexing because it looks just like mobile Safari's bottom bar. Bah, thrice bah, and humbug! But, just run FixedFixer and the crap usually disappears.</p>

<p>The code I am using is very slightly adapted from the HN post. In readable form:</p>
<pre>
  (function(elements, elem, style, i) {
    elements = document.getElementsByTagName('*');
    for (i = 0; elem = elements[i]; i++) {
      style = getComputedStyle(elem);
      if (style && style.position == 'fixed')
        elem.style.position = 'static';
    }
  })();
</pre>

<p>Or in bookmarklet style:<br>
<tt>javascript:(function(ee,e,s,i){ee=document.getElementsByTagName('*');for(i=0;e=ee[i];i++){s=getComputedStyle(e);if(s&&s.position=='fixed')e.style.position='static';}})();</tt></p>

<p>Adding bookmarklets in iOS is a bit annoying because you can't edit the URL when adding a bookmark. You have to add a rubbish bookmark then as a separate step, edit it to replace the URL with the bookmarklet. Sigh.</p>

<p>I have since added a second bookmarklet, because when I was trying to read about AWS a large part of the text was off the right edge of the screen and they had disabled scrolling and zooming so it could not be read. How on earth can they publish a responsive website which does not actually work on a large number of phones?!</p>

<p>Anyway, OverflowFixer is the same as FixedFixer, but instead of changing position='fixed' to position='static', it changes overflow='hidden' to overflow='visible'.</p>

<p>When I mentioned these on Twitter, <a href="https://twitter.com/furrfu/status/533624959227412481">Tom said "Please share!"</a> so this is a slightly belated reply. Do you have any bookmarklets that you particularly like? Write a comment!</p>
