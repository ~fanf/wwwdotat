---
dw:
  anum: 156
  eventtime: "2006-01-04T20:07:00Z"
  itemid: 172
  logtime: "2006-01-04T20:15:12Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/44522
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/44188.html"
format: casual
lj:
  anum: 234
  can_comment: 1
  ditemid: 44522
  event_timestamp: 1136405220
  eventtime: "2006-01-04T20:07:00Z"
  itemid: 173
  logtime: "2006-01-04T20:15:12Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/44522.html"
title: Joe jobbing
...

While upgrading the Exim configuration on ppswitch, I noticed that the Computer Lab is suffering from a fairly vicious joe-job. It started just over a week ago and seems to be centred on three addresses; fortunately all of them are invalid. It has upped their junk email counts by just over a factor of ten and it's still increasing. The following are daily rejections for the last fortnight in blog order...
<pre>
.00     528711 (8 messages per second)
.01     281682
.02     128430
.03     124632 (1 Jan 2006)
.04     169562
.05     166818
.06     190821
.07     204235
.08      67480
.09      29091
.10      31859 (Newton's birthday)
.11      30676
.12      42042
.13      39324
.14      44769
</pre>
