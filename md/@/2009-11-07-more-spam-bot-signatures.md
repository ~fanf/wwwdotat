---
format: html
lj:
  anum: 114
  can_comment: 1
  ditemid: 104050
  event_timestamp: 1257617040
  eventtime: "2009-11-07T18:04:00Z"
  itemid: 406
  logtime: "2009-11-07T18:29:03Z"
  props:
    personifi_tags: "15:4,17:8,18:4,8:41,23:16,16:8,1:37,3:12,4:8,19:8,9:37,nterms:no"
    verticals_list: computers_and_software
  reply_count: 2
  url: "https://fanf.livejournal.com/104050.html"
title: More spam bot signatures
...

<p>There's another spam bot heuristic which is the exact complement to the one described in <a href="http://fanf.livejournal.com/103833.html">my prevous post</a>. The idea is to keep rack of how many different HELO domains an SMTP client uses.</p>

<pre>
  defer
    message   = Probable spam bot HELO varies between $sender_rate domains
    # whitelist checks go here
    ratelimit = 2.2 / 1w / per_conn / strict \
      / unique=$sender_helo_name / $sender_host_address
</pre>

<p>This is mostly the same as the code in my previous post, but the lookup key is the client's IP address, and we only increase the measured rate if the client uses a different unique HELO domain.</p>

<p>This check also works very well at detecting spam bots. When testing it I noticed that one particular bot likes to use a HELO domain consisting entirely of random upper-case letters, and it only talks to one of our servers with the lowest IP address. So I added a specific (cheaper) check to deal with it. This causes the bot to go away, and legitimate senders will retry with a different server.</p>

<pre>
  defer
    message   = Probable spam bot HELO - please try another server
    condition = ${if and{{ eq{$primary_hostname}{ppsw-0.csi.cam.ac.uk} } \
                              { match{$sender_helo_name}{^[A-Z]+\$} }} }
</pre>

<p>There is a small risk of false positives with the variable HELO domain test. Some outgoing mail server clusters are behind a NAT, so we see HELO domains from multiple servers coming from the same IP address. I also found a number of false positives for the popular HELO domain check (most prominently rediffmail.com and easyjet.com). The way I'm dealing with them (which I hope will work in the long term) is as follows.</p>

<p>For the popular HELO domain check described in my previous post, I maintain a lookup table of legitimate HELO domains that trigger the check. The following replaces the hard-coded check for <tt>localhost.localdomain</tt> that appears in my previous entry.</p>

<pre>
    condition = ${if !match_domain{$sender_helo_name}{cdb;DB/helo_ok.cdb} }
</pre>

<p>As well as allowing through clients whose HELO domain can be verified, I now also check <a href="http://www.dnswl.org">dnswl.org</a> for well-known legitimate senders, and I maintain a table of sending hosts that slip through the other checks.</p>

<pre>
  ! verify   = helo
  ! hosts    = +helo_ok
  ! dnslists = list.dnswl.org
</pre>

<p>To avoid problems with false positives, my anti-spam-bot checks return a temporary error code ("defer" in Exim-speak instead of "deny"). I then have a nightly audit script which looks for hosts that appear to be repeatedly retrying a messages and which should be added to my whitelist tables. It might be possible to automate this table maintenance, again using the ratelimit feature, but I expect that will require a bit more experience to determine the right thresholds.</p>
