---
dw:
  anum: 66
  eventtime: "2006-01-16T21:11:00Z"
  itemid: 178
  logtime: "2006-01-16T21:25:02Z"
  props:
    commentalter: 1491292328
    import_source: livejournal.com/fanf/45938
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/45634.html"
format: casual
lj:
  anum: 114
  can_comment: 1
  ditemid: 45938
  event_timestamp: 1137445860
  eventtime: "2006-01-16T21:11:00Z"
  itemid: 179
  logtime: "2006-01-16T21:25:02Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/45938.html"
title: rate limiting again
...

At <a href="http://www.ukuug.org/events/spring2006/">the UKUUG <strike>winter</strike> spring conference</a> I'm going to be presenting a paper on my email rate-limiting work. This gives me some incentive to work a bit more on its deployment :-)

I've been discovering that it's very hard to set a limit which minimizes the inconvenicence (e.g. admin work maintaining the list of ratelimit exemptions; false positives because people don't realize they need to warn us about bulk email beforehand) but at the same time provides decent protection against unwanted floods. The spam incident last term illustrated this well: I had (foolishly) assumed that spam would typically be one recipient per message, but the spammers managed to find a hole that allowed them many recipients per message, so my ratelimiting system didn't spot the flood.

This problem is similar to the problem of setting an appropriate work factor for anti-spam proof-of-work systems: http://www.cl.cam.ac.uk/~rnc1/proofwork.pdf

So I'm now experimenting with per-recipient limits in conjunction with per-message limits, to see how awkward it is to set the limits for that - probably just as bad, but per-recipient limits are closer to what we actually care about.

I've also had an idea about making the countermeasures less irritating for the user. Rejecting the message (even with a 450 try later code) is likely to cause problems for shitty mailmerge software that can't retry. So what we can probably do instead is accept the message and freeze it on ppswitch's queue, using the <tt>control</tt> = <tt>freeze</tt> ACL modifier, and after we have been alerted to the problem, we can check the messages and thaw them with <tt>exim</tt> <tt>-Mt</tt>. This is a fair amount of admin faff, so I'll probably develop a web interface to move the work to the users.
