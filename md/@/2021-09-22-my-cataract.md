---
dw:
  anum: 117
  eventtime: "2021-09-22T18:18:00Z"
  itemid: 532
  logtime: "2021-09-22T17:26:33Z"
  props:
    commentalter: 1639611238
    interface: flat
    picture_keyword: ""
    revnum: 2
    revtime: 1638888036
  url: "https://fanf.dreamwidth.org/136309.html"
format: md
...

My cataract
===========

As well as being very short-sighted in my right eye, I have a cataract
in my left eye.

_This post is going to discuss medical stuff that you might prefer to skip._

> This story continues with an eye examination at Addenbrooke's
> [cataract clinic][], and eventually [cataract surgery][].

[cataract clinic]: https://dotat.at/@/2022-02-23-cataract-clinic.html
[cataract surgery]: https://dotat.at/@/2023-01-14-cataract-op.html



how it was
----------

When I was very small, my grandmother saw me squinting and told my
parents that they should get my eyes looked at; and that's how we
found out I had a congenital cataract.

It is (or was) weird enough to make opticians curious: it was like a
little crystal in the middle of the lens. The effect on my vision was
curious too.

In bright light, when my pupil was small, I could see almost nothing
with my left eye, so I had no depth perception. This was problematic
for school games involving flying balls, like cricket or tennis.

In low light I could see blurry shapes, but for a long time I thought
my left eye was basically useless. But when I was about 20 we went on
a trip to a theme park. I went in to a 3D cinema, not expecting to get
much out of it, but I didn't want to be separated from my friends. I
was surprised that I did, in fact, see the exaggerated 3D effects of a
dog putting its nose in my face and things like that. Fun!


(lack of) treatment
-------------------

When I was still growing, I regularly (once or twice a year) went to
Moorfields Eye Hospital in London to get the cataract examined by an
expert. It never really changed much, and it wasn't troublesome enough
to justify surgery, especially since I was still growing and surgical
techniques were improving, so it made sense to leave it.

I became short-sighted around puberty, and since my teenage years my
eyes have just been checked by normal opticians, and my right eye
messed around a lot more than my left. We continued to leave the
cataract alone.


middle age
----------

Now I am in my late 40s, and in the last few years I have started
getting presbyopia. Many years ago I chose to get glasses with small
frames so that the edges of the lenses were not too thick; now I peer
under the lenses when I need to look closely at something.

At about the same time as I noticed I was getting long-sighted, my
cataract also changed. Basically, the whole lens clouded over. This
has made it obvious that I had a useful amount of peripheral vision in
my left eye, because now I am much more frequently surprised by people
or things that sneak up on my left.


surgery?
--------

I had a long-delayed [eye test earlier this month][rmc28] during which
we discussed my cataract. Cataract surgery is a lot better now than it
was, and my cataract is a lot more annoying than it was, so I think
it's worth getting a specialist opinion on whether surgery will help
more than it hurts.

To be honest the idea of it is freaky and scary, but rationally I know
a lot of people have cataract surgery each year, and I hear less
horrible things about it than I do about laser surgery for myopia.

Today I got a letter from Addenbrooke's to say their triage team had
rejected my referral, because the referral form was incomplete or
unclear or sent to the wrong place or something. Vexing. So I emailed
my optician and my GP with a list of things that I think need to be
mentioned in the referral, with reference to some [useful documents][]
about the clinical criteria needed to justify it.

Hopefully the second try will actually get a specialist to agree to
eyeball my eyeball...

[rmc28]: https://rmc28.dreamwidth.org/1493898.html

[useful documents]: https://www.loc-online.co.uk/cambridgeshire-loc/documents-you-may-find-useful/useful-documents/

postscript
----------

A few weeks later some combination of my GP and optometrist managed to get the referral un-rejected, so I have an appointment with the Addenbrooke's cataract clinic on 23rd February 2022. A bit of a wait, but I was told to expect it...
