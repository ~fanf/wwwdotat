---
dw:
  anum: 28
  eventtime: "2007-06-23T13:55:00Z"
  itemid: 290
  logtime: "2007-06-23T14:15:34Z"
  props:
    commentalter: 1491292342
    import_source: livejournal.com/fanf/74647
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/74268.html"
format: html
lj:
  anum: 151
  can_comment: 1
  ditemid: 74647
  event_timestamp: 1182606900
  eventtime: "2007-06-23T13:55:00Z"
  itemid: 291
  logtime: "2007-06-23T14:15:34Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/74647.html"
title: Spelling reform is pointless
...

<p>Here are some of my favourite pointless suggestions for syntactic changes to programming languages.</p>

<h3>Bitwise not and exclusive or</h3>

<p>By analogy with negation and subtraction, I think the operators for not and xor should be spelled the same. I.e. instead of <tt>a ^ b</tt> write <tt>a ~ b</tt>.</p>

<h3>Indirection</h3>

<p>Prefix <tt>*</tt> in C is disastrous for both expression and declaration syntax. Pascal's postfix <tt>^</tt> for indirection is much more sensible. Fortunately the previous reform frees up <tt>^</tt> in C for this purpose. We can then abolish <tt>-></tt> since it becomes superfluous.</p>

<p>Instead of
<pre>    <b>void</b> (*signal(<b>int</b> sig, <b>void</b> (*func)(<b>int</b>)))(<b>int</b>);</pre>
We get
<pre>    <b>void</b> signal(<b>int</b> sig, <b>void</b> func^(<b>int</b>))^(<b>int</b>);</pre>
</p>

<h3>Associativity of relational operators</h3>

<p>It doesn't make sense for relational operators to be associative. To specify that in <tt>a > b > c</tt> the first comparison gives a boolean result that is compared with <tt>c</tt> is utter nonsense. It's much more sensible to say that relational operators chain as in mathematical notation, so that <i>expr relop expr relop expr ... expr relop expr</i> is equivalent to <i>expr relop expr</i> <b>and</b> <i>expr relop expr</i> <b>and</b> ... <b>and</b> <i>expr relop expr</i> except that each <i>expr</i> is evaluated at most once. BCPL has this syntax except for the latter guarantee, so you can't say <tt>’0’&lt;=rdch()&lt;=’9’</tt> which is a shame.</p>

<h3>Precedence of logical <b>not</b></h3>

<p>Logical <b>not</b> or <tt>!</tt> should have precedence between logical <b>and</b> / <tt>&&</tt> and the relational operators, so that there's much less need for an <b>unless</b> keyword.</p>

<h3>Counting with <b>for</b> loops</h3>

<p>The Modula (etc.) style <b>for</b> <i>var</i> <tt>=</tt> <i>init</i> <b>to</b> <i>limit</i> <b>by</b> <i>step</i> <b>do</b> ... is too wordy. The C style <b>for</b> <tt>(</tt><i>var</i> <tt>=</tt> <i>init</i><tt>;</tt> <i>var</i> <tt>&lt;</tt> <i>limit</i><tt>;</tt> <i>var</i> <tt>+=</tt> <i>step</i><tt>)</tt> ... is too generic and too repetitive. Lua's <b>for</b> <i>var</i> <tt>=</tt> <i>init</i><tt>,</tt> <i>limit</i><tt>,</tt> <i>step</i> <b>do</b> ... is way too cryptic. I quite like an idea from one of <a href="https://simont.livejournal.com/">👤simont</a>atham's school friends, which fits in nicely with chained relational operators and mathematical notation. It comes in a few variants, for counting up or down, with exclusive or inclusive limits, with unity or non-unity step:
<pre>
    <b>for</b> <i>init</i> <tt>&lt;=</tt> <i>var</i> <tt>&lt;</tt> <i>limit</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&gt;=</tt> <i>var</i> <tt>&gt;</tt> <i>limit</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&lt;=</tt> <i>var</i> <tt>&lt;=</tt> <i>limit</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&gt;=</tt> <i>var</i> <tt>&gt;=</tt> <i>limit</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&lt;=</tt> <i>var</i> <tt>&lt;</tt> <i>limit</i> <b>by</b> <i>step</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&gt;=</tt> <i>var</i> <tt>&gt;</tt> <i>limit</i> <b>by</b> <i>step</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&lt;=</tt> <i>var</i> <tt>&lt;=</tt> <i>limit</i> <b>by</b> <i>step</i> <b>do</b>...
    <b>for</b> <i>init</i> <tt>&gt;=</tt> <i>var</i> <tt>&gt;=</tt> <i>limit</i> <b>by</b> <i>step</i> <b>do</b>...
</pre>
</p>

<h3>Assignment and equality</h3>

<p>I'm a firm believer in shunning bare <tt>=</tt> as an operator, and using <tt>:=</tt> for assignment and <tt>==</tt> for testing equality - at least in languages where assignment is an expression. There's much less opportunity for confusion in languages where assignment is a statement and therefore <b>if</b> <i>var</i> <tt>=</tt> <i>expr</i> <b>then</b> ... is a syntax error. Since I have a head full of Lua at the moment I will note that it has one place where assignment and equality can clash, in table constructor expressions, where the former is key/value construction and the latter is list-style construction with a boolean value.</p>
