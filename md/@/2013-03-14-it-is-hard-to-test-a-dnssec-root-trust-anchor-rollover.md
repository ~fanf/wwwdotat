---
format: html
lj:
  anum: 160
  can_comment: 1
  ditemid: 126368
  event_timestamp: 1363263420
  eventtime: "2013-03-14T12:17:00Z"
  itemid: 493
  logtime: "2013-03-14T12:17:25Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/126368.html"
title: It is hard to test a DNSSEC root trust anchor rollover
...

<p>ICANN are running <a href="http://www.icann.org/en/news/public-comment/root-zone-consultation-08mar13-en.htm">a consultation on their plans for replacing the DNSSEC root key</a>. I wondered if there was any way to be more confident that the new key is properly trusted before the old key is retired. My vague idea was to have a test TLD (along the lines of the <a href="http://idn.icann.org/">internationalized test domains</a>) whose delegation is only signed by the new root key, not the normal zone-signing keys used for the production TLDs. However this won't provide a meaningful test: the prospective root key becomes trusted because it is signed by the old root key, just like the zone-signing keys that sign the rest of the root zone. So my test TLD would ultimately be validated by the old root key; you can't use a trick like this to find out what will happen when the old key is removed.</p>

<p>So it looks like people who run validating resolvers will have to use some out-of-band diagnostics to verify that their software is tracking the key rollover correctly. In the case of BIND, I think the only way to do this currently is to cat the <tt>managed-keys.bind</tt> pseudo-zone, and compare this with the root DNSKEY RRset. Not user-friendly.</p>
