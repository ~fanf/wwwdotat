---
dw:
  anum: 2
  eventtime: "2006-12-22T20:22:00Z"
  itemid: 270
  logtime: "2006-12-22T20:22:23Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/69586
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/69122.html"
format: html
lj:
  anum: 210
  can_comment: 1
  ditemid: 69586
  event_timestamp: 1166818920
  eventtime: "2006-12-22T20:22:00Z"
  itemid: 271
  logtime: "2006-12-22T20:22:23Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/69586.html"
title: Proleptic UTC
...

<p>A "proleptic" time scale is one whose definition has been extended so that it can be used outside its normal range of validity. For example, it's common for computer software to implement only the current leap year rules regardless of the year in question, which means they implement the proleptic Gregorian calendar. This is a straight-forward simplification that's useful if you aren't too worried about handling historical dates anachronistically.</p>

<p>Most current computer time scales follow a form of Universal Time which has a fixed 24 * 60 * 60 = 86400 seconds per day, synchronized to UTC but with some kind of fudge to handle leap seconds. It would be much more satisfactory for the computer's clock to count seconds of atomic time all the time, and use a leap seconds table to implement UTC properly.</p>

<p>However, the current rules for translating between atomic time and universal time only date back to 1972, which is fairly hopeless if you want your time scale to be usable for historical purposes. Of course, atomic time only dates back to 1958 which is not much better. We need to come up with some plausible rules for proleptic TAI and proleptic UTC so that we can handle historical times within the same framework that we use for times nearer the present.</p>

<p>Since there was no TAI before 1958, and TAI is defined to be synchronized with UT at the start of 1958, the obvious way of dealing with earlier times is to assume that TAI = UTC = UT before that point.</p>

<p>Between 1958 and 1972 there was a time scale called "UTC" but it is nothing like the current version. This old UTC (oUTC) had variable-length seconds and occasionally made 0.1s steps. The history of its variations is kept by the <a href="ftp://maia.usno.navy.mil/ser7/tai-utc.dat">US Naval Observatory</a>. The time scale described in that table does mesh reasonably smoothly with TAI-UT=0 at the start of 1958 (even though 1961 is the start of the oUTC time scale) and TAI-UTC=10 at the start of 1972. The problem with using this table directly is that it means we have to introduce complications into the code which are not necessary for most purposes. It also implies a degree of accuracy that can't really be justified for events that old.</p>

<p>(Digression: I'm deliberately not distinguishing between UT0 / UT1 / UT2 before 1958 because practical timescales back then were not well synchronized owing to disagreements about the locations of observatories. An alternative that would appeal to pedantic astronomers would be to align our base time scale to <a href="http://en.wikipedia.org/wiki/Ephemeris_Time">ephemeris time</a> and <a href="http://en.wikipedia.org/wiki/Terrestrial_Time">terrestrial time</a> which is TAI+32.184s. However that needlessly complicates things since we would have to work out how to extend back into history the non-uniform translation to UT, and it requires all the complexity of oUTC.)</p>

<p>It seems to me that a reasonable solution is to use the USNO's table to extend the leap second system back to 1958. This means that we will not need to deal with non-integer offsets between time scales nor with variable-rate seconds. My method is to use the equations in the table to work out the TAI-oUTC values for 00:00 on 1st January and 1st July each year (the points immediately after each possible leap second insertion). Assuming that these values are a good approximation to TAI-UT1, I add leap seconds to create a proleptic UTC (pUTC) which keeps pUTC-oUTC less than 0.9. This is the same as the modern UTC-UT1 (DUT1) rule.</p>

<pre>
date		mjd	tai - utc	leap
1958-01-01	36204	0.0024020	0
1958-07-01	36385	0.2369780	0
1959-01-01	36569	0.4754420	0
1959-07-01	36750	0.7100180	1
1960-01-01	36934	0.9484820	1
1960-07-01	37116	1.1843540	1
1961-01-01	37300	1.4228180	1
1961-07-01	37481	1.6573940	2
1962-01-01	37665	1.8458580	2
1962-07-01	37846	2.0491572	2
1963-01-01	38030	2.2558260	2
1963-07-01	38211	2.4591252	3
1964-01-01	38395	2.7657940	3
1964-07-01	38577	3.1016660	3
1965-01-01	38761	3.5401300	4
1965-07-01	38942	3.9747060	4
1966-01-01	39126	4.3131700	4
1966-07-01	39307	4.7823220	5
1967-01-01	39491	5.2592500	5
1967-07-01	39672	5.7284020	6
1968-01-01	39856	6.2053300	6
1968-07-01	40038	6.5770740	7
1969-01-01	40222	7.0540020	7
1969-07-01	40403	7.5231540	8
1970-01-01	40587	8.0000820	8
1970-07-01	40768	8.4692340	9
1971-01-01	40952	8.9461620	9
1971-07-01	41133	9.4153140	10
1972-01-01	41317	9.8922420	10
</pre>

<p>Thus my leap second table looks like this:</p>

<pre>
   date      time   pUTC-TAI
1959-06-30 23:59:60     1
1961-06-30 23:59:60     2
1963-06-30 23:59:60     3
1964-12-31 23:59:60     4
1966-06-30 23:59:60     5
1967-06-30 23:59:60     6
1968-06-30 23:59:60     7
1969-06-30 23:59:60     8
1970-06-30 23:59:60     9
1971-06-30 23:59:60    10
1972-06-30 23:59:60    11
1972-12-31 23:59:60    12
1973-12-31 23:59:60    13
1974-12-31 23:59:60    14
1975-12-31 23:59:60    15
1976-12-31 23:59:60    16
1977-12-31 23:59:60    17
1978-12-31 23:59:60    18
1979-12-31 23:59:60    19
1981-06-30 23:59:60    20
1982-06-30 23:59:60    21
1983-06-30 23:59:60    22
1985-06-30 23:59:60    23
1987-12-31 23:59:60    24
1989-12-31 23:59:60    25
1990-12-31 23:59:60    26
1992-06-30 23:59:60    27
1993-06-30 23:59:60    28
1994-06-30 23:59:60    29
1995-12-31 23:59:60    30
1997-06-30 23:59:60    31
1998-12-31 23:59:60    32
2005-12-31 23:59:60    33
</pre>
