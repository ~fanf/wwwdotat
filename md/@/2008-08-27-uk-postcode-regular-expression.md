---
dw:
  anum: 191
  eventtime: "2008-08-27T17:43:00Z"
  itemid: 357
  logtime: "2008-08-27T17:43:12Z"
  props:
    commentalter: 1491292390
    hasscreened: 1
    import_source: livejournal.com/fanf/92539
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/91583.html"
format: html
lj:
  anum: 123
  can_comment: 1
  ditemid: 92539
  event_timestamp: 1219858980
  eventtime: "2008-08-27T17:43:00Z"
  itemid: 361
  logtime: "2008-08-27T17:43:12Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 15
  url: "https://fanf.livejournal.com/92539.html"
title: UK postcode regular expression
...

<p>A little contribution for anyone else who searches the web for this in the future.</p>

<p>The <a href="http://www.govtalk.gov.uk/gdsc/html/noframes/PostCode-2-1-Release.htm">UK postcode</a> consists of two parts. The first part is the Outward Postcode, or Outcode. This is separated by a single space from the second part which is the Inward Postcode, or Incode. The Outcode directs mail to the correct local area for delivery. The Incode is used to sort the mail at the local area delivery office.</p>

<p>The Outcode has 6 possible formats (as follows) and the Incode is consistently numeric, alpha, alpha format.</p>
<ul>
<li>AN NAA</li>
<li>ANN NAA</li>
<li>ANA NAA</li>
<li>AAN NAA</li>
<li>AANN NAA</li>
<li>AANA NAA</li>
</ul>
<p>There are some restrictions on the letters:</p>
<ol>
<li>The letters [QVX] are not used in the first position.</li>
<li>The letters [IJZ] are not used in the second position.</li>
<li>The only letters to appear in the third position are [ABCDEFGHJKSTUW].</li>
<li>The only letters to appear in the fourth position are [ABEHMNPRVWXY].</li>
<li>The letters [CIKMOV] are not used in the second part.</li>
</ol>

<p>This translates into a perl extended regex as follows (with slightly relaxed whitespace):</p>
<pre>qr{\b
    ([A-PR-UWYZ]\d[\dA-HJKSTUW]? # rules 1,3
    |[A-PR-UWYZ][A-HK-Y]\d[\dABEHMNPRVWXY]? # rules 1,2,4
    )[\t ]{1,2}
    (\d[ABD-HJLNP-UW-Z]{2}) # rule 5
\b}x</pre>

<p><i>Update: <a href="http://fanf.livejournal.com/92745.html">more here</a>.</i></p>
