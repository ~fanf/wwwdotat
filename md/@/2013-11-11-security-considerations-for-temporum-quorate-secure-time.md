---
dw:
  anum: 152
  eventtime: "2013-11-11T23:12:00Z"
  itemid: 399
  logtime: "2013-11-11T23:12:05Z"
  props:
    import_source: livejournal.com/fanf/129371
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/102296.html"
format: html
lj:
  anum: 91
  can_comment: 1
  ditemid: 129371
  event_timestamp: 1384211520
  eventtime: "2013-11-11T23:12:00Z"
  itemid: 505
  logtime: "2013-11-11T23:12:05Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/129371.html"
title: "Security considerations for temporum: quorate secure time"
...

<p>The security of <a href="http://fanf.livejournal.com/128861.html">temporum</a> is based on the idea that you can convince
yourself that several different sources agree on what the time is,
with the emphasis on different. Where are the weaknesses in the way it
determines if sources are different?

<p>The starting point for temporum is a list of host names to try. It is
OK if lots of them fail (e.g. because your device has been switched
off on a shelf for years) provided you have a good chance of
eventually getting a quorum.

<p>The list of host names is very large, and temporum selects candidates
from the list at random. This makes it hard for an attacker to target
the particular infrastructure that temporum might use. I hope your
device is able to produce decent random numbers immediately after
booting!

<p>The list of host names is statically configured. This is important to
thwart Sybil attacks: you don't want an attacker to convince you to
try a list of apparently-different host names which are all under the
attacker's control. Question: can the host list be made dynamic
without making it vulnerable?

<p>Hostnames are turned into IP addresses using the DNS. Temporum uses
the TLS X.509 PKI to give some assurance that the DNS returned the
correct result, about which more below. The DNS isn't
security-critical, but if it worries you perhaps temporum could be
configured with a list of IP addresses instead - but maybe that will 
make the device-on-shelf less likely to boot successfully.

<p>Temporum does not compare the IP addresses of "different" host
names. This might become a problem once TLS SNI makes large-scale
virtual hosting easier. More subtly, there is a risk that temporum
happens to query lots of servers that are hosted on the same
infrastructure. This can be mitigated by being careful about selecting
which host names to include in the list - no more than a few each of
Blogspot, Tumblr, Livejournal, GoDaddy vhosts, etc. More than one of
each is OK since it helps with on-shelf robustness.

<p>The TLS security model hopes that X.509 certification authorities will
only hand out certificates for host names to the organizations that
run the hosts. This is a forlorn hope: CAs have had their
infrastructure completely compromised; they have handed out
intermediate signing certificates to uncontrolled third parties; they
are controlled by nation states that treat our information security
with contempt.

<p>In the context of temporum, we can reduce this problem by checking
that the quorum hosts are authenticated by diverse CAs. Then an
attacker would have to compromise multiple CAs to convince us of an
incorrect time. Question: are there enough different CAs used by
popular sites that temporum can quickly find a usable set?
