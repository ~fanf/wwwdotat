---
format: html
lj:
  anum: 137
  can_comment: 1
  ditemid: 118665
  event_timestamp: 1328286840
  eventtime: "2012-02-03T16:34:00Z"
  itemid: 463
  logtime: "2012-02-03T16:34:22Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/118665.html"
title: Adventures with IPv6 DNS hosting
...

<p>Last summer I upgraded my DNS setup to support IPv6 and DNSSEC. After a bit of searching around I settled on using <a href="https://puck.nether.net/dns/">puck.nether.net</a> and <a href="https://web.gratisdns.dk/">GratisDNS</a> as my new secondary servers. GratisDNS has <a href="https://web.gratisdns.dk/?q=node/105">very good DNS server diversity</a> though the web site is entirely in Danish. (Google Translate to the rescue.) Puck gives me a bit of organizational diversity, and supported the remarkably shiny NOTIFY+IXFR for fast update propagation. (But that doesn't seem to be working any more.)</p>

<p>The DNSSEC side of the upgrade has been pretty trouble-free. The main problem is that <a href="http://www.nic.at/">nic.at</a> do not yet support DNSSEC, so I am relying on the <a href="https://dlv.isc.org/">ISC DLV</a> to provide a chain of trust to my zone. The .at zone was actually signed towards the end of last year, though they do not yet have a secure delegation from the root. Hopefully I will be able to get a secure delegation from them before very long.</p>

<p>IPv6 has been a bit more difficult. When I was changing my zone's delegation records in June, I was not able to put more than one IP address for each name server. I could have one IPv4 address or one IPv6 address, not both. I reported this problem to nic.at, and to work around it I created lots of aliases for my name servers for use in the delegation NS records:</p>

<pre>
 black.ns4.dotat.at. A     131.111.11.130
 gdk3.ns4.dotat.at.  A     194.0.2.6
 puck.ns4.dotat.at.  A     204.42.254.5
 black.ns6.dotat.at. AAAA  2001:630:212:100:646f:7461:742e:6174
 gdk3.ns6.dotat.at.  AAAA  2001:678:5::6
 puck.ns6.dotat.at.  AAAA  2001:418:3f4::5
</pre>

<p>However a few days later I got an email from GratisDNS complaining that they wanted me to list their name servers by their canonical names in my zone or they would cease slaving it. So I ended up with a delegation NS RRset in the .at zone looking like this, to appease nic.at:</p>

<pre>
 dotat.at. NS black.ns4.dotat.at.
 dotat.at. NS gdk3.ns4.dotat.at.
 dotat.at. NS puck.ns4.dotat.at.
 dotat.at. NS black.ns6.dotat.at.
 dotat.at. NS gdk3.ns6.dotat.at.
 dotat.at. NS puck.ns6.dotat.at.
</pre>

<p>And an an apex NS RRset in the dotat.at zone looking like this, to appease GratisDNS:</p>

<pre>
 dotat.at. NS ns1.gratisdns.dk.
 dotat.at. NS ns2.gratisdns.dk.
 dotat.at. NS ns3.gratisdns.dk.
 dotat.at. NS ns4.gratisdns.dk.
 dotat.at. NS ns5.gratisdns.dk.
 dotat.at. NS puck.nether.net.
 dotat.at. NS black.dotat.at.
</pre>

<p>This was rather ugly but it worked - mostly. Last week I got a report from <a href="https://twitter.com/sevanjaniyan">Sevan Janiyan</a> that <a href="http://www.opendns.com/">OpenDNS</a> was unable to resolve dotat.at. I reported the problem to OpenDNS and I was pleased to see that they were interested in fixing it.</p>

<p>This prompted me to see if I could make the delegation records for dotat.at less insane, and I was happy to find out that nic.at had fixed the bug I found in June. So I changed the delegation NS RRset to match the apex RRset and deleted the superfluous ns4 and ns6 aliases. Once these changes had taken effect OpenDNS was able to resolve my domain again. Hooray!</p>

<p>However this made it difficult for the OpenDNS techies to reproduce and debug the problem I reported, so I set up a test domain fanf2.ucam.org with a copy of dotat.at's old weird delegation. This allowed them to find the bug, and they are in the process of rolling out a fix. They have even promised to send me some swag in thanks!</p>

<p>So it has been a bit bumpy but it is nice to see the rough edges being rubbed off.</p>
