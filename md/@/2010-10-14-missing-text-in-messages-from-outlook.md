---
format: html
lj:
  anum: 4
  can_comment: 1
  ditemid: 109316
  event_timestamp: 1287080700
  eventtime: "2010-10-14T18:25:00Z"
  itemid: 427
  logtime: "2010-10-14T17:24:44Z"
  props:
    personifi_tags: "40:17,8:58,31:5,30:5,32:5,1:41,3:11,5:5,20:11,9:17,nterms:yes"
    verticals_list: computers_and_software
  reply_count: 2
  url: "https://fanf.livejournal.com/109316.html"
title: Missing text in messages from Outlook
...

<p>A user just reported a problem to us involving messages they received
which appeared to have text missing. The sender's signature and the quote
of the message that was being replied to appeared OK, but no additional
text from the sender could be seen. The recipient was using Thunderbird.
The messages were multipart/alternative messages sent by Outlook, with
text/plain and text/html parts.</p>

<p>The HTML parts did in fact include the expected text, except that it was wrapped in
an IE conditional comment, like the following, where the ... was the
substantive content.</p>

<pre>
  &lt;!--[if !mso]> ... &lt;![endif]-->
</pre>

<p>Note that this is a comment so non-Microsoft HTML processors will not display its contents, so
the reply appears to be missing!</p>

<p>Putting IE conditional comments in email is bad enough, but using them to
hide the important part of the message is simply braindamaged. The bit
that takes the cake is that Microsoft's own HTML to plain text converter
also ignores IE conditional comments, so the text/plain alternative also
omits the sender's reply!</p>

<p>Does anyone have a link to any Microsoft documentation about this problem?
Or any other good descriptions, for that matter.</p>
