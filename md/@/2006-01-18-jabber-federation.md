---
dw:
  anum: 22
  eventtime: "2006-01-18T10:44:00Z"
  itemid: 182
  logtime: "2006-01-18T10:49:24Z"
  props:
    import_source: livejournal.com/fanf/47055
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/46614.html"
format: casual
lj:
  anum: 207
  can_comment: 1
  ditemid: 47055
  event_timestamp: 1137581040
  eventtime: "2006-01-18T10:44:00Z"
  itemid: 183
  logtime: "2006-01-18T10:49:24Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/47055.html"
title: Jabber federation
...

http://www.livejournal.com/users/fanf/46715.html

Turns out this is a bug in jabberd2. What my server sends to Google is <tt>&lt;presence.xmlns='jabber:client'.from='dot@dotat.at' to='tony.finch@gmail.com' type='subscribe'/&gt;</tt>. Note that the XML namespace is set to <tt>jabber:client</tt> despite the fact that this is a server-to-server connection. D'oh!

http://j2.openaether.org/bugzilla/show_bug.cgi?id=159

In any case, Google have added an interop work-around and I can now communicate between dot@dotat.at and any @gmail.com Jabber user. Sweet.
