---
dw:
  anum: 135
  eventtime: "2005-04-14T14:00:00Z"
  itemid: 139
  logtime: "2005-04-14T14:01:11Z"
  props:
    commentalter: 1491292322
    import_source: livejournal.com/fanf/35852
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/35719.html"
format: casual
lj:
  anum: 12
  can_comment: 1
  ditemid: 35852
  event_timestamp: 1113487200
  eventtime: "2005-04-14T14:00:00Z"
  itemid: 140
  logtime: "2005-04-14T14:01:11Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/35852.html"
title: ""
...

<em>To obfuscate : assombrir, rendre confus, peu clair. Un obfuscated code est un programme dont la source ne permet pas de comprendre directement ce qu'il fait. C'est une sorte de jeu (de défouloir ?) pratiqué par quelques informaticiens... On peut le voir comme l'équivalent informatique de la poésie, par opposition aux masses énormes de prose produites chaque année par les développeurs ordinaires. Petit extrait signé Tony Finch : <code>l opt b (S (S I (S (BB (CC B) CI (BB K K make)) (S (BB C (BB C (C (CI qk))) (SS (SS S) (S (BB (BB (S I)) S (BB (BB (CC B) CI) (BB K K) make)) (B (CC B (BB (CC C) (BB (C (CI qk)) (pair (atom qk))) pair)) make)) (B (C (BB B C (C (CI qi)))) make))) make))) (B K make))</code></em>

http://dvanw.blogspot.com/2005/04/041-obfuscated-code.html

I like this guy's analogies :-)
