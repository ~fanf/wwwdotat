---
dw:
  anum: 180
  eventtime: "2004-08-11T17:08:00Z"
  itemid: 97
  logtime: "2004-08-11T10:22:44Z"
  props:
    commentalter: 1491292313
    import_source: livejournal.com/fanf/25068
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/25012.html"
format: casual
lj:
  anum: 236
  can_comment: 1
  ditemid: 25068
  event_timestamp: 1092244080
  eventtime: "2004-08-11T17:08:00Z"
  itemid: 97
  logtime: "2004-08-11T10:22:44Z"
  props: {}
  reply_count: 8
  url: "https://fanf.livejournal.com/25068.html"
title: Clever packaging
...

I've recently discovered the wonder of Ritter Sport's packaging. The chocolate comes as a 100g 4x4 square, and to open the wrapper you break it along the 3/4 line under the packaging's seam. This pulls the seam apart and immediately gives you a line of chocolate to eat. Absolutely brilliant.

I was looking for a picture of this, and found out that they came up with this packaging in 1976 which is about 20 years before I expected. What's even better is that the company has for a long time had good environmental policies.

Oh, and the choccy is nice.

http://www.rittersport.de/en/ueberuns/220_firma.htm
