---
format: html
lj:
  anum: 208
  can_comment: 1
  ditemid: 114640
  event_timestamp: 1310125260
  eventtime: "2011-07-08T11:41:00Z"
  itemid: 447
  logtime: "2011-07-08T10:41:49Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/114640.html"
title: unifdef and getline()
...

<p>I get email from Philip Paeps, FreeBSD hacker at large:</p>

<blockquote>
  <p>I'm spending some quality time with Linux kernels and buildroot this week. Fun fun. Not.</p>

  <p>Every time I get thrown in this space, one of the first things I have to do, is fix unifdef.c to spell 'getline()' differently so as not to clash with the libc version.  (I've got a patch in my $HOME/patches that I apply time and time again -- trivial but tedious).</p>

  <p>So I've been wondering: is there a reason unifdef.c spells getline() like that?  "If you can't make the tool compile, you shouldn't be using it?" or is it just sadism? :-) Inquisitive minds...</p>

  <p>Remind me to continue to buy you beers for unifdef. :)</p>
</blockquote>

<p>Well, I started working on unifdef on 2002-04-25 and one of the earliest changes was this one:</P>

<pre>
  commit ae32111731291ff29a80c51c8405fe3a6e886e78
  Author: Tony Finch <dot@dotat.at>
  Date:   2002-04-27 17:23:47 +0000

    spell getlin() with an e
</pre>

<p>The commit message is a reference to <a href="http://books.google.com/books?id=poFQAAAAMAAJ&q=%22spell+creat+with+an+e%22&dq=%22spell+creat+with+an+e%22">the footnote on page 204 of Kernighan and Pike</a>:</p>

<blockquote>
<p>Ken Thompson was once asked what he would do differently if he were redesigning the UNIX system. His reply: "I'd spell creat with an e."</p>
</blockquote>

<p>So that's where it comes from and at the time this spelling fix was not a problem. I didn't expect glibc and POSIX to steal the name from me! Compare <a href="http://pubs.opengroup.org/onlinepubs/009695399/idx/ig.html">this list of functions beginning with "g" in POSIX 2004</a> with <a href="http://pubs.opengroup.org/onlinepubs/9699919799/idx/ig.html">with the corresponding one from POSIX 2008</a>. WTF! You can't add a function with such a simple name to <tt>stdio.h</tt>!</p>

<p>So I fixed it in my repository nearly two years ago. I was perhaps a bit slow to do so - I had not been on top of unifdef maintenance for a while.</p>

<pre>
  commit c018c45e1e9372c428028dc333142467678c41ae
  Author: Tony Finch <dot@dotat.at>
  Date:   2009-11-24 11:58:41 +0000

    Rename getline() to parseline() to avoid clashing with a glibc function.
</pre>

<p>FreeBSD got the fix the following day:</p>

<pre>
  r199813 | fanf | 2009-11-25 20:23:18 +0000 (Wed, 25 Nov 2009) | 21 lines

  Update unifdef to my upstream version 1.188
</pre>

<p>The Linux copy was fixed earlier the same year:</p>

<pre>
  commit d15bd1067b1fcb2b7250d22bc0c7c7fea0b759f7
  Author: Justin P. Mattock <justinmattock@gmail.com>
  Date:   2009-03-07 13:31:29 +0100

    kbuild: fix C libary confusion in unifdef.c due to getline()
</pre>

<p>And before then Linux had been using unifdef happily for 2.5 years:</p>

<pre>
  commit 01f1c8799ad8b23c190d59cf1c9e28e6fed390a4
  Author: Sam Ravnborg <sam@mars.ravnborg.org>
  Date:   2006-07-23 20:39:59 +0200

    kbuild: add unifdef
</pre>

<p>I do not understand why incompatible versions of unifdef have persisted for so long. Part of it seems to be old kernel branches that are still used but not well maintained, but it is rather surprising that this patch hasn't been backported if those old branches are still in use. But then I don't understand the Linux kernel branching & maintenance model. I wonder if I can prod someone to improve the situation.</p>
