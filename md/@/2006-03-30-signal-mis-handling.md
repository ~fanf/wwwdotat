---
dw:
  anum: 27
  eventtime: "2006-03-30T00:26:00Z"
  itemid: 213
  logtime: "2006-03-30T00:45:00Z"
  props:
    commentalter: 1491292334
    import_source: livejournal.com/fanf/54983
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/54555.html"
format: casual
lj:
  anum: 199
  can_comment: 1
  ditemid: 54983
  event_timestamp: 1143678360
  eventtime: "2006-03-30T00:26:00Z"
  itemid: 214
  logtime: "2006-03-30T00:45:00Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/54983.html"
title: Signal mis-handling
...

One of the things that made the Bourne Shell notorious (apart from the obvious <a href="http://minnie.tuhs.org/UnixTree/V7/usr/src/cmd/sh/mac.h.html">Bournegol</a>) was its memory management. Its <a href="http://minnie.tuhs.org/UnixTree/V7/usr/src/cmd/sh/blok.c.html">allocator</a> just walked up the address space as needed. If it hit the end of its mapped memory, it would SEGV, drop into its <a href="http://minnie.tuhs.org/UnixTree/V7/usr/src/cmd/sh/fault.c.html">signal handler</a>, ask the kernel for more memory, and return. This caused inordinate problems later when Unix was ported to systems with less amenable signal handling. (See <a href="http://cvs.opensolaris.org/source/xref/on/usr/src/cmd/sh/blok.c">OpenSolaris</a> for a slightly cleaned-up Bourne shell.)

<a href="http://www.ioccc.org/1987/wall.c">Larry Wall's 1987 IOCCC winner</a> goes something like:<pre>
    extern int fd[];
    signal(SIGPIPE, start);
    pipe(fd);
    close(fd[0]);
    for(;;)
        write(fd[1],ptr,n);
</pre>The write serves only to trigger <tt>SIGPIPE</tt>, and the signal handler is repeatedly changed in order to implement the flow of control (which the judges called "amazing").

I thought it would be amusing to combine these ideas.<pre>
    extern char *prog[];
    extern int pc;
    for(;;)
        ((void(*)(void))dlsym(NULL,prog[pc]))();
</pre>This looks like a pretty boring interpreter if you assume that the <tt>prog</tt> consists entirely of built-in function names. But what if it doesn't? Then <tt>dlsym()</tt> will return <tt>NULL</tt> and the program will SEGV. The signal handler can then push a return address on the interpreter stack, change <tt>pc</tt> to point to the user-defined function definition, and <tt>longjmp()</tt> back to the loop.

(Aside: you can have symbolic builtins like <tt>+-*/</tt> if you choose function names that have the same <a href="http://www.freebsd.org/cgi/man.cgi?query=elf_hash&amp;manpath=SunOS+5.9">ELF hash</a> as the symbols you want, then run <tt>sed</tt> over the object code to change their names to symbols.)

The problem is that <tt>dlsym()</tt> isn't very portable - certainly not enough for the IOCCC. So we need a better way of constructing a symbol table, and preferably without having to mention the function name loads of times (e.g. as in Larry's program). The problem is that you can't interleave the array initializers with the function definitions (at least not without non-portable hacks like <a href="http://www.freebsd.org/cgi/cvsweb.cgi/src/sys/sys/linker_set.h?rev=HEAD">linker sets</a>). But how about this:<pre>
    #define BUILTIN(name, code) \
        void name(void) code { #name, name },
    
    BUILTIN(add,{
        int b = pop();
        int a = pop();
        push(a + b);
    }
    
    BUILTIN(sub,{
        int b = pop();
        int a = pop();
        push(a - b);
    }
    
    BUILTIN(exch,{
        int b = pop();
        int a = pop();
        push(b);
        push(a);
    }
    
    struct {
        const char *name;
        void (*code)(void);
    } builtin[] = {
          )))
        { 0,0 }
    };
</pre>
