---
dw:
  anum: 43
  eventtime: "2003-03-28T17:17:00Z"
  itemid: 12
  logtime: "2003-03-28T09:48:57Z"
  props:
    import_source: livejournal.com/fanf/3137
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/3115.html"
format: casual
lj:
  anum: 65
  can_comment: 1
  ditemid: 3137
  event_timestamp: 1048871820
  eventtime: "2003-03-28T17:17:00Z"
  itemid: 12
  logtime: "2003-03-28T09:48:57Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/3137.html"
title: The return of the B
...

Drake's may be relatively inconvenient but it is nice of them to fix my bike for free.

In other news we have 8 new machines for the replacement email data store, with more to come later -- they look particularly smart all racked up together. At this afternoon's little meeting we were talking about problems of power, heat, and weight, and exactly what the U measurement of rack space is. Our supercomputer people worry about gigaflops per kilowatt and gigaflops per tonne, but we're more modest: we're concerned with users per inch. (Slice them thinly.)
