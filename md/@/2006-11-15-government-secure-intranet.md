---
dw:
  anum: 164
  eventtime: "2006-11-15T15:34:00Z"
  itemid: 264
  logtime: "2006-11-15T15:34:53Z"
  props:
    commentalter: 1491292367
    import_source: livejournal.com/fanf/68094
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/67748.html"
format: html
lj:
  anum: 254
  can_comment: 1
  ditemid: 68094
  event_timestamp: 1163604840
  eventtime: "2006-11-15T15:34:00Z"
  itemid: 265
  logtime: "2006-11-15T15:34:53Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/68094.html"
title: Government Secure Intranet
...

<p>How stupid is this? Repeated irrelevant advertising as well as a stupid disclaimer. YOUR TAXES AT WORK!</p>

<p>(This came from a user who was having problems receiving a message from a government researcher, because the 4MB message was too big for the poor GSI email system. I also loved the X.400 failure report, which I shall not reproduce here for privacy reasons.)</p>

<pre>
PLEASE NOTE: THE ABOVE MESSAGE WAS RECEIVED FROM THE INTERNET.
On entering the GSI, this email was scanned for viruses by the
Government Secure Intranet (GSi) virus scanning service supplied
exclusively by Cable & Wireless in partnership with MessageLabs. In
case of problems, please call your organisational IT Helpdesk. The
MessageLabs Anti Virus Service is the first managed service to achieve
the CSIA Claims Tested Mark (CCTM Certificate Number 2006/04/0007),
the UK Government quality mark initiative for information security
products and services. For more information about this please visit
www.cctmark.gov.uk

**********************************************************************

This email and any files transmitted with it are private and intended
solely for the use of the individual or entity to whom they are
addressed. If you have received this email in error please return it
to the address it came from telling them it is not for you and then
delete it from your system.

This email message has been swept for computer viruses.

**********************************************************************

The original of this email was scanned for viruses by Government
Secure Intranet (GSi) virus scanning service supplied exclusively by
Cable & Wireless in partnership with MessageLabs. On leaving the GSI
this email was certified virus free. The MessageLabs Anti Virus
Service is the first managed service to achieve the CSIA Claims Tested
Mark (CCTM Certificate Number 2006/04/0007), the UK Government quality
mark initiative for information security products and services. For
more information about this please visit www.cctmark.gov.uk
</pre>
