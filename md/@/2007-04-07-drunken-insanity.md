---
dw:
  anum: 138
  eventtime: "2007-04-07T17:28:00Z"
  itemid: 283
  logtime: "2007-04-07T16:32:06Z"
  props:
    commentalter: 1491292340
    import_source: livejournal.com/fanf/72912
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/72586.html"
format: casual
lj:
  anum: 208
  can_comment: 1
  ditemid: 72912
  event_timestamp: 1175966880
  eventtime: "2007-04-07T17:28:00Z"
  itemid: 284
  logtime: "2007-04-07T16:32:06Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/72912.html"
title: Drunken insanity
...

Dear lazyweb,

Please could I have an implementation of TeX written in Javascript which renders beautifully typeset mathematics to a &lt;canvas&gt;.
