---
dw:
  anum: 250
  eventtime: "2016-10-05T23:07:00Z"
  itemid: 470
  logtime: "2016-10-05T22:07:50Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/147606
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/120570.html"
format: html
lj:
  anum: 150
  can_comment: 1
  ditemid: 147606
  event_timestamp: 1475708820
  eventtime: "2016-10-05T23:07:00Z"
  itemid: 576
  logtime: "2016-10-05T22:07:50Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/147606.html"
title: Version number comparisons in Apache and GNU coreutils
...

<p>So I have a toy DNS server which runs <a href="https://source.isc.org">bleeding edge BIND 9</a> with a bunch of patches which I have submitted upstream, or which are work in progress, or just stupid. It gets upgraded a lot.

<p>My <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/bind9.git/blob/f64892a481ee9afabc10afff5c2e0507a73c9c7e:/Build.sh">build and install script</a> puts the version number, git revision, and an install counter into the name of the install directory. So, when I deploy a custom hack which segfaults, I can just flip a symlink and revert to the previous less incompetently modified version.

<p>More recently I have added an auto-upgrade feature to my BIND rc script. This was really simple, stupid: it just used the last install directory from the <tt>ls</tt> lexical sort. (You can tell it is newish because it could not possibly have coped with the BIND 9.9 -> 9.10 transition.)

<p>It broke today.

<p>BIND 9.11 has been released! Yay! <a href="https://twitter.com/ISCdotORG/status/763117673425145856">I have lots of patches in it!</a>

<p>Unfortunately 9.11.0 sorts lexically before 9.11.0rc3. So my dumb auto update script refused to update. This can only happen in the transition period between a major release and the version bump on the master branch for the next pre-alpha version. This is a rare edge case for code only I will ever use.

<p>But, I fixed it!

<p>And I learned several cool things in the process!

<p>I started off by wondering if I could plug <a href="https://www.freebsd.org/cgi/man.cgi?query=dpkg"><tt>dpkg --compare-versions</tt></a> into a sorting algorithm. But then I found that <a href="https://www.gnu.org/software/coreutils/manual/coreutils.html#sort-invocation">GNU sort has a -V version comparison mode</a>. Yay! <tt>ls | sort -V</tt>!

<p>But what is its version comparison algorithm? Is it as good as dpkg's? <a href="https://www.gnu.org/software/coreutils/manual/coreutils.html#Details-about-version-sort">The coreutils documentation refers to the gnulib <tt>filevercmp()</tt> function</a>. This appears not to exist, but gnulib does have a <a href="https://www.gnu.org/software/gnulib/manual/html_node/strverscmp.html">strverscmp()</a> function cloned from glibc.

<p>So look at the glibc <a href="https://www.freebsd.org/cgi/man.cgi?query=strverscmp&amp;manpath=Debian+8.1.0">man page for strverscmp()</a>. It is a much simpler algorithm than dpkg, but adequate for my purposes. And, I see, built in to GNU ls! (I should have spotted this when reading the coreutils docs, because that uses ls -v as an example!)

<p>Problem solved!

<p>Add an option in <tt>ls | tail -1</tt> so it uses <tt>ls -v</tt> and my script upgrades from 9.11.0rc3 to 9.11.0 without manual jiggery pokery!

<p>And I learned about some GNU carnival organ bells and whistles!

<p>But ... have I seen something like this before?

<p>If you look at <a href="http://ftp.isc.org/isc/bind9/">the isc.org BIND distribution server</a>, its listing is sorted by version number, not lexically, i.e. 10 sorts after 9 not after 1.

<p>They are using the <a href="https://httpd.apache.org/docs/2.2/mod/mod_autoindex.html#indexoptions">Apache mod_autoindex IndexOptions VersionSort</a> directive, which works the same as GNU version number sorting.

<p>Nice!

<p>It's pleasing to see widespread support for version number comparisons, even if they aren't properly thought-through elaborate battle hardened dpkg version number comparisons.
