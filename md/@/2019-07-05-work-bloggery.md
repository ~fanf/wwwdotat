---
dw:
  anum: 14
  eventtime: "2019-07-05T19:03:00Z"
  itemid: 514
  logtime: "2019-07-05T18:07:48Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 3
    revtime: 1562350352
  url: "https://fanf.dreamwidth.org/131598.html"
format: md
...

Work bloggery
=============

For those who are interested in my work, my various ops and web dev
activity and some of my DNS work is now being posted on
[www.dns.cam.ac.uk/news/](https://www.dns.cam.ac.uk/news/) the Atom
feed is available on Dreamwidth at
[👤dns_cam_feed](https://dns-cam-feed.dreamwidth.org/)
