---
dw:
  anum: 196
  eventtime: "2006-01-31T22:01:00Z"
  itemid: 192
  logtime: "2006-01-31T22:20:55Z"
  props:
    commentalter: 1491292394
    hasscreened: 1
    import_source: livejournal.com/fanf/49655
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/49348.html"
format: casual
lj:
  anum: 247
  can_comment: 1
  ditemid: 49655
  event_timestamp: 1138744860
  eventtime: "2006-01-31T22:01:00Z"
  itemid: 193
  logtime: "2006-01-31T22:20:55Z"
  props: {}
  reply_count: 11
  url: "https://fanf.livejournal.com/49655.html"
title: Millennium Post
...

http://www.bbc.co.uk/radio4/connect/pip/dk7o2/

I just heard a pretty amazing programme about technology and the postal service. The thing that struck me was the way they have mechanized bulk sorting of letters. There are 73 major sorting centres in Britain which between them have 200-300 letter sorting machines, which do the obvious jobs of working out the letter's orientation and photographing it for OCR. (Really fast - 30,000 items per hour per machine.) What surprised me is that the OCR is not done on site, but instead the photos are transmitted over the post office's data network to a single centralized data centre which contains all the clever computers. Of course they aren't so clever that they can deal with all letters, so - second surprise - unrecognized letters are handled by sending the images to offices full of people who type in post codes all day.

No significant sorting intelligence is on the same site as the sorting machines.

Hmm, perhaps this is the manufacturer of the machines: http://www.abprecision.co.uk/businessunits/hsp/postalservices.htm

And perhaps this is a press release about the data centres: http://www.prnewswire.co.uk/cgi/news/release?id=59114
