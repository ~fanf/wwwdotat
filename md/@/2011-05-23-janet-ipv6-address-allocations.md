---
format: html
lj:
  anum: 111
  can_comment: 1
  ditemid: 113007
  event_timestamp: 1306169700
  eventtime: "2011-05-23T16:55:00Z"
  itemid: 441
  logtime: "2011-05-23T15:55:15Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/113007.html"
title: JANET IPv6 address allocations
...


<h2>A script to generate the list</h2>

<pre>
    #!/usr/bin/perl

    use strict;
    use warnings;

    my %netname;
    my %descr;

    my $longest = 0;

    my $start;
    my $block;

    for (qx{whois3 -M 2001:630::/32}) {
        if (/^$/) {
            $start=1;
            undef $block;
        } elsif (/^inet6num:\s+(\S+)/ and $start) {
            $block = $1;
            $block =~ tr/A-F/a-f/;
            $block =~ s/([0-9a-f]+:)/substr "000$1", -5/ge;
            $block =~ s|::0/|::/|;
            undef $start;
        } elsif (/^netname:\s+(\S+)/ and $block) {
            $netname{$block} = $1;
            $longest = length $1 if $longest < length $1;
            undef $block if defined $descr{$block};
        } elsif (/^descr:\s+(.*)/ and $block) {
            $descr{$block} = $1;
            undef $block if defined $netname{$block};
        } else {
            undef $start;
        }
    }

    for my $block (sort keys %netname) {
        my $need = $longest - length $netname{$block};
        my $netname = $netname{$block}. " " x $need;
        print "$block $netname $descr{$block}\n";
    }
</pre>

<h2>The current list of JANET IPv6 allocations</h2>

<pre>
2001:0630:0000::/48 JANET-BACKBONE               Joint Academic Network (JANET) Backbone
2001:0630:0001::/48 JANET-SERV                   JANET(UK)
2001:0630:0009::/48 LSE                          London School of Economics and Political
2001:0630:0010::/48 BRUNEL-IPV6                  Brunel University IPv6 Network
2001:0630:0011::/48 QMUL-V6                      Queen Mary and Westfield College
2001:0630:0012::/48 ICV6NET1                     Imperial College London
2001:0630:0013::/48 UCL-6NET                     University College London
2001:0630:0014::/48 KENCHELNET-IPV6              Kensington and Chelsea College
2001:0630:0015::/48 BIRKBECK-IPv6                Birkbeck College
2001:0630:0016::/48 HACK-COM-COL                 Hackney Community College
2001:0630:0017::/48 GREENWICH-IPV6               University of Greenwich IPv6 Network
2001:0630:0018::/48 KCLNET                       King's College London
2001:0630:0019::/48 ULCC                         University of London Computer Centre
2001:0630:001a::/48 LONMAN                       London Metropolitan Network Limited
2001:0630:001b::/48 SOAS                         School of Oriental and African Studies
2001:0630:001c::/48 SCI-UK-NET                   Steel Construction Institute
2001:0630:001d::/48 UKCITY-NET                   City University London
2001:0630:001e::/48 SOUTH-THAMES-COLL-V6         South Thames College
2001:0630:001f::/48 KING-AC                      Kingston University
2001:0630:0020::/48 NETNORTHWEST-IPV6            Net Northwest Limited IPv6 Network
2001:0630:0021::/48 LIVIPV6                      The University of Liverpool IPV6 Network
2001:0630:0022::/48 MANLAN-V6                    University of Manchester Local Area Network
2001:0630:0023::/48 STAFFORDSHIRE-IPV6           Staffordshire University IPv6 Network
2001:0630:0024::/48 STOKECOLL                    Stoke-On-Trent College
2001:0630:0025::/48 MMU-IPv6                     Manchester Metropolitan University
2001:0630:0026::/48 UCLAN-IPV6                   University of Central Lancashire
2001:0630:0027::/48 STOCKPORT-COLLEGE            Stockport College of Further and Higher
2001:0630:0028::/48 BOLTON-UNIVERSITY            The University of Bolton
2001:0630:0030::/48 EMBL-EBI                     Wellcome Trust Genome Campus
2001:0630:0031::/48 JANET-TRAINING               JANET(UK)
2001:0630:0032::/48 LUTON-IPV6                   University of Luton
2001:0630:0033::/48 LOGICALIS-CLOUD              Logicalis UK Limited
2001:0630:0034::/48 ESHER-COLLEGE-NET            Esher College
2001:0630:0040::/48 GLANET-v6                    University of Glasgow
2001:0630:0041::/48 STRATHCLYDE-IPV6             University of Strathclyde IPv6
2001:0630:0042::/48 CLYDENET                     ClydeNET, Glasgow MAN
2001:0630:0050::/48 JANET-UK                     JANET(UK)
2001:0630:0051::/48 OXPOLYNET                    Oxford Brookes University
2001:0630:0052::/48 TVN-IPv6                     University of London Computer Centre
2001:0630:0053::/48 UOR-NET                      The University of Reading, UK
2001:0630:0055::/48 ECMWF-ECNET                  European Centre for Medium Range Weather
2001:0630:0060::/48 SHU-IPV6                     Sheffield Hallam University
2001:0630:0061::/48 YORK-IPV6-NET                University of York Campus IPv6 Network
2001:0630:0062::/48 LEEDS-UNIVERSITY-IPV6        The University of Leeds
2001:0630:0063::/48 SHEFFIELD-IPV6               University of Sheffield IPv6 Network
2001:0630:0064::/48 SHEFFCOL                     Sheffield College
2001:0630:0065::/48 YHMAN-CORE                   YHMAN Ltd
2001:0630:0066::/48 YORK-ST-JOHN                 York St John University
2001:0630:0067::/48 LEEDSMET-V6                  Leeds Metropolitan University
2001:0630:0080::/48 LANCASTER-IPv6               Lancaster University
2001:0630:0081::/48 CANLMAN-IPV6                 Cumbria and North Lancashire MAN
2001:0630:00a0::/48 NCLIPV6NET                   University of Newcastle upon Tyne IPv6 Network
2001:0630:00a1::/48 TYNE-MET                     Tyne Metropolitan College
2001:0630:00a2::/48 NORMAN-UK-NET                NorMAN NOC
2001:0630:00c0::/48 TOTTON-COLLEGE-IPV6          Totton College
2001:0630:00c1::/48 LENSE-LTD                    LeNSE (Learning Network South East) MAN
2001:0630:00c2::/48 JANET-IPv6-TBS               JANET(UK)
2001:0630:00c3::/48 QUEEN-MARYS-COLL             Queen Mary's College
2001:0630:00d0::/48 SOTON-ECS                    Electronics and Computer Science
2001:0630:00e1::/48 BATH-AC-UK                   The University of Bath
2001:0630:00e2::/48 EXETER-UNIVERSITY            Exeter University Campus Network
2001:0630:00e3::/48 PLYM-AC-UK                   The University of Plymouth
2001:0630:00e4::/48 BRISTOL-IPV6NET              University of Bristol
2001:0630:00e5::/48 SWERN-MAN                    South West England Regional Network
2001:0630:00e6::/48 CITY-COLLEGE-PLYMOUTH        City College Plymouth
2001:0630:00e7::/48 UWE-IPV6                     University of the West of England
2001:0630:00e8::/48 WILTSHIRE-COLLEGE            Wiltshire College
2001:0630:00e9::/48 UNIVERSITY-COLLEGE-FALMOUTH  University College Falmouth
2001:0630:00ea::/48 HARTPURY-COLL                Hartpury College
2001:0630:00eb::/48 RICHHUISH-NET                Richard Huish College
2001:0630:0140::/48 UNI-ST-ANDREWS               University of St. Andrews
2001:0630:0141::/48 FATMAN-IPV6                  FaTMAN
2001:0630:0142::/48 DUNDEE-UNIV                  University of Dundee
2001:0630:0180::/48 OPEN-UNI-IPV6                Open University IPv6
2001:0630:0181::/48 NOMINET-UK-IPV6              NOMINET UK
2001:0630:01c0::/48 NEWORCS-IPV6                 North East Worcestershire College
2001:0630:01c1::/48 WOLVES-IPV6                  University of Wolverhampton IPv6 address space
2001:0630:01c2::/48 JANET-WMRN                   The JNT Association
2001:0630:01c3::/48 UNI-WARWICK                  The University of Warwick
2001:0630:01c4::/48 WALSALL-MBC                  Walsall Metropolitan Borough Council
2001:0630:01c5::/48 BHAM-V6                      University of Birmingham
2001:0630:0200::/48 CAM-AC-UK                    University of Cambridge
2001:0630:0201::/48 CRANFIELD-IPV6               Cranfield University
2001:0630:0202::/48 EASTNET                      EASTnet
2001:0630:0203::/48 W-HERTS-COL-01               West Herts College
2001:0630:0204::/48 UNI-HERTS                    University of Hertfordshire
2001:0630:0205::/48 PROQUEST                     Proquest Information and Learning Limited
2001:0630:0206::/48 SANGER                       Wellcome Trust Sanger Institute
2001:0630:0207::/48 ASHRIDGE-BUS-SCHL            Ashridge (Bonar Law Memorial) Trust
2001:0630:0208::/48 ESSEX-UNI-V6                 University of Essex
2001:0630:0240::/48 ABMAN-IPV6                   AbMAN IPv6 Network
2001:0630:0241::/48 ABERDEEN-IPV6                University of Aberdeen IPv6 network
2001:0630:0242::/48 RGU-NET-IPV6                 The Robert Gordon University
2001:0630:0243::/48 MARLAB                       Fisheries Research Services
2001:0630:0244::/48 ROWETT-NET                   Rowett Research Institute
2001:0630:0245::/48 ABERDEEN-COLLEGE             Aberdeen College
2001:0630:0280::/48 DANTE-IPV6                   DANTE (1)
2001:0630:02c0::/48 GLAM-UNI                     University of Glamorgan
2001:0630:02ff::/48 BANGOR                       University of Bangor
2001:0630:0300::/48 LINKAGE-COLL                 Linkage College
2001:0630:0301::/48 LOUGHBOROUGH-UNIVERSITY-IPV6 Loughborough University IPv6 Network
2001:0630:0302::/48 EMMAN                        East Midlands MAN IPV6 Network
2001:0630:0303::/48 NOTTINGHAM-TRENT             Nottingham Trent University
2001:0630:0304::/48 LINCOLN-IPV6                 University of Lincoln IPv6 Network
2001:0630:0305::/48 NOTNET                       University of Nottingham
2001:0630:0306::/48 LEICESTER                    University of Leicester
2001:0630:0340::/48 KENT-UNIVERSITY              University of Kent
2001:0630:0341::/48 KENTISH-MAN                  Kent MAN Ltd
2001:0630:0342::/48 KENT-COMMUNITY-NET           Kent County Council
2001:0630:0343::/48 CCC-UNIVERSITY               Canterbury Christ Church University
2001:0630:0344::/48 CANTCOL                      Canterbury College
2001:0630:0380::/48 DRINDOD-6                    Trinity College Carmarthen
2001:0630:0381::/48 CARDIFF-UNIVERSITY           Cardiff University
2001:0630:03c0::/48 EASTMAN-CONSORT              EaStMAN Consortium
2001:0630:03c1::/48 UNIVERSITY-EDINBURGH         The University of Edinburgh
2001:0630:03c2::/48 UNI-STIRLING                 University of Stirling
2001:0630:0400::/48 QUB-IPV6                     Queen's University of Belfast
2001:0630:0401::/48 NIRAN                        NIRAN Limited
2001:0630:0440::/44 UNIV-OF-OXFORD               University of Oxford
2001:0630:0480::/48 LTS-GLOW                     Learning and Teaching Scotland
</pre>
