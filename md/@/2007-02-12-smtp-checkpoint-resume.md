---
dw:
  anum: 93
  eventtime: "2007-02-12T21:44:00Z"
  itemid: 273
  logtime: "2007-02-12T21:55:28Z"
  props:
    import_source: livejournal.com/fanf/70394
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/69981.html"
format: casual
lj:
  anum: 250
  can_comment: 1
  ditemid: 70394
  event_timestamp: 1171316640
  eventtime: "2007-02-12T21:44:00Z"
  itemid: 274
  logtime: "2007-02-12T21:55:28Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/70394.html"
title: SMTP checkpoint/resume
...

Following on from http://fanf.livejournal.com/67571.html

People who follow <a href="https://dotaturls.livejournal.com/">👤dotaturls</a> will have noticed that I have published a couple of revisions of <a href="https://fanf2.user.srcf.net/hermes/doc/qsmtp/draft-fanf-smtp-rfc1845bis.html">RFC 1845bis</a> as official Internet Drafts. My intention is to get it published as an RFC through the "individual submission" route. This means it goes through the IETF review & consensus process, but it is not the product of an IETF working group. RFC 1845 was published as "experimental" but I'd like 1845bis to be on the Standards Track.

(Documents can also become RFCs through the "independent submission" route, which mostly involves just the RFC editor. This is used by 1st April RFCs, for example, and these documents are not eligible for the Standards Track. <a href="http://www.ietf.org/rfc/rfc1796.txt">Not all RFCs are standards</a>.)
