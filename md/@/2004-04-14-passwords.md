---
dw:
  anum: 92
  eventtime: "2004-04-14T13:02:00Z"
  itemid: 81
  logtime: "2004-04-14T06:02:51Z"
  props:
    commentalter: 1491292311
    import_source: livejournal.com/fanf/20767
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/20828.html"
format: casual
lj:
  anum: 31
  can_comment: 1
  ditemid: 20767
  event_timestamp: 1081947720
  eventtime: "2004-04-14T13:02:00Z"
  itemid: 81
  logtime: "2004-04-14T06:02:51Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/20767.html"
title: Passwords
...

At lunch I was reminded of an old Demon root password. Like many places the admins at demon chose passwords by thinking of a memorable phrase and then obfuscating it. This particular password was obfuscated into a state of total punctuation, something like <code>&gt;$!=&gt;:-)</code>

[<a href="http://www.livejournal.com/poll/?id=278605">Poll #278605</a>]
