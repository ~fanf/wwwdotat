---
dw:
  anum: 4
  eventtime: "2004-02-13T16:36:00Z"
  itemid: 60
  logtime: "2004-02-13T16:40:39Z"
  props:
    commentalter: 1491292308
    current_moodid: 31
    current_music: the quiet after the techno storm of creativity
    import_source: livejournal.com/fanf/15550
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/15364.html"
format: casual
lj:
  anum: 190
  can_comment: 1
  ditemid: 15550
  event_timestamp: 1076690160
  eventtime: "2004-02-13T16:36:00Z"
  itemid: 60
  logtime: "2004-02-13T16:40:39Z"
  props:
    current_moodid: 31
    current_music: the quiet after the techno storm of creativity
  reply_count: 2
  url: "https://fanf.livejournal.com/15550.html"
title: Big presentation excitement!
...

Finally finished and submitted my paper for the <a href="http://www.ukuug.org/events/winter2004/">UKUUG Winter Conference</a> at lunchtime today. I hope there are no big fuckups in it because it's too late now!

Next week I shall be mostly making slides for the talk.

Is anyone reading this going to the conference too? I'm quite looking forward to having a beer or two with Julian Field (who wrote MailScanner, which I have inflicted on all my users).
