---
dw:
  anum: 210
  eventtime: "2016-04-29T02:16:00Z"
  itemid: 453
  logtime: "2016-04-29T01:16:19Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/143262
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/116178.html"
format: html
lj:
  anum: 158
  can_comment: 1
  ditemid: 143262
  event_timestamp: 1461896160
  eventtime: "2016-04-29T02:16:00Z"
  itemid: 559
  logtime: "2016-04-29T01:16:19Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/143262.html"
title: Capability myths demolished
...

<p>Blogging after the pub is a mixed blessing. A time when I <em>want</em> to
write about things! But! A time when I should be sleeping.</p>

<p>Last week I wrote nearly 2000 boring words about keyboards and stayed
up till nearly 4am. A stupid amount of effort for something no-one
should ever have to care about.</p>

<p>Feh! what about something to expand the mind rather than dull it?</p>

<h1>CAPABILITIES</h1>

<p>Years and years ago I read practically everything on
<a href="http://erights.org">http://erights.org</a> - the website for the E programming language -
which is a great exposition of how capabilities are <em>the</em>
way to handle access control.</p>

<p>I was reminded of these awesome ideas this evening when talking to
<a href="https://wiki.ubuntu.com/ColinWatson">Colin Watson</a> about how he is
putting
<a href="http://theory.stanford.edu/~ataly/Papers/macaroons.pdf">Macaroons</a>
into practice.</p>

<p>One of the great E papers is <a href="http://srl.cs.jhu.edu/pubs/SRL2003-02.pdf">Capability Myths
Demolished</a>. It's extremely
readable, has brilliant diagrams, and really, it's the whole point of
this blog post.</p>

<p>Go and read it: <a href="http://srl.cs.jhu.edu/pubs/SRL2003-02.pdf">http://srl.cs.jhu.edu/pubs/SRL2003-02.pdf</a></p>

<h2>Ambient authority</h2>

<p>One of the key terms it introduces is "ambient authority". This is
what you have when you are able to find a thing and do something to
it, without being given a capability to do so.</p>

<p>In POSIX terms, file descriptors are capabilities, and file names are
ambient authority. A capability-style POSIX would, amongst other
things, eliminate the current root directory and the current working
directory, and limit applications to <code>openat()</code> with relative paths.</p>

<p>Practical applications of capabilities to existing systems usually
require intensive "taming" of the APIs to eliminate ambient authority
- see for example
<a href="http://www.cl.cam.ac.uk/research/security/capsicum/">Capsicum</a>, and
<a href="http://www.links.org/?p=271">CaPerl and CaJa</a>.</p>

<p>From the programming point of view (rather than the larger systems
point of view) eliminating ambient authority is basically eliminating
global variables.</p>

<p>From the hipster web devops point of view, the "<a href="http://12factor.net/config">12 factor
application</a>" idea of storing
configuration in the environment is basically a technique for reducing
ambient authority.</p>

<h2>Capability attenuation</h2>

<p>Another useful idea in the <a href="http://srl.cs.jhu.edu/pubs/SRL2003-02.pdf">Capability Myths
Demolished</a> paper is
capability attenuation: if you don't entirely trust someone, don't
give them the true capabilty, give them a capability on a proxy. The
proxy can then restrict or revoke access to the true object, according
to whatever rules you want.</p>

<p>(<a href="http://www.chiark.greenend.org.uk/~ian/userv/">userv</a> proxies POSIX
file descriptors rather than passing them as-is, to avoid transmitting
unwanted capabilities - it always gives you a pipe, not a socket, nor
a device, etc.)</p>

<h2>And stuff</h2>

<p>It is too late and my mind is too dull now to do justice to these
ideas, so go and read their paper instead of my witterings.</p>
