---
dw:
  anum: 3
  eventtime: "2008-02-12T12:05:00Z"
  itemid: 323
  logtime: "2008-02-12T12:05:06Z"
  props:
    commentalter: 1491292351
    import_source: livejournal.com/fanf/83398
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/82691.html"
format: html
lj:
  anum: 198
  can_comment: 1
  ditemid: 83398
  event_timestamp: 1202817900
  eventtime: "2008-02-12T12:05:00Z"
  itemid: 325
  logtime: "2008-02-12T12:05:06Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/83398.html"
title: What goes around comes around
...

<p>It's nice to see Microsoft's own incompetence coming back to bite them. Here are a couple of slightly edited posts to the exim-users list, from an innocent victim and from me.</p>

<p><a href="http://lists.exim.org/lurker/message/20080212.090648.3d47e47d.en.html">
<b>From:</b> mwi78 -at- gmx.de<br/>
<b>Date:</b> 2008-02-12 09:06 -000<br/>
<b>To:</b> exim-users -at- exim.org<br/>
<b>Subject:</b> Exim and POP3 Connector at Win2003 SBS Server<br/>
</a></p>

<p>Hello,</p>

<p>my SBS server connects via pop3connector to my exim4 MTA and login via postmaster-account to any pop3 mailboxes. In server settings I setup to connect only to one mailbox to prevent parallel logins with the same login-data.</p>

<p>I have the problem, that some of these mail accounts fail to login.</p>

<p>Error log at SBS Server</p>

<pre>
------------------------------
Ereignistyp:        Fehler
Ereignisquelle:     POP3 Connector
Ereigniskategorie:  Download
Ereigniskennung:    1036
Datum:              12.02.2008
Zeit:               07:15:13
Benutzer:           Nicht zutreffend
Computer:           WINSBS2K3
Beschreibung:
Während einer POP3-Transaktion auf Server &lt;mail.piw-windfuhr.de
[User@???]&gt; ist ein Fehler aufgetreten. Der Fehler ist 1232 (Das
Netzlaufwerk ist nicht erreichbar. Weitere Informationen über die
Behebung von Netzwerkproblemen finden Sie in der Windows-Hilfe.).

Weitere Informationen über die Hilfe- und Supportdienste erhalten
Sie unter http://go.microsoft.com/fwlink/events.asp.
------------------------------
</pre>

<p>The failed logins from the SBS Server are not logged in the syslog logfile.</p>
<p>Have anyone an idea?</p>
<p>Thanks in advance</p>
<p>Martin</p>

<p><a href="http://lists.exim.org/lurker/message/20080212.115507.65725b75.en.html">
<b>From:</b> Tony Finch &lt;dot@dotat.at&gt;<br/>
<b>Date:</b> 2008-02-12 11:55 -000<br/>
<b>To:</b> exim-users -at- exim.org<br/>
<b>Subject:</b> Re: Exim and POP3 Connector at Win2003 SBS Server<br/>
</a></p>

<p>This is an Exchange problem, not an Exim problem, but the explanation is
too amusing not to post. Firstly, the error message and its pseudo
explanation "the network drive is not available" is complete crap. The
actual explanation can be found here:
<a href="http://support.microsoft.com/kb/280331">http://support.microsoft.com/kb/280331</a></p>

<blockquote>This behavior occurs because one or more e-mail messages in the mailbox
of the POP3 server may contain Null characters and the POP3 Connector for
Small Business Server 4.5 is not able to download e-mail messages if they
contain Null characters.</blockquote>

<p>We run Cyrus which also chokes on messages containing null characters. We
can't just reject these messages because legitimate but malformed messages
sometimes contain null characters. The main source of these messages is of
course Microsoft MTAs. This fills me with a mixture of glee and loathing.</p>

<p>(It's also amusing that my emotion is schadenfreude, and I'm replying to
a German and I have an Austrian email address, but I don't speak German...)</p>
