---
dw:
  anum: 233
  eventtime: "2007-03-09T16:52:00Z"
  itemid: 278
  logtime: "2007-03-09T16:59:08Z"
  props:
    import_source: livejournal.com/fanf/71613
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/71401.html"
format: casual
lj:
  anum: 189
  can_comment: 1
  ditemid: 71613
  event_timestamp: 1173459120
  eventtime: "2007-03-09T16:52:00Z"
  itemid: 279
  logtime: "2007-03-09T16:59:08Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/71613.html"
title: Good anti-spam news
...

The United States Securities and Exchange Commission is taking action against the stock pump-and-dump fraud spammers.

http://news.google.com/news?q=sec+spam

I wonder whether the effect of pump-and-dump on the companies' stock price and volatility is worse (or not) than the SEC suspending trading. I wonder if pump-and-dump spam could be used to make money by blackmailing the targeted companies. I wonder if the companies are legitimate victims, or are they colluding with the spammers?
