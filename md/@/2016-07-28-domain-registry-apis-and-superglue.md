---
dw:
  anum: 216
  eventtime: "2016-07-28T16:01:00Z"
  itemid: 463
  logtime: "2016-07-28T15:01:09Z"
  props:
    commentalter: 1491292418
    import_source: livejournal.com/fanf/145708
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/118744.html"
format: html
lj:
  anum: 44
  can_comment: 1
  ditemid: 145708
  event_timestamp: 1469721660
  eventtime: "2016-07-28T16:01:00Z"
  itemid: 569
  logtime: "2016-07-28T15:01:09Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/145708.html"
title: "Domain registry APIs and \"superglue\""
...

<p>On Tuesday (2016-07-26) I gave a talk at work about domain registry
APIs. It was mainly humorous complaining about needlessly complicated
and broken stuff, but I slipped in a few cool ideas and
recommendations for software I found useful.</p>

<p>I have published
the <a href="https://fanf2.user.srcf.net/dns/2016-07-seminar/slides.pdf">slides</a>
and <a href="https://fanf2.user.srcf.net/dns/2016-07-seminar/notes.pdf">notes</a>
(both PDF).</p>

<p>The talk was based on what I learned last year when writing some
software that I called
<a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/superglue.git"><code>superglue</code></a>,
but the talk wasn't about the software. That's what this article is about.</p>

<h2>What is superglue?</h2>

<p>Firstly, it isn't finished. To be honest, it's barely started!
Which is why I haven't written about it before.</p>

<p>The goal is automated management of DNS delegations in parent zones -
hence "super" (Latin for over/above/beyond, like a parent domain)
"glue" (address records sometimes required as part of a delegation).</p>

<p>The basic idea is that <code>superglue</code> should work roughly like
<a href="http://dotat.at/prog/nsdiff/"><code>nsdiff</code> | <code>nsupdate</code></a>.</p>

<p>Recall, <code>nsdiff</code> takes a DNS master file, and it produces an
<code>nsupdate</code> script which makes the live version of the DNS zone match
what the master file says it should be.</p>

<p>Similarly, <code>superglue</code> takes a collection of DNS records that describe
a delegation, and updates the parent zone to match what the delegation
records say it should be.</p>

<h2>A uniform interface to several domain registries</h2>

<p>Actually <code>superglue</code> is a suite of programs.</p>

<p>When I wrote it I needed to be able to update parent delegations
managed by RIPE, JISC, Nominet, and Gandi; this year I have started
moving our domains to Mythic Beasts. They all have different APIs, and
only Nominet supports the open standard <a href="https://en.wikipedia.org/wiki/Extensible_Provisioning_Protocol">EPP</a>.</p>

<p>So <code>superglue</code> has a framework which helps each program conform to a
consistent command line interface.</p>

<p>Eventually there should be a <code>superglue</code> wrapper which knows which
provider-specific script to invoke for each domain.</p>

<h2>Refining the user interface</h2>

<p><a href="http://www.zytrax.com/books/dns/ch9/delegate.html">DNS delegation is tricky</a>
even if you only have the DNS to deal with. However, <code>superglue</code> also
has to take into account the different rules that various domain
registries require.</p>

<p>For example, in my initial design, the input to <code>superglue</code> was going
to simply be the delegations records that should go in the parent
zone, verbatim.</p>

<p>But some domain registries to not accept DS records; instead you have
to give them your DNSKEY records, and the registry will generate their
preferred form of DS records in the delegation. So <code>superglue</code> needs
to take DNSKEY records in its input, and do its own DS generation for
registries that require DS rather than DNSKEY.</p>

<h2>Sticky problems</h2>

<p>There is also a problem with glue records: my idea was that
<code>superglue</code> would only need address records for nameservers that are
within the domain whose delegation is being updated. This is the
minimal set of glue records that the DNS requires, and in many cases
it is easy to pull them out of the master file for the delegated zone.</p>

<p>However, sometimes a delegation includes name servers in a sibling
domain. For example, <code>cam.ac.uk</code> and <code>ic.ac.uk</code> are siblings.</p>

<pre><code>    cam.ac.uk.  NS  authdns0.csx.cam.ac.uk.
    cam.ac.uk.  NS  ns2.ic.ac.uk.
    cam.ac.uk.  NS  sns-pb.isc.org.
    ; plus a few more
</code></pre>

<p>In our delegation, glue is required for nameservers in <code>cam.ac.uk</code>,
like <code>authdns0</code>; glue is forbidden for nameservers outside <code>ac.uk</code>,
like <code>sns-pb.isc.org</code>. Those two rules are fixed by the DNS.</p>

<p>But for nameservers in a sibling domain, like <code>ns2.ic.ac.uk</code>, the DNS
says glue may be present or omitted. Some registries say this optional
sibling glue must be present; some registries say it must be absent.</p>

<p>In registries which require optional sibling glue, there is a quagmire
of problems. In many cases the glue is already present, because it is
part of the sibling delegation and, therefore, required - this is the
case for <code>ns2.ic.ac.uk</code>. But when the glue is truly optional it
becomes unclear who is responsible for maintaining it: the parent
domain of the nameserver, or the domain that needs it for their
delegation?</p>

<p>I basically decided to ignore that problem. I think you can work
around it by doing a one-off manual set up of the optional glue, after
which <code>superglue</code> will work. So it's similar to the other delegation
management tasks that are out of scope for <code>superglue</code>, like
registering or renewing domains.</p>

<h2>Captain Adorable</h2>

<p>The motivation for <code>superglue</code> was, in the short term, to do a bulk
update of out 100+ delegations to add <code>sns-pb.isc.org</code>, and in the
longer term, to support automatic DNSSEC key rollovers.</p>

<p>I wrote barely enough code to help with the short term goal, so what I
have is undocumented, incomplete, and inconsistent. (Especially wrt
DNSSEC support.)</p>

<p>And since then I haven't had time or motivation to work on it, so it
remains a complete <a href="http://www.bbc.co.uk/cbeebies/shows/gigglebiz">shambles</a>.</p>
