---
dw:
  anum: 215
  eventtime: "2018-10-12T22:04:00Z"
  itemid: 500
  logtime: "2018-10-12T21:05:54Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/128215.html"
format: md
...

Amsterdam day 0
===============

Today I travelled to Amsterdam for a *bunch* of conferences. This
weekend there is a [joint DNS-OARC and CENTR
workshop](https://indico.dns-oarc.net/event/29/), and Monday - Friday
there is a [RIPE meeting](https://ripe77.ripe.net/).

The [DNS Operations Analysis and Research Centre](https://www.dns-oarc.net/) holds peripatetic
workshops a few times a year, usually just before an ICANN or RIR
meeting. They are always super interesting and relevant to my work,
but usually a very long way away, so I make do with looking at the
slides and other meeting materials from afar.

[CENTR](https://centr.org/) is the association of European
country-code TLD registries. Way above my level :-)

[RIPE](https://www.ripe.net/) is the regional Internet registry for
Europe and the Middle East. Earlier this year, the University of
Cambridge became a local Internet registry (i.e. an organization
responsible for sub-allocating IP address space) and new members get a
couple of free tickets to a RIPE meeting. RIPE meetings also usually
have a lot of interesting presentations, and there's a DNS track which
is super relevant to me.

I haven't been to any of these meetings before, so it's a bit of an
adventure, though I know quite a lot of the people who will be here
from other meetings! This week I've been doing some bits and bobs that
I hope to talk about with other DNS people while I am here.

doh101
------

Last month [I deployed DNS-over-TLS and DNS-over-HTTPS on the
University's central DNS recolvers](https://www.dns.cam.ac.uk/news/2018-09-05-doth.html).
This turned out to be a bit more interesting than expected, mainly
because a number of Android users started automatically using it
straight away. Ólafur Guðmundsson from Cloudflare is talking about DoH
and DoT tomorrow, and I'm planning to do a lightning talk on Sunday
about my experience. So on Wednesday I gathered some up-to-to-date
stats, including the undergraduates who were not yet around last
month.

(My DoT stats are a bit feeble at the moment because I need full query
logs to get a proper idea of what is going on, but they are usually
turned off.)

Rollover
--------

Yesterday evening was [the belated DNSSEC root key
rollover](https://www.icann.org/resources/pages/ksk-rollover). There
are some interesting graphs produced by [SIDN
labs[(https://www.sidnlabs.nl/) and [NLnet
Labs](https://nlnetlabs.nl/) on the NLnet Labs home page. These stats
are gathered using [RIPE Atlas](https://atlas.ripe.net/) which is a
distributed Internet measurement platform.

I found the rollover very distracting, although it was mostly quite
boring, which is exactly what it should be!

ANAME
-----

The [IETF dnsop working
group](https://datatracker.ietf.org/group/dnsop/about/) is
collectively unhappy with [the recently expired ANAME
draft](https://tools.ietf.org/html/draft-ietf-dnsop-aname-01) -
including at least some of the draft authors. Since this is something
dear to my heart (because web site aliases are one of the more
troublesome parts of my job, and I want better features in the DNS to
help make the trouble go away) I spent most of this week turning [my
simplified ANAME
proposal](https://mailarchive.ietf.org/arch/msg/dnsop/LrQ0LRVXkLYhs_phM6O7b2VY8pQ)
into a proper draft.

I'm hoping to discuss it with a few people (including some of the
existing draft authors) with the aim of submitting the revised draft
before the deadline on Monday 22nd for [next month's IETF
meeting](https://datatracker.ietf.org/meeting/important-dates/).
