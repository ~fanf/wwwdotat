---
dw:
  anum: 84
  eventtime: "2007-08-03T15:44:00Z"
  itemid: 293
  logtime: "2007-08-03T17:12:35Z"
  props:
    commentalter: 1491292343
    import_source: livejournal.com/fanf/75454
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/75092.html"
format: casual
lj:
  anum: 190
  can_comment: 1
  ditemid: 75454
  event_timestamp: 1186155840
  eventtime: "2007-08-03T15:44:00Z"
  itemid: 294
  logtime: "2007-08-03T17:12:35Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/75454.html"
title: Warning to Linux postmasters
...

Make sure you have the line <tt>mdns off</tt> in <tt>/etc/host.conf</tt> on your incoming SMTP servers.

One of my colleagues in our network engineering team discovered today that ppswitch was spewing multicast packets, much to our surprise. It turns out that recent versions of glibc have quietly added support for multicast DNS to the resolver. Multicast DNS is part of <a href="http://www.zeroconf.org/">Apple's zeroconf networking system</a> (aka Bonjour, previously known as Rendezvous), and it takes over host names ending in .local. See it in action by typing <tt>strace ping foo.local</tt> and observe it sending a DNS query to the class D multicast address 224.0.0.251.

Since MXs have to deal with untold quantities of crap (at the moment about 96% of the email we're offered - 6 million messages per day - is junk) and since one of the key crap detection tools is the DNS, ppswitch ends up doing a lot of crap DNS lookups. A significant number of these (10,000 per day) are names ending in .local which thereby trigger mdns lookups. However these names do not come from machines named via zeroconf: they are mostly Small Business Server installations which have followed <a href="http://support.microsoft.com/kb/296250">Microsoft's recommendations for choosing a domain name</a>.

It is a great source of joy and wonder that Apple and Microsoft both use .local in conflicting ways. This is truly the Zen of standards: contemplate it deeply and you may achieve enlightenment. <small>(if you don't go mad)</small>
