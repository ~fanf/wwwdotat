---
dw:
  anum: 180
  eventtime: "2009-03-15T19:57:00Z"
  itemid: 378
  logtime: "2009-03-15T19:57:40Z"
  props:
    commentalter: 1491292363
    import_source: livejournal.com/fanf/98545
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/96948.html"
format: html
lj:
  anum: 241
  can_comment: 1
  ditemid: 98545
  event_timestamp: 1237147020
  eventtime: "2009-03-15T19:57:00Z"
  itemid: 384
  logtime: "2009-03-15T19:57:40Z"
  props:
    personifi_tags: "nterms:yes"
    verticals_list: life
  reply_count: 5
  url: "https://fanf.livejournal.com/98545.html"
title: John Taylor talks about the Corpus clock
...

<p>I posted a couple of articles (<a href="http://fanf.livejournal.com/94043.html">one</a>, <a href="http://fanf.livejournal.com/94411.html">two</a>) about <a href="http://en.wikipedia.org/wiki/Corpus_Clock">the Corpus clock</a> soon after it was unveiled, and I optained a copy of <a href="http://www.chronophage.co.uk/shop.htm">the Chronophage book</a> soon after it became available. The book is a bit lacking in technical detail, so I was pleased to find out that John Taylor would be talking about it for <a href="http://www.admin.cam.ac.uk/sciencefestival/events.shtml?id=410&amp;template=/sciencefestival/events/item.template">the Cambridge Science Festival</a> and I attended his talk yesterday. The lecture theatre was packed.</p>  <p>The talk was in three parts: a bit of autobiography, then a quick overview of the clock and its development, then questions. The talk revealed some interesting facts which do not appear in the book.</p>

<p><a href="http://en.wikipedia.org/wiki/John_C._Taylor_(inventor)">Taylor's story</a> of how he came to make a fortune in kettle thermostats is relevant to the clock not just because that's how he made the money: his horological hero, <a href="http://en.wikipedia.org/wiki/John_Harrison">John Harrison</a>, invented the <a href="http://en.wikipedia.org/wiki/Grasshopper_escapement">grasshopper escapement</a> that inspired the Chronophage, and also <a href="http://en.wikipedia.org/wiki/Bimetallic_strip">invented the bimetal</a> on which Taylor's thermostats rely. He explained his snap-action bimetallic disc, which has a C cut out to form a tongue in the middle; this tongue moves further than the centre of a disc without the cut-out does, which loosens the tolerances required to manufacture a thermostat. The calligraphic flourish engraved below his name on the clock's pendulum bob forms the shape of his bimetallic disc. (Sorry, I can't find a picture on line though it's visible in the book.)</p>

<p><b>ETA:</b> I have found <a href="http://www.flickr.com/photos/36320341@N02/3351359967/sizes/o/">a large picture of the pendulum bob</a> in which you can see the shape of Taylor's bimetallic disc under the date. It's overdrawn by some of the flourish so it's a bit obscure unless you know what you are looking for.</p>

<p>There were a couple of hints at the clock's workings. It is usual for clocks with chiming mechanisms to have a second set of drive springs or weights for the chimes. The Corpus clock has only one spring, so its hourly death-rattle is driven in an unusual way. This is why the clock ticks back and forth while rattling the chain in the coffin. There are two main mechanisms inside the Chronophage itself. It contains a mechanical pseudo-random mechanism to control the blinking and biting. The sting, however, is regular: it slowly rises then jabs down on each quarter hour.</p>  <p>He also mentioned that the clock &quot;has fun&quot; four times a year: on 24th March, which is the date of Harrison's birth and also his death; on 25th November, <a href="http://www.cambridge-news.co.uk/cn_news_home/displayarticle.asp?id=368991">Taylor's birthday</a>; on new year's eve and new year's day; and on Corpus Christi day, the Church's festival that occurs on the Thurdsday after Trinity Sunday, the 8th Sunday after Easter. The clock is away for maintenance this week, so I hope it'll be back in time to muck around on Tuesday week. The fun seems to involve even more erratic ticking than usual, with more colourful lights.</p>

<p>The clock's spring has to be particularly strong in order to overcome the momentum of the large escape wheel that goes around the outside of the clock. When prototyping the clock this caused a serious problem: the force of the spring is transmitted through the escapement to the pendulum, making it swing higher and higher and eventually breaking the clock. Taylor overcame this problem by adding a regulator, which also serves two other functions: it produces the clock's erratic behaviour that plays tricks with observers, and it listens to <a href="http://en.wikipedia.org/wiki/MSF_time_signal">the MSF time signal</a> to synchronize the clock every five minutes. (Taylor confirmed to me after the talk that these are all functions of the same mechanism. He also said that the hollow pendulum bob is not in fact &quot;massive and weighty&quot; as the book says, which answered my question about conservation of momentum.) I'm not sure how this is consistent with his assertions that the clock is purely mechanical - would it work if the computer regulator broke? <a href="http://www.cambridge-news.co.uk/cn_news_cambridge/displayarticle.asp?id=357677">News reports</a> about the clock's teething problems suggest that it's pretty vital. I wonder if the maintenance periods this week and back in January are for software patches rather than mechanical adjustment?</p>

<p>I asked Taylor if he would publish more technical details about the clock, but he thinks that the mystery makes it fun. I disagree :-/</p>
