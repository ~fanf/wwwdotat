---
dw:
  anum: 12
  eventtime: "2005-03-09T20:30:00Z"
  itemid: 134
  logtime: "2005-03-09T20:33:25Z"
  props:
    import_source: livejournal.com/fanf/34693
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/34316.html"
format: casual
lj:
  anum: 133
  can_comment: 1
  ditemid: 34693
  event_timestamp: 1110400200
  eventtime: "2005-03-09T20:30:00Z"
  itemid: 135
  logtime: "2005-03-09T20:33:25Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/34693.html"
title: Exim CSA patch
...

My CSA implementation seems to work OK now (according to my tests). Any more testing, code review, suggestions, bug reports etc. welcome. The following patch is against the code from CVS, but it applies to 4.50 OK.
                                                                                                                                                      
http://www.cus.cam.ac.uk/~fanf2/hermes/doc/antiforgery/exim-csa.patch
http://www.cus.cam.ac.uk/~fanf2/hermes/doc/antiforgery/exim-csa.docs

For those who like tales of woe, search for "gross" in the patch. That cost me two hours!
