---
dw:
  anum: 162
  eventtime: "2016-02-19T21:00:00Z"
  itemid: 445
  logtime: "2016-02-19T21:00:20Z"
  props:
    commentalter: 1491292415
    import_source: livejournal.com/fanf/141066
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/114082.html"
format: html
lj:
  anum: 10
  can_comment: 1
  ditemid: 141066
  event_timestamp: 1455915600
  eventtime: "2016-02-19T21:00:00Z"
  itemid: 551
  logtime: "2016-02-19T21:00:20Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/141066.html"
title: Raspberry Pi 2 vs Sun E450
...

<p>This afternoon I was talking to an undergrad about email in Cambridge, and showing some <a href="https://fanf2.user.srcf.net/photos.html">old pictures of Hermes</a> to give some idea of what things were like many years ago. This made me wonder, how does a <a href="https://shop.pimoroni.com/products/raspberry-pi-2-with-pibow">Raspberry Pi 2 (in a Pibow case)</a> compare to a <a href="http://shrubbery.net/~heas/sun-feh-2_1/Systems/E450/E450.html">Sun E450</a> like the ones we decommissioned in 2004?</p>

<p><b>ETA (2016-02-29):</b> I have embiggened this table to include details of the Raspberry Pi 3. It's worth noting when comparing the IO ports that both models of Raspberry Pi hang the ethernet and four USB ports off one USB2 480 Mbit/s bus. In the Raspberry Pi 3 the WiFi shares the SDIO bus with the SD card slot, and the Bluetooth hangs off a UART. The E450 has <i>a lot more</i> IO bandwidth.</p>

<table>
  <tr>
    <td></td>
    <th align="left">
      Raspberry Pi 2
    </th>
    <th align="left">
      Raspberry Pi 3
    </th>
    <th align="left">
      Sun Enterprise 450
    </th>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      (in PiBow case)
    </td>
    <td>☜</td>
    <td>
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      2015 -
    </td>
    <td>
      2016 -
    </td>
    <td>
      1997-2002
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      £44
    </td>
    <td>☜</td>
    <td>
      <a href="http://www.cnet.com/products/sun-enterprise-450-ultrasparc-ii-400-mhz-monitor-none-series/specs/">$14 000</a>
    </td>
  </tr>

  <tr>
    <th align="left">
      physical
    </th>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>
      dimensions
    </td>
    <td>
      99 x 66 x 30 mm
    </td>
    <td>☜</td>
    <td>
      696 x 448 x 581 mm
    </td>
  </tr>
  <tr>
    <td>
      volume
    </td>
    <td>
      196 cm<sup>3</sup>
    </td>
    <td>☜</td>
    <td>
      181 000 cm<sup>3</sup>
    </td>
  </tr>
  <tr>
    <td>
      weight
    </td>
    <td>
      137 g
    </td>
    <td>☜</td>
    <td>
      94 000 g
    </td>
  </tr>

  <tr>
    <th align="left">
      CPU
    </th>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>
      cores
    </td>
    <td>
      4 x ARM Cortex-A7
    </td>
    <td>
      4 x ARM Cortex-A53
    </td>
    <td>
      4 x UltraSPARC II
    </td>
  </tr>
  <tr>
    <td>
      word size
    </td>
    <td>
      32 bit
    </td>
    <td>
      64 bit
    </td>
    <td>
      64 bit
    </td>
  </tr>
  <tr>
    <td>
      issue width
    </td>
    <td>
      2-way in-order
    </td>
    <td>☜</td>
    <td>
      4-way in-order
    </td>
  </tr>
  <tr>
    <td>
      clock
    </td>
    <td>
      900 MHz
    </td>
    <td>
      1200 MHz
    </td>
    <td>
      400 MHz
    </td>
  </tr>
  <tr>
    <td>
      GPU
    </td>
    <td>
      Videocore IV
    </td>
    <td>☜</td>
    <td>
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      250 MHz
    </td>
    <td>
      300 MHz
    </td>
    <td>
    </td>
  </tr>

  <tr>
    <th align="left">
      Memory
    </th>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>
      L1 i-cache
    </td>
    <td>
      32 KB
    </td>
    <td>☜</td>
    <td>
      16 KB
    </td>
  </tr>
  <tr>
    <td>
      L1 d-cache
    </td>
    <td>
      32 KB
    </td>
    <td>☜</td>
    <td>
      16 KB
    </td>
  </tr>
  <tr>
    <td>
      L2 cache
    </td>
    <td>
      512 KB
    </td>
    <td>☜</td>
    <td>
      4 MB
    </td>
  </tr>
  <tr>
    <td>
      RAM
    </td>
    <td>
      1 GB
    </td>
    <td>☜</td>
    <td>
      4 GB
    </td>
  </tr>
  <tr>
    <td>
      bandwidth
    </td>
    <td>
      900 MB/s
    </td>
    <td>☜</td>
    <td>
      1778 MB/s
    </td>
  </tr>

  <tr>
    <th align="left">
      I/O
    </th>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>
      Network
    </td>
    <td>
      1 x 100 Mb/s ethernet
    </td>
    <td>
      1 x 100 Mb/s ethernet
    </td>
    <td>
      1 x 100 Mb/s ethernet
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
    </td>
    <td>
      1 x 72 Mb/s 802.11n WiFi
    </td>
    <td>
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
    </td>
    <td>
      1 x 24 Mb/s Bluetooth 4.1
    </td>
    <td>
    </td>
  </tr>
  <tr>
    <td>
      Disk bus speed
    </td>
    <td>
      25 MB/s
    </td>
    <td>☜</td>
    <td>
      40 MB/s
    </td>
  </tr>
  <tr>
    <td>
      Disk interface
    </td>
    <td>
      1 x MicroSD
    </td>
    <td>☜</td>
    <td>
      5 x UltraSCSI-3
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
      1 x UltraSCSI-2
      <br/>
      1 x floppy
      <br/>
      1 x CD-ROM
    </td>
  </tr>
  <tr>
    <td>
      other
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
    </td>
    <td>
      1 x UART
      <br/>
      4 x USB
      <br/>
      8 x GPIO
      <br/>
      2 x I²C
      <br/>
      2 x SPI
      <br/>
      HDMI
      <br/>
      DSI
      <br/>
      CSI
    </td>
    <td>
      ☜
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
    </td>
    <td>
      2 x UART
      <br/>
      1 x mini DIN-8
      <br/>
      1 x centronics
      <br/>
      3 x 64 bit 66 MHz PCI
      <br/>
      4 x 64 bit 33 MHz PCI
      <br/>
      3 x 32 bit 33 MHz PCI
      <br/>
      <br/>
      <br/>
    </td>
  </tr>

</table>
