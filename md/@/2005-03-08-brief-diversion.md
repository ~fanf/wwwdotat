---
dw:
  anum: 172
  eventtime: "2005-03-08T12:56:00Z"
  itemid: 132
  logtime: "2005-03-08T13:02:58Z"
  props:
    import_source: livejournal.com/fanf/34244
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/33964.html"
format: casual
lj:
  anum: 196
  can_comment: 1
  ditemid: 34244
  event_timestamp: 1110286560
  eventtime: "2005-03-08T12:56:00Z"
  itemid: 133
  logtime: "2005-03-08T13:02:58Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/34244.html"
title: Brief diversion
...

I've updated <code>unifdef</code> with a fix for a fairly stupid bug - it was not ignoring comment markers inside strings (in fact it was completely oblivious of strings). What makes this bug particularly stupid is that the code I started with did recognize strings, though not according to C syntax, and I ripped it out because it was broken. D'oh! The bug report has also prompted me to update the version distributed with FreeBSD, which happened to be missing some additions I made in August 2003. Good thing too, because it's been a while since my last commit.

New code at http://dotat.at/prog/misc/ and http://www.freebsd.org/cgi/cvsweb.cgi/src/usr.bin/unifdef/
