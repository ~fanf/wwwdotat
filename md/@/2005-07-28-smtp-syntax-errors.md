---
dw:
  anum: 62
  eventtime: "2005-07-28T15:00:00Z"
  itemid: 148
  logtime: "2005-07-28T15:06:18Z"
  props:
    commentalter: 1491292323
    import_source: livejournal.com/fanf/38174
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/37950.html"
format: casual
lj:
  anum: 30
  can_comment: 1
  ditemid: 38174
  event_timestamp: 1122562800
  eventtime: "2005-07-28T15:00:00Z"
  itemid: 149
  logtime: "2005-07-28T15:06:18Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/38174.html"
title: SMTP syntax errors
...

Phil Chambers complains about non-ASCII junk in SMTP envelopes:
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050718/msg00029.html

I note that another common syntax error is to put a space between : and < and talk about building support into Exim to deal with this misbehaviour:
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00102.html

More followups discussing ways of configuring existing versions of Exim to do this, and the wiseness thereof:
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00141.html
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00147.html
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00169.html

(I also note that Exim is already strict in this manner about HELO domains.)

[update]
Using $smtp_command_argument to implement :< strictness:
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00184.html
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050725/msg00191.html
