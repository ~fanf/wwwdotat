---
format: html
lj:
  anum: 92
  can_comment: 1
  ditemid: 112476
  event_timestamp: 1304532180
  eventtime: "2011-05-04T18:03:00Z"
  itemid: 439
  logtime: "2011-05-04T17:03:13Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/112476.html"
title: DNSSEC with BIND 9.8.0
...

<p>The latest version of BIND makes DNSSEC validation very easy to set up. Just put the following lines into the "<tt>options</tt>" section of your <tt>named.conf</tt>:</p>

<pre>
  dnssec-validation auto;
  dnssec-lookaside auto;
</pre>

<p>When you upgrade from an older version of BIND you need to delete the <tt>managed-keys.bind</tt> pseudo-zone - BIND will only add its built-in root and DLV trust anchors when it first creates the file.</p>

<p>That's it! Easy! Do it!</p>

<p>Publishing signed zones is getting easier too. If you are an old-skool DNS admin who is a dab hand at editing flat text master files, then the main thing that takes some getting used to is wrangling dynamic DNS instead. Signed zones need to be dynamic so that BIND can refresh the RRSIG records periodically, so you might as well use <a href="http://dotat.at/tmp/arm98/man.nsupdate.html"><tt>nsupdate</tt></a> to make changes too, and enjoy the shiny future.</p>

<p>To sign a zone, <tt>cd</tt> to <tt>named</tt>'s working directory where you will create a set of keys for the zone. (You can tell BIND to look for keys elsewhere using a <tt>key-directory</tt> statement in each <tt>zone</tt> block or set it globally in the <tt>options</tt> section.) Then run these commands:</p>

<pre>
  dnssec-keygen -f KSK <i>$zone</i>
  dnssec-keygen <i>$zone</i>
</pre>

<p>This creates two key pairs with the default settings: a key signing key pair and a zone signing key pair. Ensure they are readable by the BIND user.<p>

<p>Then create an initial zone file. It has to have at least a SOA and an NS record. I start off with a copy of my local empty zone and change the SOA and NS later.</p>

<pre>
  $TTL 1h
  @ SOA localhost. root.localhost. 1 1h 1000 1w 1h
    NS  localhost.
</pre>

<p>Then add a <tt>zone</tt> statement to <tt>named.conf</tt>.</p>

<pre>
  zone "<i>$zone</i>" {
    type master;
    file "<i>$zone</i>";
    update-policy local;
    auto-dnssec maintain;
  };
</pre>

<p>The <tt>update-policy</tt> statement lets you run <tt>nsupdate</tt>&nbsp;<tt>-l</tt> on the same machine as the nameserver to make changes to the zone. The <tt>auto-dnssec</tt> statement tells named to handle re-signing automatically. (It will also handle key rollovers if you pre-generate keys and <a href="http://dotat.at/tmp/arm98/man.dnssec-settime.html">set their lifetimes</a>.)</p>

<p>Then run <tt>rndc</tt> <tt>reconfig</tt> and you are all set!</p>

<p>A couple of other non-default settings are possibly worth noting. There is a new feature which makes adding and deleting zones marginally neater. If you put <tt>allow-new-zones yes</tt> in your <tt>options</tt> section then you can use the <tt>rndc</tt> <tt>addzone</tt> and <tt>delzone</tt> commands instead of editing <tt>named.conf</tt>. When adding a zone you still have to create the keys and zone file first. The other tweak  is to set <tt>dnssec-dnskey-kskonly yes</tt> which reduces the size of the zone apex RRSIG RRset (which should probably be the default).</p>
