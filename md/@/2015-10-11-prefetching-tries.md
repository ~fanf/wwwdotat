---
dw:
  anum: 191
  eventtime: "2015-10-11T20:33:00Z"
  itemid: 432
  logtime: "2015-10-11T19:33:47Z"
  props:
    commentalter: 1491292413
    import_source: livejournal.com/fanf/137953
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/110783.html"
format: html
lj:
  anum: 225
  can_comment: 1
  ditemid: 137953
  event_timestamp: 1444595580
  eventtime: "2015-10-11T20:33:00Z"
  itemid: 538
  logtime: "2015-10-11T19:33:47Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/137953.html"
title: prefetching tries
...

<p>The inner loop in <a href="http://dotat.at/prog/qp">qp trie</a> lookups is roughly</p>

<pre><code>    while(t-&gt;isbranch) {
        __builtin_prefetch(t-&gt;twigs);
        b = 1 &lt;&lt; key[t-&gt;index]; // simplified
        if((t-&gt;bitmap &amp; b) == 0) return(NULL);
        t = t-&gt;twigs + popcount(t-&gt;bitmap &amp; b-1);
    }
</code></pre>

<p>The efficiency of this loop depends on how quickly we can get from one
indirection down the trie to the next. There is quite a lot of work in
the loop, enough to slow it down significantly compared to the
crit-bit search loop. Although qp tries are half the depth of crit-bit
tries on average, they don't run twice as fast. The prefetch
compensates in a big way: without it, qp tries are about 10% faster;
with it they are about 30% faster.</p>

<p>I adjusted the code above to emphasize that in one iteration of the
loop it accesses two locations: the key, which it is traversing
linearly with small skips, so access is fast; and the tree node <code>t</code>,
whose location jumps around all over the place, so access is slow. The
body of the loop calculates the next location of <code>t</code>, but we know at
the start that it is going to be some smallish offset from <code>t-&gt;twigs</code>,
so the prefetch is very effective at overlapping calculation and
memory latency.</p>

<p>It was entirely accidental that prefetching works well for qp tries. I
was trying not to waste space, so the thought process was roughly, a
leaf has to be two words:</p>

<pre><code>    struct Tleaf { const char *key; void *value; };
</code></pre>

<p>Leaves should be embedded in the twig array, to avoid a wasteful
indirection, so branches have to be the same size as leaves.</p>

<pre><code>    union Tnode { struct Tleaf leaf; struct Tbranch branch; };
</code></pre>

<p>A branch has to have a pointer to its twigs, so there is space in the
other word for the metadata: bitmap, index, flags. (The limited space
in one word is partly why qp tries test a nibble at a time.) Putting
the metadata about the twigs next to the pointer to the twigs is the
key thing that makes prefetching work.</p>

<p>One of the inspirations of qp tries was <a href="http://infoscience.epfl.ch/record/64398/files/idealhashtrees.pdf">Phil Bagwell's hash array mapped
tries</a>.
HAMTs use the same popcount trick, but instead of using the PATRICIA
method of skipping redundant branch nodes, they hash the key and use
the hash as the trie index. The hashes should very rarely collide, so
redundant branches should also be rare. Like qp tries, HAMTs put the
twig metadata (just a bitmap in their case) next to the twig pointer,
so they are friendly to prefetching.</p>

<p>So, if you are designing a tree structure, put the metdadata for
choosing which child is next adjacent to the node's pointer in its
parent, not inside the node itself. That allows you to overlap the
computation of choosing which child is next with the memory latency
for fetching the child pointers.</p>
