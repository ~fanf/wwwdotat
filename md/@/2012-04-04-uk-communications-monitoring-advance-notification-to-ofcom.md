---
format: html
lj:
  anum: 124
  can_comment: 1
  ditemid: 119420
  event_timestamp: 1333545780
  eventtime: "2012-04-04T13:23:00Z"
  itemid: 466
  logtime: "2012-04-04T12:23:10Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/119420.html"
title: UK communications monitoring / advance notification to Ofcom
...

<p>If you follow <a href="http://dotat.at/:/">my link log</a> (also <lj user="dotaturls">) you'll see several links recently about the Home Office's "communications capabilities development programme". This is the rebranding of the "interception modernisation programme" which started under the previous government. The key difference between them is replacing a centralized database of communications metadata with a distributed one, hosted by ISPs.</p>

<p>There are technical questions about what data can reasonably be obtained, especially when the media keep using Skype as an example of what the government want to track, though it isn't technically feasible for ISPs to do so. Much communication is mediated by American businesses (Google, Microsoft, Twitter, Facebook) over encrypted connections so access to those logs would require co-operation from the US. (I suppose that's what the "special relationship" is for.) Despite these technicalities, there are also oddities in the legal coverage, at least in the current law.</p>

<p>For example, the EU communications data retention directive requires public communications providers to retain logs for a year, in case law enforcement agencies want access to them. This does not apply to private services, such as businesses or universities. At Cambridge the central email services keep logs for four weeks. We occasionally (less than once a year) get a request to take a snapshot of a particular account which we retain on DVD in a safe until a warrant requires us to hand it over.</p>

<p>Heading off on a tangent, I was reading bits of legislation about obligations on non-public communications providers when I found <a href="http://www.legislation.gov.uk/ukpga/2003/21/section/33">section 33 of the communications act 2003</a>:</p>

<blockquote>
33 Advance notification to OFCOM<br>
&nbsp;&nbsp;(1) A person shall not—<br>
&nbsp;&nbsp;&nbsp;&nbsp;(a) provide a designated electronic communications network,<br>
&nbsp;&nbsp;&nbsp;&nbsp;(b) provide a designated electronic communications service, or<br>
&nbsp;&nbsp;&nbsp;&nbsp;(c) make available a designated associated facility,<br>
&nbsp;&nbsp;unless, before beginning to provide it or to make it available, he has given a notification to OFCOM of his intention to provide that network or service, or to make that facility available.
</blockquote>

<p>The definition of electronic communications networks and services is so broad that it covers home networks and personal web sites, for which it would be insane to require advance notification, so I wondered what subset of these Ofcom had designated.</p>

<p>I gather that the overall shape of the Communications Act 2003 was in response to an EU directive that required a technology-neutral regime, hence consolidating Oftel and the Radio Authority into one organisation. However the Ofcom web site is still somewhat divided into technology silos, so there's a section for radio licensing and a section for telephony. Another effect of the 2003 act was to liberalise the telephony licensing regime, replacing it with a <a href="http://stakeholders.ofcom.org.uk/telecoms/ga-scheme/">General Authorization regime</a> in which no licence is required though providers must follow the regulations. On the surface these so-called General Conditions are couched in technology-neutral terms, but in fact they are specific to telephony (e.g. condition 16 requires support for DTMF dialling). On the other hand, Condition 1 seems to apply to Internet peering within the EU.</p>

<p>Having failed to find anything on the Ofcom website about how section 33 of the act applies to internet services, I decided to make <a href="http://www.whatdotheyknow.com/request/advance_notification_under_secti">an FOI request asking them to explain what kinds of networks, services, and facilities are designated as requiring advance notification</a>.</p>
