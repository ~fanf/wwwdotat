BIND9 dnssec-policy appendices
==============================

Here are some miscellaneous unsorted notes about BIND9's
`dnssec-policy` that turned out not to be useful in my previous blog
posts, but which some readers might find informative. Some of them I
learned the hard way, so I hope I can make it easier for others!

<toc>

This is the third article in a three part series:

  * [Introducing BIND9 dnssec-policy](https://dotat.at/@/2024-05-08-dnssec-policy.html)
  * [Migrating to BIND9 dnssec-policy](https://dotat.at/@/2024-05-11-dnssec-policy.html)
  * BIND9 dnssec-policy appendices (this post)


contents of key files
---------------------

The key files contain a bunch of stuff:

  * the key material itself:
      - the secret part(s) in the `.private` file
      - a `DNSKEY` record in zone file syntax in the `.key` file

  * whether it is a ZSK or KSK or CSK
      - with `auto-dnssec` this was indicated by the DNSKEY flags
        field: 256 for a ZSK or 257 for a KSK
      - the flags field is also explained by a comment in the `.key` file
      - if there was only one key then it was implicitly a CSK
      - with `dnssec-policy` the key type is more explicitly described in
        the `.state` file

  * its timing metadata
      - when the keys were created, published, activated
      - when they will be retired (or not)
      - this information is repeated in all three files
      - (except that the DSPublish time is missing from the `.key` file)

  * its `dnssec-policy` states
      - for the key, its signatures, etc.
      - "hidden" / "rumoured" / "omnipresent" / "unretentive"
      - the times these last changed
      - only in the `.state` file

Not all of this stuff can be manipulated with `dnssec-settime`.

  * There's no option to set the `dnssec-policy` key type in the
    `.state` file.

  * The man page has firm imprecations that the options for setting
    `dnssec-policy` states are for developer testing only.

So I spent a fair amount of effort working out how to get the right
`dnssec-policy` contents in the key files without using
`dnssec-settime`. I got there eventually...


changes to key files
--------------------

During testing, when I failed to prepare the key files properly,
`dnssec-policy` would fill in the missing times for me. If the `.key`
and/or `.private` files changed when I enabled it, that was a clue I
had made a mistake.

The "Change" times in the `.state` file depend on when `dnssec-policy`
is enabled, so the `.state` files from my test server were not the
same as the ones on my live primary DNS server. Not a problem, but
possibly useful to know in advance.


fast timers?
------------

When I was repeatedly breaking my test server, I reduced the timing
settings to their minium:

        # go fast
        purge-keys 0;
        parent-ds-ttl 1s;
        publish-safety 1s;
        retire-safety 1s;
        parent-propagation-delay 1s;
        zone-propagation-delay 1s;

With these settings, the DS-related state transitions took only a few
seconds, instead of 26 hours.

At one point I thought this would be necessary to get the key files
into the right state in a reasonable amount of time, until I worked
out that my keys needed more preparation before switching on
`dnssec-policy`.


parental agents
---------------

I have also added the following line to all my zone blocks.
(It cannot be configured globally like many other zone settings.)

        parental-agents { ::1; };

A "parental agent" is a resolver that `dnssec-policy` can use to see
when the zone's DS records change during a rollover. When
`parental-agents` are not configured, you need to tell `dnssec-policy`
about changes to DS records using `rndc dnssec -checkds`.

I have left recursion enabled on my primary DNS server, so it will answer
recursive queries from localhost. This means I can tell `named` to use
itself (i.e. `::1`) as its own parental agent.

Since I'm not planning any rollovers, this setting isn't necessary,
but I thought it might be a good idea to let `dnssec-policy` observe
the state of the real world.
