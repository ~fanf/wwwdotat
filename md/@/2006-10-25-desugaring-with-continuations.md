---
dw:
  anum: 121
  eventtime: "2006-10-25T19:09:00Z"
  itemid: 258
  logtime: "2006-10-25T19:11:35Z"
  props:
    commentalter: 1491292356
    import_source: livejournal.com/fanf/66407
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/66169.html"
format: html
lj:
  anum: 103
  can_comment: 1
  ditemid: 66407
  event_timestamp: 1161803340
  eventtime: "2006-10-25T19:09:00Z"
  itemid: 259
  logtime: "2006-10-25T19:11:35Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/66407.html"
title: Desugaring with continuations
...

<p>Recently I've read about <a
href="http://www.lua.org/manual/5.1/manual.html">Lua</a>. It's a nice
simple language, with a syntax that's quite close to my ideal for a
dynamically-typed imperative language. One of the things it omits for
simplicity's sake is first-class continuations. I've been wondering
how one might specify a similar syntax if you have a <a
href="http://www.schemers.org/Documents/Standards/R5RS/HTML/">Scheme</a>ish
infrastructure, so that the usual control structures can be desugared
into functions, conditionals, and tail-calls. I also have in mind <a
href="http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-52.pdf">Modula-3</a>'s
definition of some control structures in terms of exceptions. In the
following I'm not going to talk about the whole syntax; I'll omit
things which don't involve blocks.</p>

<p>The basic form of a block expression is:</p>
<pre>
	<i>name</i><b>(</b><i>arguments</i><b>): do</b>
		<i>statements</i>
	<b>end</b>
</pre>
<p>The value of this expression is a function which takes some
<i>arguments</i> and which when called executes the <i>statements</i>.
The scope of its <i>name</i> is just within the block itself; it is
used in inner scopes to refer to shadowed variables in outer scopes.</p>

<p>To execute the block immediately, the <i>arguments</i> can be
omitted:</p>
<pre>
	<i>name</i><b>: do</b> <i>statements</i><b>; end</b>
</pre>
<p>is equivalent to</p>
<pre>
	<i>name</i><b>(): do</b> <i>statements</i><b>; end()</b>
</pre>
<p>that is, it specifies a function that has no arguments and which is
called immediately.</p>

<p>The <i>name</i> can also be omitted. Doing so just means there's no way
to refer to this block's variables if they are shadowed in an inner
scope. For example, an anonymous function (lambda expression) is
typically</p>
<pre>
	<b>(</b><i>args</i><b>): do</b> <i>statements</i><b>; end</b>
</pre>
<p>You might also want to allow a shorter version of</p>
<pre>
	<b>(</b><i>args</i><b>): do return(</b><i>values</i><b>); end</b>
</pre>
<p>for use in expressions, perhaps</p>
<pre>
	<b>(</b><i>args</i><b>): return (</b><i>values</i><b>)</b>
</pre>

<p>A minimal block like the following is just like a Lua block:</p>
<pre>
	<b>do</b> <i>statements</i><b>; end</b>
</pre>

<p>Just before the <b>end</b> of each block there's an implicit
<b>return(nil)</b>. You can <b>return</b> from a block early with
whatever value or list of values you want.</p>

<p>Instead of <b>do</b>...<b>end</b> you can write a conditional:</p>
<pre>
	<b>if</b> <i>expression</i> <b>then</b>
		<i>true statements</i>
	<b>else</b>
		<i>false statements</i>
	<b>end</b>
</pre>
<p>If the <b>else</b> part is omitted then the <i>false statements</i>
are implicitly <b>return(nil)</b>. You can also add <b>elsif</b>
<i>expression</i> <b>then</b>
<i>alternate statements</i> parts in the usual way.</p>

<p>The basic form of a loop is</p>
<pre>
	<i>name</i><b>: do</b>
		<i>statements</i>
	<b>loop</b>
</pre>
<p>which is equivalent to</p>
<pre>
	<i>name</i><b>: do</b>
		<i>statements</i>
		<b>return(</b><i>name</i><b>())</b>
	<b>end</b>
</pre>
<p>that is, loops are implemented using tail recursion. (Loops do not
have to be named, but I've written the desugaring in terms of a named
block to avoid having to talk about hypothetical unique names.)</p>

<pre>
	<i>name</i><b>: while</b> <i>expression</i> <b>do</b>
		<i>statements</i>
	<b>loop</b>
</pre>
<p>is equivalent to</p>
<pre>
	<i>name</i><b>: if</b> <i>expression</i> <b>then</b>
		<i>statements</i>
		<b>return(</b><i>name</i><b>())</b>
	<b>end</b>
</pre>
<p>and</p>
<pre>
	<i>name</i><b>: do</b>
		<i>statements</i>
	<b>while</b> <i>expression</i> <b>loop</b>
</pre>
<p>is equivalent to</p>
<pre>
	<i>name</i><b>: do</b>
		<i>statements</i>
		<b>if</b> <i>expression</i> <b>then</b>
			<b>return(</b><i>name</i><b>())</b>
		<b>end</b>
	<b>end</b>
</pre>
<p>You can use <b>until</b> <i>expression</i> instead of <b>while
not(</b><i>expression</i><b>)</b>.</p>

<p>Given the above, you can see that <b>return</b> inside a loop is
similar to <b>break</b> in C. It's often nice to be able to break out
of more than one nested loop at a time, or return from a function
inside a loop. This is where a block's <i>name</i> comes into play. To
return from a <i>name</i>d outer block, call
<i>name</i><b>.return</b>. For example:</p>
<pre>
	def bsearch(arr, val): do
		def lo, hi := 1, #arr
		while lo <= hi do
			def mid := math.floor((lo + hi) / 2);
			if arr[mid] == val then
				bsearch.return(mid)
			elsif arr[mid] < val then
				lo := mid + 1
			else
				hi := mid - 1
			end
		loop
	end
</pre>
<p>A bare <b>return</b> in the above would specify the value of the
<b>if</b> block, not the function.</p>

<p>This kind of return makes it easy to write
call-with-current-continuation:</p>
<pre>
	def callcc(f): do
		def cont(...): do
			callcc.return(...)
		end
		return(f(cont))
	end
</pre>
<p>However this is unnecessary because <b>return</b> is already an
expression whose value is the current continuation. It's just like
other function arguments, except that it's implicit (a bit like the
<b>self</b> argument in Lua's methods) and can't be assigned to (so
that the compiler can do tail-call optimisation). <small>(Actually, what if
you *could* assign to <b>return</b>? Eeeeevil!)</small></p>

<p>There are three forms of statement within a block: definitions,
assignments, and function calls. Since a block without arguments is
equivalent to a function expression that is immediately called,
non-function blocks can be statements. A function definition</p>
<pre>
	<b>def</b> <i>name</i><b>(</b><i>arguments</i><b>): do</b> ...
</pre>
<p>is equivalent to</p>
<pre>
	<b>def</b> <i>name</i> <b>:=</b> <i>name</i><b>(</b><i>arguments</i><b>): do</b> ...
</pre>

<p>For bigger code, it's useful to allow a block's <i>name</i>
to be repeated after its <b>end</b> or <b>loop</b>, for example:</p>
<pre>
	<b>def</b> long_function<b>(</b><i>arguments</i><b>): do</b>
		<i>code</i>
		<i>code</i>
		<i>code</i>
	<b>end</b> long_function
</pre>

<p>You can further desugar definitions, since</p>
<pre>
	<i>old_name</i><b>: do</b>
		<i>before statements</i>
		<b>def</b> <i>variables</i> <b>:=</b> <i>values</i>
		<i>after statements</i>
	<b>end</b>
</pre>
<p>is equivalent to</p>
<pre>
	<i>outer_name</i><b>: do</b>
		<i>before statements</i>
		<i>inner_name</i><b>(</b><i>variables</i><b>): do</b>
			<i>after statements</i>
		<b>end(</b><i>values</i><b>)</b>
	<b>end</b>
</pre>
<p>except that you need to fix up the <i>after statements</i> so that
unqualified <b>return</b>s are qualified with the
<i>outer_name</i>, and variables qualified with the <i>old_name</i>
are qualified with the <i>outer_name</i> or <i>inner_name</i> as
necessary.</p>

<p>Some things are still missing from this description. A <b>for</b>
loop is necessary, probably based on some kind of iterator concept,
but that's too far away from the topic of this post to be worth
pursuing in detail now. Also, we would probably like a standard
exception mechanism; this is not trivial to implement in terms of
continuations because the code that handles an exception is determined
dynamically (like Perl <b>local</b>) whereas continuations are lexical
(like Perl <b>my</b>). Finally, the combination of continuations and
concurrency is utterly filthy: imagine what happens if one thread
calls a continuation created by another...</p>
