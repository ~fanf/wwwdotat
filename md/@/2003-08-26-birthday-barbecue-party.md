---
dw:
  allowmask: 1
  anum: 92
  eventtime: "2003-08-26T16:43:00Z"
  itemid: 31
  logtime: "2003-08-26T16:44:37Z"
  props:
    commentalter: 1491292305
    current_moodid: 91
    import_source: livejournal.com/fanf/8016
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  security: usemask
  url: "https://fanf.dreamwidth.org/8028.html"
format: casual
lj:
  allowmask: 1
  anum: 80
  can_comment: 1
  ditemid: 8016
  event_timestamp: 1061916180
  eventtime: "2003-08-26T16:43:00Z"
  itemid: 31
  logtime: "2003-08-26T16:44:37Z"
  props:
    current_moodid: 91
  reply_count: 1
  security: usemask
  url: "https://fanf.livejournal.com/8016.html"
title: Birthday Barbecue + Party
...

[Most of you should have been invited via email, so this is just to cover any gaps...]

I will be entering the last year of my 20s on the 27th of August, so I have decided to have a party on Saturday the 30th -- that's THIS SATURDAY! The location will be 11 Atherton Close, CB4 2BE.

http://www.streetmap.co.uk/streetmap.dll?G2M?X=545529&Y=259960&A=Y&Z=1

There will be a "party size" barbecue (i.e. BIG) so we'll be starting at lunchtime (lighting up at 1300ish) and continuing until late. Limited crash space is available, but please ask. Please bring a bottle, some food (though don't bring any salsa dips because we have pints of the stuff), a friend, etc. and feel free to tell me if you are coming :-)
