---
dw:
  anum: 191
  eventtime: "2015-07-23T01:40:00Z"
  itemid: 423
  logtime: "2015-07-23T00:40:55Z"
  props:
    import_source: livejournal.com/fanf/135486
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/108479.html"
format: html
lj:
  anum: 62
  can_comment: 1
  ditemid: 135486
  event_timestamp: 1437615600
  eventtime: "2015-07-23T01:40:00Z"
  itemid: 529
  logtime: "2015-07-23T00:40:55Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/135486.html"
title: nsdiff-1.70 now with added nsvi
...

<p>I have released nsdiff-1.70 which is now available from <a href="http://dotat.at/prog/nsdiff/">the nsdiff home page</a>. nsdiff creates an nsupdate script from the differences between two versions of a zone. We use it at work for pushing changes from our IP Register database into our DNSSEC signing server.

<p>This release incorporates a couple of suggestions from Jordan Rieger of webnames.ca. The first relaxes domain name syntax in places where domain names do not have to be host names, e.g. in the SOA RNAME field, which is the email address of the people responsible for the zone. The second allows you to optionally choose case-insensitive comparison of records.

<p>The other new feature is an <a href="http://dotat.at/prog/nsdiff/nsvi.html"><b>nsvi</b></a> command which makes it nice and easy to edit a dynamic zone. Why didn't I write this years ago? It was inspired by <a href="https://twitter.com/jpmens/status/623425681691660292">a suggestion from @jpmens and @Habbie on Twitter</a> and fuelled by a few pints of <a href="http://www.oakhamales.com/citra.html">Citra</a>.
