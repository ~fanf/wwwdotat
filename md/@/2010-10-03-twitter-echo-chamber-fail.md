---
format: html
lj:
  anum: 187
  can_comment: 1
  ditemid: 109243
  event_timestamp: 1286136840
  eventtime: "2010-10-03T20:14:00Z"
  itemid: 426
  logtime: "2010-10-03T19:14:03Z"
  props:
    personifi_tags: "15:33,28:8,2:8,17:8,8:41,11:8,23:16,1:8,16:8,19:8,9:50,nterms:yes"
    verticals_list: "life,technology"
  reply_count: 33
  url: "https://fanf.livejournal.com/109243.html"
title: Twitter echo chamber fail
...

<p><i>"<a href="http://xkcd.com/386/">Lots of people are WRONG on the Internet!</a>"</i></p>

<p>Twitter users have a propensity to retweet, echo and rebroadcast interesting facts and links without very much fact checking. For example,</p>
<blockquote>
<a href="http://twitter.com/_468/statuses/26181199168">@_468</a> This October has 5 Fridays, 5 Saturdays and 5 Sundays all in one month. It happens only once in 823 years. waw.<br>(700+ retweets according to search.twitter.com)
</blockquote>
<blockquote>
<a href="http://twitter.com/bryanthatcher/status/26199993388">@bryanthatcher</a> This month has 5 Fridays, 5 Saturdays and 5 Sundays. It happens only once in 823 years.<br>(300+ retweets according to search.twitter.com)
</blockquote>
<p>Those retweet counts do not include the vast numbers of copies that didn't use twitter's retweet mechanism.</p>

<p>If you think about it, this happens whenever the month has 31 days and the first day of the month is a Friday. Because the years cycle through the days of the weeks fairly evenly, you would expect this to happen for a particular month about 1/7th of the time - in fact if you average over the entire Gregorian 400 year cycle it happens 1 in 7.1429 Octobers. Each year has 7 months with 31 days, and if you average over all of them you expect about one month in each year to have this property, and in fact you get precisely this result if you average over the entire Gregorian cycle. There happen to have been two months this year with 5 Fridays, Saturdays, and Sundays because January also started with a Friday.</p>

<p>1 in 7 and 1 in 1 are both rather more frequent than 1 in 823.</p>
