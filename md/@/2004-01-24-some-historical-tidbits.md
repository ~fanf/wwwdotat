---
dw:
  anum: 218
  eventtime: "2004-01-24T15:57:00Z"
  itemid: 56
  logtime: "2004-01-24T16:03:52Z"
  props:
    import_source: livejournal.com/fanf/14581
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/14554.html"
format: casual
lj:
  anum: 245
  can_comment: 1
  ditemid: 14581
  event_timestamp: 1074959820
  eventtime: "2004-01-24T15:57:00Z"
  itemid: 56
  logtime: "2004-01-24T16:03:52Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/14581.html"
title: Some historical tidbits
...

http://www.cam.ac.uk/cs/newsletter/1992/nl165.html

"the Central mail switch is now in service, and is processing nearly 2,000 messages per day"

It's now about 450,000.


http://www.cam.ac.uk/cs/newsletter/1995/nl181.html

Hermes is running very smoothly after the conversion [from PP]
to Smail and the reduction in the maintenance workload is
staggering.
