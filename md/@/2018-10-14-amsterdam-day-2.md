---
dw:
  anum: 48
  eventtime: "2018-10-14T23:40:00Z"
  itemid: 502
  logtime: "2018-10-14T21:41:32Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1539553864
  url: "https://fanf.dreamwidth.org/128560.html"
format: md
...

Amsterdam day 2
===============

Up at the crack of dawn for [the second half of the DNS-OARC
workshop](https://indico.dns-oarc.net/event/29/timetable/#20181014.detailed).
(See the timetable for links to slides etc.) The coffee I bought
[yesterday](https://dotat.at/@/2018-10-14-amsterdam-day-1.html) morning made a
few satisfactory cups to help me get started.

Before leaving the restaurant this evening I mentioned writing my notes to Dave Knight, who said his approach is to incrementally add to an email as the week goes on. I kind of like my daily reviews for remembering interesting side conversations, which are the major advantage of the value of attending these events in person.

DoT / DoH
---------

Sara Dickinson of Sinodun did a [really good talk on the consequences
of DNS
encryption](https://indico.dns-oarc.net/event/29/contributions/649/attachments/637/1024/OARC29_Encrypted_DNS.pdf),
with a very insightful analysis of the implications for how this might
change the architectural relationships between the web and the DNS.

DNS operators should read [RFC
8404](https://tools.ietf.org/html/rfc8404) on "Effects of Pervasive
Encryption on Operators". (I have not read it yet.)

Sara encouraged operators to implement DoT and DoH on their resolvers.

My lightning talk on [DoT and DoH at
Cambridge](https://indico.dns-oarc.net/event/29/sessions/98/attachments/646/1060/fanf-DoT-at-cam.pdf)
was basically a few (very small) numbers to give operators an idea of
what they can expect if they actually do this. I'm going to submit the
same talk for the RIPE lightning talks session later this week.

I had some good conversations with Baptiste Jonglez (who is doing a
PhD at Univ. Grenoble Alpes) and with Sara about DoT performance
measurements. At the moment BIND doesn't collect statistics that allow
me to know interesting things about DoT usage like DoT query rate and
timing of queries within a connection. (The latter is useful for
setting connection idle timeouts.) Something to add to the todo list...

CNAME at apex
-------------

Ondřej Surý of ISC.org talked about some experiments to find out how
much actually breaks in practice if you put a CNAME and other data at
a zone apex. Many resolvers break, but surprisingly many resolvers
kind of work.

Interestingly, CNAME+DNAME at the same name is pretty close to
working. This has been discussed in the past as "BNAME" (B for both)
with the idea of using it for completely aliasing a DNS subtree to
cope with internationalized domain names that are semantically
equivalent but have different Unicode encodings (e.g. ss / ß). However
the records have to be put in the parent zone, which is problematic if
the parent is a TLD.

The questions afterwards predictably veered towards ANAME and I spoke
up to encourage the audience to take a look at my [revamped ANAME
draft](https://github.com/each/draft-aname) when it is submitted. (I
hope to do a submission early this week to give it a wider audience
for comments before a revised submission near the deadline next
Monday.)

Tale Lawrence mentioned the various proposals for multiple queries in
a single DNS request as another angle for improving performance. (A
super simplified version of this is actually a stealth feature of the
ANAME draft, but don't tell anyone.)

I spoke to a few people about ANAME today and there's more enthusiasm
than I feared, though it tends to be pretty guarded. So I think the
draft's success really depends on getting the semantics right.

C-DNS / `dnstap`
----------------

Early in the morning was Jim Hague also of Sinodun talked about
C-DNS, which is a compressed DNS packet capture format used for
[DITL](https://www.dns-oarc.net/oarc/data/ditl) ("day in the life" or
"dittle") data collection from [ICANN L-root
servers](http://l.root-servers.org). (There was a special DITL
collection for a couple of days around the DNSSEC key rollover this
weekend).

C-DNS is based on [CBOR](http://cbor.io/) which is a pretty nice IETF
standard binary serialization format with a very JSON-like flavour.

Jim was talking partly about recent work on importing C-DNS data into
the [ClickHouse](https://clickhouse.yandex/) column-oriented SQLish
time-series database.

I'm vaguely interested in this area because various people have made
casual requests for DNS telemetry from my servers. (None of them have
followed through yet, so I don't do any query data collection at the
moment.) I kind of hoped that [`dnstap`](http://dnstap.info/) would be
a thing, but the casual requests for telemetry have been more
interested in pcaps. Someone (I failed to make a note of who, drat)
mentioned that there is a `dnstap` fanout/filter tool, which was on my
todo list in case we ever needed to provide multiple feeds containing
different data.

I spoke to Robert Edmonds (the `dnstap` developer, who is now at
Fastly) who thinks in retrospect that protobufs was an unfortunate
choice. I wonder if it would be a good idea to re-do `dnstap` using
uncompressed C-DNS for framing, but I didn't manage to talk to Jim
about this before he had to leave.

DNS Flag day
------------

A couple of talks on what will happen next year after the open source
DNS resolvers remove their workaround code for broken authoritative
servers. Lots of people collaborating on this including Sebastián
Castro (.nz), Hugo Salgado (.cl), Petr Špaček (.cz).

Their analysis is rapidly becoming more informative and actionable,
which is great. They have a fairly short list of mass hosting
providers that will be responsible for the vast majority of the potential
breakage, if they aren't fixed in time.

Smaller notes
-------------

Giovane Moura (SIDN) - DNS Defenses During DDoS

  * also to appear at SIGCOMM

  * headline number on effectiveness of DNS caches: 70% hit rate

  * query amplification during an outage can be 8x - unbound has
    mitigation for this which I should have a look at.

Duane Wessels (Verisign) - zone digests

  * really good slide on channel vs data security

  * he surprised me by saying there is no validation for zone transfer
    SOA queries - I feel I need to look at the code but I can imagine
    why it works that way...

  * zone digests potentially great for safer stealth secondaries which
    we have a lot of in Cambridge

  * Petr Spacek complained about the implementation complexity ...
	I wonder if there's a cunning qp hack to make it easier :-)

Peter van Dijk (PowerDNS) - NSEC aggressive use and TTLs

  * there are now three instead of two TTLs that affect negative
    cacheing: SOA TTL, SOA MINIMUM, plus now NSEC TTL.

  * new operational advice: be careful to make NSEC TTL and SOA
    negative TTLs match!
