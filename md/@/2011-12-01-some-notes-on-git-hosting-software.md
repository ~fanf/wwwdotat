---
format: html
lj:
  anum: 165
  can_comment: 1
  ditemid: 117157
  event_timestamp: 1322768580
  eventtime: "2011-12-01T19:43:00Z"
  itemid: 457
  logtime: "2011-12-01T19:44:07Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 12
  url: "https://fanf.livejournal.com/117157.html"
title: Some notes on git hosting software
...

<p>My colleague <a href="http://bjh21.me.uk">Ben Harris</a>
has been working on a configuration management system,
based on my
<a href="http://fanf.livejournal.com/98862.html">git-deploy</a>
idea with added cryptographic security. Ben's "starling" system will
ensure that servers will only deploy configurations that have been gpg
signed by a trusted sysadmin.</p>

<p>We have not yet got a git repository hosting setup, though it is an
obviously required part of the system. So this week I have been
looking at what software is out there. To make it interesting we would
like to go beyond the usual corporate deployment and see if we can do
something useful as a university-wide service. "Something useful" unpacks
into the following shopping list of features, which I am posting here in
case anyone outside the CS has good suggestions.</p>

<dl>
<dt>Basics

<dd>Read/write access via ssh, with per-repository and per-branch access
controls. Browse repositories via the web.

<dt>Delegated access control

<dd>We would like to delegate repository creation and management to groups
(such as the computing service itself, other University departments,
research groups, etc.) and we would like group managers to be able to
delegate repository access control to repository managers.

<dt>Repositories for individuals

<dd>Each user should have an automatically provisioned group of their own
(like github).

<dt>Public and private repositories

<dd>Repository managers should be able to allow anonymous read-only access
via the git protocol and the web repository browser.

<dt>Authenticated browsing

<dd>Users who have read access to a repository should be able to browse it
via the web.

<dt>External collaborators

<dd>Allow repository managers to give access to users without University
accounts.

</dl>

<p>What software can do this for us? Here's a quick review of the candidates that I know of. Any other suggestions are welcome. (I have not included gitosis since it was made obsolete by gitolite.)</p>

<dl>

<dt><a href="https://github.com/">github</a>

<dd>The obvious outsourcing option. Has all the features we want, I think,
though we would have to pay, and they don't advertise prices for the
scale we would need just for the computing service.

<dt><a href="https://github.com/sitaramc/gitolite">gitolite</a>

<dd>A set of perl scripts that just does access control to repositories
via ssh. Management is done by commits to an admin repository. This
model implies a petty bureaucracy of people who have commit access to
the admin repository. For delegated management we would need a
gitolite install per group. It doesn't support delegating access
control decisions per repository. Individual setups are probably not
feasible - point them at github instead? Web access is anonymous-only.
External collaborators are easy since the repoman has complete control
over which ssh keys have what access.

<dt><a href="http://code.google.com/p/gerrit/">gerrit</a>

<dd>The web-based code review tool developed for the Android project. As
such its focus is on a feature we don't particularly care about. A big
Java program with its own ssh and git implementations. It allows
access control delegation per repository, but it does not allow
delegation of repository creation. It supports web access controls and
has hooks for web single sign-on, by default using OpenID but the
"siteminder" support can probably be used with <a href="http://raven.cam.ac.uk/">Raven</a>.

<dt><a href="https://gitorious.org/gitorious">gitorious</a>

<dd>Affero-GPL source for a github competitor. Big Ruby-on-Rails app.
Designed to allow users to do their own access control and set up
their own groups. Big downside is lack of support for private
repositories (but see <a href="https://gitorious.org/gitorious/mainline/merge_requests/115">this
merge request</a>). Bonus wiki feature.

</dl>

<p>I think the choice is between gitolite and gitorious. Gitolite has the
advantage of simplicity at the cost of several desirable features.
Gitorious would require us to maintain a fork.</p>
