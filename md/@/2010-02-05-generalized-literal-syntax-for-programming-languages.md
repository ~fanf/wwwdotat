---
format: html
lj:
  anum: 151
  can_comment: 1
  ditemid: 105879
  event_timestamp: 1265400780
  eventtime: "2010-02-05T20:13:00Z"
  itemid: 413
  logtime: "2010-02-05T20:13:42Z"
  props:
    personifi_tags: "17:10,8:52,31:0,33:1,10:1,23:8,32:0,1:52,3:11,4:10,20:3,9:14,21:0,nterms:yes"
  reply_count: 14
  url: "https://fanf.livejournal.com/105879.html"
title: Generalized literal syntax for programming languages
...

<p>For a while I have had a fantasy programming language syntax idea that I have been failing to write up. This week I found out that it has already been implemented twice, which is cheering news :-)</p>

<p>The main inspiration is the special support for regular expression literals in languages such as Perl and Javascript. It's annoying that regexes are privileged with special syntax, but library writers can't define domain-specific syntax for their own purposes.</p>

<p>In fact Perl has <a href="http://perldoc.perl.org/perlop.html#Quote-and-Quote-like-Operators">several special literal syntaxes</a>: single-quotes and q{} for verbatim strings; double quotes and qq{} for backslash-escaped interpolated strings; backticks and qx{} for shell commands; qw{} for word lists; slashes, m{}, s{}{}, and qr{} for regular expressions; tr{}{} for character substitution; and << for "here" documents.</p>

<p><a href="http://digitalmars.com/d/">The D programming language</a> also has some extra flavourful <a href="http://www.digitalmars.com/d/1.0/lex.html#StringLiteral">string literals</a>: r"" or `` for verbatim strings; "" for backslash-escaped strings; x"" for hex-encoded data; and bare backslash escape sequences.</p>

<p>What I'd like to be able to do is define a library for handling my special syntax. It would work as a plugin for the compiler that would parse and check my special literals at compile time (no run-time syntax errors!) and emit code that implements their special semantics. This framework means that, instead of being built into the language, libraries can provide features like converting backslash escape sequences into control characters, turning interpolated strings into a series of concatenations, arranging for regular expressions to be compiled once, and so forth.</p>

<p>You could then provide support for (say) XML literals, <a href="http://www.w3.org/TR/xpath/">XPath</a> expressions, <a href="http://pdos.csail.mit.edu/~baford/packrat/popl04/">better pattern matching syntax</a>, or whatever else you might fancy.</p>

<p>One possibility that is very enticing is to make string interpolation context-aware, so that interpolated strings can be automatically escaped properly. The mixture of languages and syntaxes in a web page makes this fiendishly complicated. Different SQL engines have different escaping requirements. If this kind of hard-won knowledge can be implemented once in a library, security vulnerabilities such as cross-site scripting and SQL injection would be easier to avoid. The <a href="http://google-caja.googlecode.com">Caja</a> project includes <a href="http://google-caja.googlecode.com/svn/changes/mikesamuel/string-interpolation-29-Jan-2008/trunk/src/js/com/google/caja/interp/index.html">a proposal for secure string interpolation in Javascript</a> which explains these issues very well.</p>

<p>The syntax I had in mind is inspired by Perl (and D's letter prefixes are along similar lines), for example, <tt>re$/^ *#/</tt> is a regular expression for matching comment lines. A literal starts off with an identifier that specifies the literal's compiler. These identifiers have their own namespace so they can be very terse without clashing with variable names. The identifier is followed by a <tt>$</tt> which indicates this is a string. The contents of the literal are delimited in the same way that Perl's generic literals, either with nesting {} () &lt;&gt; [] brackets or with matching punctuation. There's no way to escape delimiters within the literal, so that the literal can be passed unmodified to its compiler. For longer literals, or if single character delimiters are awkward, you can use <tt>$$</tt> instead of <tt>$</tt> and the rest of the line forms the delimiter, a bit like a here document. A literal compiler may or may not support interpolation, and defines its own syntax for doing so.</p>

<p>This week I was pleased to find out that the Glasgow Haskell Compiler supports my generalized literal idea. They call it "<a href="http://haskell.org/haskellwiki/Quasiquotation">quasiquoting</a>" after Lisp. The syntax it supports looks like <tt>[$re|^ *#|]</tt> though this is likely to change to <tt>[re|^ *#|]</tt>. The brackets are based on <a href="http://www.haskell.org/haskellwiki/Template_Haskell">Template Haskell</a>'s <tt>[| |]</tt> quotation brackets. The <a href="http://www.eecs.harvard.edu/~mainland/ghc-quasiquoting/mainland07quasiquoting.pdf">quasiquoting paper</a> has lots of good rationale for the feature and great examples of how easy Haskell makes it to write certain kinds of literal compilers. It also allows literal compilers to define their own interpolation syntax.</p>

<p><a href="http://erights.org/">The E programming language</a> has a feature called "<a href="http://www.erights.org/elang/grammar/quasi-overview.html">quasi-literals</a>" which look like <tt>rx`^ *#`</tt> with back-ticks for delimiters. The <a href="http://google-caja.googlecode.com/svn/changes/mikesamuel/string-interpolation-29-Jan-2008/trunk/src/js/com/google/caja/interp/index.html#-autogen-id-8">secure string interpolation</a> document criticizes them for being executed at run-time, not compile time, so perhaps they aren't quite what I have in mind; also I hope they are wrong to say that the feature can only compile quasi-literals to parse trees. The E documentation is sparse so it's hard to tell.</p>

<p>I should also mention Lisp here, since it's the king of metaprogramming. I expect you could implement a feature like this using Common Lisp reader macros...</p>

<p>Do any other languages have a feature like this?</p>
