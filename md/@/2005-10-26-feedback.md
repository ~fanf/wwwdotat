---
dw:
  anum: 102
  eventtime: "2005-10-26T14:33:00Z"
  itemid: 160
  logtime: "2005-10-26T16:03:00Z"
  props:
    commentalter: 1491292325
    import_source: livejournal.com/fanf/41218
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/41062.html"
format: casual
lj:
  anum: 2
  can_comment: 1
  ditemid: 41218
  event_timestamp: 1130337180
  eventtime: "2005-10-26T14:33:00Z"
  itemid: 161
  logtime: "2005-10-26T16:03:00Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/41218.html"
title: Feedback
...

Thanks for everyone's comments on <a href="http://www.livejournal.com/users/fanf/40706.html?style=mine">yesterday's post</a>. Since there's a fair amount of overlap I thought I'd write a consolidated reply here.

Regarding the target audience, <a href="https://claerwen.livejournal.com/">👤claerwen</a> is right that it is aimed at Computer Officers and other people involved in computer support, plus knowledgable users or those with unusual requirements who need to know. Obviously most users won't have the time or the inclination to familiarize themselves with all our documentation - see the profusion of links under http://www.cam.ac.uk/cs/email - so we rely on the techlinks to do that for them :-)

<a href="https://stephdiary.livejournal.com/">👤stephdiary</a>: Yes, they will know about the CUDN.
<a href="https://hsenag.livejournal.com/">👤hsenag</a>: We already have more friendly documentation for our average end-users.

Regarding the various pedant points, I'm being careful (too careful?) in my use of language but the distinctions I am making are often not very important to the reader. So although I make a distinction between message submission servers and smart hosts, for most purposes they can be treated as synonyms for "outgoing SMTP server".

<a href="https://mouse262.livejournal.com/">👤mouse262</a>: Good point about Google Mail (though they seem to think that my browser isn't in the UK). I've avoided the problem by using the example of a visitor from Oxford who uses Herald, instead of advertising a competitor :-)
<a href="https://bjh21.livejournal.com/">👤bjh21</a>: Yes, that sentence is a mess, and I probably shouldn't try to editorialize in that way. I have neutralized it.
(<em>Anonymous</em>): I think your suggestion of a summary page would mostly be addressed by a table of contents when it gets HTMLified. The details of configuration for Hermes users isn't really in scope for this document (because it's covered elsewhere), but you are probably right that front-loading the document with waffle isn't helpful so I have re-ordered it.
The actual limit is currently more than 60 but I used that figure because it's a good point to start worrying about the amount of traffic you are generating. At least Unix boxes will generally get SMTP retries right.
