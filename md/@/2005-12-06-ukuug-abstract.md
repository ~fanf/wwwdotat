---
dw:
  anum: 173
  eventtime: "2005-12-06T11:07:00Z"
  itemid: 166
  logtime: "2005-12-06T11:15:36Z"
  props:
    import_source: livejournal.com/fanf/42893
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/42669.html"
format: casual
lj:
  anum: 141
  can_comment: 1
  ditemid: 42893
  event_timestamp: 1133867220
  eventtime: "2005-12-06T11:07:00Z"
  itemid: 167
  logtime: "2005-12-06T11:15:36Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/42893.html"
title: UKUUG abstract
...

The UKUUG is holding its <strike>Winter</strike> <a href="http://www.ukuug.org/events/spring2006/">Spring Conference</a> in Durham in March, and the Call for Participation closes in a couple of weeks. I've been preparing an abstract for a paper and talk about <a href="http://www.livejournal.com/users/fanf/36373.html">the ratelimit feature</a> <a href="http://www.livejournal.com/users/fanf/37052.html">I implemented for Exim</a>, and our experiences of deploying it in Cambridge.

Any comments on the following?
<blockquote><p>Cambridge University has pretty good email security, but even so we have a couple of incidents each year when a security breach results in a flood of spam from our network. In order to protect against this in the future, we needed a system for throttling these floods before they cause damage, such as Cambridge being blacklisted by AOL.</p><p>I implemented a general-purpose rate-limiting facility for Exim 4.52. It is extremely flexible and allows you to specify almost any policy you want. It can measure the rate of messages, recipients, SMTP commands, or bytes of data from a particular sender; and senders can be identified by IP address, authenticated username, or almost anything else.</p><p>I deployed this facility on the central email systems in Cambridge. It ran in logging-only mode for several weeks while I tuned the policy to mimimize the disruption to legitimate email. This exposed the slightly surprising extent of bulk email usage in the University, and a number of particularly problematic cases. An important task was to communicate the change in policy to less technical users.</p><p>I will describe Exim's ratelimit facility and report on our deployment experiences.</p></blockquote>
