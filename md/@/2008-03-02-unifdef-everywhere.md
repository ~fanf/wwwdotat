---
dw:
  anum: 202
  eventtime: "2008-03-02T22:07:00Z"
  itemid: 326
  logtime: "2008-03-02T23:08:43Z"
  props:
    commentalter: 1491292351
    import_source: livejournal.com/fanf/84193
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/83658.html"
format: casual
lj:
  anum: 225
  can_comment: 1
  ditemid: 84193
  event_timestamp: 1204495620
  eventtime: "2008-03-02T22:07:00Z"
  itemid: 328
  logtime: "2008-03-02T23:08:43Z"
  props:
    verticals_list: technology
  reply_count: 1
  url: "https://fanf.livejournal.com/84193.html"
title: unifdef everywhere
...

My version of unifdef is in all the BSDs, including Mac OS X (yay!). I was disappointed that it isn't in Tiger, but <a href="http://www.opensource.apple.com/darwinsource/10.5.2/developer_cmds-48/unifdef/">it is in Leopard</a>, though they still ship the old man page (FAIL). It also lurks in <a href="http://git.kernel.org/?p=linux/kernel/git/stable/linux-2.6.24.y.git;a=blob;f=scripts/unifdef.c">the Linux-2.6 build system</a>, though without the man page, and patched because glibc is too lame to include strlcpy(). Solaris needs to get a grip since it still ships with <a href="http://cvs.opensolaris.org/source/xref/onnv/onnv-gate/usr/src/cmd/sgs/unifdef/common/unifdef.c">the old pre-ANSI version</a> by Dave Yost. Based on <a href="http://publib.boulder.ibm.com/infocenter/systems/index.jsp?topic=/com.ibm.aix.cmds/doc/aixcmds5/unifdef.htm">its man page</a>, AIX is in the same situation. The <a href="http://docs.hp.com/en/B9106-90007/unifdef.1.html">HP-UX man page</a> has been rewritten but it too looks like the Yost version (which was BSD licenced, not public domain!). Irix is unusual, since it doesn't use the old Berkeley code but has <a href="http://techpubs.sgi.com/library/tpl/cgi-bin/getdoc.cgi?db=man&fname=/usr/share/catman/u_man/cat1/unifdef.z">a version written by Brendan Eich</a> (also known as the creator of JavaScript). It looks like I could steal some features from his version.
