---
format: html
lj:
  anum: 123
  can_comment: 1
  ditemid: 127355
  event_timestamp: 1370182920
  eventtime: "2013-06-02T14:22:00Z"
  itemid: 497
  logtime: "2013-06-02T13:23:00Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/127355.html"
title: Guilty pleasures
...

<p><b>Corned beef / bully beef</b>

<p>Not the American thing we call salt beef (which is excellent and one of my non-guilty pleasures) but the stuff that comes in cans from Fray Bentos in South America. Cheap meat and many food miles, but yummy salty fatty.

<p>I tend to have enthusiasms for a particular food, and will eat a lot of it for a few weeks until I tire of it and move on to something else. Recently i have been making sort of ersatz Reuben sandwiches, with bully beef, emmental, sauerkraut, and mustard, often toasted gently in a frying pan to melt the filling.

<p><b>Heinz Sandwich Spread</b>

<p>Finely diced vegetables in salad cream. Sweet and sour and crunchy. A good alternative to a pickle like Branston, especially before they did a sandwich version.

<p>Salad cream is another of those dubious cost-reduced foods, like mayo but with less egg and oil, more vinegar, and added water and sugar. A stronger sweeter and more vinegary flavour.
