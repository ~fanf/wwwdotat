---
dw:
  anum: 234
  eventtime: "2005-12-12T19:02:00Z"
  itemid: 170
  logtime: "2005-12-12T19:02:30Z"
  props:
    import_source: livejournal.com/fanf/43969
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/43754.html"
format: casual
lj:
  anum: 193
  can_comment: 1
  ditemid: 43969
  event_timestamp: 1134414120
  eventtime: "2005-12-12T19:02:00Z"
  itemid: 171
  logtime: "2005-12-12T19:02:30Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/43969.html"
title: Jingle approaches JEP
...

A couple of announcements have recently been posted to the standards-JIG mailing list about the Jingle specifications. This is noteworthy because Jingle is the XMPP extension for peer-to-peer session initiation, including Voice Over IP, and it is essentially the protocol used by <a href="http://www.google.com/talk/">Google Talk</a>. It will be interesting to see non-Google clients implement the protocol, and how soon gateways between <a href="http://en.wikipedia.org/wiki/Session_Initiation_Protocol">SIP</a>, <a href="http://en.wikipedia.org/wiki/H.323">H.323</a>, and Jingle get implemented.

http://mail.jabber.org/pipermail/standards-jig/2005-December/009315.html
http://mail.jabber.org/pipermail/standards-jig/2005-December/009316.html

JEP = "Jabber enhancement proposal"
JIG = "Jabber interest group"
SIP = the "session initiation protocol", the IETF's VOIP standard
H.323 = the ITU's VOIP standard
VOIP = voice over IP
XMPP = the standard core of Jabber
