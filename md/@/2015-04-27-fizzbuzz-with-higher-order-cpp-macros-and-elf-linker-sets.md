---
dw:
  anum: 168
  eventtime: "2015-04-27T17:03:00Z"
  itemid: 420
  logtime: "2015-04-27T16:03:22Z"
  props:
    commentalter: 1491292409
    import_source: livejournal.com/fanf/134900
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/107688.html"
format: html
lj:
  anum: 244
  can_comment: 1
  ditemid: 134900
  event_timestamp: 1430154180
  eventtime: "2015-04-27T17:03:00Z"
  itemid: 526
  logtime: "2015-04-27T16:03:22Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/134900.html"
title: FizzBuzz with higher-order cpp macros and ELF linker sets
...

<p>Here are a couple of fun ways to reduce repetitive code in C.
To illustrate them, I'll implement FizzBuzz with the constraint
that I must mention Fizz and Buzz only once in each implementation.

<p>The generic context is that I want to declare some functions, and
each function has an object containing some metadata. In this case the
function is a predicate like &quot;divisible by three&quot; and the
metadata is &quot;Fizz&quot;. In a more realistic situation the
function might be a driver entry point and the metadata might be a
description of the hardware it supports.

<p>Both implementations of FizzBuzz fit into the following generic
FizzBuzz framework, which knows the general form of the game but not
the specific rules about when to utter silly words instead of numbers.

<pre>
    #include &lt;stdbool.h>
    #include &lt;stdio.h>
    #include &lt;err.h>
    
    // predicate metadata
    typedef struct pred {
        bool (*pred)(int);
        const char *name;
    } pred;
    
    // some other file declares a table of predicates
    #include PTBL
    
    static bool putsafe(const char *s) {
        return s != NULL && fputs(s, stdout) != EOF;
    }
    
    int main(void) {
        for (int i = 0; true; i++) {
            bool done = false;
            // if a predicate is true, print its name
            for (pred *p = ptbl; p &lt; ptbl_end; p++)
                done |= putsafe(p->pred(i) ? p->name : NULL);
            // if no predicate was true, print the number
            if (printf(done ? "\n" : "%d\n", i) &lt; 0)
                err(1, "printf");
        }
    }
</pre>

<p>To compile this code you need to define the PTBL macro to the name
of a file that implements a FizzBuzz predicate table.

<h2>Higher-order cpp macros</h2>

<p>A higher-order macro is a macro which takes a macro as an argument.
This can be useful if you want the macro to do different things each
time it is invoked.

<p>For FizzBuzz we use a higher-order macro to tersely define all the
predicates in one place. What this macro actually does depends on the
macro name <i>p</i> that we pass to it.

<pre>
    #define predicates(p) \
        p(Fizz, i % 3 == 0) \
        p(Buzz, i % 5 == 0)
</pre>

<p>We can then define a function-defining macro, and pass it to our
higher-order macro to define all the predicate functions.

<pre>
    #define pred_fun(name, test) \
        static bool name(int i) { return test; }
    
    predicates(pred_fun)
</pre>

<p>And we can define a macro to declare a metadata table entry (using
the C preprocessor stringification operator), and pass it to our
higher-order macro to fill in the whole metadata table.

<pre>
    #define pred_ent(name, test) { name, #name },
    
    pred ptbl[] = {
        predicates(pred_ent)
    };
</pre>

<p>For the purposes of the main program we also need to declare the
end of the table.

<pre>
    #define ptbl_end (ptbl + sizeof(ptbl)/sizeof(*ptbl))
</pre>

<p>Higher-order macros can get unweildy, especially if each item in
the list is large. An alternative is to use a higher-order include
file, where instead of passing a macro to another macro,
you <tt>#define</tt> a macro with a particular name, <tt>#include</tt>
a file of macro invocations, then <tt>#undef</tt> the special macro.
This saves you from having to end dozens of lines with backslash
continuations.
<h2>ELF linker sets</h2>

<p>The linker takes collections of definitions, separates them into
different sections (e.g. code and data), and concatenates each section
into a contiguous block of memory. The effect is that although you can
interleave code and data in your C source file, the linker
disentangles the little pieces into one code section and one data
section.

<p>You can also define your own sections, if you like, by using gcc
declaration attributes, so the linker will gather the declarations
together in your binary regardless of how spread out they were in the
source. The FreeBSD kernel calls these
&quot;<a href="https://svnweb.freebsd.org/base/head/sys/sys/linker_set.h?view=markup">linker
sets</a>&quot; and uses them extensively to construct lists of
drivers, initialization actions, etc.

<p>This allows us to declare the metadata for a FizzBuzz predicate
alongside its function definition, and use the linker to gather all
the metadata into the array expected by the main program. The key
part of the macro below is the <tt>__attribute__((section("pred")))</tt>.

<pre>
    #define predicate(name, test) \
        static bool name(int i) { return test; } \
        pred pred_##name __attribute__((section("pred"))) \
            = { name, #name }
</pre>

<p>With that convenience macro we can define our predicates in
whatever order or in whatever files we want.

<pre>
    predicate(Fizz, i % 3 == 0);
    predicate(Buzz, i % 5 == 0);
</pre>

<p>To access the metadata, the linker helpfully defines some symbols
identifying the start and end of the section, which we can pass to our
main program.

<pre>
    extern pred __start_pred[], __stop_pred[];
    
    #define ptbl    __start_pred
    #define ptbl_end __stop_pred
</pre>

<h2>Source code</h2>

<p><tt>git clone <a href="https://github.com/fanf2/dry-fizz-buzz">https://github.com/fanf2/dry-fizz-buzz</a></tt>
