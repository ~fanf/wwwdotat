---
dw:
  anum: 174
  eventtime: "2007-08-22T17:15:00Z"
  itemid: 299
  logtime: "2007-08-22T17:32:45Z"
  props:
    commentalter: 1491292344
    import_source: livejournal.com/fanf/76839
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/76718.html"
format: html
lj:
  anum: 39
  can_comment: 1
  ditemid: 76839
  event_timestamp: 1187802900
  eventtime: "2007-08-22T17:15:00Z"
  itemid: 300
  logtime: "2007-08-22T17:32:45Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/76839.html"
title: A song of the weather
...

<p><i>August, dark and dank and wet, brings more rain than any yet!</i></p>

<p>Our flat roof is having water permeability problems <a href="https://fanf2.user.srcf.net/hermes/doc/misc/shed/">again</a>. This time it has leaked onto the fire suppression system control panel and broken it, meaning that in the event of a fire someone will have to go downstairs to the room full of gas cylinders to welease <strike>Woger</strike> the suppwession gas. Should be fixed tomorrow...</p>

<p>Also, the delightful old computer lab tower (metal stairs and ugly <a href="http://www.bellglass.co.uk/images/Pat_Glass_02.jpg">wired glass</a> cladding) is leaking copiously, and giving anyone who goes to the Titan rooms a light shower.</p>
