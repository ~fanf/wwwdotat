---
format: html
lj:
  anum: 182
  can_comment: 1
  ditemid: 102582
  event_timestamp: 1251905460
  eventtime: "2009-09-02T15:31:00Z"
  itemid: 400
  logtime: "2009-09-02T16:10:46Z"
  props:
    personifi_tags: "40:5,2:2,17:5,8:57,23:5,16:5,1:51,3:2,19:2,9:34,21:2,nterms:no"
  reply_count: 3
  url: "https://fanf.livejournal.com/102582.html"
title: Firewall fallout / Exchange oddity
...

<p>This morning my colleagues turned off <a href="http://fanf.livejournal.com/102206.html">their Cisco PIX SMTP fuxup mode</a> and most of the failing email finally made it through. But not all. So I broke out <tt>tcpdump</tt> again.</p>

<p>BTW, a handy way to split a tcpdump output file called <tt>dump</tt> into separate TCP connections is something like the following. This assumes that one of the endpoints of each connection is always the same port on the same server IP address.</p>

<pre>
    tcpdump -vvv -r dump |
    sed '/.* clientname[.]\([0-9]*\) .*/!d;s//\1/' |
    sort -u |
    while read p; do tcpdump -r dump -w dump.$p port $p; done
</pre>

<p>I looked at the connections which ended with a timeout during the data transmission phase. It turned out that just two messages were still having problems. And what strange beasts they were. The following is a paraphrase of the key features of the messages.</p>

<pre>
MAIL FROM:<MAILSERVER@dept.cam.ac.uk> SIZE=12345
250 OK
RCPT TO:<exchange@dept.cam.ac.uk>
250 Accepted
DATA
354 Enter message, ending with "." on a line by itself
X-MimeOLE: Produced By Microsoft Exchange V6.5
Content-class: urn:content-classes:message
MIME-Version: 1.0
Content-Type: application/ms-tnef;
        name="winmail.dat"
Content-Transfer-Encoding: binary
Subject: Service Unavailable
Date: Wed, 2 Sep 2009 12:34:56 +0100
Message-ID: <i6WnZXXs3ctmf9qb@mailserver.dept.cam.ac.uk>
X-MS-Has-Attach:
X-MS-TNEF-Correlator: <i6WnZXXs3ctmf9qb@mailserver.dept.cam.ac.uk>
Thread-Topic: Service Unavailable
Thread-Index: GV7mWrV6mdBQLdgGFAfkSCsA
From: "/O=IT Support/OU=Department/cn=Configuration"
      "/cn=Servers/cn=MAILSERVER/cn=Microsoft Public MDB"
      <MAILSERVER@dept.cam.ac.uk>

... loads of binary TNEF guff with lots of UTF-16 ...
</pre>

<p>The X.400 address is funny, but what was actually causing the problem was the attempt to send a binary message. Our servers don't support the BINARYMIME extension, so this is verboten. The reason it caused a timeout is that (being binary rather than lines of text) the message didn't end with a CRLF newline sequence, so the <tt>DATA</tt> dot-CRLF terminator did not appear at the start of a line, so our server thought it was part of the data and continued waiting for more.</p>

<p>Bizarre. Why is the message being sent to my servers when it should have been delivered internally? (Its recipient is a departmental address.) Why isn't Exchange downgrading the binary content as required by the specification? Alternatively, why is it using DATA to send a binary message when you can only do that using the BDAT command?</p>

<p>Very strange.</p>
