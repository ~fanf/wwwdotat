---
dw:
  anum: 204
  eventtime: "2015-07-02T16:19:00Z"
  itemid: 422
  logtime: "2015-07-02T15:19:01Z"
  props:
    commentalter: 1491292409
    import_source: livejournal.com/fanf/135257
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/108236.html"
format: html
lj:
  anum: 89
  can_comment: 1
  ditemid: 135257
  event_timestamp: 1435853940
  eventtime: "2015-07-02T16:19:00Z"
  itemid: 528
  logtime: "2015-07-02T15:19:01Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/135257.html"
title: "nsnotifyd-1.1: prompt DNS zone transfers for stealth secondaries"
...

<p>nsnotifyd is my tiny DNS server that only handles DNS NOTIFY messages by running a command. (See <a href="http://fanf.livejournal.com/134988.html">my announcement of nsnotifyd</a> and <a href="http://dotat.at/prog/nsnotifyd/">the nsnotifyd home page</a>.)

<p>At Cambridge we have a lot of stealth secondary name servers. We encourage admins who run resolvers to configure them in this way in order to resolve names in our private zones; it also reduces load on our central resolvers which used to be important. This is documented in our <a href="https://jackdaw.cam.ac.uk/ipreg/nsconfig/">sample configuration for stealth nameservers on the CUDN</a>.

<p>The problem with this is that a stealth secondary can be slow to update its copy of a zone. It doesn't receive NOTIFY messages (because it is stealth) so it has to rely on the zone's SOA refresh and retry timing parameters. I have mitigated this somewhat by reducing our refresh timer from 4 hours to 30 minutes, but it might be nice to do better.

<p>A similar problem came up in another scenario recently. I had a brief exchange with someone at JANET about DNS block lists and <a href="https://dnsrpz.info/">response policy zones</a> in particular. RPZ block lists are distributed by standard zone transfers. If the RPZ users are stealth secondaries then they are not going to get updates in a very timely manner. (They might not be entirely stealth: RPZ vendors maintain ACLs listing their customers which they might also use for sending notifies.) JANET were concerned that if they provided an RPZ mirror it might exacerbate the staleness problem.

<p>So I thought it might be reasonable to:
<ul>
<li>Analyze a BIND log to extract lists of zone transfer clients, which are presumably mostly stealth secondaries.
(A little script called <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/nsnotifyd.git/blob/HEAD:/nsnotify-liststealth">nsnotify-liststealth</a>)
<li>Write a tool called <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/nsnotifyd.git/blob/HEAD:/nsnotify-fanout.c">nsnotify-fanout</a> to send notify messages to a list of targets.
<li>And hook them up to nsnotifyd with a script called <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/nsnotifyd.git/blob/HEAD:/nsnotify2stealth">nsnotify2stealth</a>.
</ul>

<p>The result is that you can just configure your authoritative name server to send NOTIFYs to nsnotifyd, and it will automatically NOTIFY all of your stealth secondaries as soon as the zone changes.

<p>This seems to work pretty well, but there is a caveat!

<p>You will now get a massive thundering herd of zone transfers as soon as a zone changes. Previously your stealth secondaries would have tended to spread their load over the SOA refresh period. Not any more!

<p>The ISC has a helpful page on <a href="https://kb.isc.org/article/AA-00726/0/Tuning-your-BIND-configuration-effectively-for-zone-transfers-particularly-with-many-frequently-updated-zones.html">tuning BIND for high zone transfer volume</a> which you should read if you want to use nsnotify2stealth.
