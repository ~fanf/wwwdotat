---
dw:
  anum: 216
  eventtime: "2006-01-10T19:59:00Z"
  itemid: 177
  logtime: "2006-01-10T20:38:59Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/45732
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/45528.html"
format: casual
lj:
  anum: 164
  can_comment: 1
  ditemid: 45732
  event_timestamp: 1136923140
  eventtime: "2006-01-10T19:59:00Z"
  itemid: 178
  logtime: "2006-01-10T20:38:59Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/45732.html"
title: Attack-resistant trust metrics.
...

Current blacklists have a number of (not entirely orthogonal) problems:

(1) obscure editorial policies
(2) centralized, so vulnerable to attack
(3) not easily extended to identities other than IP addresses
(4) difficult to contribute to

A while back I was thinking about anti-spam blacklists and wondering how one could derive one algorithmically based on input from lots of email servers whilst being resistant to gaming. There is (or used to be - I can't find it any more) a DNSBL which consolidated multiple local blacklists, but AFAIK there isn't anything clever about it, so it's utterly untrustworthy. I imagined that each email server (or server of any other kind) could gossip with its peers and thereby automatically find out what they think of each other. Each server could seed its opinions with data from anti-spam or anti-virus scanners or from protocol fingerprinting heuristics. The algorithm would establish each server's reputation, which would include not only the desirability of communicating with it but also the trustworthiness of its gossip. However I'm not enough of a mathematician to solve this problem.

However, and not particularly surprisingly, this kind of thing has already been investigated albeit in another field. I stumbled across an article by Ralph Levien when searching for opinions about <strike>Jabber</strike>XMPP versus <strike>BXXP</strike>BEEP, which led to some interesting articles from the Chandler development list, one of which included a reference to &lt;http://www.levien.com/free/tmetric-HOWTO.html&gt;. I wonder if this could be applied to my idea.
