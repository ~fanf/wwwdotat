---
dw:
  anum: 26
  eventtime: "2016-02-29T13:18:00Z"
  itemid: 446
  logtime: "2016-02-29T13:18:06Z"
  props:
    import_source: livejournal.com/fanf/141388
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/114202.html"
format: casual
lj:
  anum: 76
  can_comment: 1
  ditemid: 141388
  event_timestamp: 1456751880
  eventtime: "2016-02-29T13:18:00Z"
  itemid: 552
  logtime: "2016-02-29T13:18:06Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/141388.html"
title: Update to my Raspberry Pi vs E450 comparison
...

I have embiggened the table in my previous entry http://fanf.livejournal.com/141066.html to add the new Raspberry Pi 3 to the comparison!
