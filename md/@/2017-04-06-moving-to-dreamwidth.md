---
dw:
  anum: 10
  eventtime: "2017-04-06T10:00:00Z"
  itemid: 479
  logtime: "2017-04-06T09:00:27Z"
  props:
    commentalter: 1491991864
    import_source: livejournal.com/fanf/149889
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/122634.html"
format: casual
lj:
  anum: 129
  can_comment: 1
  ditemid: 149889
  event_timestamp: 1491472800
  eventtime: "2017-04-06T10:00:00Z"
  itemid: 585
  logtime: "2017-04-06T09:00:27Z"
  props:
    give_features: 1
    og_image: "http://l-files.livejournal.net/og_image/936728/585?v=1491469228"
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/149889.html"
title: Moving to Dreamwidth
...

I am in the process of moving this blog to https://fanf.dreamwidth.org/ - follow me there!
