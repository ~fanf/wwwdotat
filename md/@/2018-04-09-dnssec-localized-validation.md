---
dw:
  anum: 91
  eventtime: "2018-04-09T23:15:00Z"
  itemid: 490
  logtime: "2018-04-09T22:16:23Z"
  props:
    commentalter: 1523645202
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/125531.html"
format: md
lj:
  anum: 181
  can_comment: 1
  ditemid: 152757
  event_timestamp: 1523315760
  eventtime: "2018-04-09T23:16:00Z"
  itemid: 596
  logtime: "2018-04-09T22:17:19Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/152757.html"
...

DNSSEC localized validation
===========================

At Cambridge since approximately I don't know about 25 years ago or
more, we have encouraged sysadmins to set up
[stealth secondary DNS servers](https://www.dns.cam.ac.uk/servers/stealth.html).
This has a couple of advantages:

* It distributes the DNS resolution load, so looking up names of
  on-site services is consistently fast.

* It has better failure isolation, so the local DNS still works even
  if the University or the department have connectivity problems.

It has a disadvantage too:

* It is complicated to configure, whereas a forwarding cache has
  nearly the same advantages and is a lot simpler to configure.

* Stealth secondaries don't have any good way to authenticate zone
  transfers - TSIG only provides mutual authentication by prior
  arrangement, and part of being stealthy is there's none of that.

DNSSEC and stealth servers
--------------------------

Disappointingly, DNSSEC does not help with this stealth secondary
setup, and in some ways hurts:

* Zone transfers do not validate DNSSEC signatures, so it doesn't
  provide a replacement for TSIG. You can sort-of implement a lash-up
  ([RFC 7706](https://tools.ietf.org/html/rfc7706#appendix-B.1) has
  examples for the root zone) but if the transfer gets corrupted your
  stealth secondary goes bogus without any attempt at automatic
  recovery.

* Validation requires chasing a chain of trust from the root, which
  requires external connectivity, even when you have a local copy of
  the data you are validating. So you lose much of the robustness.

* You could in theory mitigate this by distributing trust anchors,
  but that's an much bigger configuration maintenance burden.

Work in progress
----------------

We have been living with this unsatisfactory situation for nearly 10
years, but things are at last starting to look promising. Here are a
few technologies in the works that might address these problems.

DNS-over-TLS for zone transfers
-------------------------------

To provide on-the-wire security for zone transfers, we need a
one-sided alternative to TSIG that authenticates the server while
allowing the client to remain anonymous. In theory SIG(0) could do
that, but it has never been widely implemented.

Instead, we have DNS-over-TLS which can do the job admirably.
The server side can be implemented now with
[a simple configuration for a proxy like NGINX](https://gitlab.isc.org/isc-projects/bind9/tree/master/contrib/dnspriv);
the client side needs a little bit more work.

Built-in support for RFC 7706
-----------------------------

Authenticating the server isn't quite enough, since it doesn't provide
end-to-end validation of the contents of the zone. It looks like there
is interest in adding native support for DNSSEC authenticated zone
transfers to the open source DNS servers, so they can support RFC 7706
without the lash-ups and bogosity pitfalls.

I would like to see this support in a generalized form, so it can be
used for any zones, not just the root.

Catalog zones
-------------

To simplify the setup of stealth secondaries, I provide
[a Cambridge catalog zone](https://www.dns.cam.ac.uk/news/2017-09-06-catz.html).
This makes the setup much easier, almost comparable to a forwarding
configuration. If only we could do this for trust anchors as well...

DLV delenda est
---------------

Before the root zone was signed, [isc.org](https://www.isc.org)
created a mechanism called "DNSSEC lookaside validation", which
allowed "islands of trust" to publish their trust anchors in a special
`dlv.isc.org` zone, in a way that made it easy for third parties to use
them.

Now that the root is signed and support for DNSSEC is widespread, DLV
has been decommissioned. But if we tweak it a bit, maybe it will gain
a new lease of life...?

DLV TNG
-------

DLV acted as a fallback, to be used when the normal chain of trust
from the root was incomplete. I would like to be able to set up my own
local DLV, to be used as a replacement for the normal chain of trust,
not a fallback. The advantages would be:

* When we have connectivity problems, DNSSEC validators can still work
  for local names because they will not need to chase a validation
  chain off site.

* I can distribute just one trust anchor, covering all our zones,
  including disconnected ones such as reverse DNS for RFC 1918
  addresses and IPv6 unique local address prefixes.

* We get tinfoil-hat safety: localized DNSSEC validation for on-site
  services can't be compromised by attacks from those in control of
  keys nearer the root.

* Even better if my DLV could be used as a stealth secondary zone
  obtained via our catalog zone.

DLV on the edge
---------------

That sounds nice for recursive DNS servers, but for DNSSEC to be
really successful we need validation on end-user devices. And that
undermines the robustifications I just listed.

But if your validating stub resolver supports localized DLV, and it
has been configured by a group policy or similar configuration
management system (like those corporate TLS trust anchors some
enterprises have) then you have won those advantages back.

Summary
-------

I want:

* Support for DNS zone transfers over TLS

* Validation of zone contents after transfer, and automatic retransfer
  to recover from corrupted zones

* A localized DLV to act as an enterprise trust anchor distribution
  mechanism

I mentioned this last feature to Evan Hunt at the IETF 101 London
meeting. I feared he would think it is too horrible to contemplate,
but in fact he thought the use case is quite reasonable. So I have
written this down so I can give these ideas a wider airing.
