---
dw:
  anum: 124
  eventtime: "2017-03-27T17:25:00Z"
  itemid: 477
  logtime: "2017-03-27T16:25:31Z"
  props:
    commentalter: 1491292431
    hasscreened: 1
    import_source: livejournal.com/fanf/149436
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/122236.html"
format: html
lj:
  anum: 188
  can_comment: 1
  ditemid: 149436
  event_timestamp: 1490635500
  eventtime: "2017-03-27T17:25:00Z"
  itemid: 583
  logtime: "2017-03-27T16:25:31Z"
  props:
    give_features: 1
    og_image: "http://l-files.livejournal.net/og_image/936728/583?v=1490631931"
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/149436.html"
title: Better terminology
...

<p>Because I just needed to remind myself of this, here are a couple of links with suggestions foe less accidentally-racist computing terminology:

<p>Firstly, <a href="https://twitter.com/kerrizor">Kerri Miller</a> suggests <a href="https://twitter.com/kerrizor/status/843970538963853312">blocklist / safelist</a> for naming deny / allow data sources.

<p>Less briefly, <a href="https://twitter.com/bryanl">Bryan Liles</a> got lots of suggestions for <a href="https://twitter.com/bryanl/status/822093922549764096">better distributed systems terminology</a>. If you use a wider vocabulary to describe your systems, you can more quickly give your reader a more precise idea of how your systems work, and the relative roles of their parts.
