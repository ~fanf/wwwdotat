---
format: html
lj:
  anum: 57
  can_comment: 1
  ditemid: 103225
  event_timestamp: 1256908140
  eventtime: "2009-10-30T13:09:00Z"
  itemid: 403
  logtime: "2009-10-30T13:13:21Z"
  props:
    personifi_tags: "15:16,8:50,1:33,25:50,26:50"
    verticals_list: technology
  reply_count: 2
  url: "https://fanf.livejournal.com/103225.html"
title: ENHANCEDSTATUSCODES FAIL
...

<p>I sort of expect an SMTP server that claims to support enhanced status codes to, y'know, put enhanced status codes in its responses...</p>
<pre>
220 bill.internal.[redacted] ESMTP Symantec Mail Security
EHLO ppsw-0.csi.cam.ac.uk
250-bill.internal.[redacted] says EHLO to 10.0.64.17:58126
250-ENHANCEDSTATUSCODES
250-PIPELINING
250 8BITMIME
MAIL FROM:<dot@dotat.at>
250 MAIL FROM accepted
RCPT TO:<asldkja@sjkfsajhg>
554 Recipient address rejected: User unknown
RSET
250 RSET OK
QUIT
221 bill.internal.[redacted] closing connection
</pre>
