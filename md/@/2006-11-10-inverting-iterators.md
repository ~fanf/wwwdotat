---
dw:
  anum: 76
  eventtime: "2006-11-10T23:23:00Z"
  itemid: 263
  logtime: "2006-11-10T23:23:01Z"
  props:
    commentalter: 1491292338
    import_source: livejournal.com/fanf/67588
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/67404.html"
format: html
lj:
  anum: 4
  can_comment: 1
  ditemid: 67588
  event_timestamp: 1163200980
  eventtime: "2006-11-10T23:23:00Z"
  itemid: 264
  logtime: "2006-11-10T23:23:01Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 9
  url: "https://fanf.livejournal.com/67588.html"
title: Inverting iterators
...

<p>Iterators are used in some programming languages to generalise the <b>for</b> loop. They were introduced by CLU in the 1970s and have been adopted by Python and Lua, amongst others. They are traditionally a special kind of function that has <b>yield</b> statements that return successive values in the iteration. This function is called at the start of the loop, and when it yields, the loop body is run. Each time round the loop the function is resumed until it yields again.</p>

<p>The iterator's activation frame is typically on top of the stack. This seems a bit backwards to me, because it leads to contortions to maintain the iterator's state between yields, e.g. using an auxiliary object or first-class closures. (<a href="http://www.lua.org/pil/7.1.html">See the first example here.</a>)</p>

<p>I think it's simpler to desugar a for loop like this:</p>

<pre>
    <b>for</b> <i>varlist</i> <b>in</b> <i>iterator</i>(<i>arglist</i>) <b>do</b>
        <i>statements</i>
    <b>loop</b>
</pre>

<p>into</p>

<pre>
    <i>iterator</i>(
        (<i>varlist</i>): <b>do</b>
            <i>statements</i>
        <b>end</b>,
        <i>arglist</i>)
</pre>

<p>That is, you turn the loop body into an anonymous function which is passed to the iterator function. Instead of yielding, the iterator just calls the body function. The iterator can then be written as a normal loop without contortions to keep state between yields. (<a href="http://www.lua.org/pil/7.5.html">Pretty much like this, in fact.</a>)</p>

<p>What makes this slightly interesting is how to desugar <b>break</b>, <b>continue</b>, and <b>return</b> statements inside the loop. A <b>continue</b> becomes just a <b>return</b> from the anonymous function, but the others have to escape from the iterator function as well as the anonymous function. This could be done with exceptions or continuation passing.</p>
