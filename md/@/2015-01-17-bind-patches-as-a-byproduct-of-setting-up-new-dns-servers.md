---
dw:
  anum: 59
  eventtime: "2015-01-17T16:12:00Z"
  itemid: 415
  logtime: "2015-01-17T16:11:37Z"
  props:
    import_source: livejournal.com/fanf/133623
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/106299.html"
format: html
lj:
  anum: 247
  can_comment: 1
  ditemid: 133623
  event_timestamp: 1421511120
  eventtime: "2015-01-17T16:12:00Z"
  itemid: 521
  logtime: "2015-01-17T16:11:37Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/133623.html"
title: BIND patches as a byproduct of setting up new DNS servers
...

<p>On Friday evening I reached a BIG milestone in my project to replace
Cambridge University's DNS servers. I finished porting and rewriting
the dynamic name server configuration and zone data update scripts,
and I was - at last! - able to get the new servers up to pretty much
full functionality, pulling lists of zones and their contents from <a
href="http://jackdaw.cam.ac.uk/ipreg/">the IP Register database</a>
and the <a
href="http://www.ucs.cam.ac.uk/managed-zone-service">managed zone
service</a>, and with DNSSEC signing on the new hidden master.</p>

<p>There is still some final cleanup and robustifying to do, and checks
to make sure I haven't missed anything. And I have to work out the
exact process I will follow to put the new system into live service
with minimum risk and disruption. But the end is tantalizingly within
reach!</p>

<p>In the last couple of weeks I have also got several small patches into
BIND.</p>

<ul>
<li>Jan 7: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=47ba2677db0e7830">
documentation for <tt>named -L</tt>
</a>

<p>This was a follow-up to a patch I submitted in April last year.
<a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=44613d4d868ed5e7">
The <tt>named -L</tt> option</a> specifies a log file to use at
startup for recording the BIND version banners and other startup
information. Previously this information would always go to syslog
regardless of your logging configuration.</p>

<p>This feature will be in BIND 9.11.</p>

<li>Jan 8: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=69a838727b18f26c">
typo in comment
</a>

<p>Trivial :-)</p>

</li>
<li>Jan 12: <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/nsdiff.git/commitdiff/594c7f8a6a4af527">
teach nsdiff to AXFR from non-standard ports
</a>

<p>Not a BIND patch, but one of my own companion utilities. Our managed
zone service runs a name server on a non-standard port, and our new
setup will use nsdiff | nsupdate to implement bump-in-the-wire signing
for the MZS.</p>

</li>
<li>Jan 13: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=03f979494f5c80e0">
document default DNSKEY TTL
</a>

<p>Took me a while to work out where that value came from.
Submitted on Jan 4. Included in 9.10 ARM.</p>

</li>
<li>Jan 13: <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/bind9.git/commitdiff/58a460bb760e7250">
automatically tune max-journal-size
</a>

<p>Our old DNS build scripts have a couple of mechanisms for tuning
BIND's max-journal-size setting. By default a zone's incremental
update journal will grow without bound, which is not helpful. Having
to set the parameter by hand is annoying, especially since it should
be simple to automatically tune the limit based on the size of the
zone.</p>

<p>Rather than re-implementing some annoying plumbing for yet another
setting, I thought I would try to automate it away. I have submitted
this patch as RT#38324. In response I was told there is also RT#36279
which sounds like a request for this feature, and RT#25274 which
sounds like another implementation of my patch. Based on the ticket
number it dates from 2011.</p>

<p>I hope this gets into 9.11, or something like it. I suppose that
rather than maintaining this patch I could do something equivalent in
my build scripts...</p>

</li>
<li>Jan 14: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=eb4221895fb15aa5">
doc: ignore and clean up isc-notes-html.xsl
</a>

<p>I found some cruft in a supposedly-clean source tree.</p>

<p>This one actually got committed under my name, which I think is a
first for me and BIND :-) (RT#38330)</p>

</li>
<li>Jan 14: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=f91c369b4ac84fad">
close new zone file before renaming, for win32 compatibility
</a>

</li>
<li>Jan 14: <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=b05a50c852608a40">
use a safe temporary new zone file name
</a>

<p>These two arose from <a href="https://lists.isc.org/pipermail/bind-users/2015-January/094384.html">a
problem report on the bind-users list</a>. The conversation moved to
private mail which I find a bit annoying - I tend to think it is more
helpful for other users if problems are fixed in public.</p>

<p>But it turned out that BIND's error logging in this area is basically
negligible, even when you turn on debug logging :-( But the Windows
Process Explorer is able to monitor filesystem events, and it reported
a 'SHARING VIOLATION' and 'NAME NOT FOUND'. This gave me the clue that
it was a POSIX vs Windows portability bug.</p>

<p>So in the end this problem was more interesting than I expected.</p>

</li>
<li>Jan 16: <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/bind9.git/commitdiff/61b6e03c8dd61765">
critical: ratelimiter.c:151: REQUIRE(ev->ev_sender == ((void *)0)) failed
</a>

<p>My build scripts are designed so that Ansible sets up the name servers
with a static configuration which contains everything except for the
<tt>zone {}</tt> clauses. The zone configuration is provisioned by the
dynamic reconfiguration scripts. Ansible runs are triggered manually;
dynamic reconfiguration runs from cron.</p>

<p>I discovered a number of problems with bootstrapping from a bare
server with no zones to a fully-populated server with all the zones
and their contents on the new hidden master.</p>

<p>The process is basically,</p>

<ul>
<li>if there are any missing master files, initialise them as <a href="http://anonscm.debian.org/cgit/users/lamont/bind9.git/tree/debian/db.empty">minimal zone files</a></li>
<li>write zone configuration file and run rndc reconfig</li>
<li>run nsdiff | nsupdate for every zone to fill them with the correct contents</li>
</ul>

<p>When bootstrapping, the master server would load 123 new zones, then
shortly after the nsdiff | nsupdate process started, named crashed
with the assertion failure quoted above.</p>

<p>Mark Andrews replied overnight with the linked patch (he lives in
Australia) which fixed the problem. Yay!</p>

<p>The other bootstrapping problem was to do with BIND's zone
integrity checks. <tt>nsdiff</tt> is not very clever about the order
in which it emits changes; in particular it does <i>not</i> ensure
that hostnames exist before any NS or MX or SRV records are created to
point to them. You can turn off most of the integrity checks, but not
the NS record checks.</p>

<p>This causes trouble for us when bootstrapping the cam.ac.uk zone,
which is the only zone we have with in-zone NS records. It also has
lots of delegations which can also trip the checks.</p>

<p>My solution is to create a special bootstrap version of the zone,
which contains the apex and delegation records (which are built from
configuration stored in git) but not the bulk of the zone contents
from the IP Register database. The zone can then be succesfully loaded
in two stages, first <tt>`nsdiff cam.ac.uk DB.bootstrap | nsupdate -l`</tt>
then <tt>`nsdiff cam.ac.uk zones/cam.ac.uk | nsupdate -l`</tt>.</p>

<p>Bootstrapping isn't something I expect to do very often, but I want to
be sure it is easy to rebuild all the servers from scratch, including
the hidden master, in case of major OS upgrades, VM vs hardware
changes, disasters, etc.</p>

<p>No more special snowflake servers!</p>

</ul>
