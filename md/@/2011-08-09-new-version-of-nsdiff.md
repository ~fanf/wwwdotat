---
format: html
lj:
  anum: 122
  can_comment: 1
  ditemid: 114810
  event_timestamp: 1312897200
  eventtime: "2011-08-09T13:40:00Z"
  itemid: 448
  logtime: "2011-08-09T12:40:02Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/114810.html"
title: New version of nsdiff
...

<p>A few people have shown interest in <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/nsdiff">nsdiff</a> which is more than I expected for a quick hack. In fact, <a href="http://www.terryburton.co.uk/">Terry Burton</a> at the University of Leicester is planning to put it into production! He also reported a bug (it got TTL changes wrong) and said they needed TSIG support so they can control which view a zone is transferred from. So I have fixed those problems and even added documentation! Maybe I should give it a home page...</p>

<p>(<a href="http://fanf.livejournal.com/112749.html">Previously</a>, <a href="http://fanf.livejournal.com/112476.html">previously</a>.)</p>
