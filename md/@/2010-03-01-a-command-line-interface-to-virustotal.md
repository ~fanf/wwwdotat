---
format: html
lj:
  anum: 65
  can_comment: 1
  ditemid: 106049
  event_timestamp: 1267447320
  eventtime: "2010-03-01T12:42:00Z"
  itemid: 414
  logtime: "2010-03-01T12:42:51Z"
  props:
    personifi_tags: "40:4,17:8,8:69,10:4,27:4,36:4,1:65,19:4,9:13,13:4,nterms:yes"
    verticals_list: "computers_and_software,technology"
  reply_count: 0
  url: "https://fanf.livejournal.com/106049.html"
title: A command-line interface to Virustotal
...

<p>The <a href="http://www.virustotal.com/">Virustotal</a> web site is a pretty nice way to scan a file with about 42 different virus scanners. It uses AJAX to dynamically update the results page as the various scanners complete, which can take many seconds. Even better, it feeds sample files to the ClamAV team so they can improve their detection rate.</p>

<p>Recently I have been receiving a lot of trojan spam which ClamAV has not detected. Usually it gets blocked by Spamhaus, but it can bypass that if it gets to me via my postmaster address or via one of my forwarded addresses. I got pretty bored of feeding infected messages to my web browser, especially since it runs on a different computer to my MUA.</p>

<p>So I translated <a href="http://www.virustotal.com/analisisV2.js">the relevant bits of the Virustotal Javscript</a> into Perl so I could feed files to Virustotal from the command line. If you want you can <a href="https://fanf2.user.srcf.net/hermes/src/misc/virustotal.pl">download a copy of the resulting script</a>. There were a couple of amusing things about writing it: The Virustotal Javascript is written in Spanish, but happily it's nice straightforward code so I only needed to use Google Translate a couple of times :-) Also, they chose to send back the AJAX results in a dialect of JSON based on <a href="https://developer.mozilla.org/en/Core_JavaScript_1.5_Guide/Literals#Array_Literals">array literals</a> rather than the more standard <a href="https://developer.mozilla.org/en/Core_JavaScript_1.5_Guide/Literals#Object_Literals">object literals</a>. Perl has exactly the same syntax as Javascript for array literals which made it particularly easy to parse the results :-)</p>
