---
dw:
  anum: 57
  eventtime: "2008-01-27T17:32:00Z"
  itemid: 317
  logtime: "2008-01-27T20:06:20Z"
  props:
    commentalter: 1491292377
    import_source: livejournal.com/fanf/81696
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/81209.html"
format: html
lj:
  anum: 32
  can_comment: 1
  ditemid: 81696
  event_timestamp: 1201455120
  eventtime: "2008-01-27T17:32:00Z"
  itemid: 319
  logtime: "2008-01-27T20:06:20Z"
  props:
    personifi_tags: "nterms:yes"
    verticals_list: technology
  reply_count: 3
  url: "https://fanf.livejournal.com/81696.html"
title: Some notes on Bloom filters
...

<p><a href="http://portal.acm.org/citation.cfm?id=362692">Bloom filters</a> are a simple and clever data structure that every programmer should know about. A Bloom filter represents a set where the universe of possible members is very large and where it doesn't matter if the filter occasionally falsely claims that a non-member is in the set. It is useful for filtering lookups into more expensive (larger) data structures, so that most of the time you can avoid performing lookups that will fail. For example, a hyphenation program on a small computer might have some rules that cover most words, and a dictionary of exceptions; it can represent the entries in the exceptions dictionary as a Bloom filter that fits in RAM, so that it doesn't have to go to disk to find out that its standard rules apply.</p>

<p>The way it works is to represent the set as an array of <i>size</i> bits, which is typically a small multiple of the expected population of the set. To add an item to the set, you hash it with <i>nh</i> independent hash functions, and set the <i>nh</i> bits in the array indexed by the hashes. To check if an item is in the set, you hash it in the same way, and check that all <i>nh</i> bits are set. You get a false positive when hash collisions cause all the bits to be set by accident.</p>

<p>There are a few neat tricks you can perform with Bloom filters. You can take the union of two sets by ORing the two Bloom filters together. You can halve the size of a bloom filter by ORing the top half with the bottom half. You can't delete elements from a Bloom filter, though if false negatives are OK as well as false positives, you can represent the set of deletions as a second Bloom filter. Alternatively, you can use a counting Bloom filter, in which each bit is replaced with a small counter that is incremented for insertions and decremented for deletions. Four bit counters overflow with a probability of <a href="http://portal.acm.org/citation.cfm?id=343572">1.37 &lowast; 10<sup>-15</sup></a>.</p>

<p>The following analysis is mostly based on <a href="http://www.eecs.harvard.edu/~michaelm/postscripts/im2005b.pdf">Andrei Broder and Michael Mitzenmacher's survey</a> of network applications of Bloom filters.</p>

<p>After <i>pop</i> elements have been added to the set, the probability that a particular bit is zero is
<blockquote>
<i>pz&prime;</i> = (1 &minus; 1/<i>size</i>) <sup><i>nh</i> &lowast; <i>pop</i></sup>
</blockquote>
which is approximately
<blockquote>
<i>pz</i> =  exp(&minus;<i>nh</i> &lowast; <i>pop</i> / <i>size</i>)
</blockquote>
The probability of a false positive is
<blockquote><table>
<tr><td><i>fpr</i></td><td>= (1 &minus; <i>pz</i>) <sup><i>nh</i></sup></td></tr>
<tr><td></td><td>= exp(<i>nh</i> &lowast; ln( 1 &minus; <i>pz</i>))</tr>
</table></blockquote>
Given a fixed array size and population, we can work out the number of hash functions that minimizes the false positive rate. Increasing <i>nh</i> makes the exponent larger which reduces <i>fpr</i>, but beyond a certain point the smaller <i>pz</i> starts making <i>fpr</i> larger again. First note that
<blockquote><table>
<tr><td>ln(<i>pz</i>)</td><td>= &minus;<i>nh</i> &lowast; <i>pop</i> / <i>size</i></td></tr>
<tr><td><i>nh</i></td><td>= &minus;ln(<i>pz</i>) &lowast; <i>size</i> / <i>pop</i></td></tr>
</table></blockquote>
which gives us
<blockquote><table>
<tr><td>ln(<i>fpr</i>)</td><td>= <i>nh</i> &lowast; ln(1 &minus; <i>pz</i>)</td></tr>
<tr><td></td><td>= &minus;(<i>size</i> / <i>pop</i>) &lowast; ln(<i>pz</i>) &lowast; ln(1 &minus; <i>pz</i>)</td></tr>
</table></blockquote>
Symmetry reveals that the minimum occurs when <i>pz</i> = 1/2, i.e. half the bits are set, so when the array is full (i.e. at maximum information density),
<blockquote><table>
<tr><td><i>nh</i></td><td>= ln(2) &lowast; <i>size</i> / <i>pop</i></td></tr>
<tr><td><i>size</i></td><td>= <i>pop</i> &lowast; <i>nh</i> / ln(2)</td></tr>
<tr><td></td><td>= <i>pop</i> &lowast; <i>nh</i> &lowast; log<sub>2</sub>(<i>e</i>)</td></tr>
<tr><td><i>fpr</i></td><td>= exp(&minus;<i>nh</i> &lowast; ln(2))</td></tr>
<tr><td></td><td>= 2<sup>&minus;<i>nh</i></sup></td></tr>
</table></blockquote>
The <i>size</i> is a factor of only log<sub>2</sub>(<i>e</i>) &asymp; 1.44 larger than the information-theoretic lower bound (which I'm not going to explain here).
</p>

<p>Let's compare a Bloom filter with a simple one-bit-per-element hashed set, with a target false positive rate of 2<sup>-<i>k</i></sup>. If you set only one bit per element, then (assuming no hash collisions) <i>fpr</i> = <i>pop</i> / <i>size</i>, so <i>size</i> = <i>pop</i> &lowast; 2<sup><i>k</i></sup>, which is exponentially worse than <i>k</i>-hash Bloom filter.</p>

<p>It seems reasonable (for the application I have in mind) to set <i>size</i> = 16 &lowast; <i>limit</i> and <i>nh</i> = 8, which gives us headroom for the population to overflow <i>limit</i> by 2 &lowast; ln 2 &asymp; 1.39 before <i>fpr</i> rises to 1/256.</p>

<p><a href="http://www.eecs.harvard.edu/~kirsch/pubs/bbbf/esa06.pdf">Adam Kirsch and Michael Mitzenmacher</a> show that, rather than requiring <i>nh</i> independent hash functions, we can use a set of functions of the form <i>h</i><sub>1</sub>(<i>x</i>) + <i>i</i> &lowast; <i>h</i><sub>2</sub>(<i>x</i>) where 0 &le; <i>i</i> &lt; <i>nh</i>, without increasing the false positive rate. A hash function might be implemented by taking log<sub>2</sub>(<i>size</i>) bits from the result of MD5(<i>x</i>), so without this trick, <i>nh</i> = 8 limits <i>size</i> to 2<sup>16</sup>, whereas with the trick we only need two functions so the limit is large enough that we don't need to worry.</p>
