---
dw:
  anum: 164
  eventtime: "2006-01-18T19:09:00Z"
  itemid: 183
  logtime: "2006-01-18T19:10:42Z"
  props:
    import_source: livejournal.com/fanf/47187
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/47012.html"
format: casual
lj:
  anum: 83
  can_comment: 1
  ditemid: 47187
  event_timestamp: 1137611340
  eventtime: "2006-01-18T19:09:00Z"
  itemid: 184
  logtime: "2006-01-18T19:10:42Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/47187.html"
title: IETF widget exchange working group
...

http://www.ietf.org/html.charters/widex-charter.html

I don't understand the point of this. What does it give us that we can't already do with AJAX?
