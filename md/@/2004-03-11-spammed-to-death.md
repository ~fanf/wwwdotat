---
dw:
  anum: 105
  eventtime: "2004-03-11T15:25:00Z"
  itemid: 72
  logtime: "2004-03-11T07:28:35Z"
  props:
    commentalter: 1491292310
    import_source: livejournal.com/fanf/18450
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/18537.html"
format: md
lj:
  anum: 18
  can_comment: 1
  ditemid: 18450
  event_timestamp: 1079018700
  eventtime: "2004-03-11T15:25:00Z"
  itemid: 72
  logtime: "2004-03-11T07:28:35Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/18450.html"
title: Spammed to death
...

Here are the counts of messages that landed in my spam folder each day
in the last few weeks. I recently moved my personal email from Chiark
to Hermes because I needed the content filtering. Note that these
numbers do not include the thousand-or-so viruses each day that are
forwarded from Chiark and deleted by Hermes.

     37  Fri Feb 20  2004
     42  Sat Feb 21  2004
     37  Sun Feb 22  2004
     26  Mon Feb 23  2004
     15  Tue Feb 24  2004
     25  Wed Feb 25  2004
     35  Thu Feb 26  2004
     41  Fri Feb 27  2004
     45  Sat Feb 28  2004
     58  Sun Feb 29  2004
     33  Mon Mar 01  2004
     49  Tue Mar 02  2004
    110  Wed Mar 03  2004
    371  Thu Mar 04  2004
    452  Fri Mar 05  2004
    348  Sat Mar 06  2004
    287  Sun Mar 07  2004
    556  Mon Mar 08  2004
    441  Tue Mar 09  2004
    377  Wed Mar 10  2004
    265  Thu Mar 11  2004
