---
dw:
  anum: 227
  eventtime: "2018-10-23T17:07:00Z"
  itemid: 507
  logtime: "2018-10-23T16:08:42Z"
  props:
    commentalter: 1540316087
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/130019.html"
format: md
...

Amsterdam day 7
===============

([Fri](https://dotat.at/@/2018-10-12-amsterdam-day-0.html)
[Sat](https://dotat.at/@/2018-10-14-amsterdam-day-1.html)
[Sun](https://dotat.at/@/2018-10-14-amsterdam-day-2.html)
[Mon](https://dotat.at/@/2018-10-16-amsterdam-day-3.html)
[Tue](https://dotat.at/@/2018-10-17-amsterdam-day-4.html)
[Wed](https://dotat.at/@/2018-10-18-amsterdam-day-5.html)
[Thu](https://dotat.at/@/2018-10-18-amsterdam-day-6.html))

Very belated, but there was not a great deal to report from the last
day of the [RIPE
meeting](https://ripe77.ripe.net/programme/meeting-plan/) and I have
spent the last few days doing Other Things.

One of the more useful acronyms I learned was the [secretly
humorous](https://www.secret-wg.org/) "DONUTS": DNS Over Normal
Unencrypted TCP Sessions.

Following Thurday's presentation by Jen Linkova on IETF IPv6 activity,
Sander Steffann did a lightning talk about the IPv6-only RA flag.
There was quite a lot of discussion, and it was generally agreed to be
a terrible idea: instead, operators who want to suppress IPv4 should
use packet filters on switches rather than adding denial-of-service
features to end hosts.

Amanda Gowland gave a well-received talk on the women's lunch and
diversity efforts in general. There was lots of friendly amusement
about there being no acronyms (except for "RIPE").

Razvan Oprea gave the RIPE NCC tech report on the meeting's infrastructure:

  - 10Gbit connection to the meeting - "you haven't used much of it to
    be honest, so you need to try harder" - peaking at 300Mbit/s

  - 800 simultaneous devices on the wireless net

  - They need more feedback on how well the NAT64 network works

  - There were a few devices using DNS-over-TLS on the Knot resolvers

One of the unusual and popular features of RIPE meetings is the
real-time captioning produced by a small team of stenographers. In
addition to their normal dictionary of 65,000 common English words,
they have a custom dictionary of 36,000 specialized technical terms
and acronyms. Towards the end of the week they relaxed a bit and in
the more informal parts of the meeting (especially when they were
being praised) they talked back via the steno transcript display :-)
(Tho those parts aren't included in the [steno copy on the
web](https://ripe77.ripe.net/archives/steno/47/)).

That's about it for the daily-ish notes. Now to distill them into an
overall summary of the week for my colleagues...
