---
dw:
  anum: 113
  eventtime: "2015-11-11T11:08:00Z"
  itemid: 435
  logtime: "2015-11-11T11:08:02Z"
  props:
    commentalter: 1491292414
    import_source: livejournal.com/fanf/138594
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/111473.html"
format: html
lj:
  anum: 98
  can_comment: 1
  ditemid: 138594
  event_timestamp: 1447240080
  eventtime: "2015-11-11T11:08:00Z"
  itemid: 541
  logtime: "2015-11-11T11:08:02Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 17
  url: "https://fanf.livejournal.com/138594.html"
title: Chemical element symbols that are also ISO 3166 country code abbreviations
...

<pre>
Ag Silver	Antigua and Barbuda
Al Aluminum	Albania
Am Americium	Armenia
Ar Argon	Argentina
As Arsenic	American Samoa
At Astatine	Austria
Au Gold		Australia
Ba Barium	Bosnia and Herzegovina
Be Beryllium	Belgium
Bh Bohrium	Bahrain
Bi Bismuth	Burundi
Br Bromine	Brazil
Ca Calcium	Canada
Cd Cadmium	Democratic Republic of the Congo
Cf Californium	Central African Republic
Cl Chlorine	Chile
Cm Curium	Cameroon
Cn Copernicium	China
Co Cobalt	Colombia
Cr Chromium	Costa Rica
Cs Cesium	Serbia and Montenegro
Cu Copper	Cuba
Er Erbium	Eritrea
Es Einsteinium	Spain
Eu Europium	Europe
Fm Fermium	Federated States of Micronesia
Fr Francium	France
Ga Gallium	Gabon
Gd Gadolinium	Grenada
Ge Germanium	Georgia
In Indium	India
Ir Iridium	Iran
Kr Krypton	South Korea
La Lanthanum	Laos
Li Lithium	Liechtenstein
Lr Lawrencium	Liberia
Lu Lutetium	Luxembourg
Lv Livermorium	Latvia
Md Mendelevium	Moldova
Mg Magnesium	Madagascar
Mn Manganese	Mongolia
Mo Molybdenum	Macau
Mt Meitnerium	Malta
Na Sodium	Namibia
Ne Neon		Niger
Ni Nickel	Nicaragua
No Nobelium	Norway
Np Neptunium	Nepal
Os Osmium	Oman
Pa Protactinium	Panama
Pm Promethium	Saint Pierre and Miquelon
Pr Praseodymium	Puerto Rico
Pt Platinum	Portugal
Re Rhenium	Reunion
Ru Ruthenium	Russia
Sb Antimony	Solomon Islands
Sc Scandium	Seychelles
Se Selenium	Sweden
Sg Seaborgium	Singapore
Si Silicon	Slovenia
Sm Samarium	San Marino
Sn Tin		Senegal
Sr Strontium	Suriname
Tc Technetium	Turks and Caicos Islands
Th Thorium	Thailand
Tm Thulium	Turkmenistan
</pre>

Elemental domain names that exist:

<pre>
<a href="http://www.Silver.Ag">Silver.Ag</a>
Beryllium.Be
Calcium.Ca
Cobalt.Co
Francium.Fr
Gallium.Ga
Indium.In
<a href="http://www.Krypton.Kr">Krypton.Kr</a>
Lanthanum.La
<a href="http://www.Lithium.Li">Lithium.Li</a>
<a href="http://www.Mendelevium.Md">Mendelevium.Md</a>
<a href="http://www.Platinum.Pt">Platinum.Pt</a>
Rhenium.Re
Ruthenium.Ru
<a href="http://www.Selenium.Se">Selenium.Se</a>
Silicon.Si
</pre>

Oh, let's do US states as well:

<pre>
Al Aluminum     Alabama
Ar Argon        Arkansas
Ca Calcium      California
Co Cobalt       Colorado
Fl Flerovium    Florida
Ga Gallium      Georgia
In Indium       Indiana
La Lanthanum    Louisiana
Md Mendelevium  Maryland
Mn Manganese    Minnesota
Mo Molybdenum   Missouri
Mt Meitnerium   Montana
Nd Neodymium    North Dakota
Ne Neon         Nebraska
Pa Protactinium Pennsylvania
Sc Scandium     South Carolina
</pre>
