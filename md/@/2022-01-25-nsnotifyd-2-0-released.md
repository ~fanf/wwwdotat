---
dw:
  anum: 160
  eventtime: "2022-01-25T14:38:00Z"
  itemid: 535
  logtime: "2022-01-25T14:43:47Z"
  props:
    commentalter: 1643223222
    interface: flat
    picture_keyword: ""
    revnum: 2
    revtime: 1643223201
  url: "https://fanf.dreamwidth.org/137120.html"
format: md
...

nsnotifyd-2.0 released
======================

I have made
[a new release of `nsnotifyd`][nsnotifyd]
a tiny DNS server that just listens for NOTIFY messages
and runs a script when one of your zones changes.

[This `nsnotifyd-2.0` release][nsnotifyd] adds support for TCP. The
original `nsnotifyd` only supported UDP, which works fine with BIND,
but [Knot DNS](https://www.knot-dns.cz/) sends NOTIFY messages over
TCP.

As well as its new TCP support, the `nsnotify` client program that sends
NOTIFY messages can now send bulk notifications for lots of zones, as
well as being able to send notifications to lots of recipient servers.

Many thanks to Niels Haarbo and [DK Hostmaster](https://www.dk-hostmaster.dk/)
for requesting TCP support and for sponsoring my work to implement it.

I like `nsnotifyd` because I wrote it in 2015, and I haven’t touched
it since then (until this month). I usually hear nothing about
`nsnotifyd`, but occasionally someone mentions they are using it. For
example [the Guardian tech blog][graun] said of `nsnotifyd`, "like all
good *nix tools it does one thing well", and [JP Mens][] called it "a
gem of a utility".

Happy users, no bug reports, software bliss.

[nsnotifyd]: https://dotat.at/prog/nsnotifyd/
[graun]: https://www.theguardian.com/info/developer-blog/2016/dec/23/multiple-dns-synchronising-dyn-to-aws-route-53
[JP Mens]: http://jpmens.net/2015/06/16/alert-on-dns-notify/
