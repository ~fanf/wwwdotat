---
dw:
  anum: 4
  eventtime: "2008-03-05T21:11:00Z"
  itemid: 329
  logtime: "2008-03-05T21:26:59Z"
  props:
    import_source: livejournal.com/fanf/85199
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/84228.html"
format: html
lj:
  anum: 207
  can_comment: 1
  ditemid: 85199
  event_timestamp: 1204751460
  eventtime: "2008-03-05T21:11:00Z"
  itemid: 332
  logtime: "2008-03-05T21:26:59Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/85199.html"
title: leaky life
...

<p>The past light cones of the cells on the unbounded edges of the pattern have a nasty effect. As you go further into the future, even though you only display a small quadrant near the origin, more and more empty space in the past must be examined to work out the state of the cells in the present. This space leak causes the calculation of each generation to get more expensive as the square of the generation number, even though it would ideally be constant.</p>

<p>To fix this, the size of the original universe must be restricted, instead of restricting its size just for display. Divide the <tt>display</tt> function into two:</p>

<pre>
  restrict (x,y) = map (take x) . take y
  display = unlines . map (concatMap show)
</pre>

<p>We also need to change the <tt>grow</tt> function to deal with the other two edges of the universe.</p>

<pre>
  bracket a xs = [a] ++ xs ++ [a]
  grow = (bracket deadline) . map (bracket deadcell)
</pre>

<p>Then in the loop we remove the dimension argument to <tt>display</tt> and change the main program to:</p>

<pre>
  main = loop $ restrict (10,10) glider
</pre>

<p>This is enough to make it possible to compute the 1103 interesting generations of the R pentomino at a few generations per second (text output being rather slow).</p>
