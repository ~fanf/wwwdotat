---
dw:
  anum: 164
  eventtime: "2007-02-22T22:50:00Z"
  itemid: 275
  logtime: "2007-02-22T23:09:30Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/70745
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/70564.html"
format: casual
lj:
  anum: 89
  can_comment: 1
  ditemid: 70745
  event_timestamp: 1172184600
  eventtime: "2007-02-22T22:50:00Z"
  itemid: 276
  logtime: "2007-02-22T23:09:30Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/70745.html"
title: Drunken insanity
...

Following a discussion about source code revision control systems, something fairly similar to the following conversation occurred...

iwj describes his CVS hack which allows you to move repositories around, and which will chase chains of moves.

fanf is reminded of <a href="http://dotat.at/prog/scripts/sshtrace">a script</a> that he wrote to trace routes through a network using `route get` to find the next hop then ssh to connect to the next hop then a Quine to transmit itself to the next hop where it then repeats the process. (You need a network based on BSD routers and you need root on all the machines. It is useful for networks with asymmetrical routing.)

mdw, who also likes Quines, says that he wrote a program that can turn any C program into a Quine.

iwj says that gcc should obviously have a --quine option to output its own source code for easier GPL conformance.

mdw says that Linux should similarly have /proc/source/.

sgt points out that /proc/include would actually be useful for compiling things, prompting brief amazement that Quines can have so many practical purposes.

iwj (slightly missing the point of Quines) suggests that this would cause modprobe to pull in the source code dynamically and eat your RAM.

fanf is reminded of an earlier discussion about LD_PRELOAD hacks (in which we agreed that a good way of discouraging programmers who are too enthusiastic about LD_PRELOAD is to explain to them how they can do a much better job with ptrace) and suggests that, as well as source code, Linux could export object code via .so files under /proc/.

everyone discusses how this would solve the problem of kernel vs. libc version skew, since libc could just dynamically link the kernel-dependent code from procfs. We debate whether Ulrich Drepper would have babies or kittens and whether this would lead to Core Wars being played in every process.
