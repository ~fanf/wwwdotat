---
format: html
lj:
  anum: 164
  can_comment: 1
  ditemid: 104356
  event_timestamp: 1259197440
  eventtime: "2009-11-26T01:04:00Z"
  itemid: 407
  logtime: "2009-11-26T01:04:14Z"
  props:
    personifi_tags: "8:30,11:10,23:20,43:10,42:10,1:20,3:10,9:30,13:30"
    verticals_list: technology
  reply_count: 2
  url: "https://fanf.livejournal.com/104356.html"
title: FreeBSD unifdef(1) and factor(6)
...

<p>Earlier this week I got a bug report for <tt>unifdef</tt> from Jonathan Nieder, who now maintains the Debian package. This prompted me to do some work on it, which it sorely needed - I hadn't touched it since March last year, and I had a bunch of unincorporated patches from Anders Kaseor, and I haven't backported the fix for an embarrassing bug from FreeBSD-8 to FreeBSD-7.<p>

<p>I've dealt with all but the last of these, and the latest version <a href="http://dotat.at/prog/unifdef/unifdef-1.188.tar.gz"><tt>unifdef-1.188</tt></a> is available from <a href="http://dotat.at/prog/unifdef/">http://dotat.at/prog/unifdef/</a>. I have also committed it to <a href="http://www.freebsd.org/cgi/cvsweb.cgi/src/usr.bin/unifdef/unifdef.c?rev=1.24">FreeBSD-9-CURRENT</a>. When the code freeze for FreeBSD-8-STABLE is lifted (FreeBSD-8.0 should be released RSN!) I'll backport unifdef to the 8-STABLE and 7-STABLE branches.</p>

<p>This evening I also did a bit of work on FreeBSD's version of <tt>factor</tt>. I noticed a while back that it has a performance bug: it's very slow to factorize some numbers, for example my phone number. I got its bignum factoring code from NetBSD back in <a href="http://www.freebsd.org/cgi/cvsweb.cgi/src/games/factor/factor.c?rev=1.13">2002</a>, so I went back there to see if they knew anything about this bug. It turns out that <a href="http://www.srcf.ucam.org/~jsm28/">jsm28</a> fixed the bug in <a href="http://cvsweb.netbsd.org/bsdweb.cgi/src/games/factor/factor.c?rev=1.15&content-type=text/x-cvsweb-markup">2004</a>. Five and a half years later I've finally <a href="http://www.freebsd.org/cgi/cvsweb.cgi/src/games/factor/factor.c?rev=1.14">committed</a> the fix to FreeBSD...</p>
