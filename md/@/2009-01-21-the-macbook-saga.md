---
dw:
  anum: 156
  eventtime: "2009-01-21T23:42:00Z"
  itemid: 371
  logtime: "2009-01-21T23:43:28Z"
  props:
    commentalter: 1491292428
    import_source: livejournal.com/fanf/96376
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/95132.html"
format: html
lj:
  anum: 120
  can_comment: 1
  ditemid: 96376
  event_timestamp: 1232581320
  eventtime: "2009-01-21T23:42:00Z"
  itemid: 376
  logtime: "2009-01-21T23:43:28Z"
  props:
    verticals_list: technology
  reply_count: 0
  url: "https://fanf.livejournal.com/96376.html"
title: The MacBook saga
...

<p>Three months ago, Apple released their new "unibody" aluminium MacBook laptop, and just over a month before that they released the second-generation iPod touch. I was in the market for a new Mac laptop, and at some point I planned to upgrade my iPod when a 64GB Touch or iPhone became available. However when I found out that Apple were offering £95 "back to school" rebates to those who bought an iPod Touch and a MacBook, I couldn't wait any longer - this was the 30th October and the offer ceased at the end of the month.</p>

<p>Unfortunately I made the order with a rather crusty old version of Firefox which handled Apple's customization JavaScript incorrectly - it discarded all the options I chose, and because I wasn't familiar with the order process I didn't know that the final description screen should have included more details than it did. This wasn't a problem for the bits and pieces that I could also buy at the local <strike>greengrocer</strike> Apple Store, but one important option I chose was the US keyboard layout. I wanted it partly because I normally use a <a href="http://www.pfusystems.com/hhkeyboard/images/lite2_us_sl.jpg">Happy Hacking keyboard</a> which has a similar layout, and partly because <a href="http://lowendmac.com/mail/mb07/art/macbook_keyboard.jpg">the ISO layout</a> has thin return key that is awkward to press as well as being ugly.</p>

<p>When I discovered this upon unboxing the new Shiny! my heart sank. You can't change the keyboard on the current MacBooks without replacing the entire machine. But I phoned Apple to see what I could do about it, and I was pleasantly surprised to find out that they would replace the machine with the correct model for free, no questions asked.</p>

<p>I discovered Apple's FAIL when the second MacBook arrived. I had been sent the <a href="http://km.support.apple.com/library/APPLE/APPLECARE_ALLGEOS/HT2841/304933_08.gif">"International English"</a> model, which is identical to the <a href="http://km.support.apple.com/library/APPLE/APPLECARE_ALLGEOS/HT2841/304933_02.gif">"British"</a> layout apart from a couple of currency symbols. They also sent me a continental power plug instead of a BS 1363 plug. When I phoned Apple they were suitably contrite, even offering me £70 compensation for the error!</p>

<p>Until this point (two weeks after the original order) I was very happy with Apple's support - no dumb scripts, reasonably competent staff. Sadly this didn't last. While the phone staff continued to be helpful and informative about what was going on, I had to keep phoning to get an update because the supervisor who was supposed to be dealing with the problem was too busy doing other stuff. The cause seemed to be that the department which handled replacements didn't believe that US keyboards were available in the UK (despite what the online store said) and the support people were incapable of fixing or working around this failure.</p>

<p>What was worse was that it took another three weeks to come to this conclusion. (I was away in France for one of them, during which I didn't chase Apple so nothing happened.) I couldn't use my new iPod because I didn't want to have the pain of re-pairing and re-populating it with music etc. This all made me extremely cross.</p>

<p>In the end I had to return the second MacBook for a refund and buy a third one with the correct configuration, which finally arrived six weeks after the original order. Fortunately this didn't affect my eligibility for the rebate - in fact they had already sent me the £95, though I hadn't sent in the proof-of-purchase paperwork they said they needed. (Um, I didn't notice the extra money until this month!) Unfortunately they cancelled the compensation they promised me! They fixed that after I made an irate phone call, but sheesh, I still get angry when remembering it.</p>

<p>But I suppose it's all OK in the end. The MacBook and iPod Touch are things of astounding beauty and utility. The iPod utterly blows away the Nokia 770 I previously used for spodding, and the MacBook replaces both my elderly PC laptop and my Mac Mini (though the latter will probably become a backup server). It's really nice to be able to hack away on the comfy sofa, and I'm playing choonz via the Airport Express a lot more than I did.</p>
