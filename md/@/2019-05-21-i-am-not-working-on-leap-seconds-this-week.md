---
dw:
  anum: 95
  eventtime: "2019-05-21T00:49:00Z"
  itemid: 513
  logtime: "2019-05-21T00:00:27Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/131423.html"
format: md
...

I am NOT working on leap seconds this week
==========================================

For several years I have published a [leap second table][lsc] in the DNS. My
version(s) of the leap second table are (as far as I know) unique:

  * They are cryptographically signed: if you get my leap second table
    you know it came from my workstation.

  * They have the full history from 1972, in a single DNS query, and this
    needs less than 100 bytes (plain text) or 20 bytes (binary).

[lsc]: http://dotat.at/cgi/git/leapseconds.git/blob/HEAD:/syntax.md

My leap second table is sod-all use
-----------------------------------

The problem with leap seconds is not how they are published, but the
tools that (fail to) disseminate and use the leap second table.

NTP is a big problem: the traditional NTP implementation is designed
to choose and fixate on **one** server out of an ensemble of possible
upstreams. Leap seconds are *not* part of the decision process of
choosing which upstream is correct.

As a result, NTP will trust an upstream that is good at keeping time
(maybe it is in a room with good temperature stability), but
misconfigured for leap seconds, so when a leap second comes along NTP
gets surprised by a sudden one-second offset.

Leap seconds should not be a surprise
-------------------------------------

There at least two plausible ways to avoid being surprised:

  * Make your NTP server get the leap second table from a trusted
    source, e.g. my DNS records. If your upstream NTP server's leap
    indicator bits disagree with the correct table, expect that it
    will go wrong when a leap second happens.

  * Get NTP servers to disseminate the table, and only trust upstreams
    that agree with the majority. The table is less than 20 bytes
    (600x smaller than the NIST table) so it can easily fit in an NTP
    packet.

How to fix leap seconds
-----------------------

  * Make it cheap to distribute the leap second table.

    Done, but my spec needs more rigor. My binary leap second table is
    smaller than a SHA-1 hash, so it is cheaper to distribute the
    whole thing than a digest.

  * Make NTP distrust upstreams that disagree with the leap second
    table when a leap second is pending.

	This is the hard part, because it involves persuading multiple
    open source and proprietary implementers.

I am not working on this
------------------------

But I will happily rant about precise time keeping at the [Cambridge
Beer Festival][CBF] to anyone who talks about their project to
refurbish a rubidium frequency standard [not a hypothetical example]
and I will try not to bore people who are not time nuts....

[CBF]: https://www.cambridgebeerfestival.com/

I thought that this week I might work on polishing my spec for compact
leap second tables, or writing an implementation in Rust, but I have
decided that tackling the oppressive todo list of procrastinatory doom
will make me happier.
