---
dw:
  anum: 176
  eventtime: "2004-01-14T13:46:00Z"
  itemid: 50
  logtime: "2004-01-14T13:49:00Z"
  props:
    commentalter: 1491292322
    import_source: livejournal.com/fanf/12836
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/12976.html"
format: casual
lj:
  anum: 36
  can_comment: 1
  ditemid: 12836
  event_timestamp: 1074087960
  eventtime: "2004-01-14T13:46:00Z"
  itemid: 50
  logtime: "2004-01-14T13:49:00Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/12836.html"
title: Recruiter
...

Got called by a recruiter looking for people who know ML for a research programming job in Cambridge. I guess he's been googling for CVs :-) If anyone here is interested I got his phone number so I can pass it on.
