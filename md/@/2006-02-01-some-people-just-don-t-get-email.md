---
dw:
  anum: 40
  eventtime: "2006-02-01T14:50:00Z"
  itemid: 193
  logtime: "2006-02-01T14:55:58Z"
  props:
    commentalter: 1491292334
    import_source: livejournal.com/fanf/49787
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/49448.html"
format: casual
lj:
  anum: 123
  can_comment: 1
  ditemid: 49787
  event_timestamp: 1138805400
  eventtime: "2006-02-01T14:50:00Z"
  itemid: 194
  logtime: "2006-02-01T14:55:58Z"
  props: {}
  reply_count: 10
  url: "https://fanf.livejournal.com/49787.html"
title: "Some people just don't get email"
...

So we got a message from reception about some unusual forwarded email. It turns out that these mediaevalists had been confused by some misaddressed email and blamed a virus. This caused a phone call to CS reception, which our staff followed up with an email asking for the problem messages to be forwarded to us for analysis. The messages were forwarded ON PAPER with a scribbled note.
