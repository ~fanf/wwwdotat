---
dw:
  anum: 107
  eventtime: "2006-02-21T22:27:00Z"
  itemid: 201
  logtime: "2006-02-21T23:03:49Z"
  props:
    import_source: livejournal.com/fanf/51767
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/51563.html"
format: casual
lj:
  anum: 55
  can_comment: 1
  ditemid: 51767
  event_timestamp: 1140560820
  eventtime: "2006-02-21T22:27:00Z"
  itemid: 202
  logtime: "2006-02-21T23:03:49Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/51767.html"
title: Streamlined message submission
...

There was recently (mostly on the 19th and 20th) a thread on the <a href="http://www1.ietf.org/mail-archive/web/ietf/current/index.html">IETF discussion list</a> about whether round-trip times are still a concern. The combination of that, my recent <a href="http://fanf.livejournal.com/50917.html">thinking about message submission</a>, and <a href="http://www.ietf.org/html.charters/lemonade-charter.html">the LEMONADE working group</a>'s efforts to streamline it led to the following.

At the moment, a message submission packet trace to a system like Hermes goes like this very long packet trace:
<pre>
        client |         TCP SYN         | server
               |------------------------>|
               |                         |
               |      TCP SYN + ACK      |
               |<------------------------|
               |                         |
               |         TCP ACK         |
               |------------------------>|
               |                         |
               |      SMTP greeting      |
               |<------------------------|
               |                         |
               |        SMTP EHLO        |
               |------------------------>|
               |                         |
               |   SMTP extension list   |
               |<------------------------|
               |                         |
               |      SMTP STARTTLS      |
               |------------------------>|
               |                         |
               |       SMTP 220 OK       |
               |<------------------------|
               |                         |
               |        TLS hello        |
               |------------------------>|
               |                         |
               |      TLS hello etc.     |
               |<------------------------|
               |                         |
               |  TLS handshake finish   |
               |------------------------>|
               |                         |
               |   TLS handshake finish  |
               |<------------------------|
               |                         |
               |        SMTP EHLO        |
               |------------------------>|
               |                         |
               |   SMTP extension list   |
               |<------------------------|
               |                         |
               |     SASL AUTH PLAIN     |
               |------------------------>|
               |                         |
               |    SMTP 235 AUTH OK     |
               |<------------------------|
               |                         |
               |      SMTP envelope      |
               |------------------------>|
               |                         |
               |    SMTP 250 ... 354     |
               |<------------------------|
               |                         |
               |  RFC 822 message data   |
               |------------------------>|
               |                         |
               |       SMTP 250 OK       |
               |<------------------------|
               |                         |
               |        SMTP QUIT        |
               |------------------------>|
               |                         |
               |   SMTP 221 + TCP FIN    |
               |<------------------------|
               |                         |
               |      TCP FIN + ACK      |
               |------------------------>|
               |                         |
               |         TCP ACK         |
               |<------------------------|
</pre>
That's TWELVE round-trips, which can easily take over a second if you are any distance from the server. Obviously, there's a lot of scope for streamlining.

<a href="https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-smtp-tls-on-connect.txt">draft-fanf-smtp-tls-on-connect</a> attempts to resurrect the old <tt>smtps</tt> port, which is still frequently used in practice, but is frowned on by the IETF. This saves three round trips.

<a href="https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-smtp-quickstart.txt">draft-fanf-smtp-quickstart</a> slightly streamlines the ESMTP startup, to save another round trip.

<a href="http://www.ietf.org/internet-drafts/draft-ietf-lemonade-burl-04.txt">draft-ietf-lemonade-burl</a>, amongst other things, allows a client to pipeline single-exchange AUTH commands, and the message envelope and data. This saves two round trips.

<a href="https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-smtp-rcpthdr.txt">draft-fanf-smtp-rcpthdr</a> allows a client to avoid re-stating email addresses, which doesn't save round trips, but does reduce the chance that the client's submission will overflow TCP's initial window and thereby incur an extra round trip delay.

The result looks like this shorter trace:
<pre>
    client |           TCP SYN            | server
           |----------------------------->|
           |                              |
           |        TCP SYN + ACK         |
           |<-----------------------------|
           |                              |
           |     TCP ACK + TLS hello      | TLS-on-connect
           |----------------------------->|
           |                              |
           |        TLS hello etc.        |
           |<-----------------------------|
           |                              |
           |     TLS handshake finish     |
           |----------------------------->|
           |                              |
           | TLS finish + SMTP extensions | QUICKSTART
           |<-----------------------------|
           |                              |
           |    AUTH + envelope + data    | BURL + RCPTHDR
           |----------------------------->|
           |                              |
           |       235 + 250 + 250        |
           |<-----------------------------|
           |                              |
           |          SMTP QUIT           |
           |----------------------------->|
           |                              |
           |      SMTP 221 + TCP FIN      |
           |<-----------------------------|
           |                              |
           |        TCP FIN + ACK         |
           |----------------------------->|
           |                              |
           |           TCP ACK            |
           |<-----------------------------|
</pre>
That's six round trips, so half the elapsed time of message submission according to the current specifications.

I wonder if anyone will like my draft specs...

<b>Edit:</b> Actually you can pipeline the message data and quit command, which reduces the round-trip counts to eleven and five. See also <a href="https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-submission-streamlined.txt">draft-fanf-submission-streamlined</a>.
