---
dw:
  anum: 163
  eventtime: "2004-06-17T13:50:00Z"
  itemid: 89
  logtime: "2004-06-17T06:58:17Z"
  props:
    commentalter: 1491292312
    import_source: livejournal.com/fanf/22821
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/22947.html"
format: casual
lj:
  anum: 37
  can_comment: 1
  ditemid: 22821
  event_timestamp: 1087480200
  eventtime: "2004-06-17T13:50:00Z"
  itemid: 89
  logtime: "2004-06-17T06:58:17Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/22821.html"
title: Silly domain names
...

Anyone heard of any problems with all-numeric mail domain labels? e.g. dot@0010111001000000.dotat.at
They are OK in theory according to RFC 1123, but I want to know about operational experience.

Some people cause trouble by their mere existence...
