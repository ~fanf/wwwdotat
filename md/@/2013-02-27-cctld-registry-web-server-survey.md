---
format: html
lj:
  anum: 51
  can_comment: 1
  ditemid: 125747
  event_timestamp: 1361999280
  eventtime: "2013-02-27T21:08:00Z"
  itemid: 491
  logtime: "2013-02-27T21:08:47Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/125747.html"
title: ccTLD registry web server survey
...

<p>There has been some amusement on Twitter about a blog post from <a href="http://blogs.technet.com/b/security/archive/2013/02/25/microsoft-offers-security-assessment-service-for-country-code-top-level-domain-registries-cctld.aspx">Microsoft offering their security auditing services to ccTLDs</a>. This is laughable from the DNS point of view, but fairly plausible if you consider a registry's web front end. So I thought I would do a little survey. I got a list of ccTLDs from my local copy of the root zone, and looked up their web sites in Wikipedia, then asked each one in turn what web server software it uses. <a href="https://twitter.com/fcambus">Frederic Cambus</a> suggested on Twitter that I should get the registry URLs from the IANA root zone database instead, and this turned out to be much better because of the more consistent formatting.</p>

<table>
<tr><th align="left">ccTLDs</th><td>251</td></tr>
<tr><th align="left">Registries</th><td>228</td></tr>
<tr><th align="left">No Server</th><td>17</td></tr>
<tr><th align="left">Apache</th><td>168</td></tr>
<tr><th align="left">Apache/2.4</th><td>6</td></tr>
<tr><th align="left">Apache/2.2</th><td>79</td></tr>
<tr><th align="left">Apache/2.0</th><td>12</td></tr>
<tr><th align="left">Apache/2</th><td>2</td></tr>
<tr><th align="left">Apache/1.3</th><td>3</td></tr>
<tr><th align="left">Apache verbose</th><td>39</td></tr>
<tr><th align="left">Apache (Unix)</th><td>21</td></tr>
<tr><th align="left">Apache (CentOS)</th><td>20</td></tr>
<tr><th align="left">Apache (Debian)</th><td>13</td></tr>
<tr><th align="left">Apache (Ubuntu)</th><td>12</td></tr>
<tr><th align="left">Apache (FreeBSD)</th><td>7</td></tr>
<tr><th align="left">Apache (Fedora)</th><td>4</td></tr>
<tr><th align="left">Apache (Win32)</th><td>2</td></tr>
<tr><th align='left'>Microsoft-IIS</th><td>21</td></tr>
<tr><th align="left">Microsoft-IIS/4.0</th><td>1</td></tr>
<tr><th align="left">Microsoft-IIS/5.0</th><td>2</td></tr>
<tr><th align="left">Microsoft-IIS/6.0</th><td>11</td></tr>
<tr><th align="left">Microsoft-IIS/7.0</th><td>3</td></tr>
<tr><th align="left">Microsoft-IIS/7.5</th><td>4</td></tr>
<tr><th align="left">nginx</th><td>13</td></tr>
<tr><th align="left">Nginx / Varnish</th><td>1</td></tr>
<tr><th align="left">GlassFish v3</th><td>1</td></tr>
<tr><th align="left">Netscape-Enterprise/6.0</th><td>1</td></tr>
<tr><th align="left">Oracle-Application-Server-10g</th><td>1</td></tr>
<tr><th align="left">thttpd</th><td>1</td></tr>
<tr><th align="left">VNNIC IDN Proxy 2</th><td>1</td></tr>
<tr><th align="left">WEBSERVER</th><td>1</td></tr>
<tr><th align="left">Zope / Plone</th><td>1</td></tr>
<tr><th align="left">(blank)</th><td>1</td></tr>
</table>

<pre>
    dig axfr . |
    sed '/^\([A-Za-z][A-Za-z]\)[.].*/!d;
         s||\1 http://www.iana.org/domains/root/db/\1.html|' |
    sort -u |
    while read t w
    do
        r=$(curl -s $w |
            sed '/.*&lt;b>URL for registration services:&lt;.b> &lt;a href="\([^">.*/!d;
                 s//\1/')
        case $r in
        '') echo "&lt;tr>&lt;th>$t&lt;/th>&lt;td>&lt;/td>&lt;td>&lt;/td>&lt;/tr>"
            continue;;
        esac
        s=$(curl -s -I $r | sed '/^Server: \([^^M]*\).*/!d;s//\1/')
        echo "&lt;tr>&lt;th>$t&lt;/th>&lt;td>$r&lt;/td>&lt;td>$s&lt;/td>&lt;/tr>"
    done
</pre>

<h2>All the data</h2>

<table>
<tr><th>ccTLD</th><th align="left">URL</th><th align="left">Software</th>
<tr><th>ac</th><td>http://www.nic.ac/</td><td>Apache/2.4.3 (Unix) OpenSSL/1.0.1c</td></tr>
<tr><th>ad</th><td>http://www.nic.ad</td><td>Apache</td></tr>
<tr><th>ae</th><td>http://aeda.ae/</td><td>Apache</td></tr>
<tr><th>af</th><td>http://www.nic.af</td><td>Microsoft-IIS/7.5</td></tr>
<tr><th>ag</th><td>http://www.nic.ag</td><td>Apache</td></tr>
<tr><th>ai</th><td>http://whois.ai</td><td>Apache/2.2.8 (Ubuntu)</td></tr>
<tr><th>al</th><td>http://www.akep.al/sq/formulare-domain</td><td>Apache</td></tr>
<tr><th>am</th><td>https://www.amnic.net/</td><td>Apache</td></tr>
<tr><th>an</th><td>http://www.una.an/an_domreg</td><td>Apache/2.2.14 (Win32) PHP/5.3.1</td></tr>
<tr><th>ao</th><td>http://www.dns.ao/</td><td>WEBSERVER</td></tr>
<tr><th>aq</th><td></td><td></td></tr>
<tr><th>ar</th><td>http://www.nic.ar</td><td>Apache</td></tr>
<tr><th>as</th><td>http://www.nic.as</td><td>Apache/1.3.33 (Unix) mod_ssl/2.8.22 OpenSSL/0.9.7e</td></tr>
<tr><th>at</th><td>http://www.nic.at/</td><td>Apache</td></tr>
<tr><th>au</th><td>http://www.auda.org.au/domains/au-domains/</td><td>cloudflare-nginx</td></tr>
<tr><th>aw</th><td></td><td></td></tr>
<tr><th>ax</th><td>http://www.whois.ax</td><td>Apache/2.2.15 (CentOS)</td></tr>
<tr><th>az</th><td>http://www.whois.az/</td><td>Apache/2.2.9 (FreeBSD)</td></tr>
<tr><th>ba</th><td>http://www.nic.ba</td><td>Apache</td></tr>
<tr><th>bb</th><td>http://www.whois.telecoms.gov.bb/</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>bd</th><td>http://whois.btcl.net.bd/registration.html</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>be</th><td>http://www.dns.be</td><td>Apache</td></tr>
<tr><th>bf</th><td>http://www.arce.bf/domaine.php</td><td>Apache</td></tr>
<tr><th>bg</th><td>http://www.register.bg</td><td>Apache/2.2.22 (FreeBSD) mod_ssl/2.2.22 OpenSSL/0.9.8q DAV/2 mod_perl/2.0.5 Perl/v5.14.2</td></tr>
<tr><th>bh</th><td></td><td></td></tr>
<tr><th>bi</th><td>http://www.nic.bi</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>bj</th><td>http://www.nic.bj</td><td>Apache/2.2.16 (Debian) PHP/5.2.6-1+lenny16 with Suhosin-Patch mod_ssl/2.2.16 OpenSSL/0.9.8o</td></tr>
<tr><th>bm</th><td>http://www.bermudanic.bm</td><td>Microsoft-IIS/5.0</td></tr>
<tr><th>bn</th><td>http://www.telbru.com.bn</td><td>Microsoft-IIS/7.5</td></tr>
<tr><th>bo</th><td>http://www.nic.bo</td><td>Apache</td></tr>
<tr><th>br</th><td>http://registro.br/</td><td>Apache</td></tr>
<tr><th>bs</th><td>http://www.register.bs</td><td>Apache/2.0.40 (Red Hat Linux)</td></tr>
<tr><th>bt</th><td>http://www.nic.bt</td><td>Apache</td></tr>
<tr><th>bv</th><td>http://www.norid.no/domenenavnbaser/bv-sj.html</td><td>Apache/2.2.16 (Debian) PHP/5.3.3-7+squeeze14 with Suhosin-Patch mod_ssl/2.2.16 OpenSSL/0.9.8o mod_wsgi/3.3 Python/2.6.6 mod_apreq2-20090110/2.7.1 mod_perl/2.0.4 Perl/v5.10.1</td></tr>
<tr><th>bw</th><td></td><td></td></tr>
<tr><th>by</th><td>http://cctld.by</td><td>nginx</td></tr>
<tr><th>bz</th><td>http://www.belizenic.bz</td><td>Apache/2.2.15 (CentOS)</td></tr>
<tr><th>ca</th><td>http://www.cira.ca/</td><td>Apache</td></tr>
<tr><th>cc</th><td>http://www.nic.cc/</td><td>Apache-Coyote/1.1</td></tr>
<tr><th>cd</th><td>http://www.nic.cd/</td><td>GlassFish v3</td></tr>
<tr><th>cf</th><td></td><td></td></tr>
<tr><th>cg</th><td>http://www.nic.cg</td><td>Apache/2.2.15 (CentOS)</td></tr>
<tr><th>ch</th><td>http://www.nic.ch/</td><td>Apache</td></tr>
<tr><th>ci</th><td>http://www.nic.ci</td><td>Apache/2.0.54 (Debian GNU/Linux) mod_python/3.1.3 Python/2.3.5 PHP/4.3.10-16 mod_perl/1.999.21 Perl/v5.8.8</td></tr>
<tr><th>ck</th><td>http://www.oyster.net.ck</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>cl</th><td>http://www.nic.cl/</td><td>Apache</td></tr>
<tr><th>cm</th><td>http://www.info.camnet.cm</td><td></td></tr>
<tr><th>cn</th><td>http://www.cnnic.cn/</td><td>Apache</td></tr>
<tr><th>co</th><td>http://www.cointernet.co/</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>cr</th><td>http://www.nic.cr/</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>cu</th><td>http://www.nic.cu</td><td>Netscape-Enterprise/6.0</td></tr>
<tr><th>cv</th><td>http://www.dns.cv/</td><td>Oracle-Application-Server-10g/9.0.4.0.0 Oracle-HTTP-Server</td></tr>
<tr><th>cw</th><td>http://www.una.cw/cw_registry</td><td>Apache/2.2.14 (Win32) PHP/5.3.1</td></tr>
<tr><th>cx</th><td>http://www.nic.cx</td><td>Apache</td></tr>
<tr><th>cy</th><td>http://www.nic.cy</td><td>Apache/1.3.31 (Unix) PHP/4.3.8 mod_perl/1.29</td></tr>
<tr><th>cz</th><td>http://www.nic.cz/</td><td>Apache</td></tr>
<tr><th>de</th><td>http://www.denic.de/</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>dj</th><td>http://www.nic.dj</td><td>Apache/2.2.22 (CentOS)</td></tr>
<tr><th>dk</th><td>http://www.dk-hostmaster.dk/</td><td>Apache</td></tr>
<tr><th>dm</th><td>http://www.nic.dm</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>do</th><td>http://www.nic.do</td><td>Apache/2.2.14 (Ubuntu)</td></tr>
<tr><th>dz</th><td>http://www.nic.dz</td><td>Apache/2.2.15 (Red Hat)</td></tr>
<tr><th>ec</th><td>http://www.nic.ec</td><td>Microsoft-IIS/7.0</td></tr>
<tr><th>ee</th><td>http://www.eestiinternet.ee</td><td>Apache</td></tr>
<tr><th>eg</th><td>http://www.egregistry.eg/</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>er</th><td></td><td></td></tr>
<tr><th>es</th><td>http://www.nic.es/</td><td>Apache</td></tr>
<tr><th>et</th><td>http://www.ethiotelecom.et</td><td>Apache/2.0.63 (Unix) DAV/2 mod_jk/1.2.15</td></tr>
<tr><th>eu</th><td>http://www.eurid.eu</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>fi</th><td>https://domain.fi</td><td>Microsoft-IIS/7.5</td></tr>
<tr><th>fj</th><td>http://domains.fj/</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>fk</th><td>http://www.fidc.co.fk/</td><td>Apache/2</td></tr>
<tr><th>fm</th><td>http://www.dot.fm/</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>fo</th><td>http://www.nic.fo/</td><td>Apache/2.2.9 (Fedora)</td></tr>
<tr><th>fr</th><td>http://www.nic.fr/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5</td></tr>
<tr><th>ga</th><td></td><td></td></tr>
<tr><th>gb</th><td></td><td></td></tr>
<tr><th>gd</th><td>http://www.adamsnames.com</td><td>Apache</td></tr>
<tr><th>ge</th><td>http://georgia.net.ge/domain/</td><td>Apache</td></tr>
<tr><th>gf</th><td>http://www.nplus.gf/</td><td>Apache/2.2.14 (Ubuntu)</td></tr>
<tr><th>gg</th><td>http://www.channelisles.net/</td><td>Apache/2.2.22 (Ubuntu)</td></tr>
<tr><th>gh</th><td>http://www.ghana.com.gh/</td><td>Apache/1.3.33 (Unix) PHP/5.0.4</td></tr>
<tr><th>gi</th><td>http://www.nic.gi</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>gl</th><td>http://www.nic.gl/</td><td>nginx/1.1.19</td></tr>
<tr><th>gm</th><td>http://www.nic.gm</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>gn</th><td>http://psg.com/dns/gn</td><td>Apache/2.2.23 (FreeBSD) PHP/5.4.7 SVN/1.7.7 mod_ssl/2.2.23 OpenSSL/1.0.1e DAV/2</td></tr>
<tr><th>gp</th><td>http://www.nic.gp/</td><td>Apache/2.2.16 (Debian)</td></tr>
<tr><th>gq</th><td>http://www.getesa.gq/</td><td></td></tr>
<tr><th>gr</th><td>http://www.gr</td><td>Apache/2.0.49 (Unix) mod_ssl/2.0.49 OpenSSL/0.9.7d</td></tr>
<tr><th>gs</th><td>http://secure.nic.gs</td><td></td></tr>
<tr><th>gt</th><td>http://www.gt</td><td>Apache/2.2.17 (Fedora)</td></tr>
<tr><th>gu</th><td>http://gadao.gov.gu</td><td>Microsoft-IIS/4.0</td></tr>
<tr><th>gw</th><td>http://www.register.gw</td><td></td></tr>
<tr><th>gy</th><td>http://registry.gy/</td><td>Nginx / Varnish</td></tr>
<tr><th>hk</th><td>http://www.hkirc.hk</td><td></td></tr>
<tr><th>hm</th><td>http://www.registry.hm</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>hn</th><td>http://www.nic.hn</td><td>Apache</td></tr>
<tr><th>hr</th><td>http://www.dns.hr</td><td>Apache/2.2.16 (Debian)</td></tr>
<tr><th>ht</th><td>http://www.nic.ht</td><td></td></tr>
<tr><th>hu</th><td>http://www.nic.hu</td><td>Apache</td></tr>
<tr><th>id</th><td>https://register.net.id/</td><td></td></tr>
<tr><th>ie</th><td>http://www.domainregistry.ie</td><td>Apache</td></tr>
<tr><th>il</th><td>http://www.isoc.org.il/domains</td><td>Apache/2.2.22</td></tr>
<tr><th>im</th><td>http://www.nic.im</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>in</th><td>http://www.registry.in</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>io</th><td>http://www.nic.io/</td><td>Apache/2.4.3 (Unix) OpenSSL/1.0.1c</td></tr>
<tr><th>iq</th><td>https://registrar.cmc.iq</td><td></td></tr>
<tr><th>ir</th><td>http://www.nic.ir</td><td>Apache</td></tr>
<tr><th>is</th><td>http://www.isnic.is/</td><td>Apache/2.2.23 (FreeBSD) PHP/5.3.19 with Suhosin-Patch mod_ssl/2.2.23 OpenSSL/0.9.8q DAV/2</td></tr>
<tr><th>it</th><td>http://www.nic.it/</td><td>Zope/(Zope 2.11.8-final, python 2.4.5, linux2) ZServer/1.1 Plone/3.3.6</td></tr>
<tr><th>je</th><td>http://www.channelisles.net/</td><td>Apache/2.2.22 (Ubuntu)</td></tr>
<tr><th>jm</th><td></td><td></td></tr>
<tr><th>jo</th><td>https://www.dns.jo/login.aspx</td><td></td></tr>
<tr><th>jp</th><td>http://jprs.jp/</td><td>Apache/2.2.21 (Unix) mod_ssl/2.2.21 OpenSSL/0.9.8e-fips-rhel5 DAV/2 PHP/5.4.4</td></tr>
<tr><th>ke</th><td>http://www.kenic.or.ke</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>kg</th><td>http://www.domain.kg/</td><td>Apache</td></tr>
<tr><th>kh</th><td>http://www.camnet.com.kh/</td><td>Apache/2.2</td></tr>
<tr><th>ki</th><td>http://www.nic.ki</td><td>Apache</td></tr>
<tr><th>km</th><td>http://www.domaine.km/</td><td>Apache/2.2.14 (Ubuntu)</td></tr>
<tr><th>kn</th><td></td><td></td></tr>
<tr><th>kp</th><td>http://www.star.co.kp</td><td></td></tr>
<tr><th>kr</th><td>http://www.nic.or.kr/</td><td>Apache/2.4.3 (Unix) mod_jk/1.2.37</td></tr>
<tr><th>kw</th><td>http://www.kw/</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>ky</th><td>http://www.nic.ky</td><td>Apache</td></tr>
<tr><th>kz</th><td>http://www.nic.kz</td><td>Apache-Coyote/1.1</td></tr>
<tr><th>la</th><td>http://www.la</td><td>Apache</td></tr>
<tr><th>lb</th><td>http://www.aub.edu.lb/lbdr</td><td>Microsoft-IIS/7.0</td></tr>
<tr><th>lc</th><td>http://www.nic.lc</td><td>Apache</td></tr>
<tr><th>li</th><td>http://www.nic.li</td><td>Apache</td></tr>
<tr><th>lk</th><td>http://www.nic.lk/</td><td>Apache</td></tr>
<tr><th>lr</th><td>http://psg.com/dns/lr</td><td>Apache/2.2.23 (FreeBSD) PHP/5.4.7 SVN/1.7.7 mod_ssl/2.2.23 OpenSSL/1.0.1e DAV/2</td></tr>
<tr><th>ls</th><td></td><td></td></tr>
<tr><th>lt</th><td>http://www.domreg.lt</td><td>Apache/2</td></tr>
<tr><th>lu</th><td>http://www.dns.lu</td><td>nginx/1.2.4</td></tr>
<tr><th>lv</th><td>http://www.nic.lv/</td><td>nginx/0.8.54 + Phusion Passenger 3.0.2 (mod_rails/mod_rack)</td></tr>
<tr><th>ly</th><td>http://www.nic.ly/</td><td>Apache</td></tr>
<tr><th>ma</th><td>http://www.nic.ma/</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>mc</th><td>http://www.nic.mc/</td><td>Apache/2.2.16 (Debian)</td></tr>
<tr><th>md</th><td>http://www.register.md</td><td>Apache/2.2.10 (Unix) mod_ssl/2.2.10 OpenSSL/0.9.8i DAV/2 PHP/5.2.8</td></tr>
<tr><th>me</th><td>http://www.domain.me</td><td>Apache/2.2.15 (CentOS)</td></tr>
<tr><th>mg</th><td>http://www.nic.mg</td><td>Apache</td></tr>
<tr><th>mh</th><td>http://www.nic.net.mh/</td><td></td></tr>
<tr><th>mk</th><td>http://www.marnet.mk</td><td>Apache/2.2.14 (Unix) mod_ssl/2.2.14 OpenSSL/0.9.7d DAV/2 mod_fcgid/2.3.4 mod_jk/1.2.28 PHP/5.2.12 mod_perl/2.0.4 Perl/v5.8.4</td></tr>
<tr><th>ml</th><td>http://www.sotelma.ml</td><td>Microsoft-IIS/7.0</td></tr>
<tr><th>mm</th><td>http://www.nic.mm/</td><td>Microsoft-IIS/7.5</td></tr>
<tr><th>mn</th><td>http://www.nic.mn</td><td>Apache/2.2.15 (CentOS)</td></tr>
<tr><th>mo</th><td>https://www.monic.mo</td><td>Apache</td></tr>
<tr><th>mp</th><td>https://get.mp/</td><td></td></tr>
<tr><th>mq</th><td>http://www.nic.mq/</td><td>Apache/2.2.8 (Ubuntu) PHP/5.2.4-2ubuntu5.12 with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g</td></tr>
<tr><th>mr</th><td>http://www.nic.mr</td><td>Apache/2.2.6 (Fedora)</td></tr>
<tr><th>ms</th><td>http://www.nic.ms</td><td>Apache</td></tr>
<tr><th>mt</th><td>https://www.nic.org.mt/</td><td>Apache</td></tr>
<tr><th>mu</th><td>http://www.nic.mu/</td><td>Apache</td></tr>
<tr><th>mv</th><td></td><td></td></tr>
<tr><th>mw</th><td>http://www.registrar.mw</td><td>Apache/2.2.15 (Fedora)</td></tr>
<tr><th>mx</th><td>http://www.registry.mx/</td><td></td></tr>
<tr><th>my</th><td>http://www.domainregistry.my</td><td>Apache/2.0.64 (Unix) mod_ssl/2.0.64 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.14</td></tr>
<tr><th>mz</th><td></td><td></td></tr>
<tr><th>na</th><td>http://www.na-nic.com.na/</td><td>Apache/2.2.8 (Ubuntu) DAV/2 SVN/1.5.1 PHP/5.2.4-2ubuntu5.26 with Suhosin-Patch</td></tr>
<tr><th>nc</th><td>http://www.domaine.nc</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>ne</th><td>http://www.intnet.ne</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>nf</th><td>http://nic.net.nf/</td><td></td></tr>
<tr><th>ng</th><td>http://www.nira.org.ng/</td><td>Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny8 with Suhosin-Patch mod_python/3.3.1 Python/2.5.2 mod_ssl/2.2.9 OpenSSL/0.9.8g mod_perl/2.0.4 Perl/v5.10.0</td></tr>
<tr><th>ni</th><td>http://www.nic.ni</td><td> </td></tr>
<tr><th>nl</th><td>http://www.domain-registry.nl/</td><td>Apache</td></tr>
<tr><th>nn</th><td></td><td></td></tr>
<tr><th>no</th><td>http://www.norid.no</td><td>Apache/2.2.16 (Debian) PHP/5.3.3-7+squeeze14 with Suhosin-Patch mod_ssl/2.2.16 OpenSSL/0.9.8o mod_wsgi/3.3 Python/2.6.6 mod_apreq2-20090110/2.7.1 mod_perl/2.0.4 Perl/v5.10.1</td></tr>
<tr><th>np</th><td>http://www.mos.com.np</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>nr</th><td>http://www.cenpac.net.nr</td><td>Apache/2.0.54 (Mandriva Linux/PREFORK-13mdk)</td></tr>
<tr><th>ns</th><td></td><td></td></tr>
<tr><th>nu</th><td>http://www.nunames.nu/</td><td>Microsoft-IIS/5.0</td></tr>
<tr><th>nz</th><td>http://www.dnc.org.nz/</td><td>nginx</td></tr>
<tr><th>om</th><td>http://www.omantel.net.om</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>pa</th><td>http://www.nic.pa/</td><td>Apache/2.0.49 (Unix) mod_ssl/2.0.49 OpenSSL/0.9.7l PHP/4.3.3</td></tr>
<tr><th>pe</th><td>http://www.nic.pe</td><td>Apache</td></tr>
<tr><th>pf</th><td></td><td></td></tr>
<tr><th>pg</th><td></td><td></td></tr>
<tr><th>ph</th><td>http://dot.ph</td><td>nginx/1.0.14</td></tr>
<tr><th>pk</th><td>http://www.pknic.net.pk/</td><td>Apache/2.0.52 (Red Hat)</td></tr>
<tr><th>pl</th><td>http://www.dns.pl/english/</td><td>Apache</td></tr>
<tr><th>pm</th><td>http://www.nic.pm/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5 mod_perl/2.0.4 Perl/v5.8.8</td></tr>
<tr><th>pn</th><td>http://www.government.pn/PnRegistry/PnRegistry.htm</td><td>Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny4 with Suhosin-Patch</td></tr>
<tr><th>pr</th><td>http://www.nic.pr</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>ps</th><td>http://www.nic.ps</td><td>Apache/2.0.52 (CentOS)</td></tr>
<tr><th>pt</th><td>http://www.dns.pt/</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>pw</th><td>http://www.registry.pw</td><td>nginx/0.8.55</td></tr>
<tr><th>py</th><td>http://www.nic.py</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>qa</th><td>http://domains.qa</td><td>Apache/2.2.3 (Red Hat)</td></tr>
<tr><th>re</th><td>http://www.nic.re/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5 mod_perl/2.0.4 Perl/v5.8.8</td></tr>
<tr><th>ro</th><td>http://www.rotld.ro/</td><td></td></tr>
<tr><th>rs</th><td>http://www.rnids.rs</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>ru</th><td>http://www.cctld.ru/en</td><td>Apache/2.2.3 (CentOS)</td></tr>
<tr><th>rw</th><td>http://www.ricta.org.rw</td><td>Apache/2.2.22 (Ubuntu)</td></tr>
<tr><th>sa</th><td>http://www.nic.net.sa/</td><td>Apache</td></tr>
<tr><th>sb</th><td>http://www.nic.net.sb/</td><td>Apache</td></tr>
<tr><th>sc</th><td>http://www.nic.sc</td><td>Microsoft-IIS/6.0</td></tr>
<tr><th>sd</th><td>http://www.isoc.sd</td><td>Apache</td></tr>
<tr><th>se</th><td>http://www.iis.se</td><td>Apache/2.2.14 (Ubuntu) PHP/5.3.2-1ubuntu4.18 with Suhosin-Patch mod_ssl/2.2.14 OpenSSL/0.9.8k</td></tr>
<tr><th>sg</th><td>http://www.sgnic.sg</td><td>Apache/2.2.14 (Unix) PHP/5.2.11 mod_ssl/2.2.14 OpenSSL/0.9.8e mod_perl/2.0.4 Perl/v5.8.9</td></tr>
<tr><th>sh</th><td>http://www.nic.sh/</td><td>Apache/2.4.3 (Unix) OpenSSL/1.0.1c</td></tr>
<tr><th>si</th><td>http://www.registry.si/</td><td>Apache</td></tr>
<tr><th>sj</th><td>http://www.norid.no/domenenavnbaser/bv-sj.html</td><td>Apache/2.2.16 (Debian) PHP/5.3.3-7+squeeze14 with Suhosin-Patch mod_ssl/2.2.16 OpenSSL/0.9.8o mod_wsgi/3.3 Python/2.6.6 mod_apreq2-20090110/2.7.1 mod_perl/2.0.4 Perl/v5.10.1</td></tr>
<tr><th>sk</th><td>http://www.sk-nic.sk</td><td>nginx/1.2.2</td></tr>
<tr><th>sl</th><td>http://www.nic.sl</td><td>Apache/2.2.3 (Debian) PHP/4.4.4-8+etch4 mod_ssl/2.2.3 OpenSSL/0.9.8c mod_perl/2.0.2 Perl/v5.8.8</td></tr>
<tr><th>sm</th><td>http://www.nic.sm</td><td>Apache</td></tr>
<tr><th>sn</th><td>http://www.nic.sn</td><td>Apache</td></tr>
<tr><th>so</th><td>http://www.nic.so/</td><td>Apache</td></tr>
<tr><th>sr</th><td>http://www.register.sr/</td><td>Apache/2.2.16 (Debian)</td></tr>
<tr><th>st</th><td>http://www.nic.st</td><td>nginx/0.7.62</td></tr>
<tr><th>su</th><td></td><td></td></tr>
<tr><th>sv</th><td>http://www.svnet.org.sv</td><td>Apache/2.2.13 (Linux/SUSE)</td></tr>
<tr><th>sx</th><td>http://registry.sx</td><td>Apache</td></tr>
<tr><th>sy</th><td>http://tld.sy</td><td>Apache</td></tr>
<tr><th>sz</th><td>http://www.sispa.org.sz/</td><td>Apache/2.0.44 (Unix) PHP/4.4.0</td></tr>
<tr><th>tc</th><td>http://www.adamsnames.tc/</td><td>Apache</td></tr>
<tr><th>td</th><td>http://www.sotel.td</td><td></td></tr>
<tr><th>tf</th><td>http://www.nic.tf/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5 mod_perl/2.0.4 Perl/v5.8.8</td></tr>
<tr><th>tg</th><td>http://www.nic.tg</td><td>Apache/2.2.23 (Unix) mod_ssl/2.2.23 OpenSSL/1.0.0-fips mod_auth_passthrough/2.1 mod_bwlimited/1.4 FrontPage/5.0.2.2635</td></tr>
<tr><th>th</th><td>http://www.thnic.co.th</td><td>Apache/2.2.21 (FreeBSD) mod_ssl/2.2.21 OpenSSL/0.9.8q DAV/2 PHP/5.3.8 with Suhosin-Patch</td></tr>
<tr><th>tj</th><td>http://www.nic.tj</td><td>thttpd</td></tr>
<tr><th>tk</th><td>http://www.dot.tk</td><td>Apache</td></tr>
<tr><th>tl</th><td>http://www.nic.tl</td><td>Apache</td></tr>
<tr><th>tm</th><td>http://www.nic.tm/</td><td>Apache/2.4.3 (Unix) OpenSSL/1.0.1c</td></tr>
<tr><th>tn</th><td>http://whois.ati.tn</td><td>Apache/2.2.16 (Debian)</td></tr>
<tr><th>to</th><td>http://www.tonic.to/</td><td>Apache</td></tr>
<tr><th>tp</th><td>http://www.nic.tp</td><td>Apache</td></tr>
<tr><th>tr</th><td>http://www.nic.tr</td><td>Apache</td></tr>
<tr><th>tt</th><td>http://www.nic.tt</td><td>Apache/2.4.3 (Unix) OpenSSL/1.0.0-fips</td></tr>
<tr><th>tv</th><td>http://www.tv</td><td>Apache</td></tr>
<tr><th>tw</th><td>http://rs.twnic.net.tw</td><td>Apache/2.0.59 (Unix) mod_ssl/2.0.59 OpenSSL/0.9.8g</td></tr>
<tr><th>tz</th><td>http://www.tznic.or.tz/</td><td>Apache/2.2.14 (Ubuntu)</td></tr>
<tr><th>ua</th><td>http://hostmaster.ua/</td><td>nginx</td></tr>
<tr><th>ug</th><td>http://www.registry.co.ug</td><td>Apache/2.2.21 (FreeBSD) DAV/2 mod_wsgi/2.8 Python/2.7.2 PHP/5.3.8 with Suhosin-Patch mod_ssl/2.2.21 OpenSSL/0.9.8q</td></tr>
<tr><th>uk</th><td>http://www.nic.uk/</td><td>Apache</td></tr>
<tr><th>us</th><td>http://www.nic.us</td><td>Apache</td></tr>
<tr><th>uy</th><td>http://www.nic.org.uy/</td><td>Apache/2.2.3 (Debian) mod_jk/1.2.18</td></tr>
<tr><th>uz</th><td>http://www.cctld.uz/</td><td></td></tr>
<tr><th>va</th><td></td><td></td></tr>
<tr><th>vc</th><td></td><td></td></tr>
<tr><th>ve</th><td>http://www.nic.ve/</td><td>Apache</td></tr>
<tr><th>vg</th><td>http://www.adamsnames.tc/</td><td>Apache</td></tr>
<tr><th>vi</th><td>http://www.nic.vi</td><td>nginx</td></tr>
<tr><th>vn</th><td>http://www.vnnic.net.vn</td><td>VNNIC IDN Proxy 2</td></tr>
<tr><th>vu</th><td>http://www.vunic.vu</td><td>Apache/2.0.49 (Unix)</td></tr>
<tr><th>wf</th><td>http://www.nic.wf/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5 mod_perl/2.0.4 Perl/v5.8.8</td></tr>
<tr><th>ws</th><td>http://www.website.ws</td><td>nginx/1.2.4</td></tr>
<tr><th>ye</th><td></td><td></td></tr>
<tr><th>yt</th><td>http://www.nic.yt/</td><td>Apache/2.2.3 (Red Hat) DAV/2 mod_ssl/2.2.3 OpenSSL/0.9.8e-fips-rhel5 mod_perl/2.0.4 Perl/v5.8.8</td></tr>
<tr><th>za</th><td>http://www.zadna.org.za/</td><td>Apache/2.2.22 (Ubuntu)</td></tr>
<tr><th>zm</th><td></td><td></td></tr>
<tr><th>zw</th><td></td><td></td></tr>
</table>
