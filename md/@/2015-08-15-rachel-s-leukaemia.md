---
dw:
  anum: 181
  eventtime: "2015-08-15T11:27:00Z"
  itemid: 425
  logtime: "2015-08-15T10:27:05Z"
  props:
    commentalter: 1491292410
    import_source: livejournal.com/fanf/135958
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/108981.html"
format: html
lj:
  anum: 22
  can_comment: 1
  ditemid: 135958
  event_timestamp: 1439638020
  eventtime: "2015-08-15T11:27:00Z"
  itemid: 531
  logtime: "2015-08-15T10:27:05Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 11
  url: "https://fanf.livejournal.com/135958.html"
title: "Rachel's leukaemia"
...

<p>Rachel has been in hospital since Monday, and on Thursday <a href="http://rmc28.dreamwidth.org/595484.html">they told her she has Leukaemia</a>, fortunately a form that is usually curable.

<p>She started chemotherapy on Thursday evening, and it is very rough, so she is not able to have visitors right now. We'll let you know when that changes, but we expect that the side-effects will get worse for a couple of weeks.

<p>To keep her spirits up, she would greatly appreciate small photos of cute children/animals or beautiful landscapes. Send them by email to rmcf@cb4.eu or on Twitter to @rmc28, or you can send small poscards to <a href="http://www.cuh.org.uk/for-patients/ward-information/all-wards/ward-d6-haematology-and-neurology">Ward D6 at Addenbrookes</a>.

<p>Flowers are not allowed on the ward, and no food gifts please because nausea is a big problem. If you want to send a gift, something small and pretty and/or interestingly tactile is suitable.

<p>Rachel is benefiting from services that rely on donations, so you might also be about to help by <a href="http://www.blood.co.uk/giving-blood/who-can-give-blood/">giving blood</a> - for instance you can <a href="http://www.cuh.org.uk/addenbrookes/services/clinical/blood_service/blood_service_index.html">donate at Addenbrookes</a>. Or you might <a href="http://www.macmillan.org.uk/donate/">donate to Macmillan Cancer Support</a>.

<p>And, if you have any niggling health problems, or something weird is happening or getting worse, do get it checked out. Rachel's leukaemia came on over a period of about three weeks and would have been fatal in a couple of months if untreated.
