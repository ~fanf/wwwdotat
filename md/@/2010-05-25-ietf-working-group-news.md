---
format: html
lj:
  anum: 231
  can_comment: 1
  ditemid: 107239
  event_timestamp: 1274809860
  eventtime: "2010-05-25T17:51:00Z"
  itemid: 418
  logtime: "2010-05-25T17:52:09Z"
  props:
    personifi_tags: "8:40,31:20,1:20,20:40,9:20,nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/107239.html"
title: IETF working group news...
...

<p>Once upon a time there was <a href="http://www.ietf.org/wg/concluded/cat.html">the CAT working group</a> whose name was an abbreviation for "Common Authentication Technology". It produced IETF specifications for GSS-API.</p>

<p>It was succeeded by <a href="http://datatracker.ietf.org/wg/kitten/">the kitten working group</a>, which has been working on the next-generation GSS-API specifications.</p>

<p>There is now a proposal for <a href="http://www.ietf.org/mail-archive/web/kitten/current/msg01780.html">a MOGGIES working group</a>, which will work on Mechanisms Of Generating Generically Interoperable & Effective Security.</p>
