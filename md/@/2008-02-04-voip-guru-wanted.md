---
dw:
  anum: 124
  eventtime: "2008-02-04T13:10:00Z"
  itemid: 320
  logtime: "2008-02-04T13:10:22Z"
  props:
    commentalter: 1491292350
    import_source: livejournal.com/fanf/82490
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/82044.html"
format: casual
lj:
  anum: 58
  can_comment: 1
  ditemid: 82490
  event_timestamp: 1202130600
  eventtime: "2008-02-04T13:10:00Z"
  itemid: 322
  logtime: "2008-02-04T13:10:22Z"
  props:
    verticals_list: life
  reply_count: 1
  url: "https://fanf.livejournal.com/82490.html"
title: VoIP guru wanted
...

These job ads seem to arrive like buses. Anyway, as has been <a href="http://www.theregister.co.uk/2008/01/08/cambridge_uni_voip/">widely reported</a>, we're replacing our old PABX with VoIP, and we're looking for a "<a href="http://www.cam.ac.uk/cgi-bin/wwwnews?grp=ucam.jobs.offered&art=1338">Communications Systems Manager</a>". I guess this is to fill the gaping technical void in the VoIP project, so it would be a sysadmin rather than a management role. We also need someone who can clearly communicate technical requirements and options to IT staff around the University who are responsible for the edge networks to which the phones and computers connect. I expect the post will be listed on the usual jobs page RSN.
