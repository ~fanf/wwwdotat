---
dw:
  anum: 195
  eventtime: "2006-11-16T18:26:00Z"
  itemid: 266
  logtime: "2006-11-16T18:26:21Z"
  props:
    commentalter: 1491292365
    import_source: livejournal.com/fanf/68586
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/68291.html"
format: html
lj:
  anum: 234
  can_comment: 1
  ditemid: 68586
  event_timestamp: 1163701560
  eventtime: "2006-11-16T18:26:00Z"
  itemid: 267
  logtime: "2006-11-16T18:26:21Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/68586.html"
title: The recent increase in spam
...

<p>A lot of our users have been asking us about the current spam problem, so I sent the following to our computing support staff mailing list and the <tt>ucam.comp.mail</tt> newsgroup. I thought it would be worth posting here too.</p>

<p>The volume of spam we are seeing has more than doubled since the summer,
from about 15 messages blocked per second to over 35, and the amount of
spam that gets past the blocks has increased accordingly. This is really
unprecedented: in the preceding two years, the volume of blocked spam
increased gradually from about 10 to about 15 per second. For comparison, we're handling
about 7 messages per second, which includes internal email (3 per second)
as well as non-blocked spam and legitimate email from outside the
University.</p>

<p>That is, at least 90% of the 3.5 million messages offered to us each day
from outside the University are spam.</p>

<p>It is a coincidence that this increase kicked off at about the start
of term: this is a global problem that has been widely noted in the IT
press. Unfortunately the current flavours of spam are difficult for
our second-level filters (SpamAssassin) to handle because it doesn't
have many recognizable features, such as URLs for criminal web sites,
etc. We are updating SpamAssassin when new releases come out, which is
roughly monthly at the moment.</p>
