---
dw:
  anum: 243
  eventtime: "2013-11-13T00:24:00Z"
  itemid: 400
  logtime: "2013-11-13T00:24:02Z"
  props:
    commentalter: 1491292405
    import_source: livejournal.com/fanf/129569
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/102643.html"
format: html
lj:
  anum: 33
  can_comment: 1
  ditemid: 129569
  event_timestamp: 1384302240
  eventtime: "2013-11-13T00:24:00Z"
  itemid: 506
  logtime: "2013-11-13T00:24:02Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 15
  url: "https://fanf.livejournal.com/129569.html"
title: "Temporum: secure time: a paranoid fantasy"
...

<p>Imagine that...

<p>Secure NTP is an easy-to-use and universally deployed protocol extension...

<p>The NTP pool is dedicated to providing accurate time from anyone to
everyone, securely...

<p>NIST, creators of some of the world's best clocks and keepers of
official time for the USA, decide that the NTP pool is an excellent
project which they would like to help. They donatate machines and
install them around the world and dedicate them to providing time as
part of the NTP pool. Their generous funding allows them to become a
large and particularly well-connected proportion of the pool.

<p>In fact NIST is a sock-puppet of the NSA. Their time servers are
modified so that they are as truthful and as accurate as possible to
everyone, <i>except</i> those who the US government decides they do
not like.

<p>The NSA has set up a system dedicated to replay attacks. They cause
occasional minor outages and screwups in various cryptographic systems
- certificate authorities, DNS registries - which seem to be brief and
benign when they happen, but no-one notices that the bogus invalid
certificates and DNS records all have validity periods covering a
particular point in time.

<p>Now the NSA can perform a targeted attack, in which they persuade the
victim to reboot, perhaps out of desperation because nothing works and
they don't understand denial-of-service attacks. The victim's machine
reboots, and it tries to get the time from the NTP pool. The NIST
sock-puppet servers all lie to it. The victim's machine believes the
time is in the NSA replay attack window. It trustingly fetches some
crucial "software update" from a specially-provisioned malware server,
which both its DNSSEC and X.509 PKIs say is absolutely kosher. It
becomes comprehensively pwned by the NSA.

<p>How can we provide the time in a way that is secure against this
attack?

<p>(<a href="http://fanf.livejournal.com/128861.html">Previously</a>,
<a href="http://fanf.livejournal.com/129371.html">previously</a>)
