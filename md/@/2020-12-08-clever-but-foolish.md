---
dw:
  anum: 166
  eventtime: "2020-12-08T16:03:00Z"
  itemid: 524
  logtime: "2020-12-08T16:08:51Z"
  props:
    commentalter: 1607773809
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/134310.html"
format: md
...

Clever but foolish
==================

Having tried to make practical use of my multivector multiply, I'm
adding a health warning to [my notes on vanishing
zeroes](https://dotat.at/@/2020-12-06-vanishing-zeroes-for-geometric-algebra-in-rust.html).
_(This is my shamefaced grin.)_

There's a general rule with Rust that if the compiler is making things
hard for you, it's usually right, and you should pay attention.

The problem with my type system hackery is kind of right there in my
description of what was difficult about making it work: the
requirements in the trait bounds have to be spelled out in full.

I had trouble with the enormous trait bounds that I needed in order to
use the small `Zero` and `Num` overloads; the same trouble will occur,
but bigger, for anyone who tries to use the overloaded multivector
multiply in a generic way.

This means that application code will only be tolerable if it uses
concrete types at function boundaries, and avoids generic overloading
except to support type inference within functions. This is not very
far from what I had in mind, but less flexible.

It's worse for mid-level code which provides higher-level multivecor
operations. I wanted to combine generic multivector primitives into
generic higher-level operations, but the types make this too awful to
contemplate. It might be possible to rescue it by extending the
wide-and-shallow approach to these operations, i.e. define them all in
the same style as multiplication.

So it's sad and somewhat embarrassing that it didn't work out, but I
did learn quite a lot about Rust in the process :-)

Anyway, after reflecting on my mistakes, I'm putting this aside for
now, and I'm circling around to vector in on my 3D graphics project
from another angle.
