---
dw:
  anum: 29
  eventtime: "2003-04-07T18:24:00Z"
  itemid: 14
  logtime: "2003-04-07T11:25:47Z"
  props:
    commentalter: 1491292303
    current_music: Bob Marley
    import_source: livejournal.com/fanf/3783
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/3613.html"
format: casual
lj:
  anum: 199
  can_comment: 1
  ditemid: 3783
  event_timestamp: 1049739840
  eventtime: "2003-04-07T18:24:00Z"
  itemid: 14
  logtime: "2003-04-07T11:25:47Z"
  props:
    current_music: Bob Marley
  reply_count: 1
  url: "https://fanf.livejournal.com/3783.html"
title: Doing a talk
...

Tomorrow evening I'm talking to <a href="http://www.sage-wise.org/">SAGE-WISE</a> about our email setup. The only really significant feedback that I had from the first time I did  a talk was that I needed more diagrams -- in fact any diagrams at all would have been good. So I've been having some fun doing the diagrams in one of the more geeky ways possible.

I'm doing the talk using MagicPoint which in the right hands look as "professional" as your typical PowerPoint clone. However since it's written by mad Japanese people you describe your presentation in a text file rather than by faffing with a GUI. This suits me much better since I can keep the file in CVS in a sensible way. Since I also like w3m, perhaps I have a mad Japanese in my subconscious. 

Anyway I wanted a diagram tool with a similar attitude. XFig has pissed me off mightily in the past, perhaps because it is so much crapper than Acorn's !Draw and has somewhat different UI ideas. I'm not sure why -- perhaps because I like the look of the diagrams I have seen produced with it, and because I have been writing manual pages and on that basis like troff -- but I decided that Brian Kernighan's pic would be a good tool to use. I did have a bit of a fight with it on Saturday afternoon, but the problem with getting the diagram to display sensibly turned out to be mostly MagicPoint not automatically choosing a sensible scaling factor and using a black background when pic and ghostscript assume white.

But in the end the diagrams look good and were fun to produce. I'm particularly proud of my network cloud. You can see the current state of things on <a href="https://fanf2.user.srcf.net/hermes/doc/talks/2003-04-sage-wise/">my work web page</a>.
