---
format: html
lj:
  anum: 94
  can_comment: 1
  ditemid: 121694
  event_timestamp: 1339122060
  eventtime: "2012-06-08T02:21:00Z"
  itemid: 475
  logtime: "2012-06-08T01:21:11Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 7
  url: "https://fanf.livejournal.com/121694.html"
title: Blaming the spam victim
...

<p>A couple of weeks ago I read <a href="http://blogs.msdn.com/b/tzink/archive/2012/05/22/spammers-ruining-it-for-everyone.aspx">a blog post by Terry Zink titled "spammers ruining it for everyone"</a> which annoyed me. If I understand it correctly, Terry is a senior anti-spam person at Microsoft FrontBridge, dealing with their hosted Exchange service - nothing to do with Hotmail.</p>

<p>What annoyed me about his article was that it was blaming the victim. The spam recipients' natural reaction - to block mail from the site that spammed them - was, according to Terry, wrong. It wasn't Microsoft's fault that they spammed these victims: it was an "incident" with one of their customers. Perfectly normal, rather difficult to deal with, but Microsoft are not spammers so it is completely unfair to blame them and cause all this difficulty for their other customers.</p>

<p>Now I hesitate to say the following, because the juxtaposition belittles problems that are much more serious than spam. But I would not be so aware of the victim-blaming pattern of argument if I had not paid attention to the bad consequences that happen when complaints are easily dismissed by "oh, it was just a bit of fun" (no, it was sexual assault) or "sorry, mate, I didn't see you" (whoops! vehicular homicide). The second stage of blaming the victim is "you should dress more modestly" or "you should have bright lights and high-viz clothes" or "you should have better spam filters". Never mind the fact that the person responsible should not have allowed the bad thing to happen in the first place.</p>

<p>I had a discussion about Terry's blog post with some friends after the pub this evening. One of us was arguing in support of Microsoft's position - and more generally: he seemed to say it is wrong to blame a group for the bad behaviour of its members. Instead everyone should assess each individual they deal with separately, regardless of the reputation of others in the same group. The rest of us argued that you should encourage good people to improve the behaviour of their groups and avoid bad ones. Of course this counter-argument only works when the people suffering collateral damage have enough agency to improve or move - and that is the case for Microsoft's email services.</p>

<p>When we argued that people in a position of responsibility need to police bad behaviour, he brought up the vexed question of censorship and universal service obligations. Really this kind of argument is just a distraction unless the so-called censor actually has a monopoly on communications. If there is a market of comms providers (as there is for email) and you want signal rather than noise then <a href="http://lesswrong.com/lw/c1/wellkept_gardens_die_by_pacifism/">you have to moderate</a> bad behaviour - and even if you are being too harsh in your assessment that noise is unwanted, you aren't censoring it by making it go elsewhere.</p>

<p>Being a service provider is a moral quicksand. Your aim is to do a good job for your customers, but this <a href="http://www.npr.org/2012/05/01/151764534/psychology-of-fraud-why-good-people-do-bad-things">normal human imperative to be helpful</a> is sorely tried <a href="http://www.lessannoyingcrm.com/articles/262/What_we_did_when_a_patent_troll_asked_for_our_help">when one of your customers turns out to be despicable</a> - and not everyone can stand their ground. It is even harder if everyone around you acts as if bad behaviour is OK.</p>
