---
dw:
  anum: 222
  eventtime: "2006-11-23T18:17:00Z"
  itemid: 267
  logtime: "2006-11-23T18:17:40Z"
  props:
    commentalter: 1491292369
    hasscreened: 1
    import_source: livejournal.com/fanf/68755
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/68574.html"
format: html
lj:
  anum: 147
  can_comment: 1
  ditemid: 68755
  event_timestamp: 1164305820
  eventtime: "2006-11-23T18:17:00Z"
  itemid: 268
  logtime: "2006-11-23T18:17:40Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/68755.html"
title: new computer officer intro / machine room tour
...

<p>Next Wednesday (29th November) is going to be busy.</p>

<p>In the afternoon there will be a <a href="http://www-tus.csx.cam.ac.uk/techlink/workshops/">Techlinks meeting</a> to introduce new IT staff to the Computing Service. I'll be standing up briefly to say HELO on behalf of Mail Support, and so I have produced a <a href="https://fanf2.user.srcf.net/hermes/doc/talks/2006-11-techlinks/">"crib sheet"</a> which lists most of what we do so that I won't have to. I only have a few minutes so there won't be enough time to say anything substantial, and anyway it can wait until my full-length techlinks next term.</p>

<p>In the evening I'll be helping <a href="http://www-uxsup.csx.cam.ac.uk/~rjd4/">Bob</a> to give a tour of our <a href="https://fanf2.user.srcf.net/photos.html">computer room</a>. This is principally for students, but any interested member of the University is welcome. Email me for more info.</p>
