---
dw:
  anum: 244
  eventtime: "2016-05-06T02:36:00Z"
  itemid: 454
  logtime: "2016-05-06T01:36:30Z"
  props:
    commentalter: 1491292422
    import_source: livejournal.com/fanf/143489
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/116468.html"
format: html
lj:
  anum: 129
  can_comment: 1
  ditemid: 143489
  event_timestamp: 1462502160
  eventtime: "2016-05-06T02:36:00Z"
  itemid: 560
  logtime: "2016-05-06T01:36:30Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/143489.html"
title: A colophon for my link log
...

<p>The most common question I get asked about
<a href="http://dotat.at/:">my link log</a>
(<a href="http://dotaturls.livejournal.com">LJ version</a>) is</p>

<h1>How do you read all that stuff?!</h1>

<p>The answer is,</p>

<h1>I don't!</h1>

<p>My link log is basically my browser bookmarks, except public. (I don't
have any private bookmarks to speak of.) So, for it to be useful, a
link description should be reasonably explanatory and have enough
keywords that I can find it years later.</p>

<p>The links include things I have read and might want to re-read, things
I have not read but I think I should keep for future reference, things
I want to read soon, things that look interesting which I might read,
and things that are aspirational which I feel I ought to read but
probably never will.</p>

<p><i>(Edited to add)</i> I should also say that I might find an article
to be wrong or problematic, but it still might be
interesting enough to be worth logging - especially if I might want to
refer back to this wrong or problematic opinion for future discussion
or criticism. <i>(end of addition)</i></p>

<p>But because it is public, I also use it to tell people about links
that are cool, though maybe more ephemeral. This distorts the primary
purpose a lot.</p>

<p>It's my own miscellany or scrapbook, for me and for sharing.</p>

<h2>Failures</h2>

<p>A frustrating problem is that I sometimes see things which, at the
time, seem to be too trivial to log, but which later come up in
conversation and I can't cite my source because I didn't log it or
remember the details.</p>

<p>Similarly I sometimes fail to save links to unusually good blog
articles or mailing list messages, and I don't routinely keep my
own copies of either.</p>

<p>Frequently, my descriptions lack enough of the right synonym keywords
for me to find them easily. (I'm skeptical that I would be able to
think ahead well enough to add them if I tried.)</p>

<p>I make no effort to keep track of where I get links from. This is very
lazy, and very anti-social. I am sorry about this, but not sorry enough
to fix it, which makes me sorry about being too lazy to fix it. Sorry.</p>

<h2>Evolution</h2>

<p>What I choose to log changes over time. How I phrase the descriptions
also changes. (I frequently change the original title to add keywords
or to better summarize the point. My usual description template is
"Title or name: spoiler containing keywords.")</p>

<p>Increasingly in recent years I have tried to avoid the political
outrage of the day. I prefer to focus on positive or actionable things.</p>

<p>A lot (I think?) of the negative "OMG isn't this terrible" links on my
log recently are related to computer security or operations. They fall
into the "actionable" category by being cautionary tales: can I learn
from their mistakes? Please don't let me repeat them?</p>

<h2>Sources</h2>

<p>Mailing lists. I don't have a public list of the ones I subscribe to.
The tech ones are mail / DNS / network / time related - IETF,
operations, software I use, projects I contribute to, etc.</p>

<p>RSS/Atom feeds. I also use LJ as my feed reader (retro!) and sadly
they don't turn my feed list into a proper blog roll, but you might
find something marginally useful at
<a href="http://fanf.livejournal.com/profile?socconns=yfriends">http://fanf.livejournal.com/profile?socconns=yfriends</a></p>

<p>Hacker News. I use Colin Percival's <a href="http://www.daemonology.net/hn-daily/">Hacker News
Daily</a> to avoid the worst of it.
It is more miss than hit - a large proportion of the 10 "best" daily
links are about Silly Valley trivia or sophomoric politics. But when
HN has a link with discussion about something technical it can provide
several good related links and occasionally some well-informed
comments.</p>

<p><a href="https://ma.ttias.be/introducing-cron-weekly-a-new-weekly-newsletter-for-linux-sysadmins/">Mattias Geniar's cron.weekly tech newsletter</a>
is great. <a href="http://andrewducker.livejournal.com">Andrew Ducker's links</a> are great.</p>

<p><a href="https://twitter.com/fanf">Twitter</a>. I like its openness, the way it
is OK to follow someone who might be interesting. On Facebook that
would be creepy, on Linkedin that would be spammy.</p>
