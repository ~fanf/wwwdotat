---
dw:
  anum: 247
  eventtime: "2009-01-27T21:19:00Z"
  itemid: 372
  logtime: "2009-01-27T21:30:10Z"
  props:
    import_source: livejournal.com/fanf/96585
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/95479.html"
format: html
lj:
  anum: 73
  can_comment: 1
  ditemid: 96585
  event_timestamp: 1233091140
  eventtime: "2009-01-27T21:19:00Z"
  itemid: 377
  logtime: "2009-01-27T21:30:10Z"
  props:
    personifi_tags: "nterms:yes"
    verticals_list: life
  reply_count: 0
  url: "https://fanf.livejournal.com/96585.html"
title: Luck or judgment?
...

<p>In 2005 I developed a mathematical model for measuring average event rates which became the core of a new rate limiting feature for Exim. The model has a particularly useful property which I did not expect it to have and which I did not (until recently) fully understand.</p>

<h3>The model</h3>

<p>The central formula is the standard exponentially weighted moving average:</p>

<blockquote><p>
    r<sub>new</sub> = (1 &minus; a) &lowast; r<sub>inst</sub> + a &lowast; r<sub>old</sub>
</p></blockquote>

<p>We use this to calculate the new average rate from the instantaneous rate and the result of the previous average calculation. These rates are all measured in events per some configurable time period. The smoothing factor <i>a</i> is a value between 0 and 1 which determines how slowly the model forgets about past behaviour.</p>

<p>We calculate <i>r<sub>inst</sub></i> and <i>a</i> from the raw inputs to the formula, which are <i>p</i>, a time period configured by the postmaster, and <i>i</i>, the time interval between the previous event and this event. By dividing them we get</p>

<blockquote><p>
    r<sub>inst</sub> = p / i
</p></blockquote>

<p>In this formula, <i>p</i> determines the per time unit used for measuring rates, e.g. events per hour or events per day.</p>

<p>The exponentially weighted moving average is usually used to smooth samples that are measured at fixed intervals, in which case the smoothing factor, <i>a</i>, is also fixed. In our situation events occur at varying intervals, so the smoothing factor needs to be varied accordingly.</p>

<blockquote><p>
    a = e<sup>&minus;i / p</sup>
</p></blockquote>

<p>In this formula, <i>p</i> determines the smoothing period, i.e. the length of time it takes to forget 63% of past behaviour.</p>

<h3>A useful property</h3>

<p>When developing the model, I needed to understand how it reacts to changes in the rate of events. It's fairly simple to see that if the rate drops, the average decays exponentially towards the new rate. It's less clear what happens when the rate increases. A particular practical question is what happens if there's a sudden burst of messages? How much of the burst gets through before the average rate passes some configured maximum?</p>

<p>I did some trial computations and I found that when the interval is very small (i.e. the rate is high) the average rate increases by nearly one for each event (the smaller the interval, the closer to one). This means that the maximum rate is also the maximum burst size. How wonderfully simple! The postmaster can configure the model with two numbers, a maximum number of events per a time period, which also directly specifies the units of measurement, the smoothing period, and the maximum burst size.</p>

<p>(The maximum burst size is larger for slower senders, increasing to infinity for those sending below the maximum rate. However you don't have to be much above the maximum for your average to hit the limit within one period.)</p>

<p>This property produces a simple user interface, but I did not understand how it works. It's obvious that when <i>i</i> is small, <i>a &asymp; 1</i>, but it is not so clear why      
<i>(1 &minus; a) &lowast; r<sub>inst</sub> &asymp; 1</i>. It seems I landed directly on a mathematical sweet spot without the fumbling around that might have led me to understand the situation better.</p>

<h3>Arbitrary choices</h3>

<p>I made two choices when developing the model which at the time seemed arbitrary, but which both must be right to get the <i>max rate = burst size</i> property.</p>

<p>Firstly, I decided to re-use the configured period for two purposes: as the smoothing period and as the per time unit of rates. I could instead have made them independently configurable, but this seemed to give no benefits that compensated for the extra complexity. Alternatively, I could have used a fixed value instead of <i>p</i> in the formula for <i>r<sub>inst</sub></i>, but that seemed liable to be confusing or awkward, and to require more mental arithmetic to configure.</p>

<p>Secondly, I decided to use <i>e</i> as the base of the exponent. I could have used 2, in which case the smoothing period would have been a half life, or 10, so that 90% of past behaviour would be forgotten after one period. There seemed to be no clear way to choose, so I split the difference and went with <i>e</i> on the basis of mathematical superstition and because <tt>exp()</tt> has fewer arguments than <tt>pow()</tt>.</p>

<h3>How it works</h3>

<p>Looking back over my old notes this week, I had a revelation that the <i>max rate = burst size</i> property comes from the fact that the gradient of <i>e<sup>x</sup></i> is 1 when <i>x</i> is 0. To show why this is so, I first need to define a bit of notation:</p>

<blockquote><p>
    y &equiv; f(x) &equiv; e<sup>x</sup>
</p><p>
    &delta;y &equiv; f(x + &delta;x) &minus; f(x)
</p><p>
    &delta;x &equiv; &minus;i / p
</p></blockquote>

<p>This allows us to say, when <i>x</i> is zero and <i>&delta;x</i> is small,</p>

<blockquote><table><tr>
    <td></td><td>(1 &minus; a) &lowast; r<sub>inst</sub></td>
</tr><tr>
    <td> = </td><td>(1 &minus; e<sup>&minus;i/p</sup>) &lowast; p/i</td>
</tr><tr>
    <td> = </td><td>(1 &minus; e<sup>&delta;x</sup>) / &minus;&delta;x</td>
</tr><tr>
    <td> = </td><td>(e<sup>&delta;x</sup> &minus; 1) / &delta;x</td>
</tr><tr>
    <td> = </td><td>(e<sup>0 + &delta;x</sup> &minus; e<sup>0</sup>) / &delta;x</td>
</tr><tr>
    <td> = </td><td>(&thinsp;f(0 + &delta;x) &minus; f(0)&thinsp;) / &delta;x</td>
</tr><tr>
    <td> = </td><td>&delta;y / &delta;x</td>
</tr><tr>
    <td> &asymp; </td><td>dy / dx</td>
</tr><tr>
    <td> = </td><td>e<sup>x</sup></td>
</tr><tr>
    <td> = </td><td>1</td>
</tr></table></blockquote>

<p>Sweet.</p>
