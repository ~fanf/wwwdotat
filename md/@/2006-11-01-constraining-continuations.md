---
dw:
  anum: 14
  eventtime: "2006-11-01T02:26:00Z"
  itemid: 260
  logtime: "2006-11-01T02:26:54Z"
  props:
    commentalter: 1491292336
    import_source: livejournal.com/fanf/66987
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/66574.html"
format: html
lj:
  anum: 171
  can_comment: 1
  ditemid: 66987
  event_timestamp: 1162347960
  eventtime: "2006-11-01T02:26:00Z"
  itemid: 261
  logtime: "2006-11-01T02:26:54Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/66987.html"
title: Constraining continuations
...

<p>So last week <a href="http://fanf.livejournal.com/66407.html">I wrote about some simple "desugaring" transformations</a> that turn a fairly conventional block-structured programming language into the call-by-value lambda calculus with some small extensions. The main problem is that I relied on first-class continuations. This has at least two undesirable consequences:
<ul><li>
First-class continuations are <a href="http://en.wikipedia.org/wiki/Lambda_Papers">the <b>goto</b> of functional programming</a>. Actually, that understates the amount of unconstrained tangle that they can unleash - they are far more powerful and dangerous than <b>goto</b>. So one can argue that programmers are better off avoiding first-class continuations and using more constrained control flow, much like we are advised to shun <b>goto</b> in favour of <b>break</b>, <b>return</b>, and exceptions.
</li><li>
Continuations can't easily be implemented with a conventional linear stack. When you return a normal value up the stack, the top stack frame(s) can be freed, but if you return a continuation up the stack, they cannot: calling the continuation re-activates these frames. The stack becomes a <a href="http://images.google.com/images?q=saguaro">cactus</a>. A common solution to this problem is to allocate activation frames on the heap, but this seriously stresses the garbage collector and harms locality: whereas a stack works up and down over the same small area of memory, the heap blasts forever upwards.
</ul></p>

<p>Fortunately there's a nice solution: <a href="http://www-static.cc.gatech.edu/~shivers/papers/mrlc-jfp.pdf">multi-return function calls</a>. The idea is to separate continuation arguments from normal arguments, and restrict the continuations so that activation frames are still used in a normal stack-like LIFO manner. What's really neat is that it preserves a lot of the the power of first-class continuations.</p>

<p>However, the desugaring transforms become more complicated. The scope of the multi-return continuations is restricted so that you cannot (for example) return from a function by invoking its continuation from an inner block, because the continuation is not available there. Instead you have to add plumbing to pass the early-return continuation into the block along with the block's normal sequential continuation. This plumbing ends up being rather like the standard continuation-passing-style transformation described in the lambda-the-ultimate papers linked to above, but with an extra continuation for each early exit.</p>

<p>This extra-continuation plumbing is also similar to a standard implementation technique for exceptions (which I called "non-trivial" last week, but isn't actually hard to understand). As well as the sequential continuation that is passed around in CPS, you pass around an exception handler continuation which is invoked in order to throw an exception. This is very similar to implementing exceptions in C with <tt>setjmp</tt> and <tt>longjmp</tt>, except in C you can keep the current exception handler in a global variable instead of passing it around.</p>

<p>The downside of these techniques is that the plumbing is an overhead. However, it is "pay-as-you-go", in that you only need the plumbing if you use early exits from blocks. (It's less easy to avoid the exception plumbing since exceptions can usually be thrown anywhere.) By contrast, the more complicated infrastructure needed for first-class continuations affects the performance of the whole run-time system.</p>

<p>(The mathematics in the typing of these constructions relates directly to their balance of practicality and power. Constrained continuations like multi-return functions and exceptions are usually <a href="http://www.cs.bham.ac.uk/~hxt/research/LinUC-CW01.pdf">used linearly</a> and have an <a href="http://en.wikipedia.org/wiki/Intuitionistic_logic">intuitionistic (constructive)</a> typing. Call-with-current-continuation, however, has a <a href="http://www.cs.bham.ac.uk/~hxt/research/HOSC-double-barrel.pdf">classical typing</a> which is not expressible in most practical type systems. This kind of direct relationship between theory and practice is one of the beauties of computer science.)</p>
