---
dw:
  anum: 72
  eventtime: "2017-01-11T20:54:00Z"
  itemid: 475
  logtime: "2017-01-11T20:54:33Z"
  props:
    commentalter: 1491292427
    import_source: livejournal.com/fanf/148947
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/121672.html"
format: html
lj:
  anum: 211
  can_comment: 1
  ditemid: 148947
  event_timestamp: 1484168040
  eventtime: "2017-01-11T20:54:00Z"
  itemid: 581
  logtime: "2017-01-11T20:54:33Z"
  props:
    og_image: "http://l-files.livejournal.net/og_image/936728/581?v=1484210368"
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/148947.html"
title: Even more compact encoding of the leap seconds list
...

<p>Following on from <a href="http://fanf.livejournal.com/148164.html">my recent item about the leap seconds list</a> I have come up with a better binary encoding which is half the size of my previous attempt. (Compressing it with deflate no longer helps.)</p>

<p>Here's the new binary version of the leap second list, 15 bytes
displayed as hexadecimal in network byte order. I have not updated it
to reflect the latest Bulletin C which was promulgated this week, 
because the official leap second lists have not yet been updated.</p>

<pre><code>    001111111211343 12112229D5652F4
</code></pre>

<p>This is a string of 4-bit nybbles, upper nybble before lower nybble of
each byte.</p>

<p>If the value of this nybble V is less than 0x8, it is treated as a
pair of nybbles 0x9V. This abbreviates the common case.</p>

<p>Otherwise, we consider this nybble Q and the following nybble V as a
pair 0xQV.</p>

<p>Q contains four flags,</p>

<pre><code>    +-----+-----+-----+-----+
    |  W  |  M  |  N  |  P  |
    +-----+-----+-----+-----+
</code></pre>

<p>W is for width:</p>

<ul>
<li>W == 1 indicates a QV pair in the string.</li>
<li>W == 0 indicates this is a bare V nybble.</li>
</ul>

<p>M is the month multiplier:</p>

<ul>
<li>M == 1 indicates the nybble V is multiplied by 1</li>
<li>M == 0 indicates the nybble V is multiplied by 6</li>
</ul>

<p>NP together are NTP-compatible leap indicator bits:</p>

<ul>
<li>NP == 00 indicates no leap second</li>
<li>NP == 01 == 1 indicates a positive leap second</li>
<li>NP == 10 == 2 indicates a negative leap second</li>
<li>NP == 11 == 3 indicates an unknown leap second</li>
</ul>

<p>The latter is equivalent to the ? terminating the text version.</p>

<p>The event described by NP occurs a number of months after the previous
event, given by</p>

<pre><code>    (M ? 1 : 6) * (V + 1).
</code></pre>

<p>That is, a 6 month gap can be encoded as M=1, V=5 or as M=0, V=0.</p>

<p>The "no leap second" option comes into play when the gap between leap
seconds is too large to fit in 4 bits. In this situation you encode a
number of "no leap second" gaps until the remaining gap fits.</p>

<p>The recommended way to break up long gaps is as follows. Gaps up to 16
months can be encoded in one QV pair. Gaps that are a multiple of 6
months long should be encoded as a number of 16*6 month gaps, followed
by the remainder. Other gaps should be rounded down to a whole
number of years and encoded as a X*6 month gap, which is followed by a
gap for the remaining few months.</p>

<p>To align the list to a whole number of bytes, add a redundant 9 nybble
to turn a bare V nybble into a QV pair.</p>

<p>In the current leap second list, every gap is encoded as a single V
nybble, except for the 84 month gap which is encoded as QV = 0x9D, and
the last 5 months encoded as QV = 0xF4.</p>

<p>Once the leap second lists have been updated, the latest Bulletin C
will change the final nybble from 4 to A.</p>

<p>The code now includes decoding as well as encoding functions, and a little fuzz tester to ensure they are consistent with each other.
<a href="http://dotat.at/cgi/git/leapseconds.git">http://dotat.at/cgi/git/leapseconds.git</a>.</p>
