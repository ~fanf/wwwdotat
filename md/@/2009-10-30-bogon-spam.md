---
format: casual
lj:
  anum: 143
  can_comment: 1
  ditemid: 103567
  event_timestamp: 1256913360
  eventtime: "2009-10-30T14:36:00Z"
  itemid: 404
  logtime: "2009-10-30T15:59:36Z"
  props:
    personifi_tags: "8:53,31:7,32:7,1:53,19:7,20:15,9:30,nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/103567.html"
title: bogon spam
...

<p>Someone from Craigslist <a href="http://www.merit.edu/mail.archives/nanog/msg01637.html">posted to NANOG</a> saying they were suffering from a lot of spam from unallocated IP address space. So I wrote <a href="https://fanf2.user.srcf.net/hermes/conf/exim/sbin/bogons">a program</a> which downloads the RIR allocation statistics tables, compiles a list of allocated /24 blocks, and scans Exim's logs for connections from unallocated space. It turns out we are not suffering from a lot of spam from unallocated IP address space - we get on average less than one bogon connection each day.</p>
