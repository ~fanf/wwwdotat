---
dw:
  anum: 111
  eventtime: "2003-05-22T16:32:00Z"
  itemid: 22
  logtime: "2003-05-22T08:36:19Z"
  props:
    current_moodid: 31
    import_source: livejournal.com/fanf/5773
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/5743.html"
format: casual
lj:
  anum: 141
  can_comment: 1
  ditemid: 5773
  event_timestamp: 1053621120
  eventtime: "2003-05-22T16:32:00Z"
  itemid: 22
  logtime: "2003-05-22T08:36:19Z"
  props:
    current_moodid: 31
  reply_count: 0
  url: "https://fanf.livejournal.com/5773.html"
title: MailScanner
...

Well, it's finally running on ppsw, scoring messages for spamminess. No virus filtering yet, that'll come after the mega talk I have to prepare for Wednesday. Aparrently it's the most heavily-subscribed TechLinks talk this year, which is a bit scary -- I haven't talked in front of a big audience before. Even so it should go OK, and I'll probably feel better about it once the preparation has been done.
