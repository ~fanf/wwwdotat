---
dw:
  anum: 105
  eventtime: "2018-05-09T10:34:00Z"
  itemid: 492
  logtime: "2018-05-09T09:36:03Z"
  props:
    commentalter: 1525993257
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1525997452
  url: "https://fanf.dreamwidth.org/126057.html"
format: html
lj:
  anum: 33
  can_comment: 1
  ditemid: 152865
  event_timestamp: 1525862220
  eventtime: "2018-05-09T10:37:00Z"
  itemid: 597
  logtime: "2018-05-09T09:37:24Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/152865.html"
title: How to test a web server before pointing the DNS at it
...

<p>A large amount of my support work is helping people set up web sites.
It's time-consuming because we often have to co-ordinate between three
or more groups: typically University IT (me and colleagues), the
non-technical owner of the web site, and some commercial web
consultancy. And there are often problems, so the co-ordination
overhead makes them even slower to fix.</p>

<p>When moving an existing web site, I check that the new web server will
work before I update the DNS - it's embarrassing if they have an
outage because of an easy-to-avoid cockup, and it's good if we can
avoid a panic.</p>

<p>I use a little wrapper around <code>curl --resolve</code> for testing. This makes
<code>curl</code> ignore the DNS and talk to the web server I tell it to, but it
still uses the new host name when sending the Host: header and TLS SNI
and doing certificate verification.</p>

<p>You use the script like:</p>

<pre><code>    curlto &lt;target server&gt; [curl options] &lt;url&gt;
</code></pre>

<p>e.g.</p>

<pre><code>    curlto ucam-ac-uk.csi.cam.ac.uk -LI http://some.random.name
</code></pre>

<p>This needs a bit of scripting because the <code>curl --resolve</code> option is a
faff: you need to explicitly map the URL hostname to all the target IP
addresses, and you need to repeat the mapping for both http and https.</p>

<p>Here's the script:</p>

<pre><code>    #!/usr/bin/perl

    use warnings;
    use strict;

    use Net::DNS;

    my $dns = new Net::DNS::Resolver;

    sub addrs {
        my $dn = shift;
        my @a;
        for my $t (qw(A AAAA)) {
            my $r = $dns-&gt;query($dn, $t) or next;
            push @a, map $_-&gt;address, grep { $_-&gt;type eq $t } $r-&gt;answer;
        }
        die "curlto: could not resolve $dn\n" unless @a;
        return @a;
    }

    unless (@ARGV &gt; 1) {
        die "usage: curlto &lt;target server&gt; [curl options] &lt;url&gt;\n";
    }

    my $url = $ARGV[-1];
    $url =~ m{^(https?://)?([a-z0-9.-]+)}
        or die "curlto: could not parse hostname in '$url'\n";
    my $name = $2;

    my @addr = shift;
    @addr = addrs @addr unless $addr[0] =~ m{^([0-9.]+|[0-9a-f:]+)$};
    for my $addr (@addr) {
        unshift @ARGV, '--resolv', "$name:80:$addr";
        unshift @ARGV, '--resolv', "$name:443:$addr";
    }

    print "curl @ARGV\n";
    exec 'curl', @ARGV;
</code></pre>
