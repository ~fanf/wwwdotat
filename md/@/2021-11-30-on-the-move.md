---
dw:
  anum: 166
  eventtime: "2021-11-30T11:10:00Z"
  itemid: 533
  logtime: "2021-11-30T11:17:30Z"
  props:
    commentalter: 1638658643
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/136614.html"
format: md
...

On the move
===========

I have sent my formal resignation letter: I am leaving the
University. From March, I will be working full-time for
[isc.org](https://www.isc.org/) on [BIND](https://www.isc.org/bind/).

I said in my letter that it has been a privilege and a pleasure
working for the University. That isn't a polite fiction; I like the
people I have worked with, and I have had interesting and rewarding
things to do. But this is an exciting opportunity which came at the
right time, so I allowed myself to be dragged away.

I have been a contributor to BIND for several years now. When I met
Jeff Osborne (the president of ISC) in Amsterdam 3 years ago, he joked
that I worked for them but he didn't pay me :-) And I have met many of
the ISC folks at various conferences. So in some ways it's known territory.

But some things are going to be very different. The university has
more than 1000 times the number of people as ISC, whereas ISC is
almost entirely remote so spans over 1000 times the area. From my own
point of view, I am looking forward to working in a bigger team than I
have been.

This all happened fairly fast: Ondřej Surý (head of BIND development)
suggested the new job to me only a few weeks ago, and I still find it
hard to believe it's really happening! We have talked over email about
what Ondřej would like me to work on, and I'm looking forward to
getting stuck in.
