---
dw:
  anum: 174
  eventtime: "2005-03-07T22:36:00Z"
  itemid: 131
  logtime: "2005-03-07T22:46:22Z"
  props:
    commentalter: 1491292319
    import_source: livejournal.com/fanf/33833
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/33710.html"
format: casual
lj:
  anum: 41
  can_comment: 1
  ditemid: 33833
  event_timestamp: 1110234960
  eventtime: "2005-03-07T22:36:00Z"
  itemid: 132
  logtime: "2005-03-07T22:46:22Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/33833.html"
title: Client SMTP Authorization
...

So while the rest of the team are having a knees-up at the 62nd IETF in Minneapolis, I'm quietly implementing CSA for Exim. There's about 450 lines of reasonably straightforward new code to do this. You can now say something like the following in an ACL to reject any hosts which are not authorized.
<pre>
      require verify = csa
</pre>
In addition to that, the <code>dnsdb</code> lookup type will also handle CSA records. A straight SRV lookup isn't enough because CSA also involves a search for a site policy record. The dnsdb CSA lookup also does some extra checking and pretty-prints the contents of the CSA record for extra friendliness.

There are a few more details to the implementation which I will be writing down tomorrow when I get stuck into the documentation. That will be after the code has been tested (as opposed to just compiled).
