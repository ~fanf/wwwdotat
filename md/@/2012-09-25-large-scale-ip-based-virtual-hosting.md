---
format: html
lj:
  anum: 126
  can_comment: 1
  ditemid: 124030
  event_timestamp: 1348576620
  eventtime: "2012-09-25T12:37:00Z"
  itemid: 484
  logtime: "2012-09-25T11:37:02Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/124030.html"
title: Large-scale IP-based virtual hosting
...

<p>Yesterday there was a thread on the NANOG list about <a href="http://mailman.nanog.org/pipermail/nanog/2012-September/052129.html">IPv6 addressing for web sites</a>. There is a great opportunity here to switch back to having an IP address per site, like we did with IPv4 in the mid-1990s :-) Of course the web is a bit bigger now, and there is more large-scale hosting going on, but the idea of configuring a large CIDR block of addresses on a server is not unprecedented - except back then we were dealing with /18s rather than /64s.</p>

<p>Demon's Homepages service predated widespread browser support for name-based virtual hosting. In fact it provided one of the download sites for IE3, which was the first version that sent Host: headers. So Homepages was based on IPv4 virtual hosting and had IIRC 98,304 IP addresses allocated to it in three CIDR blocks. It was a single web server, in front of which were a few reverse proxy caches that took most of the load, and that also had all the IP
addresses. Every cache would accept connections on all the IP addresses, and the load was spread between them by configuring which address ranges were routed to which cache.</p>

<p>The original version of Homepages ran on Irix, and used a cunning firewall configuration to
accept connections to all the addresses without stupid numbers of virtual interfaces. Back then there were not many firewall packages that could do this, so when it moved to BSD (first NetBSD then FreeBSD) we used a "<a href="http://www.freebsd.org/cgi/query-pr.cgi?pr=kern/12071
">NETALIAS</a>" kernel hack which allowed us to use <tt>ifconfig</tt> to bring up a CIDR block in one go.</p>

<p>Sadly I have never updated the NETALIAS code to support IPv6. But I wondered if any firewalls had caught up with Irix in the last 15 years. It turns out the answer is yes, and the key feature to look for is support for transparent proxying. On FreeBSD you want the <a href="http://www.freebsd.org/cgi/man.cgi?query=ipfw">ipfw fwd</a> rule. On Linux you want the <a href="http://wiki.squid-cache.org/Features/Tproxy4">TPROXY</a> feature. You can do a similar thing with OpenBSD pf, though it requires the server to use a special API to query the firewall, rather than just using getsockname().</p>

<p>On Demon's web servers we stuffed the IP address into the filesystem path name
to find the document root, or used <a href="http://httpd.apache.org/docs/2.4/vhosts/mass.html#ipbased">various evil hacks</a> to map the IP
address to a canonical virtual server host name before stuffing the latter
in the path. <a href="http://httpd.apache.org/docs/2.4/mod/mod_vhost_alias.html">mod_vhost_alias</a> is very oriented around IPv4 addresses and host names, so probably not great for IPv6, so <a href="http://httpd.apache.org/docs/2.4/rewrite/vhosts.html">mod_rewrite</a> is a better choice if you want to break addresses up into a hierarchial layout. But perhaps it is not that ugly to run a name server which is authoritative for the reverse ip6.arpa range used by the web server, and map the address to the hostname with UseCanonicalName DNS.</p>
