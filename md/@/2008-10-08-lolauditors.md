---
dw:
  anum: 125
  eventtime: "2008-10-08T12:35:00Z"
  itemid: 365
  logtime: "2008-10-08T12:35:45Z"
  props:
    commentalter: 1491292361
    import_source: livejournal.com/fanf/94505
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/93565.html"
format: html
lj:
  anum: 41
  can_comment: 1
  ditemid: 94505
  event_timestamp: 1223469300
  eventtime: "2008-10-08T12:35:00Z"
  itemid: 369
  logtime: "2008-10-08T12:35:45Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/94505.html"
title: LOLauditors
...

<p>A colleague tells me that our auditors are quizzing our system administrators about our backup schedules. A wag asked them if they were interested in restores too. The auditors replied, "No, just backups."</p>

<p><i>PS.</i> I bought a copy of the book about the Chronophage from Corpus Christi plodge for &pound;8. It is a glossy A4 booklet of 44 pages with large type and lots of colourful pictures.</p>
