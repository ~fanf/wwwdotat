---
format: html
lj:
  anum: 98
  can_comment: 1
  ditemid: 108642
  event_timestamp: 1282664520
  eventtime: "2010-08-24T15:42:00Z"
  itemid: 424
  logtime: "2010-08-24T15:42:19Z"
  props:
    personifi_tags: "8:20,10:10,1:20,3:10,5:10,9:60,nterms:yes"
    verticals_list: life
  reply_count: 4
  url: "https://fanf.livejournal.com/108642.html"
title: Timezone display by MUAs
...

<p>One of my colleagues is currently having problems with his new Exchange server. Messages delivered internally to the server have the correct timezone (+0100) in the Date: header, but messages that are delivered externally via SMTP have a Date: header timezone of +0000. All the Received: header timestamps have the correct +0100 timezone. We haven't worked out how to fix this yet.</p>

<p>In the course of debugging this problem, my colleague was discombobulated to find that different MUAs display the dates on messages differently. <a href="http://www.mulberrymail.com/">Mulberry</a>, <a href="http://www.washington.edu/alpine/">Alpine</a>, and <a href="http://www-uxsup.csx.cam.ac.uk/~dpc22/prayer/">our webmail server</a> display the date as set by the sender (+0000 in the case of the problem messages), whereas most other MUAs (Thunderbird, Mail.app, Outlook, Gmail, Hotmail) translate the date to the recipient's local timezone.</p>

<p>Given the choice between these two options, I prefer to see the sender's date, since it provides useful clues about long-distance correspondents (such as when it might not be reasonable to expect a prompt reply). However I can see why others would prefer to see times in a consistent timezone (e.g. it makes it easier to see how old messages are).</p>

<p>But really, <a href="http://fanf.livejournal.com/104586.html">as I argued last year</a>, in this kind of situation (where the sender's and recipient's timezones differ) the MUA should display the date <i>twice</i> using <i>both</i> timezones. This makes it obvious what is going on (messages from local and long-distance correspondents are shown differently) and doesn't require users to do timezone conversions in their heads.</p>
