---
dw:
  anum: 201
  eventtime: "2008-07-23T19:20:00Z"
  itemid: 349
  logtime: "2008-07-23T20:20:14Z"
  props:
    commentalter: 1491292358
    import_source: livejournal.com/fanf/90190
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/89545.html"
format: html
lj:
  anum: 78
  can_comment: 1
  ditemid: 90190
  event_timestamp: 1216840800
  eventtime: "2008-07-23T19:20:00Z"
  itemid: 352
  logtime: "2008-07-23T20:20:14Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 15
  url: "https://fanf.livejournal.com/90190.html"
title: "Kaminsky's DNS hack"
...

<p><a href="https://beezari.livejournal.com/">👤beezari</a> posted <a href="http://beezari.livejournal.com/141796.html">a copy of the leaked Matasano explanation of Kaminsky's new DNS attack</a>. I believe the explanation isn't quite right. In <a href="http://blog.wired.com/27bstroke6/2008/07/kaminsky-on-how.html">his interview in the WIRED Threat Level blog</a> Kaminsky mentions that the attack relies on CNAMEs. This means that it does not depend on glue nor on additional section processing, which is what Matasano described. I believe the real explanation is...</p>
<pre>
$ md5 <~/doc/kaminsky
ef96f2d9e973a36e825793ddeff48ae5
</pre>
