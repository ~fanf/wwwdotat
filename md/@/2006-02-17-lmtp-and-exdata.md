---
dw:
  anum: 11
  eventtime: "2006-02-17T14:01:00Z"
  itemid: 198
  logtime: "2006-02-17T14:19:08Z"
  props:
    commentalter: 1491292330
    import_source: livejournal.com/fanf/51156
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/50699.html"
format: casual
lj:
  anum: 212
  can_comment: 1
  ditemid: 51156
  event_timestamp: 1140184860
  eventtime: "2006-02-17T14:01:00Z"
  itemid: 199
  logtime: "2006-02-17T14:19:08Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/51156.html"
title: LMTP and EXDATA
...

One of the problems with SMTP is that there is only one server reply to the message data, regardless of the number of recipients. This means that you can't apply per-recipient message data policies (such as different SpamAssassin score thresholds) without ugly hacks.

CMU Cyrus uses a special protocol for local delivery called LMTP, the local mail transport protocol. It is the same as SMTP except that the initial client command is LHLO instead of EHLO, and the server gives multiple replies to the message data, one per recipient. The reason Cyrus wants this functionality is because of quota checks: it wants to be able to deliver the message to the recipients that are below quota, but defer delivery to other recipients so that retries are all handled by the MTA.

But, as I explained in the first paragraph, this kind of feature would be useful for inter-domain mail transer as well as mail delivery. There's no particular reason that you couldn't use LMTP as it is in this context, and fall back to ESMTP if the LHLO command is rejected. But this is gratuitously ignoring SMTP's extension architecture, and apart from being ugly, it costs an extra round-trip at the start of a connection.

So I have been thinking of knocking up a quick I-D to re-cast LMTP as a proper SMTP extension and advocate its use everywhere. But, wonderful lazyweb! it has already been done: http://www.courier-mta.org/draft-varshavchik-exdata-smtpext.txt (though I would have done some of the details differently).

Of course this is of dubious utility if the spammers don't also use it...
