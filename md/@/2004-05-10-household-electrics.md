---
dw:
  anum: 164
  eventtime: "2004-05-10T18:56:00Z"
  itemid: 86
  logtime: "2004-05-10T12:15:15Z"
  props:
    commentalter: 1491292312
    import_source: livejournal.com/fanf/22186
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/22180.html"
format: casual
lj:
  anum: 170
  can_comment: 1
  ditemid: 22186
  event_timestamp: 1084215360
  eventtime: "2004-05-10T18:56:00Z"
  itemid: 86
  logtime: "2004-05-10T12:15:15Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/22186.html"
title: Household electrics
...

This weekend I learnt how NOT to replace a ceiling light fitting, and then how to do it properly.

The lighting circuit consists of a number of segments going from the consumer unit to each light fitting in turn. At a fitting there is a spur to the switch and a spur to the light (though the latter is removed with the fitting). So the circuit is something like

<pre>
                                            +--+
                          live  _______     |   \ switch
                          +---==\      \===-+  |
                          | +-==/______/===----+
      _________   live    | |      _______________
C ===\         \=========-+-)---==\               to next
U ===/_________/=========---)-+-==/_______________fitting
        cable    neutral    | |
                            | | <- neutral
           appears to be -> | |
          neutral but is     to
      either off or live    light

</pre>

So you have three cables hanging from the ceiling, one of which is special (it goes to the switch) but which might not be easy to distinguish from the others.

Do not connect all the red wires together and all the black wires together. This means the light is on when the switch is off, and turning the switch on causes a short circuit which upsets the CU. *bang*
