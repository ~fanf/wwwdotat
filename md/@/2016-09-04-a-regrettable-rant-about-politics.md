---
dw:
  anum: 135
  eventtime: "2016-09-04T00:39:00Z"
  itemid: 466
  logtime: "2016-09-03T23:39:01Z"
  props:
    commentalter: 1491292419
    import_source: livejournal.com/fanf/146634
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/119431.html"
format: casual
lj:
  anum: 202
  can_comment: 1
  ditemid: 146634
  event_timestamp: 1472949540
  eventtime: "2016-09-04T00:39:00Z"
  itemid: 572
  logtime: "2016-09-03T23:39:01Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 16
  url: "https://fanf.livejournal.com/146634.html"
title: A regrettable rant about politics
...

Oh good grief, reading this interview with Nick Clegg.

http://www.theguardian.com/politics/2016/sep/03/nick-clegg-did-not-cater-tories-brazen-ruthlessness

I am cross about the coalition. A lot of my friends are MUCH MORE cross than me, and will never vote Lib Dem again. And this interview illustrates why.

The discussion towards the end about the university tuition fee debacle really crystallises it. The framing is about the policy, in isolation. Even, even! that the Lib Dems might have got away with it by cutting university budgets, and making the universities scream for a fee increase!

(Note that this is exactly the tactic the Tories are using to privatise the NHS.)

The point is not the betrayal over this particular policy.

The point is the political symbolism.

Earlier in the interview, Clegg is very pleased about the stunning success of the coalition agreement. It was indeed wonderful, from the policy point of view. But only wonks care about policy.

From the non-wonk point of view of many Lib Dem voters, their upstart radical lefty party suddenly switched to become part of the machine.

Many people who voted for the Lib Dems in 2010 did so because they were not New Labour and - even more - not the Tories. The coalition was a huge slap in the face.

Tuition fees were just the most blatant simple example of this fact.

Free university education is a symbol of social mobility, that you can improve yourself by work regardless of parenthood. Educating Rita, a bellwether of the social safety net.

And I am hugely disappointed that Clegg still seems to think that getting lost in the weeds of policy is more important than understanding the views of his party's supporters.

He says near the start of the interview that getting things done was his main motivation. And in May 2010 that sounded pretty awesome.

But the consequence was the destruction of much of his party's support, and the loss of any media acknowledgment that the Lib Dems have a distinct political position. Both were tenuous in 2010 and both are now laughable.

The interviewer asks him about tuition fees, and still, he fails to talk about the wider implications.
