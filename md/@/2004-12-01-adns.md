---
dw:
  anum: 13
  eventtime: "2004-12-01T21:59:00Z"
  itemid: 117
  logtime: "2004-12-01T14:04:21Z"
  props:
    commentalter: 1491292318
    import_source: livejournal.com/fanf/30030
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/29965.html"
format: casual
lj:
  anum: 78
  can_comment: 1
  ditemid: 30030
  event_timestamp: 1101938340
  eventtime: "2004-12-01T21:59:00Z"
  itemid: 117
  logtime: "2004-12-01T14:04:21Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/30030.html"
title: adns
...

I wanted to compile some statistics on the correctness of SMTP clients HELO domains. This is exclusively for emal coming into our MXs, so doesn't include MUAs which tend to be very broken in this respect.

Exim in our configuration checks that the HELO domain and the reverse DNS and the forward DNS all match. However I'm also interested in whether a forward lookup on the HELO domain matches the client's IP address, and Exim doesn't record this in the logs. A quick bit of hackery with adns, and a few minutes of 10,000 concurrent DNS queries later, I have my results:

Total rejections:       123921
Failed HELO checks:     101417
Forward DNS correct:      2128

Total accepted:          31754
Failed HELO checks:      13349
Forward DNS correct:      3196

So, today this machine has rejected 80% of incoming messages. According to
SpamAssassin about 15% of the messages we accept are spam so you might
want to adjust the numbers on that basis.

Of the rejected messages, 80% have a completely bad HELO domain, and 2%
have a HELO domain that's correct only in the forward direction.

Of the accepted messages, 32% have a completely bad HELO domain, and 10%
have a HELO domain that's correct only in the forward direction.

I really like adns :-)
