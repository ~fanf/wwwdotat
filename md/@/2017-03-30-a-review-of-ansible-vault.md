---
dw:
  anum: 158
  eventtime: "2017-03-30T15:38:00Z"
  itemid: 478
  logtime: "2017-03-30T14:38:35Z"
  props:
    commentalter: 1491292431
    import_source: livejournal.com/fanf/149732
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/122526.html"
format: html
lj:
  anum: 228
  can_comment: 1
  ditemid: 149732
  event_timestamp: 1490888280
  eventtime: "2017-03-30T15:38:00Z"
  itemid: 584
  logtime: "2017-03-30T14:38:35Z"
  props:
    give_features: 1
    og_image: "http://l-files.livejournal.net/og_image/936728/584?v=1490884716"
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/149732.html"
title: A review of Ansible Vault
...

<p><a href="http://docs.ansible.com/ansible/">Ansible</a> is the configuration
management tool we use at work. It has built-in support for encrypted
secrets, called
<a href="http://docs.ansible.com/ansible/playbooks_vault.html"><code>ansible-vault</code></a>,
so you can safely store secrets in version control.</p>

<p>I thought I should review <a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py">the <code>ansible-vault</code> code</a>.</p>

<h2>Summary</h2>

<p>It's a bit shoddy but probably OK, provided you have a really strong
vault password.</p>

<h2>HAZMAT</h2>

<p>The code <a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L79">starts off with a bad sign</a>:</p>

<pre><code>    from cryptography.hazmat.primitives.hashes import SHA256 as c_SHA256
    from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
    from cryptography.hazmat.backends import default_backend
</code></pre>

<p>I like the way the Python cryptography library calls this stuff HAZMAT
but I don't like the fact that Ansible is getting its hands dirty with
HAZMAT. It's likely to lead to embarrassing cockups, and in fact
Ansible had an embarrassing cockup - there are two vault ciphers,
"AES" (the cockup, now disabled except that for compatibility you can
still decrypt) and "AES256" (fixed replacement).</p>

<p>As a consequence of basing <code>ansible-vault</code> on relatively low-level
primitives, it has its own Python implementations of constant-time
comparison and PKCS#7 padding. Ugh.</p>

<h2>Good</h2>

<p><a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L711">Proper random numbers</a>:</p>

<pre><code>    b_salt = os.urandom(32)
</code></pre>

<h2>Poor</h2>

<p><a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L681">Iteration count</a>:</p>

<pre><code>    b_derivedkey = PBKDF2(b_password, b_salt,
                          dkLen=(2 * keylength) + ivlength,
                          count=10000, prf=pbkdf2_prf)
</code></pre>

<p>PBKDF2 HMAC SHA256 takes about 24ms for 10k iterations on my machine,
which is not bad but also not great - e.g. 1Password uses 100k
iterations of the same algorithm, and gpg tunes its non-PBKDF2
password hash to take (by default) at least 100ms.</p>

<p>The deeper problem here is that Ansible has hard-coded the PBKDF2
iteration count, so it can't be changed without breaking compatibility.
In gpg an encrypted blob includes the variable iteration count as a
parameter.</p>

<h2>Ugly</h2>

<p><a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L737">ASCII armoring</a>:</p>

<pre><code>    b_vaulttext = b'\n'.join([hexlify(b_salt),
                              to_bytes(hmac.hexdigest()),
                              hexlify(b_ciphertext)])
    b_vaulttext = hexlify(b_vaulttext)
</code></pre>

<p>The ASCII-armoring of the ciphertext is as dumb as a brick, with
hex-encoding inside hex-encoding.</p>

<h2>File handling</h2>

<p>I also (more briefly) looked through <code>ansible-vault</code>'s higher-level
code for managing vault files.</p>

<p>It is based on handing decrypted YAML files to <code>$EDITOR</code>, so it's a
bit awkward if you don't want to wrap secrets in YAML or if you don't
want to manipulate them in your editor.</p>

<p>It uses
<a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L388"><code>mkstemp()</code></a>,
so the decrypted file can be placed on a ram disk, though you might
have to set <code>TMPDIR</code> to make sure.</p>

<p>It
<a href="https://github.com/ansible/ansible/blob/147c29f0/lib/ansible/parsing/vault/__init__.py#L352"><code>shred</code>(1)</a>s
the file after finishing with it.</p>
