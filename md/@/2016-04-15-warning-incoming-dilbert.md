---
dw:
  anum: 128
  eventtime: "2016-04-15T18:16:00Z"
  itemid: 451
  logtime: "2016-04-15T17:16:46Z"
  props:
    commentalter: 1491292416
    import_source: livejournal.com/fanf/142811
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/115584.html"
format: html
lj:
  anum: 219
  can_comment: 1
  ditemid: 142811
  event_timestamp: 1460744160
  eventtime: "2016-04-15T18:16:00Z"
  itemid: 557
  logtime: "2016-04-15T17:16:46Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/142811.html"
title: "Warning, incoming Dilbert"
...

<p>Just a quick note to say I have fixed the <a href="https://dilbert_zoom.livejournal.com/">👤dilbert_zoom</a> and <a href="https://dilbert_24.livejournal.com/">👤dilbert_24</a> feeds (after a very long hiatus) so if you follow them there is likely to be a sudden spooge of cartoons.</p>
