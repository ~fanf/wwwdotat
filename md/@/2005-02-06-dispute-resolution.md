---
dw:
  anum: 222
  eventtime: "2005-02-06T22:59:00Z"
  itemid: 127
  logtime: "2005-02-06T23:05:19Z"
  props:
    import_source: livejournal.com/fanf/33019
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/32734.html"
format: casual
lj:
  anum: 251
  can_comment: 1
  ditemid: 33019
  event_timestamp: 1107730740
  eventtime: "2005-02-06T22:59:00Z"
  itemid: 128
  logtime: "2005-02-06T23:05:19Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/33019.html"
title: Dispute resolution
...

Talking with <a href="https://lusercop.livejournal.com/">👤lusercop</a> and <a href="https://pjc50.livejournal.com/">👤pjc50</a> last night about dispute resolution, in particular a dispute over the name of a film. Like trademarks and stage-names these must be registered and reasonably unique. However unlike trademarks, I understand that there's a dispute resolution agency which is reasonable and inexpensive.

This makes me think that state-supported monopolies (trademarks, patents, copyright) should have state-supported dispute resolution, in order to ensure a reasonable balance of power between the deep pockets and the rest of us.
