---
format: html
lj:
  anum: 213
  can_comment: 1
  ditemid: 108245
  event_timestamp: 1280516700
  eventtime: "2010-07-30T19:05:00Z"
  itemid: 422
  logtime: "2010-07-30T19:06:20Z"
  props:
    personifi_tags: "2:14,17:12,8:40,10:2,33:2,32:1,16:12,1:40,3:14,25:2,19:2,4:14,26:2,20:4,9:18,nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/108245.html"
title: Paul Vixie declares war on malicious domain names.
...

<p>Paul Vixie has been more or less in charge of BIND development for over 20 years. He is also notable for creating the first anti-spam blacklist, the MAPS RBL, in 1997.</p>

<p>Although usenet and email were the first serious Internet battlegrounds, the main problem now is attacks on web browsers and their users. There have been some efforts at adding blacklists to browsers, for example <a href="http://code.google.com/apis/safebrowsing/">Google's safe browsing list</a>, and although <a href="http://www.google.com/tools/firefox/safebrowsing/">modern browsers use them</a> there are still a lot of old unsafe browsers out there.</p>

<p>So instead of relying on users to upgrade their browsers, another approach is to put the blacklist into the DNS resolver. This approach was pioneered by <a href="http://www.opendns.com/">OpenDNS</a>. Their resolvers deliberately lie to protect their users. The problem with lying resolvers is that they can also lie in unhelpful ways, the worst being typo squatting - directing the user to an ad-laden money-generating portal instead of simply saying a name doesn't exist. Even if the resolver only makes white lies, you might not want to entirely outsource your DNS resolvers just for an extra safety feature.</p>

<p>This week <a href="http://www.isc.org/community/blog/201007/taking-back-dns-0">Vixie announced his solution to this problem</a> at the Black Hat and DefCon security conferences. He has effectively specified a way that a recursive resolver can easily take a data feed of malicious domain names, for which it will either deny their existence or redirect the user to a "walled garden" web server that explains why they can't get to the web site they expected or which does more fine-grained filtering.</p>

<p>This is not just an attack on malicious domain names: it is also an attempt to build the foundations for a competitive market in DNS reputation providers, as there already is for email DNSBLs. Vixie also wants to allow resolvers to make "white lies" without also encouraging typo squatting and other grubby activity. He also has an eye on making this work with DNSSEC, which is designed to prevent any kind of lying.</p>

<p>I'm keen for this to work. I'm in favour of judicious measures that make it harder for criminals to exploit the Internet. However I'm not entirely sure that Vixie's proposal is quite right as currently specified.</p>

<p>"Response Policy Zones" encode two things: the list of domains that the RPZ provider says are malicious, and the kind of reply that the resolver should give when a user makes a query for one of those domains. Replies can be NXDOMAIN or NODATA resolution failures, or replacement answers that can be used to redirect the user to a walled garden.</p>

<p>This is different from email DNS blacklists. Existing DNSBLs are just lists of bad domains or IP addresses, possibly qualified with some information about why each item is listed. DNSBLs encode no policy: it's up to the postmaster whether to block a listed site, or give it a slightly spammier score, or anything else.</p>

<p>This built-in policy is probably OK if the RPZ just says the domains it lists should not exist. However in the case of a redirection, the subscriber to the RPZ is likely to want to run their own walled garden, or they may want to outsource it to someone other than the provider of the RPZ. But the RPZ fixes the target of the redirect. This niggle is acknowledged in section 4.7 of <a href="ftp://ftp.isc.org/isc/dnsrpz/isc-tn-2010-1.txt">the spec</a> but the suggested solutions aren't great.</p>

<p>RPZs are designed to use standard DNS distribution protocols, and these are designed to copy the data verbatim. Typical implementations do not have any hooks you can use to filter a replicated zone. The alternative of getting the provider to do the customization at their end will be expensive and clumsy.</p>

<p>There is another possibility, which is for redirections in the RPZ to take the form of CNAME records pointing at a magic private domain name. Each RPZ subscriber can create a local private zone containing this magic name and pointing it at whatever walled garden they wish to deploy. However it might not be possible to use this trick to turn a redirect policy into an NXDOMAIN or NODATA - the spec is unclear.</p>

<p>I would be happier if, like DNSBLs, the RPZ proposal left the choice of countermeasures to the subscriber, leaving RPZ providers to just list the zones they don't like.</p>
