---
dw:
  anum: 245
  eventtime: "2003-03-19T17:31:00Z"
  itemid: 6
  logtime: "2003-03-19T09:55:16Z"
  props:
    commentalter: 1491292303
    current_mood: procrastinating
    current_music: "Moby, \"Play\""
    import_source: livejournal.com/fanf/1558
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/1781.html"
format: casual
lj:
  anum: 22
  can_comment: 1
  ditemid: 1558
  event_timestamp: 1048095060
  eventtime: "2003-03-19T17:31:00Z"
  itemid: 6
  logtime: "2003-03-19T09:55:16Z"
  props:
    current_mood: procrastinating
    current_music: "Moby, \"Play\""
  reply_count: 8
  url: "https://fanf.livejournal.com/1558.html"
title: Pot plants
...

I sit in an office next to a window which gets the blazing sun all afternoon. I want something nicer than a blind to look at, so what kind of plant should I get?
