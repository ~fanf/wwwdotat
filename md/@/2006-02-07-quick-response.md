---
dw:
  anum: 174
  eventtime: "2006-02-07T11:58:00Z"
  itemid: 196
  logtime: "2006-02-07T12:04:18Z"
  props:
    import_source: livejournal.com/fanf/50564
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/50350.html"
format: casual
lj:
  anum: 132
  can_comment: 1
  ditemid: 50564
  event_timestamp: 1139313480
  eventtime: "2006-02-07T11:58:00Z"
  itemid: 197
  logtime: "2006-02-07T12:04:18Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/50564.html"
title: Quick response
...

I just sent out an annoucement about a new facility to help with the withdrawal of insecure access to Hermes. Computing support staff in the University can now add users to the notification schedule via a web page instead of having to go through us.

Within 10 minutes, three people had tested the form...
