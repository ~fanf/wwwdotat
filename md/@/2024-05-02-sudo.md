---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/146198.html
  Fediverse: https://mendeddrum.org/@fanf/112373856052436979
  Lobsters: https://lobste.rs/s/ldkfdg/sudon_t
...
sudon't
=======

My opinion is not mainstream, but I think if you really examine the
practices and security processes that use and recommend sudo, the
reasons for using it are mostly bullshit.

<cut>

When I started my career in the late 1990s, I was already aware of
[really(8)][really] and [userv][] because one of my friends wrote
them. As tools they embody criticisms of sudo’s design. (`really` came
from Cambridge University’s central unix timesharing service CUS in
about 1990, and before that from the [Computer Lab][cl] unix systems
in the 1980s.)

[really]: https://manpages.debian.org/stretch/chiark-really/really.8.en.html
[userv]: https://www.chiark.greenend.org.uk/~ian/userv/
[cl]: https://www.cl.cam.ac.uk/

My first job after university was at Demon Internet where it was
normal for ops staff to log in as root over ssh. Demon had patched
sshd to change how it found user public keys, which allowed the
security team to manage which staff had access to which servers. (In
practice, most ops staff had access to everything, which was
[sometimes fun][sshtrace]!) At that time I was paying attention to the
prevailing sysadmin wisdom, and sudo was being suggested as a good
idea. It was not yet dogma, but it was heading that way.

[sshtrace]: https://dotat.at/prog/scripts/sshtrace

There were a bunch of reasons for recommending sudo:

 1. As a safety guard.

    If you never have a root shell prompt it’s harder to accidentally
    do something with root powers that has catastrophic consequences.
    This is also what `really(8)` is for, and it’s about 1000x simpler
    than sudo.

    I have never been persuaded by this argument. As a safety guard
    sudo is pretty feeble. The idea is to provide some mechanical
    assistance to the sysadmin’s situational awareness: do I really
    mean to run this command as root? In my career I have always been
    working with distributed systems, so a simple
    privileged/unprivileged sudo-or-not guard fails to capture many of
    the other ways to fuck up: like, are you even logged into the
    right system?

    Other tools you can use to avoid mistakes include:

      * put your username, hostname, and working directory in your
        shell prompt, and pay attention to it so you maintain your
        situational awareness

      * use multiple terminal windows – I always took this for
        granted, but only a few years before I started my career, X
        terminals were a rare privilege. Spatial awareness is a
        powerful tool for keeping track of things, so use it to
        separate root or not, this machine or that machine.

      * use colours or decorations to make privileged terminal prompts
        or privileged windows more obvious

 3. Because you should not log in to root directly.

    There’s a lot of angst out there about the dangers of exposing a
    root login to the big bad internet. It is not backed by a rational
    assessment of the security of root’s login credentials (whether
    keys or passwords) nor by any analysis of whether you get a
    meaningful security improvement from logging in as another user
    then raising privilege with sudo.

    By the time Ansible came along, this advice had turned into dogma.
    Ansible by default prefers to log in to a remote system using a
    non-root account then escalate to root using sudo or some
    alternative. Because Ansible is an automation tool, it must be
    able to escalate privilege without interaction. So there is no
    meaningful security boundary, and this non-root account is
    effectively equivalent to root. So Ansible has a lot of gratuitous
    complexity just to conform to this dogma.

 2. For auditing.

    I doubt I have ever seen or heard of sudo’s logs being used
    effectively.

    But (assuming there is in fact some practical use to it) sudo was
    better at logging who did what than other privilege escalation
    tools, such as `login` or `su`. In particular, `sshd` was for a
    long time unable to log which public key was used to authenticate,
    which is a sad omission especially for shared privileged accounts
    such as root.

 4. It’s a general-purpose tool.

    This is sudo as a bad `userv`, rather than sudo as a bloated
    `really(8)`.

    In the early 1990s it was not easy to provide a tool that crossed
    privilege boundaries. That was before it made sense to make a web
    app to handle such things. It was wasteful of limited computing
    resources to have a persistent daemon (like userv) when you could
    use setuid (like sudo). And back then [SCM_CREDS][] was advanced
    technology not available everywhere, which led to some compromises
    in userv’s design.

    I think sudo became popular because of this broad utility, then it
    became the recommended way to escalate to root (for reasons 1 and
    2). However sudo has a really complicated rules language to
    describe which users can run which tools with which privileges,
    which is the cause of many of its security vulnerabilities
    (undermining 1 and 2).

    One of the things that surprised me about [doas][] was that it still
    implements most of a sudo-style rules language. When they said it
    was radically stripped down I expected something like `really(8)`,
    but no!

[SCM_CREDS]: https://man.freebsd.org/cgi/man.cgi?query=unix&sektion=4
[doas]: https://man.openbsd.org/doas

 5. For single-user workstations.

    After sudo was already a thing, Mac OS X and Ubuntu needed to
    solve the problem of allowing the workstation’s user to get
    administrative access without making their account trivially
    equivalent to root.

    Instead of demanding the credentials of the target account, sudo
    authenticates the user making the request. So a single user
    workstation can be set up without a root password, and its owner
    only has to worry about the credentials for a single login.

    And sudo allows you to authenticate once, then run it several
    times without re-authenticating. It’s almost as convenient as a
    root shell!

    I think this use of sudo in the most widespread unix systems
    really cemented dogmas 1 and 2. It’s popular so it must be right!

    In this case, sudo is more about usability than security. It's
    doing an important job, but it's far more complicated than
    necessary. Better would be something like `really(8)`, with more
    authentication to verify the presence of the user.

So, to summarize, I think reasons 1, 2, 3 are bad reasons for using
sudo. If you need to provide tools that cross security boundaries (4)
then [userv][] or a small web app is better. For single-user
workstations (5) unfortunately there isn't a good alternative to sudo.
Personally I prefer to set up a root account that I can log into
directly, but I'm not an average user.
