---
dw:
  anum: 229
  eventtime: "2017-01-10T15:37:00Z"
  itemid: 474
  logtime: "2017-01-10T15:37:42Z"
  props:
    commentalter: 1491292424
    import_source: livejournal.com/fanf/148506
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/121573.html"
format: html
lj:
  anum: 26
  can_comment: 1
  ditemid: 148506
  event_timestamp: 1484062620
  eventtime: "2017-01-10T15:37:00Z"
  itemid: 580
  logtime: "2017-01-10T15:37:42Z"
  props:
    og_image: "http://l-files.livejournal.net/og_image/936728/580?v=1484111708"
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/148506.html"
title: Named and optional function arguments in C99
...

<p>Here's an amusing trick that I was just discussing with Mark Wooding
on IRC.</p>

<p>Did you know you can define functions with optional and/or named
arguments in C99? It's not even completely horrible!</p>

<p>The main limitation is there must be at least one non-optional
argument, and you need to compile with <code>-std=c99</code> <code>-Wno-override-init</code>.</p>

<p><i>(I originally wrote that this code needs C11, but <a href="https://twitter.com/MiodVallat/status/818857372772945920">Miod Vallat pointed out it works fine in C99</a>)</i></p>

<p>The pattern works like this, using a function called <code>repeat()</code> as an
example.</p>

<pre><code>    #include &lt;stdio.h&gt;
    #include &lt;stdlib.h&gt;
    #include &lt;string.h&gt;

    // Define the argument list as a structure. The dummy argument
    // at the start allows you to call the function with either
    // positional or named arguments.

    struct repeat_args {
        void *dummy;
        char *str;
        int n;
        char *sep;
    };

    // Define a wrapper macro that sets the default values and
    // hides away the rubric.

    #define repeat(...) \
            repeat((struct repeat_args){ \
                    .n = 1, .sep = " ", \
                    .dummy = NULL, __VA_ARGS__ })

    // Finally, define the function,
    // but remember to suppress macro expansion!

    char *(repeat)(struct repeat_args a) {
        if(a.n &lt; 1)
            return(NULL);
        char *r = malloc((a.n - 1) * strlen(a.sep) +
                         a.n * strlen(a.str) + 1);
        if(r == NULL)
            return(NULL);
        strcpy(r, a.str);
        while(a.n-- &gt; 1) { // accidentally quadratic
            strcat(r, a.sep);
            strcat(r, a.str);
        }
        return(r);
    }

    int main(void) {

        // Invoke it like this
        printf("%s\n", repeat(.str = "ho", .n = 3));

        // Or equivalently
        printf("%s\n", repeat("ho", 3, " "));

    }
</code></pre>
