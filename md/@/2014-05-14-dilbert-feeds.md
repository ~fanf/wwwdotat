---
dw:
  anum: 38
  eventtime: "2014-05-14T12:45:00Z"
  itemid: 406
  logtime: "2014-05-14T11:45:18Z"
  props:
    commentalter: 1491292408
    import_source: livejournal.com/fanf/131309
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/103974.html"
format: casual
lj:
  anum: 237
  can_comment: 1
  ditemid: 131309
  event_timestamp: 1400071500
  eventtime: "2014-05-14T12:45:00Z"
  itemid: 512
  logtime: "2014-05-14T11:45:18Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/131309.html"
title: Dilbert feeds
...

I just noticed that Dilbert had its 25th anniversary last month. I have created an atom feed of old strips to mark this event. To avoid leap year pain the feed contains strips from the same date 24 years ago, rather than 25 years ago. See <a href="https://dilbert_zoom.livejournal.com/">👤dilbert_zoom</a> for current strips and <a href="https://dilbert_24.livejournal.com/">👤dilbert_24</a> for the old ones.
