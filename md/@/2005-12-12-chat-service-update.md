---
dw:
  anum: 18
  eventtime: "2005-12-12T16:53:00Z"
  itemid: 169
  logtime: "2005-12-12T16:58:41Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/43584
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/43282.html"
format: casual
lj:
  anum: 64
  can_comment: 1
  ditemid: 43584
  event_timestamp: 1134406380
  eventtime: "2005-12-12T16:53:00Z"
  itemid: 170
  logtime: "2005-12-12T16:58:41Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/43584.html"
title: chat service update
...

Progress on the chat service is still slow. I sent the following to <a href="https://lists.cam.ac.uk/mailman/listinfo/cs-chat-users">cs-chat-users</a>:

At the moment I'm still waiting for final approval for the name "chat.cam.ac.uk" which I should get after the next IT Syndicate meeting on the 17th Jan. I'm not going to do much work on the project until then. (Hermes will keep me busy!) However I aim to make a prototype server available soon after that date.

After that there may be some further delays caused by Jabber's need for SRV records in the DNS. Jabber uses these to route messages between servers, similar to MX records for email. The University's DNS does not currently have any SRV records, so our hostmasters will go through a period of compatibility testing before putting them in the main zone. This won't affect use of the chat service between Cambridge users, but may delay our ability to communicate with other Jabber servers.
