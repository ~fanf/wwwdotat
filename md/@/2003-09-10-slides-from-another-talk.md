---
dw:
  anum: 133
  eventtime: "2003-09-10T16:47:00Z"
  itemid: 34
  logtime: "2003-09-10T16:50:38Z"
  props:
    commentalter: 1491292305
    current_moodid: 109
    current_music: Paul Oakenfold trance mix CDs
    import_source: livejournal.com/fanf/8897
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/8837.html"
format: casual
lj:
  anum: 193
  can_comment: 1
  ditemid: 8897
  event_timestamp: 1063212420
  eventtime: "2003-09-10T16:47:00Z"
  itemid: 34
  logtime: "2003-09-10T16:50:38Z"
  props:
    current_moodid: 109
    current_music: Paul Oakenfold trance mix CDs
  reply_count: 1
  url: "https://fanf.livejournal.com/8897.html"
title: Slides from another talk
...

I did another talk about my job this afternoon, this time about how well my virus filter coped with Sobig.F. It includes the usual frivolous pictures, and some pretty graphs drawn with <a href="http://www.gnuplot.info/">Gnuplot</a>, and it can be found <a href="http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2003-09-techlinks/">on my work homepage</a>.
