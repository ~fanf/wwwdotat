---
dw:
  anum: 127
  eventtime: "2004-11-11T01:16:00Z"
  itemid: 110
  logtime: "2004-11-10T17:19:23Z"
  props:
    commentalter: 1491292316
    import_source: livejournal.com/fanf/28254
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/28287.html"
format: casual
lj:
  anum: 94
  can_comment: 1
  ditemid: 28254
  event_timestamp: 1100135760
  eventtime: "2004-11-11T01:16:00Z"
  itemid: 110
  logtime: "2004-11-10T17:19:23Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/28254.html"
title: Amusing nicknames.
...

My parents know me as "ouffie" (or however you spell it).

What's your most amusing nickname? Post here or make it a meme.
