---
dw:
  anum: 91
  eventtime: "2021-04-10T18:33:00Z"
  itemid: 529
  logtime: "2021-04-10T19:01:05Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/135515.html"
format: md
...

Miles and metres
================

Nautical miles and metres were both originally defined in the same
way: each of them was the distance on the surface of the earth
subtended by a particular angle. For nautical miles, the angle is a
minute of arc; for the metre, it is 1/40,000,000 of a turn.

I was idly wondering about the history of this way of defining a unit
of length, and how it led to these two particular units, and what (if
any) influence they had on each other. The [In Our Time episode on
Pierre-Simon Laplace][bbcinourtime] briefly discussed his
involvement in the definition of the metric system in revolutionary
France, which nudged me to actually do some reading.

[bbcinourtime]: https://www.bbc.co.uk/programmes/m000twgj

<toc>


nautical miles
--------------

[Wikipedia][nm] says Robert Hues wrote in 1594 that the distance
along a great circle was 60 miles per degree, that is, one nautical
mile per arcminute.

At that time, a mile was not a firmly fixed distance: the [English
Statute Mile][sm] was defined in 1593, and different countries had
their own definitions. So I think Hues's statement should be
understood as an approximation.

The size of the earth was known, but not to very high precision. In
1637, [Richard Norwood][nav] measured a nautical mile to be 2,040
yards (6,120 feet); this was an over-estimate: compare the mid-1800s
Admiralty nautical mile of 6,080 feet.

The nautical mile did not settle on a single international standard
length until 1970.

[nm]: https://en.wikipedia.org/wiki/Nautical_mile
[sm]: https://en.wikipedia.org/wiki/Mile#English_mile
[nav]: https://en.wikipedia.org/wiki/History_of_navigation#Modern_times


geodesy
-------

In his _Principia_ (1687) [Isaac Newton][newton] argued that (if
measured precisely) the Earth would turn out to be an oblate spheroid.
So then the question was not just the size of the earth but also its
shape. Now we know, because the earth is flattened, a minute of arc
has different lengths at different latitudes: 6,106 feet at the poles,
and 6,044 feet at the equator. But it took a long time to obtain those
measurements.

To measure the earth's size and shape, there was a steady development
of improved techniques and instruments. In 1615 [Willebrord
Snell][snell] pioneered the technique of triangulation, using a
quadrant accurate to 0.1 degree. In 1631, [Pierre Vernier][vernier]
described his clever scale that allowed him to construct a quadrant
accurate to a minute of arc.

Later on, in 1784, [Jean-Charles de Borda][borda] and his assistant
Étienne Lenoir improved the reflecting circle, creating the repeating
circle that was used by [Delambre][delambre] and [Méchain][mechain] to
survey the Paris meridian.

So, by the end of the 1700s, we were able to measure with some
precision the relationship between degrees of latitude and distance
along a meridian, and how that varies from the equator to the poles.

[newton]: https://en.wikipedia.org/wiki/Isaac_Newton
[snell]: https://en.wikipedia.org/wiki/Willebrord_Snell
[vernier]: https://en.wikipedia.org/wiki/Pierre_Vernier
[borda]: https://en.wikipedia.org/wiki/Jean-Charles_de_Borda
[delambre]: https://en.wikipedia.org/wiki/Jean_Baptiste_Joseph_Delambre
[mechain]: https://en.wikipedia.org/wiki/Pierre_M%C3%A9chain


navigation
----------

One of the major scientific problems of the 1700s was how to measure
longitude accurately and conveniently. The eventual answer was the
marine chronometer invented by [John Harrison][harrison], but before
clocks became good enough there were a couple of astronomical methods.

In France, the preferred system was to use the orbits of Jupiter's
moons as a clock. The [Paris Observatory][op] was set up to make
observations for an almanac containing predictions of when Jupiter's
moons would be eclipsed. (During these observations, [Ole
Rømer][romer] made the first measurement of the speed of light.)

In England, the preferred system was to use the motion of the moon,
observing which stars were occluded when, to establish the current
time. The [Greenwich Observatory][greenwich] was set up to make the
star atlas that was necessary for this to work.

(For more, see _Longitude_ by [Dava Sobel][sobel].)

[Giovanni Domenico Cassini][cassini1] was the first director of the
Paris Observatory; his son [Jacques Cassini][cassini2] and
great-grandson [Jean-Dominique Cassini][cassini4] made measurements of
the Paris meridian that were the starting point for
[Delambre][delambre] and [Méchain][mechain].

[harrison]: https://en.wikipedia.org/wiki/John_Harrison
[op]: https://en.wikipedia.org/wiki/Paris_Observatory
[romer]: https://en.wikipedia.org/wiki/Ole_R%C3%B8mer
[greenwich]: https://en.wikipedia.org/wiki/Royal_Observatory,_Greenwich
[sobel]: https://en.wikipedia.org/wiki/Dava_Sobel
[cassini1]: https://en.wikipedia.org/wiki/Giovanni_Domenico_Cassini
[cassini2]: https://en.wikipedia.org/wiki/Jacques_Cassini
[cassini4]: https://en.wikipedia.org/wiki/Dominique,_comte_de_Cassini


the state of the art
--------------------

Most of the answer to my idle ponderings seems to be that an educated
philosopher-savant of the [Académie des sciences][academie] in the
late 1700s would be familiar with the links between astronomy,
geodesy, and navigation, as one of the most theoretically interesting
and practically important fields of knowledge.

Better [measurements of distance on land][survey] were interesting for
geodesy and cartography, but I infer that it wasn't particularly
important to know the precise length of the nautical mile as such.
Part of the evidence for this is that it took a long time to
standardise its length. I think what mattered for navigation at sea
was being able to establish a ship's location on a chart, and exact
distances were a lesser concern.

[academie]: https://en.wikipedia.org/wiki/French_Academy_of_Sciences
[survey]: https://en.wikipedia.org/wiki/Anglo-French_Survey_(1784%E2%80%931790)


the metric system
-----------------

[Ken Alder's book _The Measure of All Things_][measure] describes (in
chapter 3) the discussions around the design of the metric system in
the [Académie des sciences][academie].

One possible basis for a unit of length was a one-second pendulum,
proposed by [Charles Maurice de Talleyrand-Périgord][talleyrand] and
[Nicolas de Condorcet][condorcet], but this was not satisfactory for a
number of reasons: it was known that the force of gravity varied
depending on your latitude (so it would be hard to define a
convincingly universal standard), and they would have a different
standards if they defined it using the Babylonian base-60 second or a
new decimal unit of time.

The rationale for the final design was explained by [Borda][], as
chair of the French commission on weights and measures.

Borda was a fan of decimalisation, so his repeating circle measured
[gradians][] (1/100 of a quarter-turn) and centigrades (1/10,000 of a
quarter turn) instead of degrees and minutes. So a kilometre was
defined as a centigrade of arc along a meridian from the north pole to
the equator.

To establish the precise length of a metre, the commission argued that
some suitably objective and universalist criteria should be followed:
at least 10 degrees of arc of the chosen meridian should be measured,
and it should span the 45th parallel. This would allow the surveyors
to get a good measurement of the oblateness of the earth, and thus the
length of the entire meridian. It _just so happens_ that the Paris
meridian from Dunkirk (51° north) to Barcelona (41° north) satisfies
these completely objective and universalist criteria.

So that was the line that was surveyed by [Jean Baptiste Joseph
Delambre][delambre] and [Pierre Méchain][mechain], using Borda's
repeating circle, and described at length by Ken Alder.

[measure]: http://kenalder.com/measure/
[talleyrand]: https://en.wikipedia.org/wiki/Charles_Maurice_de_Talleyrand-P%C3%A9rigord#Honours
[condorcet]: https://en.wikipedia.org/wiki/Marquis_de_Condorcet
[gradians]: https://en.wikipedia.org/wiki/Gradian


angular time
------------

It's a bit awkward that a whole turn of the earth is 24 hours, but as
an angle it is 360°, so there's an awkward factor of 4 or 15 when
converting between longitude and time of day.

The French revolutionary system of [decimal time][] had a similar
problem: it was based on 1000 minutes per day, although there were 400
gradians in a circle. So (if it had not been thoroughly unpopular)
decimal time would have had awkward factors of 4 or 25 when converting
between decimal longitude and time of day. Oops.

[decimal time]: https://en.wikipedia.org/wiki/Decimal_time


design vs reality
-----------------

Snark aside, after the design process in the [Académie][academie] the
story of the metre has a certain amount of tragic irony.

[Méchain][mechain]'s survey of the southern section of the meridian
did not go well; he made a number of mistakes, which he tried to cover
up by cooking his books. [Delambre][delambre] perpetuated this
cover-up, to preserve Méchain's reputation and the reputation of the
project as a whole.

So the original measurement was not, in the end, objective and
repeatable. But its universalism was also based on a mistaken
assumption: the earth is even more wonky than they thought it was, so
one meridian is not equivalent to every other.

Alder suggests that Delambre satisfied himself by reasoning that it is
more important for a standard to be widely accepted than to be
completely perfect. And that seems to have been correct for the metric
system: it has improved in precision over the decades, and maintained
backwards compatibility, even if the original prototype units were
slightly erratic.

And at last, nearly 230 years after the Académie's commission on
weights and measures, the [SI base units][si] have fully universal
definitions - universal in the modern sense of the universe, not just
the earth-bound enlightenment universalism. The new SI units are not
based on any particular unique artefact, not even the earth.

[si]: https://en.wikipedia.org/wiki/2019_redefinition_of_the_SI_base_units

postscript
----------

I'm amused that Condorcet and Borda are also known for their work on
voting: the [Condorcet method][] and the [Borda count][].

[Condorcet method]: https://en.wikipedia.org/wiki/Condorcet_method
[Borda count]: https://en.wikipedia.org/wiki/Borda_count


credits
-------

Thanks to [Rachel][rmc28] for locating my copy of _The Measure of All Things_!

[rmc28]: https://rmc28.dreamwidth.org/
