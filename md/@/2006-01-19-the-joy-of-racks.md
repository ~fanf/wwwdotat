---
dw:
  anum: 2
  eventtime: "2006-01-19T15:34:00Z"
  itemid: 185
  logtime: "2006-01-19T15:46:15Z"
  props:
    commentalter: 1491292328
    import_source: livejournal.com/fanf/47657
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/47362.html"
format: casual
lj:
  anum: 41
  can_comment: 1
  ditemid: 47657
  event_timestamp: 1137684840
  eventtime: "2006-01-19T15:34:00Z"
  itemid: 186
  logtime: "2006-01-19T15:46:15Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/47657.html"
title: The joy of racks
...

On Tuesday we spent a good chunk of the afternoon emptying the last remaining two racks of the old Hermes system, including the rack that held the two NetApp F740s and 0.5TB of disk. (<a href="https://fanf2.user.srcf.net/photos.html#2004-07-old">pictures</a>) The NetApps have now gone where much old CS hardware eventually ends up: under a desk in Unix Support's office.

For the last 18 months the NetApps have been off, reserving space for the future expansion of Hermes. We now have a job for the space, and two shiny new racks to occupy it. This afternoon we attempted to move our backup server (<a href="https://fanf2.user.srcf.net/photos.html#2003-04-new">on the right</a>) to its new home, where it will be joined by a fibrechannel switch and another 12TB of disk.

However after removing the tape robot from its old rack and wheeling it across the machine room, we discovered that the vertical rails on the new rack had been set too close together.  We rapidly gave up and put the box back where it came from. This machine has to have THE MOST IRRITATING rack mount kit ever, with lots of fiddly screws and small bits of metal that like to fall through holes in the floor. A complete pain in the neck, especially in comparison with the utter joy of toolless rackmount kits.

We will have to do the move next week, after the racks have been adjusted.
