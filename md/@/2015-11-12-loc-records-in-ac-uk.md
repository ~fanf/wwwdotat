---
dw:
  anum: 25
  eventtime: "2015-11-12T11:26:00Z"
  itemid: 436
  logtime: "2015-11-12T11:26:15Z"
  props:
    commentalter: 1491292414
    import_source: livejournal.com/fanf/138986
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/111641.html"
format: html
lj:
  anum: 234
  can_comment: 1
  ditemid: 138986
  event_timestamp: 1447327560
  eventtime: "2015-11-12T11:26:00Z"
  itemid: 542
  logtime: "2015-11-12T11:26:15Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 5
  url: "https://fanf.livejournal.com/138986.html"
title: LOC records in ac.uk
...

<p>Since September 1995 we have had a TXT record on <tt>cam.ac.uk</tt> saying "The University of Cambridge, England". Yesterday I replaced it with a LOC record. The TXT space is getting crowded with things like Google and Microsoft domain authentication tokens and SPF records.

<p>When I rebuilt our DNS servers with automatic failover earlier this year, I used the TXT record for a very basic server health check. I wanted some other ultra-stable record for this purpose which is why I added the LOC record.

<p>What other LOC records are there under <tt>ac.uk</tt>, I wonder?

<p>Anyone used to be able to AXFR the <tt>ac.uk</tt> zone but that hasn't been possible for a few years now. But there is <a href="https://www.whatdotheyknow.com/request/list_of_acuk_domain_names">a public FOI response containing a list of <tt>ac.uk</tt> domains from 2011</a> which is good enough.

<p>A bit of hackery with <tt>adns</tt> and 20 seconds later I have:

<pre>
abertay.ac.uk.        LOC 56 46  4.000 N 2 57  5.000 W 50.00m   1m 10000m 10m
cam.ac.uk.            LOC 52 12 19.000 N 0  7  5.000 E 18.00m 10000m 100m 100m
carshalton.ac.uk.     LOC 51 22 17.596 N 0  9 58.698 W 33.00m  10m   100m 10m
marywardcentre.ac.uk. LOC 51 31 15.000 N 0  7 19.000 W  0.00m   1m 10000m 10m
midchesh.ac.uk.       LOC 53 14 58.200 N 2 32 15.190 E 47.00m   1m 10000m 10m
psc.ac.uk.            LOC 51  4 17.000 N 1 19 19.000 W 70.00m 200m   100m 10m
rdg.ac.uk.            LOC 51 26 25.800 N 0 56 46.700 W 87.00m   1m 10000m 10m
reading.ac.uk.        LOC 51 26 25.800 N 0 56 46.700 W 87.00m   1m 10000m 10m
ulcc.ac.uk.           LOC 51 31 16.000 N 0  7 40.000 W 93.00m   1m 10000m 10m
wessexsfc.ac.uk.      LOC 51  4 17.000 N 1 19 19.000 W 70.00m 200m   100m 10m
wilberforce.ac.uk.    LOC 53 46 28.000 N 0 16 42.000 W  0.00m   1m 10000m 10m
</pre>

<p><a href="http://tools.ietf.org/html/rfc1876">A LOC record records latitude, longitude, altitude, diameter, horizontal precision, and vertical precision.</a>

<p>The <tt>cam.ac.uk</tt> LOC record is supposed to indicate the location of the church of St Mary the Great, which has been the nominal centre of Cambridge since a system of milestones was set up in 1725. The precincts of the University are officially the area within three miles of GSM which corresponds to a diameter a bit less than 10km. The 100m vertical precision is enough to accommodate the 80m chimney at Addenbrookes.

<p>There are a couple of anomalies in the other LOC records.

<p><tt>abertay.ac.uk</tt> indicates a random spot in the highlands, not the centre of Dundee as it should.

<p><tt>midchesh.ac.uk</tt> should be W not E.

<p>A lot of the records use the default size of 1m diameter and precision of 10km horizontal and 10m vertical.

<p>You can copy and paste the lat/long into Google Maps to see where they land :-)
