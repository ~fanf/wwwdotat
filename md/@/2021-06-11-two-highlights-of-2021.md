---
dw:
  anum: 241
  eventtime: "2021-06-11T19:01:00Z"
  itemid: 530
  logtime: "2021-06-11T18:37:31Z"
  props:
    commentalter: 1623614570
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1623436797
  url: "https://fanf.dreamwidth.org/135921.html"
format: md
...

Two highlights of 2021
======================

## video: Tim Hunkin

I first encountered Tim Hunkin via his 1980s TV series, _The Secret Life Of Machines_, in which he and Rex Garrod explained how various household electrical gadgets work. One of the remarkable experiments I remember from back then was their demo of how audio tape works: they rubbed rust on sticky tape, and used it to record and play back sound. Not very hi-fi, but it worked!

Tim Hunkin's main occupation since then seems to have been as a maker of 3D mechanical cartoons. I call his machines "cartoons" because for a while he had a regular cartoon in the Observer, [_The Rudiments Of Wisdom, or, Almost Everything There Is To Know_](https://www.timhunkin.com/40_rudiments_book.htm), and his 2D people and 3D people look very similar. He has an exhibit in the basement of the Science Museum in London, inspired by _The Secret Life Of Machines_, and [amusement arcades](https://www.timhunkin.com/control/a_arcade_index.htm) in Southwold (_the Under the Pier Show_) and Holborn (_Novelty Automation_). His machines are surprising and funny!

So, this year Tim Hunkin has made a YouTube series called [_The Secret Life of Components_](https://www.timhunkin.com/a241_component-videos.htm) in which he talks about the parts that he has used when making his machines - a different kind of component in each of the 8 episodes. It's fascinating and informative.

And, as a bonus, he has also been releasing remastered versions of [_The Secret Life of Machines_](https://www.timhunkin.com/a243_Secret-Life-of-Machines-intro.htm): 11 episodes so far, with a new one added each week, plus extra commentary with Tim Hunkin's memories of filming them.

## text: [50 Years of Text Games](https://if50.substack.com/)

I was lucky to find out about this newsletter/blog at about the time of its first article, and I have been looking forward to its weekly entries ever since.

It tends to focus on the people creating the games, the context in which they worked, with enough about the games to give you an idea of what they were like, and less about the techincal details. Each episode has an epilogue saying how you can play the game today.

What I love about it is how varied the creators are: the husband-and-wife team in Florida, the lesbian house in Cork, the Czech satirists - and the games too: history, romance, politics, horror.

I confess I'm not a keen player of adventure games, but the intersection of literature, technology, and play is so cool, and this series of articles showed me how much broader and deeper interactive fiction is than I was previously aware.
