---
dw:
  anum: 226
  eventtime: "2007-08-11T08:57:00Z"
  itemid: 297
  logtime: "2007-08-11T09:07:23Z"
  props:
    import_source: livejournal.com/fanf/76323
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/76258.html"
format: html
lj:
  anum: 35
  can_comment: 1
  ditemid: 76323
  event_timestamp: 1186822620
  eventtime: "2007-08-11T08:57:00Z"
  itemid: 298
  logtime: "2007-08-11T09:07:23Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/76323.html"
title: Quoting BETA
...

<blockquote><b>To program is to understand:</b> The development of an information system is not just a matter of writing a program that does the job. It is of utmost importance that development of this program has revealed an in-depth understanding of the application domain; otherwise, the information system will probably not fit into the organization. During the development of such systems it is important that descriptions of the application domain are communicated between system specialists and the organization.</blockquote>

<p>This quote is from the book <i>Object-oriented programming in the BETA programming language</i>, and is also quoted in the HOPL III paper <a href="http://portal.acm.org/citation.cfm?id=1238854">The when, why and why not of the BETA programming language</a> which I have recently enjoyed reading.</p>
