---
dw:
  anum: 223
  eventtime: "2018-10-18T12:12:00Z"
  itemid: 505
  logtime: "2018-10-18T11:40:51Z"
  props:
    commentalter: 1540218245
    interface: flat
    picture_keyword: ""
    revnum: 3
    revtime: 1539863108
  url: "https://fanf.dreamwidth.org/129503.html"
format: md
...

Amsterdam day 5
===============

I was out late last night so I'm writing yesterday's notes this
morning.

Yesterday I attended the
[DNS](https://ripe77.ripe.net/programme/meeting-plan/dns-wg/) and
[MAT](https://ripe77.ripe.net/programme/meeting-plan/mat-wg/)
meetings, and did some work outside the meetings.


CDS
---

Ondřej Caletka presented his work on keeping DNS zone files in git.

  - Lots of my favourite tools :-) Beamer, Gitolite, `named-compilezone`

  - How to discover someone has already written a program you are
    working on: search for a name for your project :-)

BCP 20 classless in-addr.arpa delegation led to problems for Ondřej:
[RFC2317](https://tools.ietf.org/html/rfc2317) suggests putting
slashes in zone names, which causes problems for tools that want to
use zone names for file names. In my expired [RFC2317bis
draft](https://tools.ietf.org/html/draft-fanf-dnsop-rfc2317bis) I
wanted to change the recommendation to use dash ranges instead, which
better matches BIND's `$GENERATE` directive.

At the end of his talk, Ondřej mentioned his woork on automatically
updating the RIPE database using CDS records. As planned, I commented
afterwards in support, and afterwards I sent [a message to the
`dns-wg` mailing list about
CDS](https://www.ripe.net/ripe/mail/archives/dns-wg/2018-October/003593.html)
to get the formal process moving.


DNS tooling
-----------

I spoke to Florian Streibelt who did the talk on BGP community leaks
on Tuesday. I mentioned my DNS-over-TLS measurements; he suggested
looking for an uptick after christmas, and that we might be able to
observe some interesting correlations with MAC address data, e.g.
identifying manufacturer and age using the first 4 octets of the MAC
addresss. It's probably possible to get some interesting results
without being intrusive.

I spent some time with Jerry Lundstrom and Petr Špaček to have a go at
getting `respdiff` working, with a view to automated smoke testing
during upgrades, but I ran out of battery :-) Jerry and Petr talked about improving its performance: the current code relies on multiple python processes for
concurrency.

I talked to them about whether to replace the `doh101` DNS message
parser (because deleting code is good): `dnsjit` message parsing code
is C so it will require dynamic linking into `nginx`, so it might not
actually simplify things enough to be worth it.


DNS miscellanea
---------------

Ed Lewis (ICANN) on the DNSSEC root key rollover

  - next step is January 11 when the old key gets revoked and the
    DNSKEY response size will grow a few bytes bigger than it has been
    before

  - Geoff Huston says see
	<http://www.potaroo.net/ispcol/2017-08/xtn-hdrs.html> and
	<http://www.potaroo.net/ispcol/2016-11/rootstars.html> for
	information about the risks of the large response size

Petr Špaček (CZ.NIC) on the EDNS flag day, again

  - "20 years is enough time for an upgrade"

Ermias Malelgne - performance of flows in cellular networks

 - DNS: 2% lookups fail, 15% experience loss - apalling!

Tim Wattenberg - global DNS propagation times

  - zero TTLs actually work! (with caveats!)

  - <https://ismydnslive.com/> propagation checker using RIPE Atlas


Other talks
-----------

Maxime Mouchet - learning network states from RTT

  - traceroute doesn't explain some of the changes in delay

  - nice and clever analysis

Trinh Viet Doan - tracing the path to YouTube: how do v4 and v6 differ?

  - many differences seem to be due to failure to dual-stack CDN caches in ISP networks

Kevin Vermeulen - multilevel MDA-lite Paris traceroute

  - MDA = multipath detection algorithm

  - I need to read up on what Paris traceroute is ...

  - some informative notes on difficulties of measuring using RIPE
    Atlas due to NATs messing with the probe packets
