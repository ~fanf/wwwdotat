---
dw:
  anum: 38
  eventtime: "2003-03-26T14:56:00Z"
  itemid: 9
  logtime: "2003-03-26T06:58:56Z"
  props:
    commentalter: 1491292303
    current_moodid: 125
    current_music: U2
    import_source: livejournal.com/fanf/2434
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/2342.html"
format: casual
lj:
  anum: 130
  can_comment: 1
  ditemid: 2434
  event_timestamp: 1048690560
  eventtime: "2003-03-26T14:56:00Z"
  itemid: 9
  logtime: "2003-03-26T06:58:56Z"
  props:
    current_moodid: 125
    current_music: U2
  reply_count: 1
  url: "https://fanf.livejournal.com/2434.html"
title: Fresh Coffee
...

Just popped down to the market to get another half kilo of the coffee man's own Mountain Blend with which to feed the awesome caffeine requirements of a room full of Unixheads. It's a beautiful day made even more so by the wonderful chocolatey smell of freshly-ground beans.
