---
dw:
  anum: 180
  eventtime: "2007-10-04T13:59:00Z"
  itemid: 309
  logtime: "2007-10-04T14:08:01Z"
  props:
    commentalter: 1491292347
    import_source: livejournal.com/fanf/79851
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/79284.html"
format: casual
lj:
  anum: 235
  can_comment: 1
  ditemid: 79851
  event_timestamp: 1191506340
  eventtime: "2007-10-04T13:59:00Z"
  itemid: 311
  logtime: "2007-10-04T14:08:01Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/79851.html"
title: Random observations
...

Happy new year! Cambridge has 10,000 extra enthusiastic, intelligent, (young, attractive) people again, and internal email volumes are up 50%. Summer's having its last blast, as it usually does in October, making the place look its best for the new intake.

I was interested to hear <a href="http://news.bbc.co.uk/1/hi/uk/7027088.stm">a news item</a> this morning about <abbr title="Serious Organised Crime Agency">SOCA</abbr> busting a load of 419ers. The odd thing that struck me is that they are calling the crime "mass marketing fraud" instead of "spam". Also, I think that the SOCA web site looks like a <a href="http://en.wikipedia.org/wiki/Doctor_Who_tie-in_websites">Dr Who secret agency web site</a> - for example, compare <a href="http://www.soca.gov.uk/">Geocomtex</a> with <a href="http://www.geocomtex.net/">SOCA</a>.

One sad/amusing aspect of 419 spam is that it's mostly sent manually, unlike the botnet-driven techno-spam mostly sent by Americans with the help of the Russian mafia. <a href="http://www.schneier.com/blog/archives/2007/10/the_storm_worm.html">Bruce Schneier</a> and <a href="http://www.time.com/time/magazine/article/0,9171,1666279,00.html">Time Magazine</a> write about the frightening sophistication of the Storm Worm.
