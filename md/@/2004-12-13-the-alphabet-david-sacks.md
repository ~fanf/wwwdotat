---
dw:
  anum: 242
  eventtime: "2004-12-13T21:12:00Z"
  itemid: 121
  logtime: "2004-12-13T13:24:40Z"
  props:
    commentalter: 1491292317
    import_source: livejournal.com/fanf/31122
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/31218.html"
format: casual
lj:
  anum: 146
  can_comment: 1
  ditemid: 31122
  event_timestamp: 1102972320
  eventtime: "2004-12-13T21:12:00Z"
  itemid: 121
  logtime: "2004-12-13T13:24:40Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/31122.html"
title: The Alphabet / David Sacks
...

Over the weekend I read The Alphabet by David Sacks, which is a history (or just-so story) of how our letters came to have their shapes and sounds. It's very readable and informative, and this Guardian review does a good job of summarizing the best bits.

http://books.guardian.co.uk/reviews/referenceandlanguages/0,6121,1112273,00.html

However it is a rather straight-line history. It fails to explore some of the interesting tributaries, such as the divergent shapes of S and &Sigma; or Runic script or the influence of black letter and gothic scripts. The latter is an irritating omission given their brief appearance in the explanation for the shape of lower-case t.

I'd also like to know about the genesis of modern Hebrew and Arabic scripts which have diverged interestingly from our Greek inheritance. And, closer to home, some more comparison between the different sound values used by different languages would have been nice. There's a fair amount about g/j/y (which is required to explain their history) but some more about c/s with maybe something about Cyrillic to give it more context would have been useful.

But perhaps I'm asking a bit too much for a book that's already over 400 pages long.
