---
dw:
  anum: 250
  eventtime: "2018-05-25T00:36:00Z"
  itemid: 497
  logtime: "2018-05-24T23:39:18Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 2
    revtime: 1530209471
  url: "https://fanf.dreamwidth.org/127482.html"
format: md
...

Beer Festival week hacking notes - Thursday
===========================================

([Monday's notes](https://dotat.at/@/2018-05-22-beer-festival-week-hacking-notes-for-monday.html))
([Tuesday's notes](https://dotat.at/@/2018-05-23-beer-festival-week-hacking-notes-tuesday.html))
([Wednesday's notes](https://dotat.at/@/2018-05-23-beer-festival-week-hacking-notes-wednesday.html))
([Epilogue](https://dotat.at/@/2018-06-28-beer-festival-week-hacking-notes-epilogue.html))

Today's hacking was mixed, a bit like the weather! At lunch time I
hung out at the beer festival with the Collabora crowd in the sun, and
one of my arms got slightly burned. This evening I went to the pub as
usual (since the beer festival gets impossibly rammed on Thursday and
Friday evenings) and I'm now somewhat moist from the rain.


Non-conformant C compilers
--------------------------

My refactoring seems to have been successful! I only needed to fix a
few silly mistakes to get the tests to pass, so I'm quite pleased.

But the last silly mistake was very annoying.

As part of eliminating the union, I replaced expressions like
`t->branch.twigs` with an accessor function `twigs(t)`. However
`twigs` was previously used as a variable name in a few places, and I
missed out one of the renames.

Re-using a name like this during a refactoring is asking for a cockup,
but I thought the compiler would have my back because they had such
different types.

So last night's messy crash bug was caused by a line vaguely like this:

        memmove(nt, twigs + s, size);

When `twigs` is a function pointer, this is clearly nonsense. And in
fact the C standard requires an error message for arithmetic on a
function pointer. (See the constraints in section 6.5.6 of
[C99:TC3](http://www.open-std.org/JTC1/SC22/WG14/www/docs/n1256.pdf).)
But my code compiled cleanly with `-Wall -Wextra`.

Annoyingly, `gcc`'s developers decided that pointer arithmetic is such a good idea
that it ignores this requirement in the standard unless you tell it to
be `-pedantic` or you enable `-Wpointer-arith`. And in the last year
or so I have lazily stopped using `$FANFCFLAGS` since I foolishly
thought `-Wall -Wextra` covered all the important stuff.

Well, lesson learned. I should be `-pedantic` and proud of it.


COW
---

This afternoon I turned my prose description of how copy-on-write
should work into code. It was remarkably straight-forward! The
preparatory thinking and refactoring paid off nicely.

However, I forgot to implement the delete function, oops!


TODO
----

* Rewrite the hacking branch commit history into something that makes
  sense. At the very least, I need a proper explanation of what
  happened during the refactoring.

* Tests!

There's still a lot of work needed to do copy-on-write in the DNS
parts of Knot, but I am feeling more confident that this week I have
laid down some plausible foundations.
