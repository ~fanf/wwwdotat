---
dw:
  anum: 141
  eventtime: "2006-01-20T16:46:00Z"
  itemid: 187
  logtime: "2006-01-20T17:20:19Z"
  props:
    import_source: livejournal.com/fanf/48262
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/48013.html"
format: casual
lj:
  anum: 134
  can_comment: 1
  ditemid: 48262
  event_timestamp: 1137775560
  eventtime: "2006-01-20T16:46:00Z"
  itemid: 188
  logtime: "2006-01-20T17:20:19Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/48262.html"
title: The joy of disks
...

Following http://fanf.livejournal.com/47657.html

We successfully moved the backup server today, and it is now joined with its second EonStor. The first one has 16 x 250GB SATA-1 disks in 3U for 4TB raw capacity. (<a href="http://www.infortrend.com/main/2_product/a16f-r(s)1211.asp">link</a>) The second one has 24 x 500GB SATA-2 disks in 4U for 12TB raw capacity. (<a href="http://www.infortrend.com/main/2_product/a24f-g2224.asp">link</a>) The system also has 1U of fibrechannel switch, 2U of PC, and 4U of tape robot, for a total 16TB backed-up disk in 14U of space. (Compare the 26U of space for the old 0.5TB NetApp system!) These disks store the third (warm) copy of everyone's email, conveniently close to the tapes so that we can spool off the fourth (cold, off-site) copy quickly.

The EonStors, like the NetApp disk shelves, aren't mounted on sliding rails: they're just bolted to the front vertical  rails of the rack. This makes mounting them a three person job (two to hold either side of the unit while the third has a good screw). Best done with all the disks removed because that makes the units MUCH lighter. Most racking jobs can be done comfortably with two people; our tool-less Intel servers can be racked by one. The crucial thing that makes this possible is the pegs on the sides of the server that drop into the extended rails, which is much easier than having rails on the sides which (after careful alignment) slide into the rails in the rack.
