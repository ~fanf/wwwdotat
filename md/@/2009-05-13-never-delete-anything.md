---
dw:
  anum: 73
  eventtime: "2009-05-13T22:57:00Z"
  itemid: 383
  logtime: "2009-05-13T22:13:25Z"
  props:
    commentalter: 1491292364
    hasscreened: 1
    import_source: livejournal.com/fanf/99700
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/98121.html"
format: html
lj:
  anum: 116
  can_comment: 1
  ditemid: 99700
  event_timestamp: 1242255420
  eventtime: "2009-05-13T22:57:00Z"
  itemid: 389
  logtime: "2009-05-13T22:13:25Z"
  props:
    personifi_tags: "15:10,45:5,8:45,10:5,33:3,23:5,42:5,32:5,1:35,3:5,19:3,20:8,9:16,13:5"
    verticals_list: "computers_and_software,technology"
  reply_count: 6
  url: "https://fanf.livejournal.com/99700.html"
title: Never delete anything
...

<p>How long will it be before it becomes normal to archive everything? It's already normal in some situations, and I think that's increasing. It's been the norm in software development for a long time. There's an increase in append-mostly storage systems (i.e. append-only with garbage collection) which become never-delete systems if you replace the GC with an archiver. Maybe the last hold-outs for proper deletion will be high data volume servers...</p>

<p>Anyway, I feel like listing some interesting append-only and append-mostly systems. A tangent that I'm not going to follow is the rise of functional programming and immutability outside the field of storage. Many of these systems rely on cryptographic hashes to identify stuff they have already stored and avoid storing it again, making append-only much more practical.</p>

<ul>
<li>All version control systems, and software configuration management systems even more so. The former archive source code whereas the latter archive build tools and build products as well. <a href="http://www.vestasys.org/">DEC's Vesta SCM</a> is particularly interesting, being based on a purely functional build language designed to maximize memoization - i.e. minimize unnecessary rebuilds. It's sort of <a href="http://ccache.samba.org/">ccache</a> on steroids since it caches the results of entire module builds, not just individual source file compiles.</li>

<li><a href="http://nixos.org/">Nix</a> is a purely functional package manager. Unlike most packaging systems like dpkg or rpm, Nix packages do not conflict with each other: you upgrade by installing new packages alongside your existing ones, then you stop running the old ones and start running the new ones.</li>

<li>Archival / backup systems, like <a href="http://doc.cat-v.org/plan_9/4th_edition/papers/venti/">Venti</a> which is Plan 9's append-only filesystem. Apple's <a href="http://www.apple.com/macosx/features/timemachine.html">Time Machine</a> isn't nearly as clever.</li>

<li>Most filesystems don't use hash-based uniquification. Append-mostly filesystems often provide cool undelete features like snapshots, e.g. <a href="http://media.netapp.com/documents/wp_3002.pdf">NetApp's WAFL</a> or <a href="http://www.sun.com/software/solaris/zfs.jsp">Sun's ZFS</a>. Early filesystems of this kind, e.g. <a href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.46.1317">BSD LFS</a> tried to avoid wasting space, so didn't make old data available as snapshots, and sacrificed performance to eager garbage collection. More recently, <a href="http://www.dragonflybsd.org/hammer/">DragonFly BSD's Hammer filesystem</a> doesn't even have an in-kernel garbage collector, and running it is entirely optional.</li>

<li>Email archives: gmail's ever-increasing quotas, cyrus <a href="http://www-uxsup.csx.cam.ac.uk/~dpc22/cyrus/two_phase_expunge.html">delayed expunge</a>.</li>

</ul>
