---
dw:
  anum: 133
  eventtime: "2009-04-08T13:08:00Z"
  itemid: 381
  logtime: "2009-04-08T13:18:01Z"
  props:
    commentalter: 1491292378
    import_source: livejournal.com/fanf/99168
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/97669.html"
format: html
lj:
  anum: 96
  can_comment: 1
  ditemid: 99168
  event_timestamp: 1239196080
  eventtime: "2009-04-08T13:08:00Z"
  itemid: 387
  logtime: "2009-04-08T13:18:01Z"
  props:
    personifi_tags: "17:35,18:3,6:9,8:16,27:16,23:3,1:16,3:3,19:19,9:35,14:9"
    verticals_list: "news_and_politics,life"
  reply_count: 8
  url: "https://fanf.livejournal.com/99168.html"
title: LISTSERV crapness
...

<p><a href="http://www.lsoft.com/manuals/lsv-faq.stm#_Toc196887308">LISTSERV uses a null return path (RFC821 MAIL FROM:<>) on its administrative mail, and some mail hosts reject this.</a> I discard message with a null return path that do not match a few simple heuristics, so I lose things like subscription confirmations from services like JISCfail. This makes me cross.</p>

<p>L-Soft claim that LISTSERV is following the specifications, and they cite a couple of paragraphs from RFC 821 (published in 1982) and RFC 1123 (1989). However they fail to cite text from RFC 2821 (2001) which explicitly forbids what they are doing.</p>

<blockquote>
<p>There are several types of notification messages which are required by existing and proposed standards to be sent with a null reverse path, namely non-delivery notifications, other kinds of Delivery Status Notifications (DSNs), and also Message Disposition Notifications (MDNs). [...]</p>
<p>All other types of messages (i.e., any message which is not required by a standards-track RFC to have a null reverse-path) SHOULD be sent with with a valid, non-null reverse-path.</p>
</blockquote>

<p>The only other permitted use of null return paths that I know of is vacation notifications, described in RFC 3834 (published in 2004).</p>

<p>L-Soft needs to get a grip, read some RFCS,  and fix their software.</p>
