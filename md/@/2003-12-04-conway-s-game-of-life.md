---
dw:
  anum: 82
  eventtime: "2003-12-04T19:56:00Z"
  itemid: 43
  logtime: "2003-12-04T20:02:56Z"
  props:
    commentalter: 1491292306
    import_source: livejournal.com/fanf/11260
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/11090.html"
format: casual
lj:
  anum: 252
  can_comment: 1
  ditemid: 11260
  event_timestamp: 1070567760
  eventtime: "2003-12-04T19:56:00Z"
  itemid: 43
  logtime: "2003-12-04T20:02:56Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/11260.html"
title: "Conway's Game of Life"
...

I wibbled a bit at <a href="http://www.livejournal.com/community/lisp/10014.html">this entry</a> in the <a href="https://lisp.livejournal.com/">👤lisp</a> community, which prompted me to go and dig out my old Life implementation. I'm now wondering what is a fast way of turning a list of co-ordinates into an image on modern hardware and software. Plot them into a bitmap and then use BITBLT primitives to transfer it to the screen? Use a display list mechanism to get the graphics hardware to plot them?

Compared to my lightweight life algorithm the graphics is a beast. OTOH modern computers are so fast that you can probably plot only a tiny fraction of the generations and it'll look OK.
