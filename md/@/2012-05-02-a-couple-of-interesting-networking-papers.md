---
format: html
lj:
  anum: 197
  can_comment: 1
  ditemid: 120261
  event_timestamp: 1335960720
  eventtime: "2012-05-02T12:12:00Z"
  itemid: 469
  logtime: "2012-05-02T11:12:21Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/120261.html"
title: A couple of interesting networking papers
...

<p>I read a couple of interesting papers last night, both from last month's <a href="https://www.usenix.org/conference/nsdi12">USENIX symposium on networked systems design and implementation</a>.</p>

<p>The first (linked to by <a href="http://blog.felter.org/post/22160361336/ankit-singla-chi-yao-hong-lucian-popa-p-brighten">Wes Felter</a>) is <a href="https://www.usenix.org/conference/nsdi12/jellyfish-networking-data-centers-randomly">"Jellyfish: Networking Data Centers Randomly"</a> which very thoroughly shows that a random topology outperforms a structured topology, and is more easy to grow incrementally. The challenge is for higher layers to make effective use of a Jellyfish network; key technologies probably include <a href="http://www.openflow.org/">OpenFlow</a> at layer 3 and Multipath TCP at layer 4.</p>

<p>The second is <a href="https://www.usenix.org/conference/nsdi12/how-hard-can-it-be-designing-and-implementing-deployable-multipath-tcp">"How Hard Can It Be? Designing and Implementing a Deployable Multipath TCP"</a>. This is about the pragmatics of MPTCP - previous papers have described the principles behind its load balancing and congestion control. A few aspects of the paper stood out. Firstly, they gathered a lot of data on the real-world behaviour of TCP-mangling middleboxes in order to work out what changes MPTCP could get away with, and what mechanisms it needed for falling back to traditional TCP. They very frequently found that port 80 was much more likely to be mangled than other ports. They also did a lot of work to mitigate the problems caused by <a href="http://www.bufferbloat.net/">buffer bloat</a> (though they don't use that term) to the extent that an MPTCP connection over a combination of WiFi and 3G has lower latency than a traditional TCP connection over WiFi!</p>

<p>(By coincidence both of these papers have a vague Cambridge connection. The Jellyfish paper cites <a href="http://en.wikipedia.org/wiki/B%C3%A9la_Bollob%C3%A1s">Béla Bollobás</a>'s results on random graphs. The <a href="http://nrg.cs.ucl.ac.uk/mptcp/">Multipath TCP group</a> at UCL included Damon Wischik who was at Cambridge when I was; his brother Lucian was a fellow ucam.chatterer.)</p>
