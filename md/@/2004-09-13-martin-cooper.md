---
dw:
  anum: 55
  eventtime: "2004-09-13T09:47:00Z"
  itemid: 101
  logtime: "2004-09-13T03:01:32Z"
  props:
    commentalter: 1491292314
    import_source: livejournal.com/fanf/26080
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/25911.html"
format: casual
lj:
  anum: 224
  can_comment: 1
  ditemid: 26080
  event_timestamp: 1095068820
  eventtime: "2004-09-13T09:47:00Z"
  itemid: 101
  logtime: "2004-09-13T03:01:32Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/26080.html"
title: Martin Cooper
...

I was upset by news of Martin's death last week. Martin was not a close friend - I didn't see him frequently enough, though we got on with each other reasonably well and he was on my usual invitation list.

I regret not encouraging him more to come out and have fun with us.

I expect many people reading this will like to know the following:
<blockquote>
From: Debbie Finucane <D.Finucane@damtp.cam.ac.uk>
Subject: Martin's Bench

Last April when Martin and I were in the Botanical Gardens (a place Martin loved) Martin mentioned that he would like a seat dedicated to him there when he died.

Some of us are interested in trying to get this seat.  While I was speaking to his family yesterday they liked the idea and are happy for us to go ahead. mk370@cam.ac.uk has the details of the costings (between 200 and 300) and Helen (hw233@foundation.cam.ac.uk) has agreed to coordinate it.

Therefore this email is to let you know of the bench and give you a chance to contribute if you would like to.

Please pass this on to any of Martin's friends you know may be interested. The names I've used are mainly the ones from his mobile whose email addresses I think I know.                                                                                                                                       
                                                                                                                                                     
Debbie
</blockquote>
