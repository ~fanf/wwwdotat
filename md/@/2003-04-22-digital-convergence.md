---
dw:
  anum: 98
  eventtime: "2003-04-22T13:30:00Z"
  itemid: 16
  logtime: "2003-04-22T06:41:14Z"
  props:
    commentalter: 1491292304
    current_moodid: 103
    current_music: "\"True\" Euphoria mixed by Dave Pearce"
    import_source: livejournal.com/fanf/4322
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/4194.html"
format: casual
lj:
  anum: 226
  can_comment: 1
  ditemid: 4322
  event_timestamp: 1051018200
  eventtime: "2003-04-22T13:30:00Z"
  itemid: 16
  logtime: "2003-04-22T06:41:14Z"
  props:
    current_moodid: 103
    current_music: "\"True\" Euphoria mixed by Dave Pearce"
  reply_count: 4
  url: "https://fanf.livejournal.com/4322.html"
title: Digital convergence
...

I had some interesting ideas on the journey home last night.

Tivos are really excellent things that make it simple to time-shift telly so you can watch what you want when you want, instead of either watching crap or not watching it at all. However in its usual incarnation it has the disadvantage of not being able to record more than one channel at a time because of limitations in its tuner.

However if it were integrated with a digital receiver it ought to be able to record all the MPEG streams symultaneously. This would rule. (Does the Sky Plus digital satellite receiver with digital video recorder do this?) Since radio channels are also broadcast over the same medium, the Tivo could do its thing for them too.

At this point what you want is to be able to hot-sync your iPod with your Tivo. Or alternatively, put the DAB receiver and Tivo functionality into the iPod itself. This would also rule.
