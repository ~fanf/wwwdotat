---
dw:
  anum: 150
  eventtime: "2006-01-25T17:10:00Z"
  itemid: 189
  logtime: "2006-01-25T17:11:57Z"
  props:
    commentalter: 1491292333
    import_source: livejournal.com/fanf/48782
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/48534.html"
format: casual
lj:
  anum: 142
  can_comment: 1
  ditemid: 48782
  event_timestamp: 1138209000
  eventtime: "2006-01-25T17:10:00Z"
  itemid: 190
  logtime: "2006-01-25T17:11:57Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/48782.html"
title: fun with Jabber + google maps
...

I just found out about these cool Google Maps hacks which can show your Jabber presence and location. Neat!

http://map.butterfat.net/
http://jobble.uaznia.net/map
