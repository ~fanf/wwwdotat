---
dw:
  anum: 214
  eventtime: "2005-10-04T15:44:00Z"
  itemid: 151
  logtime: "2005-10-04T16:07:57Z"
  props:
    commentalter: 1491292324
    import_source: livejournal.com/fanf/38942
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/38870.html"
format: casual
lj:
  anum: 30
  can_comment: 1
  ditemid: 38942
  event_timestamp: 1128440640
  eventtime: "2005-10-04T15:44:00Z"
  itemid: 152
  logtime: "2005-10-04T16:07:57Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/38942.html"
title: "no DNS tunnelling here, please"
...

There's a rather nasty way of using the DNS to tunnel from the outside past into a private network. If you publish an NS record whose target has a private address, and you then cause a nameserver at the private network's border to query that nameserver, it will send the query to the private network. This isn't particularly damaging because the target port is fixed to 53, but it invites abuse because it's a very cool hack and the tools to do it are becoming easier to use.

What I'd like to be able to do is to tell Bind never to send queries to <a href="http://www.ietf.org/rfc/rfc3330.txt">RFC 3330</a> addresses. Bind has a "bogus server" feature that <em>almost</em> does the trick, but it can only block one IP address at a time and I want to block millions of them. However it also has a "blackhole" feature, which allows you to block whole CIDR address ranges, with the effect of the "bogus server" feature but also blocking queries from the relevant addresses. This latter is a slightly irritating side-effect if you want to accept queries from private addresses but not send queries to those addresses; however I only have one such IP address because my name servers only accept queries from localhost.

The solution I have come up with is:

<pre>
    acl bogons {
        ! 127.0.0.1;
        0.0.0.0/8;
        10.0.0.0/8;
        127.0.0.0/8;
        169.254.0.0/16;
        172.16.0.0/12;
        192.0.2.0/24;
        192.168.0.0/16;
        198.18.0.0/15;
        224.0.0.0/3;
    };
    
    options {
        listen-on-v6 { none; };
        listen-on port 53 { 127.0.0.1; };
        query-source address * port 53;
    
        allow-query { 127.0.0.1; };
        allow-notify { none; };
        allow-transfer { none; };
        blackhole { bogons; };

        // some other options here
    };
    
    server 127.0.0.1 {
        bogus yes;
    };
</pre>
