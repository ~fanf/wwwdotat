---
format: html
lj:
  anum: 141
  can_comment: 1
  ditemid: 114317
  event_timestamp: 1307625900
  eventtime: "2011-06-09T13:25:00Z"
  itemid: 446
  logtime: "2011-06-09T12:25:57Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/114317.html"
title: IPv6 day stats
...

<p>Here are some numbers from yesterday's activity on the University of Cambridge's central mail relays. We advertised AAAA records for our services between about 08:20 and 20:30 local time (+01:00). We did not make any changes to the A records which point at IPv4-only servers; the added AAAA records pointed at a couple of dual-stack servers.</p>

<p><b>Incoming mail:</b> <tt>mx.cam.ac.uk</tt>
<ul>
<li>223651 messages over IPv4
<li>2204 messages over IPv6
<li>1.0% ipv6
</ul>
A lot of nerdy senders. Prominent on the list were NANOG, the IETF, FreeBSD, Debian, Haskell, UKNOF, Exim, Lua, ISC, Tor. The stats are somewhat distorted by my own personal mailing list subscriptions! Apart from that we got mail over IPv6 from several universities: Imperial, Vienna, Reading, TU-Berlin, Southampton (ECS), Valencia, Leicester, Oslo, Malta. Notable service providers include Retrosnub, chiark, Mythic Beasts, Fastmail, Andrews & Arnold, Bytemark.</p>

<p><b>Outgoing smart host:</b> <tt>ppsw.cam.ac.uk</tt>
<ul>
<li>122384 messages over IPv4
<li>3915 messages over IPv6
<li>3.1% IPv6
</ul>
This traffic reflects which parts of the University have IPv6 connectivity. Almost all came from the SRCF (which handles a lot of mail) and the Institute of Astronomy.</p>

<p><b>Message submission service:</b> <tt>smtp.hermes.cam.ac.uk</tt>
<ul>
<li>90015 messages over IPv4
<li>226 messages over IPv6
<li>of which 185 were over 6to4
<li>and 6 from outside the University
<li>0.25% IPv6
<li>0.20% 6to4
</ul>
Again almost all this traffic is internal to the University so it reflects the proportion of IPv6 connectivity on our edge networks. A lot of these have poor layer 2 security, in particular little protection against rogue router advertisements.</p>

<p><b>Outgoing mail</b> from the dual-stack servers
<ul>
<li>6103 outgoing deliveries total
<li>5907 over IPv4
<li>196 over IPv6
<li>3.2% IPv6
<li>1759 to internal University destinations
<li>117 over IPv6
<li>6.7% IPv6
<li>4344 messages delivered to external destinations
<li>79 over IPv6
<li>1.8% IPv6
</ul>
All this mail arrived over IPv6. You can see the effect of the SRCF and Astronomy again.</p>

<p><b>Mail reader access</b> <tt>webmail/imap/pop.hermes.cam.ac.uk</tt></p>
<p>The rightmost columns below are v6 logins from outside the University.</p>
<table>
<tr align="right">
<th></th>
<th></th>
<th>v4</th>
<th>&nbsp;&nbsp;&nbsp;v6</th>
<th>%</th>
<th>6to4</th>
<th>%</th>
<th>ext</th>
<th>%</th>
</tr>
<tr align="right">
<th>webmail</th><th>logins</th>
<td>72149</td>
<td>525</td>
<td>0.7%</td>
<td>194</td>
<td>37%</td>
<td>141</td>
<td>27%</td>
</tr>
<tr align="right">
<th></th><th>clients</th>
<td>15696</td>
<td>135</td>
<td>0.9%</td>
<td>83</td>
<td>61%</td>
<td>18</td>
<td>13%</td>
</tr>
<tr align="right">
<th>imaps</th><th>logins</th>
<td>361956</td>
<td>752</td>
<td>0.2%</td>
<td>407</td>
<td>54%</td>
<td>144</td>
<td>19%</td>
</tr>
<tr align="right">
<th></th><th>clients</th>
<td>10380</td>
<td>101</td>
<td>1.0%</td>
<td>56</td>
<td>55%</td>
<td>20</td>
<td>20%</td>
</tr>
<tr align="right">
<th>imap-tls</th><th>logins</th>
<td>117109</td>
<td>295</td>
<td>0.25%</td>
<td>27</td>
<td>9%</td>
<td>76</td>
<td>26%</td>
</tr>
<tr align="right">
<th></th><th>clients</th>
<td>4312</td>
<td>41</td>
<td>0.9%</td>
<td>10</td>
<td>24%</td>
<td>10</td>
<td>24%</td>
</tr>
<tr align="right">
<th>pops</th><th>logins</th>
<td>162738</td>
<td>128</td>
<td>0.1%</td>
<td>41</td>
<td>32%</td>
<td>55</td>
<td>43%</td>
</tr>
<tr align="right">
<th></th><th>clients</th>
<td>2629</td>
<td>6</td>
<td>0.2%</td>
<td>4</td>
<td>67%</td>
<td>1</td>
<td>17%</td>
</tr>
<tr align="right">
<th>pop-tls</th><th>logins</th>
<td>27741</td>
<td>0</td>
</tr>
<tr align="right">
<th></th><th>clients</th>
<td>453</td>
<td>0</td>
</tr>
</table>
