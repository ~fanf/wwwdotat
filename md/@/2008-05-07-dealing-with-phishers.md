---
dw:
  anum: 239
  eventtime: "2008-05-07T16:00:00Z"
  itemid: 340
  logtime: "2008-05-07T15:00:42Z"
  props:
    commentalter: 1491292353
    import_source: livejournal.com/fanf/88033
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/87279.html"
format: html
lj:
  anum: 225
  can_comment: 1
  ditemid: 88033
  event_timestamp: 1210176000
  eventtime: "2008-05-07T16:00:00Z"
  itemid: 343
  logtime: "2008-05-07T15:00:42Z"
  props: {}
  reply_count: 19
  url: "https://fanf.livejournal.com/88033.html"
title: Dealing with phishers
...

<p>There's been a raft of phishing attacks against Universities over the last few months. We received a couple of thousand of these last night:</p>
<pre>
Subject:  CONFIRM YOUR EMAIL ADDRESS
Date:     Tue, 6 May 2008 16:08:53 -0400
From:     CAM SUPPORT TEAM <phishy>
Reply-To: <phishy>

Dear Cam Subscriber,

To complete your (CAM) account, you must reply to this email
immediately and enter your password here (*********)

Failure to do this will immediately render your email address
deactivated from our database.

You can also confirm your email address by logging into your
CAM account at www.webmail.cam.ac.uk/

Thank you for using CAM.AC.UK!
FROM THE CAM SUPPORT TEAM
</pre>
<p>We did the usual announcement dance, including a notice on the <a href="https://webmail.hermes.cam.ac.uk">webmail login page</a>, but this did not prevent some users (including webmail users!) from replying to the phish.</p>

<p><a href="https://captain_aj.livejournal.com/">👤captain_aj</a> suggests scanning email to reject it if it contains the user's password. I wonder how long it would take to <tt>crypt()</tt> every word of every message... :-)</p>
