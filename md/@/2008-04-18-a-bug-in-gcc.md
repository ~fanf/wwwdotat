---
dw:
  anum: 25
  eventtime: "2008-04-18T16:34:00Z"
  itemid: 337
  logtime: "2008-04-18T15:34:16Z"
  props:
    import_source: livejournal.com/fanf/87112
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/86297.html"
format: html
lj:
  anum: 72
  can_comment: 1
  ditemid: 87112
  event_timestamp: 1208536440
  eventtime: "2008-04-18T16:34:00Z"
  itemid: 340
  logtime: "2008-04-18T15:34:16Z"
  props:
    verticals_list: "life,technology"
  reply_count: 0
  url: "https://fanf.livejournal.com/87112.html"
title: A bug in gcc
...

<p>It seems selog tickles <a href="http://gcc.gnu.org/bugzilla/show_bug.cgi?id=35973">a live-code analysis bug in gcc</a>. Here's a fairly minimal test case:</p>
<pre>
$ cat gccbug.c

struct sel {
        const char  *name;
        unsigned int enabled;
};

int selog_on(struct sel *);

#define selog_on(sel) \
        (sel.enabled == 0 \
                ? selog_on(&sel) \
                : sel.enabled != 42)

void dostuff(int *);

void gccbug1(void) {
        struct sel sel = { "test", 0 };
        if(selog_on(sel)) {
                dostuff(0);
        }
}

void gccbug2(void) {
        struct sel sel = { "test", 0 };
        if(selog_on(sel)) {
                int i;
                dostuff(&i);
        }
}


$ gcc -O -Wunreachable-code -c gccbug.c
gccbug.c: In function `gccbug2':
gccbug.c:25: warning: will never be executed
$ gcc --version
gcc --version
gcc (GCC) 3.4.4 [FreeBSD] 20050518

$ gcc -O -Wunreachable-code -c gccbug.c
gccbug.c: In function ‘gccbug2’:
gccbug.c:24: warning: will never be executed
gccbug.c: In function ‘gccbug1’:
gccbug.c:17: warning: will never be executed
$ gcc -O -Werror -Wunreachable-code -c gccbug.c
cc1: warnings being treated as errors
gccbug.c: In function ‘gccbug2’:
gccbug.c:24: warning: will never be executed
$ gcc --version
gcc (GCC) 4.1.2 20070115 (prerelease) (SUSE Linux)

$ gcc -O -Wunreachable-code -c gccbug.c
gccbug.c: In function 'gccbug2':
gccbug.c:24: warning: will never be executed
gccbug.c: In function 'gccbug1':
gccbug.c:17: warning: will never be executed
$ gcc -O -Werror -Wunreachable-code -c gccbug.c
cc1: warnings being treated as errors
gccbug.c: In function 'gccbug2':
gccbug.c:24: warning: will never be executed
$ gcc --version
gcc (GCC) 4.2.1 20070719  [FreeBSD]

</pre>
