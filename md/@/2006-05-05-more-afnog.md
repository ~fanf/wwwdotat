---
dw:
  anum: 226
  eventtime: "2006-05-05T07:36:00Z"
  itemid: 229
  logtime: "2006-05-05T07:51:26Z"
  props:
    commentalter: 1491292341
    import_source: livejournal.com/fanf/59091
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/58850.html"
format: casual
lj:
  anum: 211
  can_comment: 1
  ditemid: 59091
  event_timestamp: 1146814560
  eventtime: "2006-05-05T07:36:00Z"
  itemid: 230
  logtime: "2006-05-05T07:51:26Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/59091.html"
title: More AfNOG
...

The classrooms are close to being ready now that the PCs are no longer crammed into the staging area where they had FreeBSD installed. The classroom switches are being configured and the last bits of network are being deployed.

There was an interesting discussion at breakfast which I mostly just listened to. Apparently, the Nigerian government is keen to set up one or more local Internet exchange points - but it turns out that it won't be an IXP in the usual sense (organized as a multilateral agreement between ISPs) but a government-run transit provider that buys and sells international bandwidth. Furthermore, the idea came from the government's security people, and there's a background plan to funnel Internet traffic through this organization so that it can be monitored more easily. Pity.

We also talked about PACNOG, which will happen soon. This is the Pacific Islands analogue of AfNOG, and it turns out the ISPs in these sets of countries have similar difficulties - satellite links, poor resources, lack of knowledge.

A lot of the western instructors here are heavily involved in NOG workshops around the world, and the African instructors are key people in their countries' technical Internet communities.
