---
dw:
  anum: 190
  eventtime: "2015-10-19T22:48:00Z"
  itemid: 433
  logtime: "2015-10-19T21:48:04Z"
  props:
    import_source: livejournal.com/fanf/138238
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/111038.html"
format: html
lj:
  anum: 254
  can_comment: 1
  ditemid: 138238
  event_timestamp: 1445294880
  eventtime: "2015-10-19T22:48:00Z"
  itemid: 539
  logtime: "2015-10-19T21:48:04Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/138238.html"
title: "never mind the quadbits, feel the width!"
...

<h2>benchmarking wider-fanout versions of qp tries</h2>

<p><a href="http://dotat.at/prog/qp/">QP tries</a> are faster than crit-bit tries because they extract up to
four bits of information out of the key per branch, whereas crit-bit
tries only get one bit per branch. If branch nodes have more children,
then the trie should be shallower on average, which should mean
lookups are faster.</p>

<p>So if we extract even more bits of information from the key per
branch, it should be even better, right? But, there are downsides to
wider fanout.</p>

<p>I originally chose to index by nibbles for two reasons: they are easy
to pull out of the key, and the bitmap easily fits in a word with room
for a key index.</p>

<p>If we index keys by 5-bit chunks we pay a penalty for more complicated
indexing.</p>

<p>If we index keys by 6-bit chunks, trie nodes have to be bigger to fit
a bigger bitmap, so we pay a memory overhead penalty.</p>

<p>How do these costs compare to the benefits of wider branches?</p>

<p>I have implemented 5-bit and 6-bit versions of qp tries and benchmarked them. For those interested in the extremely nerdy details, <a href="http://dotat.at/prog/qp/blog-2015-10-19.html">the full version of "never mind the quadbits"</a> is on the <a href="http://dotat.at/prog/qp/">qp trie home page</a>.</p>

<p>The tl;dr is, 5-bit qp tries are fairly unequivocally better than 4-bit qp tries. 6-bit qp tries are less clear - they are not faster than 5-bit on my laptop and desktop (both Core 2 Duo), but they are faster on a Xeon server that I tried despite using more memory.</p>
