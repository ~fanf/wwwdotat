---
dw:
  anum: 12
  eventtime: "2003-07-22T17:08:00Z"
  itemid: 27
  logtime: "2003-07-22T17:12:43Z"
  props:
    commentalter: 1491292304
    current_moodid: 24
    current_music: Hybrid
    import_source: livejournal.com/fanf/7039
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/6924.html"
format: casual
lj:
  anum: 127
  can_comment: 1
  ditemid: 7039
  event_timestamp: 1058893680
  eventtime: "2003-07-22T17:08:00Z"
  itemid: 27
  logtime: "2003-07-22T17:12:43Z"
  props:
    current_moodid: 24
    current_music: Hybrid
  reply_count: 5
  url: "https://fanf.livejournal.com/7039.html"
title: Good grief!
...

So you're migrating away from an email domain, and have got to the final turn-it-off stage. Do you tell the central mail hub admins? Do you make sure all your users know? Do you make sure there are no references to it left in the routing for the replacement domain? Do you even know what the thousand messages a day going to that domain are?

AARGH!
