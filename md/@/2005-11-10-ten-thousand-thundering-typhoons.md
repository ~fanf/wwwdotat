---
dw:
  anum: 152
  eventtime: "2005-11-10T18:38:00Z"
  itemid: 163
  logtime: "2005-11-10T18:52:36Z"
  props:
    commentalter: 1491292325
    import_source: livejournal.com/fanf/42205
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/41880.html"
format: casual
lj:
  anum: 221
  can_comment: 1
  ditemid: 42205
  event_timestamp: 1131647880
  eventtime: "2005-11-10T18:38:00Z"
  itemid: 164
  logtime: "2005-11-10T18:52:36Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/42205.html"
title: Ten thousand thundering typhoons!
...

Once more, Pat Stewart does my dirty work. (I'm amused that none of my colleagues suggested toning down the sarcastic bit!)

http://www.cam.ac.uk/cgi-bin/wwwnews?grp=ucam.comp-serv.announce&art=1431
<blockquote style="margin-left:1em;border-left:2px solid;padding-left:1em">Since the summer, insecure access to Hermes has been forbidden to new users. We are planning to extend this rule to the whole University by next summer. In preparation for the next step, this term we have been monitoring usage of Hermes to make a list of easy cases.

Next Monday, 14th November, we will withdraw insecure access to Hermes from those users who have not used insecure configurations - that is, from those users who should not be affected by this change.

We are also deprecating the ~/mail folder name prefix. On Monday we will also disable the backwards-compatibility support for those who do not need it.

Although these changes should not affect too many users, our list of insecure users is still growing so this change will affect some people. This is inevitable whatever time we pick.

After Monday we will have somewhat over 9,000 users to reconfigure, which is a rather daunting prospect. We are proposing to sort them by affiliation before working through them, so that users in a department or college will be dealt with together, rather than in dribs and drabs. We will send a list of affected users to the relevant support staff a reasonable amount of time before they will have to change: although the timetable is ambitious we don't want to turn it into a mad rush.

We hope this process seems fair to computer officers and techlinks. If you have any suggestions for ways in which we can make it less painful, please contact &lt;postmaster&#64;hermes.cam.ac.uk&gt; - though if anyone suggests not doing it at all, we will consider giving them a particularly strict timetable. The details are deliberately vague at the moment because we expect to refine them based on experience.

For background information and links to the documentation of the correct settings for Hermes, see http://www.cam.ac.uk/cs/email/securehermes.html
</blockquote>
