---
dw:
  anum: 245
  eventtime: "2007-10-26T18:44:00Z"
  itemid: 311
  logtime: "2007-10-26T19:17:46Z"
  props:
    commentalter: 1491292348
    import_source: livejournal.com/fanf/80188
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/79861.html"
format: casual
lj:
  anum: 60
  can_comment: 1
  ditemid: 80188
  event_timestamp: 1193424240
  eventtime: "2007-10-26T18:44:00Z"
  itemid: 313
  logtime: "2007-10-26T19:17:46Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/80188.html"
title: An older 999 story
...

The previous occasion on which I should have called 999 was when I was living in Finchley in north London. I was walking home from work at Demon late one Thursday evening (I think in November 1998) along <a href="http://maps.google.com/?ll=51.599667,-0.191531&t=h&z=17">Station Road</a> when I heard a strange intermittent crashing noise coming closer. Soon a chav came into sight, holding an aluminium baseball bat. He waved it at me in a vaguely threatening manner as I passed him, but I ignored him and  we continued on our ways. It soon became apparent what the noise was: several of the cars had had their windows or mirrors smashed, and I could hear a few more getting similar treatment behind me.

<b>At this point I should have called 999</b> but instead I went home and called it in on the non-emergency number. The operator did not sound particularly interested, but took down the details. (I suppose it was around chucking-out time.)

On the following Saturday morning I got a call from the police who wanted to take a formal statement from me. It turned out that on Friday night bat-brain decided to try his toy out on someone's head. I was able to give a vague description of him, though I had only seen him for a few seconds by sodium lighting and hadn't looked too closely to avoid provoking him.

The case rumbled slowly on over the next several months. There were two attempts to organize an identification parade; the first one didn't happen because the necessary people weren't available. The second one was held in Brixton (IIRC), and I was driven there from Demon Towers and back by a police officer - it would have been faster, cheaper, and easier to take the tube. By this stage it was several months later and so I was unable to pick him out of the line-up, though some time later when the pressure was off (too late to be useful, though) I recognized him as the one with the chavvy dye job and earring. Also present was the victim and her man. I think her head was still not completely healed.

Some months after that I was contacted again and warned to be ready to be called as a witness in court. In the end I wasn't needed, I think because he pleaded guilty. I never found out what his sentence was.

The whole thing was very unsatisfactory: the slack response to my initial call; the lack of admonishment to call 999 in that kind of situation; the huge delays between arrest, id parade, and trial; and the lack of follow-up to tell me what the conclusion was. Cambridgeshire Constabulary seems much more on the ball than the Met.
