---
dw:
  anum: 88
  eventtime: "2003-07-30T15:03:00Z"
  itemid: 30
  logtime: "2003-07-30T15:05:18Z"
  props:
    current_mood: manic
    current_music: Timo Maas presents Music for the Maases
    import_source: livejournal.com/fanf/7906
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/7768.html"
format: casual
lj:
  anum: 226
  can_comment: 1
  ditemid: 7906
  event_timestamp: 1059577380
  eventtime: "2003-07-30T15:03:00Z"
  itemid: 30
  logtime: "2003-07-30T15:05:18Z"
  props:
    current_mood: manic
    current_music: Timo Maas presents Music for the Maases
  reply_count: 0
  url: "https://fanf.livejournal.com/7906.html"
title: distributed checksum clearinghouse
...

Maybe drinking lots of coffee is a bad idea when working with this software, because I'm increasingly keen to murder its author by force-feeding him copies of hier(7) and the FHS.
