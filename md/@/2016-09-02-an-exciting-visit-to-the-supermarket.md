---
dw:
  anum: 244
  eventtime: "2016-09-02T17:35:00Z"
  itemid: 465
  logtime: "2016-09-02T16:35:07Z"
  props:
    commentalter: 1491292419
    import_source: livejournal.com/fanf/146212
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/119284.html"
format: html
lj:
  anum: 36
  can_comment: 1
  ditemid: 146212
  event_timestamp: 1472837700
  eventtime: "2016-09-02T17:35:00Z"
  itemid: 571
  logtime: "2016-09-02T16:35:07Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 8
  url: "https://fanf.livejournal.com/146212.html"
title: An exciting visit to the supermarket
...

<p>Towards the end of this story, <a href="https://twitter.com/CountrySkills/status/769902726817935360">a dear friend of ours pointed
out</a> that
when I tweeted about having plenty of "doctors" I actually meant "medics".</p>

<p>Usually this kind of pedantry is not considered to be very polite, but
I live in Cambridge; pedantry is our thing, and of course she was
completely correct that amongst our friends, "Dr" usually means PhD,
and even veterinarians outnumber medics.</p>

<p>And, of course, any story where it is important to point out that the
doctor is actually someone who works in a hospital, is not an entirely
happy story.</p>

<p>At least it did not involve eye surgeons!</p>

<h2>Happy birthday</h2>

<p>My brother-in-law's birthday is near the end of August, so last
weekend we went to Sheffield to celebrate his 30th. <a href="http://rmc28.dreamwidth.org/649607.html">Rachel wrote
about our logistical
cockups</a>, but by the time of
the party we had it back under control.</p>

<p>My sister's house is between our AirB&amp;B and the shops, so we walked
round to the party, then I went on to Sainsburys to get some food and
drinks.</p>

<p>A trip to the supermarket SHOULD be boring.</p>

<h2>Prosectomy!</h2>

<p>While I was looking for some wet wipes, one of the shop staff nearby
was shelving booze. He was trying to carry too many bottles of
prosecco in his hands at once.</p>

<p>He dropped one.</p>

<p>It hit the floor about a metre from me, and smashed - exploded -
something hit me in the face!</p>

<p>"Are you ok?" he said, though he probably swore first, and I was still
working out what just happened.</p>

<p>"Er, yes?"</p>

<p>"You're bleeding!"</p>

<p>"What?" It didn't feel painful. I touched my face.</p>

<p>Blood on my hand.</p>

<p>Dripping off my nose. A few drops per second.</p>

<p>He started guiding me to the first aid kit. "Leave your shopping there."</p>

<p>We had to go past some other staff stacking shelves. "I just glassed a
customer!" he said, probably with some other explanation I didn't hear
so clearly.</p>

<p>I was more surprised than anything else!</p>

<h2>What's the damage?</h2>

<p>In the back of the shop I was given some tissue to hold against the
wound. I could see in the mirror in the staff loo it was just a cut on
the bridge of my nose.</p>

<p>I wear specs.</p>

<p>It could have been a LOT worse.</p>

<p>I rinsed off the blood and <em>Signor Caduto Prosecco</em> found some sterile
wipes and a chair for me to sit on.</p>

<p>Did I need to go to hospital, I wondered? I wasn't in pain, the
bleeding was mostly under control. Will I need stitches? Good grief,
what a faff.</p>

<p>I phoned my sister, the junior doctor.</p>

<p>"Are you OK?" she asked. (Was she amazingly perceptive or just
expecting a minor shopping quandary?)</p>

<p>"Er, no." (Usual bromides not helpful right now!) I summarized what
had happened.</p>

<p>"I'll come and get you."</p>

<h2>Suddenly!</h2>

<p>I can't remember the exact order of events, but before I was properly
under control (maybe before I made the phone call?) the staff call
bell rang several times in quick succession and they all ran for the
front of house.</p>

<p>It was a shoplifter alert!</p>

<p>I gathered from the staff discussions that this guy had failed to
steal a chicken and had escaped in a car. Another customer was scared
by the would-be-thief following her around the shop, and took refuge
in the back of the shop.</p>

<h2>Departure</h2>

<p>In due course my sister arrived, and we went with <em>Signor Rompere
Bottiglie</em> past large areas of freshly-cleaned floor to get my
shopping. He gave us a £10 discount (I wasn't in the mood to fight for
more) and I pointedly told him not to try carrying so many bottles in
the future.</p>

<p>At the party there were at least half a dozen medics :-)</p>

<p>By this point my nose had stopped bleeding so I was able to inspect
the damage and <a href="https://twitter.com/fanf/status/769901503318548480">tweet about what happened</a>.</p>

<p>My sister asked around to find someone who had plenty of A&amp;E
experience and a first aid kit. One of her friends cleaned
the wound and applied steri-strips to minimize the scarring.</p>

<p><img src="https://pbs.twimg.com/media/Cq89vb1XgAEpx_D.jpg" width=200>
<img src="https://pbs.twimg.com/media/Cq9BYbuW8AEaKmv.jpg" width=200></p>

<p>It's now healing nicely, though still rather sore if I touch it without care.</p>

<p>I just hope I don't have another trip to the supermarket which is
quite so eventful...</p>
