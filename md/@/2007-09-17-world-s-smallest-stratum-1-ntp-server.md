---
dw:
  anum: 59
  eventtime: "2007-09-17T17:25:00Z"
  itemid: 306
  logtime: "2007-09-17T17:40:40Z"
  props:
    import_source: livejournal.com/fanf/78648
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/78395.html"
format: casual
lj:
  anum: 56
  can_comment: 1
  ditemid: 78648
  event_timestamp: 1190049900
  eventtime: "2007-09-17T17:25:00Z"
  itemid: 307
  logtime: "2007-09-17T17:40:40Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/78648.html"
title: "World's smallest stratum 1 NTP server?"
...

Really tiny ARM computer the size of an RJ45 socket:
http://www.digi.com/products/embeddedsolutions/digiconnectmespecs.jsp

Really tiny GPS module (also contains an ARM):
http://www.rfsolutions.co.uk/acatalog/GPS_Module.html

I think all you'd need to add is a power converter from POE (48V) to the 3.3V wanted by the modules.
