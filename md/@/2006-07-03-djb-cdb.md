---
dw:
  anum: 20
  eventtime: "2006-07-03T21:56:00Z"
  itemid: 243
  logtime: "2006-07-03T21:23:20Z"
  props:
    import_source: livejournal.com/fanf/62543
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/62228.html"
format: casual
lj:
  anum: 79
  can_comment: 1
  ditemid: 62543
  event_timestamp: 1151963760
  eventtime: "2006-07-03T21:56:00Z"
  itemid: 244
  logtime: "2006-07-03T21:23:20Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/62543.html"
title: DJB / cdb
...

I've been playing around with Dan Bernstien's cdb recently. We use it quite heavily - most of the configuration tables on our mail servers are cdb files. I'm quite fond of its simplicity. But the code... DJB doesn't really do layers of abstraction. (He even says <a href="http://cr.yp.to/cdb.html">"Packages that need to read cdb files should incorporate the necessary portions of the cdb library rather than relying on an external cdb library."</a>) He also doesn't believe in <tt>libc</tt>: the cdb code includes an incorrect declaration for <tt>errno</tt>, a braindead wrapper around <tt>malloc()</tt>, inefficient string functions, a dumb reinvented <strike>wheel</strike> <tt>strerror()</tt>, etc. etc. When I was reading the algorithm that constructs the hash tables, I added comments as I went. One of them said:
<pre>
  /* Afterwards c->start[i] points to the *end* of the ith block. */
</pre>Nice variable name!

(Admittedly the next loop adjusts the pointers so that the name becomes correct, but still.)
