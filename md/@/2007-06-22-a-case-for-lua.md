---
dw:
  anum: 36
  eventtime: "2007-06-22T21:39:00Z"
  itemid: 289
  logtime: "2007-06-22T23:10:37Z"
  props:
    commentalter: 1491292343
    import_source: livejournal.com/fanf/74389
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/74020.html"
format: html
lj:
  anum: 149
  can_comment: 1
  ditemid: 74389
  event_timestamp: 1182548340
  eventtime: "2007-06-22T21:39:00Z"
  itemid: 290
  logtime: "2007-06-22T23:10:37Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/74389.html"
title: A case for Lua
...

<p>I've been playing around with <a href="http://www.lua.org/">Lua</a> recently. It's a really sweet little language. A particular highlight is its "tables", which are generalized associative arrays that can have keys of any type. (Most languages only allow strings and/or numbers.) It also has pretty nice uniform handling of multiple function arguments, multiple return values, and multiple assignment.</p>

<p>A frequent topic of discussion on <a href="http://www.lua.org/lua-l.html">the Lua list</a> is the fact that Lua does not have a switch/case statement. Most of the people who propose how one should be added tend to copy other languages which can only switch on one value at a time. I think it would me more in keeping with the rest of Lua to have a multi-valued case statement. I also quite like the idea of making it capable of ML-style pattern matching and variable binding.</p>

<p>The syntax I have in mind is as follows. I'm using <tt>?</tt> to mean 0 or 1 of the following, <tt>*</tt> to mean 0 or more, and <tt>+</tt> to mean 1 or more.
<pre>
    <b>case</b> <i>explist1</i>
+(+(<b>match</b> <i>patlist</i>
      ?(<b>if</b> <i>exp</i>) )
    <b>then</b>
        <i>block</i>)
  ?(<b>else</b>
        <i>block</i>)
    <b>end</b>
</pre>
As in C, multiple matches for a given block are expressed using multiple match clauses, instead of the alternatives being listed in a single clause as in Modula. Each match can be guarded by a conditional expression; when it evaluates to false the match fails and later ones are tried. The statements following <b>then</b> are evaluated if one of the preceding <b>match</b>es succeeds. You can put a "default" or "otherwise" case after an <b>else</b>. (I quite like the re-use of keywords, though perhaps it is too cute.) All variables bound by the <b>match</b> clauses are visible in the following block; the variables in a pattern list must all be different; if the clause binding a variable isn't the one that matched then the variable's value is nil.</p>

<p>Lua also needs more expressive exception handling - I'm not too fond of its "protected call" mechanism. It would be more palatable if it were dressed up in something like:
<pre>
    <b>try</b>
        <i>block</i>
+(+(<b>catch</b> <i>patlist</i>
      ?(<b>if</b> <i>exp</i>) )
    <b>then</b>
        <i>block</i> )
  ?(<b>finally</b>
        <i>block</i> )
    <b>end</b>
</pre>
</p>

<p>A useful bit of syntactic sugar is to be able to declare functions in a pattern matching style instead of an argument-passing style, so:
<pre>
    <b>function</b> (...)
        <b>case</b> ...
        <b>match</b> a, b, c
            blah blah
        <b>end</b>
    <b>end</b>
</pre>
could be more concisely written:
<pre>
    <b>function</b>
    <b>match</b> a, b, c
        blah blah
    <b>end</b>
</pre>
</p>

<p>I'm still pondering the syntax for pattern lists. A basic principle is that all direct comparisons should be against constants, so that the match can be compiled into a table lookup against a constant table. This means only numbers and strings - other objects in Lua either do not have compile-time expressions (C userdata) or have expressions that do not denote constant values but instead describe how to create new objects at run-time (function closures and tables). I also want to allow indirect comparisons against table elements, so that it's possible to match against structured data. Finally there needs to be a way of binding a variable to the value being matched against.</p>

<p>I do not intend to support exact matches against tables: only the elements that you specify in the match will be checked and if the table has more elements they will be ignored. This is because the extra check to make the match exact may not be entirely trivial to implement, and because it makes code brittle in the presence of unexpected values squirreled away in a table - it's quite common in prototype-based object systems to add methods to an object without co-operation from the code that created it.</p>

<p>So the basic syntax for a pattern to match against simple constants or to bind a variable is:
<pre>
    <i>String</i> | <i>Number</i> | <i>Name</i>
</pre>
Matching against the first few integer indexed elements of a table in list style is also simple:
<pre>
    "{" <i>pattern</i> *( "," <i>pattern</i> ) "}"
</pre>
</p>

<p>It's also useful to both match against a table and bind a variable, so that you can refer to the table in the later statements. (It isn't possible to reconstruct the table because it may have extra elements, and even if it were possible it would be inefficient and the result would have a different identity.) It's also useful to both match a constant and bind a variable, if you have multiple match clauses: <b>case</b> f() <b>match</b> 3 <b>match</b> 4 <b>then</b> -- did f return three or four? I'm still unsure about the syntax for matching and binding. Some possibilities are:
<pre>
    <i>pattern</i> <b>and</b> <i>pattern</i>
    <i>pattern</i> <i>pattern</i>
    <i>Name</i> "=" <i>pattern</i>
    <i>Name</i> "==" <i>pattern</i>
</pre>
</p>

<p>The idea of "<b>and</b>" patterns is that you are matching both the left-hand side and the right-hand side. It suggests that there should also be "<b>or</b>" patterns, as in <b>case</b> f() <b>match</b> 3 <b>or</b> 4 <b>then</b> ... In <a href="http://www.cl.cam.ac.uk/~mr10/MCPL.html">MCPL</a> you get the same effect as <b>and</b> by just writing the patterns side-by-side, and (like sh) | is used instead of <b>or</b>.</p>

<p>The idea of using "=" is to make binding in a pattern look like assignment, whereas "==" is supposed to be like an assertion. However these get problematic when we try to fit key/value style table matching into the syntax. Table constructors in Lua look like:
<pre>
    <i>tableconstructor</i> = "{" ?<i>fieldlist</i> "}"
    <i>fieldlist</i> = <i>field</i> *(<i>fieldsep</i> <i>field</i>) ?<i>fieldsep</i>
    <i>fieldsep</i> = "," | ";"
    <i>field</i> = "[" <i>exp</i> "]" "=" <i>exp</i>
        | <i>Name</i> "=" <i>exp</i>
        | <i>exp</i>
</pre>
(The first form of field is by analogy with the <tt>table[key]</tt> form of index expression. The second is like <tt>table.name</tt> indexing, which is syntactic sugar for <tt>table["name"]</tt>. The last is for list-style construction, where the value is assigned to the next integer index starting from 1.)</p>

<p>If we follow constructor syntax closely, we get the serious difficulty that a pattern like <tt>{ foo = bar }</tt> isn't clear which name is the table index and which is the variable being bound. This probably indicates that <tt>=</tt> should be avoided.</p>

<p>Perhaps the solution is to use an arrow to indicate which way values flow, as in:
<pre>
    <i>tablepattern</i> = "{" ?<i>fieldpatterns</i> "}"
    <i>fieldpatterns</i> = <i>fieldpat</i> *(<i>fieldsep</i> <i>fieldpat</i>) ?<i>fieldsep</i>
    <i>fieldpat</i> = "[" (<i>String</i> | <i>Number</i>) "]" "->" <i>pattern</i>
        | <i>Name</i> "->" <i>pattern</i>
        | <i>pattern</i>
    <i>pattern</i> = <i>String</i> | <i>Number</i> | <i>Name</i>
        | <i>pattern</i> <b>and</b> <i>pattern</i>
        | <i>pattern</i> <b>or</b> <i>pattern</i>
</pre>
This seems reasonably OK, I think. It also suggests adding a free-standing match operator <tt>-></tt> that produces a boolean result (and probably should not bind variables).</p>

<p>It makes me want to change the assignment operator to <tt>&lt;-</tt> for symmetry :-) But fiddling with Lua's existing features is the topic for another post.<p>
