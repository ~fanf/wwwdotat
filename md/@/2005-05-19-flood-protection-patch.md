---
dw:
  anum: 108
  eventtime: "2005-05-19T18:25:00Z"
  itemid: 143
  logtime: "2005-05-19T18:26:07Z"
  props:
    commentalter: 1491292322
    import_source: livejournal.com/fanf/37052
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/36716.html"
format: casual
lj:
  anum: 188
  can_comment: 1
  ditemid: 37052
  event_timestamp: 1116527100
  eventtime: "2005-05-19T18:25:00Z"
  itemid: 144
  logtime: "2005-05-19T18:26:07Z"
  props: {}
  reply_count: 10
  url: "https://fanf.livejournal.com/37052.html"
title: Flood protection patch
...

I've posted my rate limiting patch to the exim-users mailing list. You can see it at: 

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/antiforgery/exim-ratelimit.patch
