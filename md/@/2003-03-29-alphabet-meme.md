---
dw:
  anum: 32
  eventtime: "2003-03-29T02:29:00Z"
  itemid: 13
  logtime: "2003-03-28T18:57:15Z"
  props:
    current_moodid: 77
    current_music: "Queen , Fat Bottomed Girls"
    import_source: livejournal.com/fanf/3419
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/3360.html"
format: casual
lj:
  anum: 91
  can_comment: 1
  ditemid: 3419
  event_timestamp: 1048904940
  eventtime: "2003-03-29T02:29:00Z"
  itemid: 13
  logtime: "2003-03-28T18:57:15Z"
  props:
    current_moodid: 77
    current_music: "Queen , Fat Bottomed Girls"
  reply_count: 0
  url: "https://fanf.livejournal.com/3419.html"
title: Alphabet meme
...

I'm going to do this from the point of view of my Brompton. (The mood above is a lie.)

A - Act your age? Need more TLC, but this may be more to do with quantity of work rather than age.
B - Born on which day of the week? Unknown and uninteresting.
C - Chore you hate? Any maintenance involving the rear wheel assembly.
D - Dad's name? Invented by Andrew Richie.
E - Essential makeup item? Chain lubricant.
F - Favourite actor? Nicole Kidman (as seen in BMX Bandits)
G - Gold or silver? Black (colour of silver oxide)
H - Hometown? Built and purchased in London, but now in Cambridge where Andrew Richie studied engineering.
I - Instruments you play? Guitar (like Brian May in "Bicycle Race")
J - Job title? Bicycle.
K - Kids? Small wheels are for adult bikes too.
L - Living arrangements? Under the stairs during the night, and under the desk during the day.
M - Mum's name? Unknown and possibly interesting.
N - Number of people you have slept with? Close contact with four or five bottoms.
O - Overnight hospital stay? Three times, mainly for transmission repairs.
P - Phobia? Pot-holes are bad for small wheels.
Q - Quote you like? "Small wheels suggest the rider is a clown."
R - Religious affiliation? uk.rec.cycling
S - Siblings? The Moulton, the Birdy, and other lesser imitators.
T - Time you wake up? 10:00-12:00 ish.
U - Unique habit? Carrying a loudly rattling D lock.
V - Vegetable you refuse to eat? All of them.
W - Worst habit? Twitchy steering.
X - X-rays you've had? None.
Y - Yummy food you make? Bumcheese.
Z - Zodiac sign? Bumcheese.
