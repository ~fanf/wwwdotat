---
dw:
  anum: 107
  eventtime: "2005-10-25T16:13:00Z"
  itemid: 158
  logtime: "2005-10-25T16:19:08Z"
  props:
    commentalter: 1491292359
    import_source: livejournal.com/fanf/40706
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/40555.html"
format: casual
lj:
  anum: 2
  can_comment: 1
  ditemid: 40706
  event_timestamp: 1130256780
  eventtime: "2005-10-25T16:13:00Z"
  itemid: 159
  logtime: "2005-10-25T16:19:08Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/40706.html"
title: Deathless prose
...

I'm preparing a document that gives advice on sending email from within Cambridge University. The aim is to correct some confusion about the difference between our message submission server and our smart host, which has not mattered much in the past but will when we disable unauthenticated access to the former. I'm also documenting the forthcoming rate limiting restrictions.

Any comments or questions are welcomed. It'll be announced properly in due course...

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/misc/sending-from-cudn.txt
