---
dw:
  anum: 255
  eventtime: "2008-11-18T12:26:00Z"
  itemid: 368
  logtime: "2008-11-18T12:26:57Z"
  props:
    commentalter: 1491292362
    import_source: livejournal.com/fanf/95681
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/94463.html"
format: html
lj:
  anum: 193
  can_comment: 1
  ditemid: 95681
  event_timestamp: 1227011160
  eventtime: "2008-11-18T12:26:00Z"
  itemid: 373
  logtime: "2008-11-18T12:26:57Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/95681.html"
title: Licence revoked
...

<p>I'm happy to say that the problem I ranted about in <a href="http://fanf.livejournal.com/95404.html">my previous entry</a> has been fixed. The general <a href="http://www.admin.cam.ac.uk/committee/isss/otherguidelines/bulkemail.html">guidelines for bulk email</a> have been restored, and the new <a href="http://www.admin.cam.ac.uk/committee/isss/otherguidelines/internalbulkemail.html">guidelines for <strike>intraspam</strike> large internal mailing lists</a> have been published on their own page.</p>
