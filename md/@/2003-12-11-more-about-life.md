---
dw:
  anum: 253
  eventtime: "2003-12-11T15:46:00Z"
  itemid: 45
  logtime: "2003-12-11T15:51:29Z"
  props:
    commentalter: 1491292307
    current_moodid: 103
    current_music: Chemical Brothers -- Dig your own hole
    import_source: livejournal.com/fanf/11733
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/11773.html"
format: casual
lj:
  anum: 213
  can_comment: 1
  ditemid: 11733
  event_timestamp: 1071157560
  eventtime: "2003-12-11T15:46:00Z"
  itemid: 45
  logtime: "2003-12-11T15:51:29Z"
  props:
    current_moodid: 103
    current_music: Chemical Brothers -- Dig your own hole
  reply_count: 1
  url: "https://fanf.livejournal.com/11733.html"
title: More about Life
...

I've written more about my latest fun hacking and stuck it on my web page at http://dotat.at/prog/misc/life.html

I also posted about the early days of Life on a local computing history newsgroup...

I have a piece of paper kicking around in my archives which is a printout of an email containing a forwarded copy of a Usenet article in rec.games.programmer on 25th Jan 1988 (which is not in Google's archive). In this article, John Francis reminisces about being the first person to implement Conway's Game of Life on a computer. He says:

<blockquote>I was a student at Cambridge University when John Conway was developing the game, and I happened to stroll into the Mathematics common room when he was tracking the fate of the 'r' pentomino (using a 'go' board and counters). This seemed to me to be a task ideally suited for a computer, so I headed off to the Institute of Theoretical Astronomy, where there was an IBM 360 Model 44 with a lot of free time on it.</blockquote>
Life was first described to the general public by Martin Gardner in Scientific American in October 1970, which puts Francis's episode around the time that the Phoenix mainframe (also an IBM) was being commissioned. (I can't find a more precise date than this.)

You can read Gardner's article at the following URL. It mentions Mike Guy who works at the Computing Service, and Steve Bourne who wrote a certain shell language, but their implementation was on a PDP7 rather than an IBM.

http://ddi.cs.uni-potsdam.de/HyFISCH/Produzieren/lis_projekt/proj_gamelife/ConwayScientificAmerican.htm
