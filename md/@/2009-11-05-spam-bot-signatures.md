---
format: html
lj:
  anum: 153
  can_comment: 1
  ditemid: 103833
  event_timestamp: 1257433440
  eventtime: "2009-11-05T15:04:00Z"
  itemid: 405
  logtime: "2009-11-05T15:57:53Z"
  props:
    personifi_tags: "28:5,17:5,18:3,8:44,23:5,42:5,16:5,1:44,29:1,3:12,19:3,4:3,9:31,nterms:yes"
    verticals_list: life
  reply_count: 6
  url: "https://fanf.livejournal.com/103833.html"
title: Spam bot signatures
...

<p>Recently I have been investigating spam bot signatures, specifically the characteristic domain names they choose to put in their SMTP HELO commands. A lot of spam bots use the same HELO domains from lots of different compromised PCs, which makes them quite easy to spot and block without any risk of blocking legitimate email. This kind of block can take care of about 15%-20% of spam without relying on 3rd party services like DNSBLs. Of course, this is one of the techniques used to populate the <a href="http://www.spamhaus.org/xbl/index.lasso">Spamhaus XBL</a> so the only advantage of doing it yourself is if you want to spot spam bots that have not yet been spotted by the Spamhaus guys.</p>

<p>Steve Champeon's <a href="http://enemieslist.com/">Enemieslist service</a> is the highly developed commercial implementation of this idea. His patterns are much more comprehensive, covering spam bot signatures, domestic IP connectivity (like the Spamhaus PBL), and spam-infested netblocks.</p>

<p>Yesterday evening I was thinking about how to automatically identify spam bot signatures, when I realised that <a href="http://fanf.livejournal.com/82764.html">I had already written the code to do the job</a>! I wanted to count how many different IP addresses were using the same HELO domain, and block connections that used excessively popular domains. All I needed was a few lines of Exim configuration:</p>

<pre>
  deny
    message   = Probable spam bot HELO seen from $sender_rate networks
    condition = ${if !eqi{localhost.localdomain}{$sender_helo_name} }
  ! verify    = helo
    ratelimit = 4 / 1w / per_conn / strict \
      / unique=${mask:$sender_host_address/24} / ${lc:$sender_helo_name}
</pre>

<p>Let's unpack this in reverse order.</p>

<p>We're measuring the rate of use of HELO domains, so the ratelimit key is <tt>${lc:$sender_helo_name}</tt>. It's forced to lower case so that <tt>SERVER</tt> and <tt>server</tt> are treated as the same thing.</p>

<p>But we don't care about the total usage rate, only the rate of uses from different unique IP addresses. The <tt>unique=</tt> option invokes the Bloom filter code to avoid counting each spam bot more than once. In fact we only count different unique /24 network blocks, in order to avoid false positives from mail clusters in which all servers use the same name. For example, Facebook's MTAs all say <tt>HELO mx-out.facebook.com</tt> though they are spread across about 100 IP addresses on a couple of /24 networks.</p>

<p>The <tt>strict</tt> option means keep counting even when the measured rate has passed the limit. The <tt>per_conn</tt> option means only count once for each connection (which mainly helps with efficiency).</p>

<p>The smoothing period is set to one week, which should mean that Exim doesn't easily forget which HELO domains have been abused.</p>

<p>I've currently got the limit set to 4 blocks of /24. It might even be reasonable to reduce this to 3. I'll need to run it a bit longer to see if any more odd false positives sneak out of the woodwork.</p>

<p>We do not apply this check if the DNS agrees with the HELO domain. There are some legitimate host names which are being heavily abused by spam bots, such as <tt>mail.aol.com</tt> and <tt>mx54.mail.com</tt>, so we want to block them if the connection comes from anywhere other than the host itself. (Sadly Facebook's MTAs are misconfigured so they don't pass this check.)</p>

<p>The only exception to this (so far) is <tt>localhost.localdomain</tt> which is the result of a popular misconfiguration (or lack of configuration) on legitimate Unix MTAs. If I find any other false positives they'll get checked in a similar way.</p>

<p><b>ETA</b> <tt>rediffmail.com</tt> also needs whitelisting - it's the mail service of <a href="http://www.rediff.com/">rediff.com</a> which is a portal for Indian expats. Also <tt>easyjet.com</tt>.</p>

<p>This heuristic seems to catch about three different kinds of spam bot behaviour.</p>

<ul>
<li>The HELO domain is the same as the domain in the MAIL FROM address. Spammers like to forge email "from" surprisingly few popular sites.</li>
<li>The HELO domain is one of a few bare hostnames, such as <tt>pc</tt> or <tt>computer</tt> - I guess they use the name of the compromised host.</li>
<li>The HELO domain is a parent domain of an ISP's edge network, e.g. <tt>telesp.net.br</tt>.</li>
</ul>

<p>I'm really pleased by how easy and effective this has turned out to be. The only annoyance is that it took me 20 months to realise that my Bloom filter ratelimit code could do this! Also I hope there aren't too many lurking gotchas that I haven't spotted yet.</p>

<p>This check really shows up a long-standing weakness in Exim's hints database implementation. It just uses a local DBM file to store ratelimit data, so each individual server in my SMTP cluster has to accumulate data on spam bot HELO domains without being able to benefit from the experience of the rest of the cluster. I suppose I should spend some quality time with <a href="http://1978th.net/tokyotyrant/">Tokyo Tyrant</a>...</p>
