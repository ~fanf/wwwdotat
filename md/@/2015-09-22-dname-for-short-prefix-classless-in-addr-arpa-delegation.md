---
dw:
  anum: 177
  eventtime: "2015-09-22T14:05:00Z"
  itemid: 429
  logtime: "2015-09-22T13:05:58Z"
  props:
    import_source: livejournal.com/fanf/137204
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/110001.html"
format: html
lj:
  anum: 244
  can_comment: 1
  ditemid: 137204
  event_timestamp: 1442930700
  eventtime: "2015-09-22T14:05:00Z"
  itemid: 535
  logtime: "2015-09-22T13:05:58Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/137204.html"
title: DNAME for short-prefix classless in-addr.arpa delegation
...

<p><a href="https://tools.ietf.org/html/rfc2317">RFC 2317</a> describes classless <tt>in-addr.arpa</tt> delegation. The <tt>in-addr.arpa</tt> reverse DNS name space allows for delegation at octet boundaries, /8 or /16 or /24, so DNS delegation dots match the dots in IP addresses. This worked OK for the old classful IP address architecture, but classless routing allows networks with longer prefixes, e.g. /26, which makes it tricky to delegate the reverse DNS to the user of the IP address space.

<p>The RFC 2317 solution is a clever use of standard DNS features. If you have been delegated 192.168.144.128/26, in the parent reverse DNS zone <tt>144.168.192.in-addr.arpa</tt> your ISP sets up CNAME records instead of PTR records for all 64 addresses in the subnet. These CNAME records point into some other zone controlled by you where the actual PTR records can be found.

<p>RFC 2317 suggests subdomain names like <tt>128/26.144.168.192.in-addr.arpa</tt> but the / in the label is a bit alarming to people and tools that expect domain names to follow strict letter-digit-hyphen syntax. You can just as well use a subdomain name like <tt>128-255.144.168.192.in-addr.arpa</tt> or even put the PTR records in a completely different branch of the DNS.

<p>For shorter prefixes it is still normal to delegate reverse DNS at multiple-of-8 boundaries. This can require an awkwardly large number of zones, especially  if your prefix is a multiple-of-8 plus one. For instance, our <a href="http://www.cl.cam.ac.uk">Computer Laboratory</a> delegated half of their /16 to be used by the rest of the University, and <a href="http://news.uis.cam.ac.uk/articles/2015/09/17/10-128-0-0-9-reassignment-and-university-wireless-address-changes">we are preparing to use half of 10.0.0.0/8 to provide address space for our wireless service</a>. Both of these address space allocations would normally require 128 zones to be delegated.

<p>Fortunately there is a way to reduce this to one zone, analogous to the RFC 2317 trick, but using a more modern DNS feature, the DNAME record (<a href="https://tools.ietf.org/html/rfc2672"><strike>RFC 2672</strike></a> <a href="https://tools.ietf.org/html/rfc6672">RFC 6672</a>). So RFC 2317 says, for long prefixes you replace a lot of PTR records with CNAME records pointing into another zone. Correspondingly, for short prefixes you replace a lot of delegations (NS and DS records) with DNAME records pointing into another zone.

<p>We are using this technique in 128.232.128.0/17 so that we only need to have one reverse DNS zone instead of 128 zones. The DNAME records point from (for instance) <tt>255.232.128.in-addr.arpa</tt> to <tt>255.232.128.in-addr.arpa.cam.ac.uk</tt>. Yes, we are using part of the "forward" DNS namespace to hold "reverse" DNS records! The apex of this zone is <tt>in-addr.arpa.cam.ac.uk</tt> so we can in principle consolidate any other reverse DNS address space into this same zone.

<p>This works really nicely - DNAME support is sufficiently widespread that <a href="http://fanf.livejournal.com/116744.html">it mostly just works, with a few caveats</a> mainly affecting outgoing mail servers.

<p>For 10.128.0.0/9 we are planning to do use the DNAME trick again. However, because it is private address space we don't want to consolidate it into the public <tt>in-addr.arpa.cam.ac.uk</tt> zone. The options are to use <tt>in-addr.arpa.private.cam.ac.uk</tt> (which would allow us to consolidate 172.16.0.0/12 if we choose) or <tt>128-255.10.in-addr.arpa</tt> which would be more similar to usual RFC 2317 style.

<p>Example zone file for short-prefix classless in-addr.arpa delegation:
<pre>
    $ORIGIN 10.in-addr.arpa.
    $TTL 1h

    @         SOA   ns0.example.com. (
                    hostmaster.example.com.
                    1 30m 15m 1w 1h )

              NS    ns1.example.com.
              NS    ns2.example.com.

    0-127     NS    ns1.example.com.
              NS    ns2.example.com.

    128-255   NS    ns1.example.com.
              NS    ns2.example.com.

    $GENERATE 0-127   $ DNAME $.0-127
    $GENERATE 128-255 $ DNAME $.128-255
</pre>

<p>With classless delegations like that your PTR records have names like <tt>40.30.20.0-127.10.in-addr.arpa</tt>.

<p><small><i>(I should probably write this up as an Internet-Draft but a quick blog post will do for now.)</i></small>
