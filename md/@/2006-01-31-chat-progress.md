---
dw:
  anum: 254
  eventtime: "2006-01-31T16:37:00Z"
  itemid: 191
  logtime: "2006-01-31T16:37:20Z"
  props:
    commentalter: 1491292328
    import_source: livejournal.com/fanf/49239
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/49150.html"
format: casual
lj:
  anum: 87
  can_comment: 1
  ditemid: 49239
  event_timestamp: 1138725420
  eventtime: "2006-01-31T16:37:00Z"
  itemid: 192
  logtime: "2006-01-31T16:37:20Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/49239.html"
title: Chat progress?
...

So the Information Technology Syndicate Technical Committe Meeting happened this afternoon. I prepared <a href="http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2006-01-itstc/">a handout</a> which (in the absence of any guidelines about what it should contain) was basically a brain dump. I quoted Blaise Pascal - "I didn't have time to make it shorter" - which (surprisingly) got a chuckle from the attendees. Rather than going through it in detail I talked briefly about the motivation for the project and the reasons for choosing Jabber and the things that I hope will make it popular. There were a few questions, the most penetrating of which were about whether we'll provide any kind of role addresses (the answer being no, because it requires support from the protocol which isn't really there yet) and whether spam will be a problem (maybe in the future if Jabber becomes very popular, but not yet, and at least Jabber is resistent to spoofing). All in all a fairly easy ride.

So maybe I'll finally (after three months of faff) be allowed to make progress... Next job: finish racking the irritating bastards Wogan and Parky.
