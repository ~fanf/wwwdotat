---
dw:
  anum: 23
  eventtime: "2015-09-04T16:59:00Z"
  itemid: 428
  logtime: "2015-09-04T15:59:19Z"
  props:
    commentalter: 1491292410
    hasscreened: 1
    import_source: livejournal.com/fanf/136801
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/109591.html"
format: casual
lj:
  anum: 97
  can_comment: 1
  ditemid: 136801
  event_timestamp: 1441385940
  eventtime: "2015-09-04T16:59:00Z"
  itemid: 534
  logtime: "2015-09-04T15:59:19Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/136801.html"
title: Rachel update
...

I visited <a href="https://rmc28.livejournal.com/">👤rmc28</a> this morning. She's having a hard time this week: a secondary infection has given her a high fever. (Something like this is apparently typical at this point in the treatment.) The antibiotics are playing havoc with her guts and her hair is starting to come out. She's very tired and dopey, finding it difficult to think or read or do very much at all. Rough.

Visitors are still welcome, though you'll probably find she's happy to listen but not very talkative.
