---
dw:
  anum: 98
  eventtime: "2007-05-20T02:16:00Z"
  itemid: 286
  logtime: "2007-05-20T02:02:08Z"
  props:
    import_source: livejournal.com/fanf/73562
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/73314.html"
format: casual
lj:
  anum: 90
  can_comment: 1
  ditemid: 73562
  event_timestamp: 1179627360
  eventtime: "2007-05-20T02:16:00Z"
  itemid: 287
  logtime: "2007-05-20T02:02:08Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/73562.html"
title: Promises are equivalent to channels
...

You can classify formalisms for message-passing concurrency into channel-oriented (like <a href="http://en.wikipedia.org/wiki/Pi-calculus">the &pi; calculus</a>) or destination-oriented (like <a href="http://en.wikipedia.org/wiki/Actor_model">the actor model</a>).

The <a href="http://www.erights.org/#2levels">E programming language</a> is based on a kind of remote procedure call, and so is superficially destination-based: the communication described by the RPC is targeted at a particular destination object. However the process performing the RPC does not block waiting for the result, since this can be a very long time in widely distributed systems. Instead it immediately gets a <a href="http://www.erights.org/elib/distrib/pipeline.html">promise</a> for the eventual result; it can then perform RPCs on the promise which will be handled by the evential result as soon as it has been determined. This allows you to pipeline RPCs so that networking delays overlap instead of accumulate.

E also allows you to create promises without making an RPC, and these promises are directly equivalent to a one-shot channel. You can pass the sending and receiving ends around in any way you like, and the receivers can attach actions to the promise to be performed when it has been fulfilled. On this foundation it's reasonably straight-forward to create persistent channels: whenever a channel is used, the sender has to create a replacement promise which is passed with its main message; after handling the message, the receivers listen for the next message on the new promise instead of the old one. If there are multiple senders then they must have copies of both ends of the promise, so that they can spot when it has been used and update their reference to the replacement.

However not all promises are first class. When you perform an E-style RPC, you get an explicit handle on the receiving end of a promise, but not on the sending end - that is just implicit in the return value of the remote procedure. If this procedure returns a promise (the result of another RPC) then the two promises effectively become unified: fulfilling the second promise fulfills the first. There's no way of having more than one sender on this chain of promises because you can't branch it, unless you add first-class promises.

This all has some implications for distributed garbage collection: as well as direct references to remote objects, E can refer to them via promises. With first-class promises the distributed topology can be arbitrarily complicated via both kinds of references. I do not yet know whether restricting promises to RPC only is worth it for simplifying GC and/or too harmful to programming flexibility.
