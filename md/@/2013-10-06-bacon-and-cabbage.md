---
dw:
  anum: 99
  eventtime: "2013-10-06T15:23:00Z"
  itemid: 394
  logtime: "2013-10-06T14:23:41Z"
  props:
    import_source: livejournal.com/fanf/128236
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/100963.html"
format: html
lj:
  anum: 236
  can_comment: 1
  ditemid: 128236
  event_timestamp: 1381072980
  eventtime: "2013-10-06T15:23:00Z"
  itemid: 500
  logtime: "2013-10-06T14:23:41Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/128236.html"
title: Bacon and cabbage
...

<p>Bacon and brassicas are best friends. Here's a very simple recipe which is popular in the Finch household.

<h3>Ingredients</h3>

<ul>
<li>A Savoy cabbage
<li>Two large or three small onions
<li>A few cloves of garlic
<li>A 200g pack of bacon
<li>Oil or butter for frying
<li>Soured cream
</ul>

<h3>Prep</h3>

<p>Chop the onion
<p>Slice the cabbage to make strips about 1cm wide
<p>Cut up the bacon to make lardons

<h3>Cook</h3>

<p>Get a large pan that is big enough to hold all the cabbage. Heat the fat, press in the garlic, then bung in the onion and bacon. Fry over a moderate heat until the bacon is cooked.

<p>Add the cabbage. Stir to mix everything together and keep stirring so it cooks evenly. As the cabbage cooks down and becomes more manageable you can put the heat right up to brown it slightly. Keep stir-frying until the thick ribby parts of the cabbage are soft as you like, usually several minutes. (I haven't timed it since I taste to decide when it is done...)

<p>I serve this as a main dish with just a few dollops of sour cream on top and plenty of black pepper.
