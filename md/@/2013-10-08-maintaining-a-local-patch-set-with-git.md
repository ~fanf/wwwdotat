---
dw:
  anum: 92
  eventtime: "2013-10-08T16:30:00Z"
  itemid: 395
  logtime: "2013-10-08T15:30:46Z"
  props:
    commentalter: 1491292431
    import_source: livejournal.com/fanf/128282
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/101212.html"
format: html
lj:
  anum: 26
  can_comment: 1
  ditemid: 128282
  event_timestamp: 1381249800
  eventtime: "2013-10-08T16:30:00Z"
  itemid: 501
  logtime: "2013-10-08T15:30:46Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/128282.html"
title: Maintaining a local patch set with git
...

<p>We often need to patch the software that we run in order to fix bugs quickly rather than wait for an official release, or to add functionality that we need. In many cases we have to maintain a locally-developed patch for a significant length of time, across multiple upstream releases, either because it is not yet ready for incorporation into a stable upstream version, or because it is too specific to our setup so will not be suitable for passing upstream without significant extra work.

<p>I have been experimenting with a git workflow in which I have a feature branch per patch. (Usually there is only one patch for each change we make.) To move them on to a new feature release, I tag the feature branch heads (to preserve history), rebase them onto the new release version, and octopus merge them to create a new deployment version. This is rather unsatisfactory, because there is a lot of tedious per-branch work, and I would prefer to have branches recording the development of our patches rather than a series of tags.

<p>Here is a git workflow suggested by Ian Jackson which I am trying out instead. I don't yet have much experience with it; I am writing it down now as a form of documentation.

<p>There are three branches:
<ul>
<li><i>upstream</i>, which is where public releases live
<li><i>working</i>, which is where development happens
<li><i>deployment</i>, which is what we run
</ul>

<p>Which branch corresponds to <i>upstream</i> may change over time, for instance when we move from one stable version to the next one.

<p>The <i>working</i> branch exists on the developer's workstation and is not normally published. There might be multiple <i>working</i> branches for work-in-progress. They get rebased a lot.

<p>Starting from an <i>upstream</i> version, a <i>working</i> branch will have a number of mature patches. The developer works on top of these in commit-early-commit-often mode, without worrying about order of changes or cleanliness. Every so often we use <tt>git</tt> <tt>rebase</tt> <tt>--interactive</tt> to tidy up the patch set. Often we'll use the "squash" command to combine new commits with the mature patches that they amend. Sometimes it will be rebased onto a new upstream version.

<p>When the <i>working</i> branch is ready, we use the commands below to update the <i>deployment</i> branch. The aim is to make it look like updates from the <i>working</i> branch are repeatedly merged into the <i>deployment</i> branch. This is so that we can push updated versions of the patch set to a server without having to use <tt>--force</tt>, and pulling updates into a checked out version is just a fast-forward. However this isn't a normal merge since the tree at the head of <i>deployment</i> always matches the most recent good version of <i>working</i>. (This is similar to what <a href="http://www.procode.org/stgit/doc/stg-publish.html"><tt>stg</tt> <tt>publish</tt></a> does.) Diagramatically,

<pre>
     |
    1.1
     | \
     |  `A---B-- 1.1-patched
     |    \       |
     |     \      |
     |      `C-- 1.1-revised
     |            |
    2.0           |
     | \          |
     |  `-C--D-- 2.0-patched
     |            |
    3.1           |
     | \          |
     |  `-C--E-- 3.1-patched
     |            |
  <i>upstream</i>        |
              <i>deployment</i>
</pre>

<p>The horizontal-ish lines are different rebased versions of the patch set. Letters represent patches and numbers represent version tags. The tags on the <i>deployment</i> branch are for the install scripts so I probably won't need one on every update.

<p>Ideally we would be able to do this with the following commands:

<pre>
    <b>$</b> git checkout <i>deployment</i>
    <b>$</b> git merge -s theirs <i>working</i>
</pre>

<p>However there is an "ours" merge strategy but not a "theirs" merge strategy. <a href="http://article.gmane.org/gmane.comp.version-control.git/163631">Johannes Sixt described how to simulate git merge -s theirs</a> in a post to the git mailing list in 2010. So the commands are:

<pre>
    <b>$</b> git checkout <i>deployment</i>
    <b>$</b> git merge --no-commit -s ours <i>working</i>
    <b>$</b> git read-tree -m -u <i>working</i>
    <b>$</b> git commit -m "Update to $(git describe <i>working</i>)"
</pre>

<p>Mark Wooding suggested the following more plumbing-based version, which unlike the above does not involve switching to the <i>deployment</i> branch.

<pre>
    <b>$</b> d="$(git rev-parse <i>deployment</i>)"
    <b>$</b> w="$(git rev-parse <i>working</i>)"
    <b>$</b> m="Update <i>deployment</i> to $(git describe <i>working</i>)"
    <b>$</b> c="$(echo "$m" | git commit-tree -p $d -p $w <i>working</i>^{tree})
    <b>$</b> git update-ref -m "$m" <i>deployment</i> $c $d
    <b>$</b> unset c d w
</pre>

<p>Now to go and turn this into a script...
