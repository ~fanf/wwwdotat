---
dw:
  allowmask: 1
  anum: 234
  eventtime: "2003-09-29T16:07:00Z"
  itemid: 36
  logtime: "2003-09-29T16:08:13Z"
  props:
    commentalter: 1491292306
    import_source: livejournal.com/fanf/9330
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  security: usemask
  url: "https://fanf.dreamwidth.org/9450.html"
format: casual
lj:
  allowmask: 1
  anum: 114
  can_comment: 1
  ditemid: 9330
  event_timestamp: 1064851620
  eventtime: "2003-09-29T16:07:00Z"
  itemid: 36
  logtime: "2003-09-29T16:08:13Z"
  props: {}
  reply_count: 2
  security: usemask
  url: "https://fanf.livejournal.com/9330.html"
title: "Of course there's a party too..."
...

If you can read this, you are invited :)

Date: Saturday 18th October 2003
Time: 2pm until late, drop in any time
Place: 11 Atherton Close, Cambridge, CB4 2BE
Map: http://www.streetmap.co.uk/streetmap.dll?G2M?X=545528&Y=259962&A=Y&Z=4

We'll have barbeque if weather permits, and beer from Milton Brewery in any case. Please bring your choice of food and drink. Crash space and/or local B&B information available on request.

Apologies for the short notice; even if you can only make some of the party we would be very glad to see you.
