---
dw:
  anum: 49
  eventtime: "2003-05-12T08:30:00Z"
  itemid: 20
  logtime: "2003-05-12T01:56:27Z"
  props:
    commentalter: 1491292304
    current_moodid: 27
    current_music: Music for the Maases 2
    import_source: livejournal.com/fanf/5160
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/5169.html"
format: casual
lj:
  anum: 40
  can_comment: 1
  ditemid: 5160
  event_timestamp: 1052728200
  eventtime: "2003-05-12T08:30:00Z"
  itemid: 20
  logtime: "2003-05-12T01:56:27Z"
  props:
    current_moodid: 27
    current_music: Music for the Maases 2
  reply_count: 1
  url: "https://fanf.livejournal.com/5160.html"
title: Peak District
...

On Saturday, rmc28 and I were dragged up to Derbyshire by cjo24 and ser28 for a bit of real climbing. It was only my fourth outing but I'm clearly being properly sucked in -- I spent £40 on some climbing shoes which definitely helped with their awesome grippiness. We met up with some of the Mepal crowd on <a href="http://www.streetmap.co.uk/streetmap.dll?G2M?X=426530&Y=382460&A=Y&Z=4">the Burbage rocks</a> and spent some pleasant hours until we got rained off. I managed to climb up <a href="http://www.pardoes.nildram.co.uk/pages/burbage.htm">Black Slab</a> (belayed from above) which appears to have no holds on it to speak of, apart from the surface being quite rough -- I still don't know how I did it.

After that we spent a while wandering around the very picturesque Derwent Valley reservoirs before stuffing our faces with nice Chinese food at a restaurant in a town called Hope. Then back to Cam. listening to a Billy Conolly tape. Sunday was given over to lengthy relaxation :-)

We're going to do this all again next weekend, but properly this time: up on Friday evening and back on Sunday, camping overnight. Fun fun fun!
