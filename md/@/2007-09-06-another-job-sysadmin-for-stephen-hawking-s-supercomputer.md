---
dw:
  anum: 99
  eventtime: "2007-09-06T16:55:00Z"
  itemid: 303
  logtime: "2007-09-06T17:05:32Z"
  props:
    commentalter: 1491292354
    import_source: livejournal.com/fanf/77880
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/77667.html"
format: casual
lj:
  anum: 56
  can_comment: 1
  ditemid: 77880
  event_timestamp: 1189097700
  eventtime: "2007-09-06T16:55:00Z"
  itemid: 304
  logtime: "2007-09-06T17:05:32Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/77880.html"
title: "Another job: sysadmin for Stephen Hawking's supercomputer"
...

(Thanks to Debbie for pointing out this one.)

<b>COSMOS System Manager and Relativity Group Computer Officer in the Department of Applied Maths and Theoretical Physics</b>

<a href="http://www.damtp.cam.ac.uk/cosmos">The COSMOS Supercomputer</a> is a national level high performance computing facility, dedicated to cosmological research, involving a collaboration of 28 investigators from 10 UK Universities. Professor Hawking is the principal investigator of the COSMOS consortium which has been in existence for 10 years. At present the consortium operates a 152 core SGI Altix 4700, with 456 Gb globally shared memory and over 20Tb storage, and applications are pending for a state-of-the-art 10 Tflop system.

The Relativity & Gravitation Group is a large research group comprising at present six Professors, three Readers, one lecturer, three teaching fellows, six postdoctoral researchers and twenty two PhD students. It has a group secretary, plus a computer officer/administrator shared with COSMOS.

We are seeking a System Administrator to manage the COSMOS supercomputer facility and inter-University consortium, including configuration and troubleshooting of the physical system, software maintenance and development, user administration, support and training. The role holder will also provide administration of COSMOS financial matters, including grant preparation, procurement and budgeting.

The role holder will also provide system management of Relativity & Gravitation Group computer resources, including procurement, system configuration, maintenance and development, user support and training. The role holder also maintains contact with computer support staff at a Departmental and University level where this is required to further the Group's research objectives, and occasionally provides support and training outside the Group context where the Department requires additional support or developmental effort.

You should hold a good first degree, preferably in a science or computer science subject, and at least 2 years' experience with networked, unix-based computers, in particular Linux, at system administration level, while supporting a large scientific user base. At least 1 year's experience of administering a large supercomputer facility is strongly preferred. Familiarity with high performance computing resources, such as the configuration and maintenance of clusters, storage area networks and numerical software, would also be useful. You will need to communicate clearly with individuals for training purposes and with representatives of external organisations.

You will have the ability to balance a budget, the ability to write coherently for applications and scientific reports using TeX/LaTeX, and the ability to write HTML web pages for user documentation and public interest.

This post is available for 2 years with the possibility of further extension, subject to funding. <a href="http://www.damtp.cam.ac.uk/vacancy/">More information can be found on the DAMTP web site</a>. (They don't say what grade the role has, but I'd expect <a href="http://www.admin.cam.ac.uk/offices/personnel/salary/grades2.html">9ish</a>.)
