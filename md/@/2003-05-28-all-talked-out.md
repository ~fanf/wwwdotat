---
dw:
  anum: 12
  eventtime: "2003-05-28T15:45:00Z"
  itemid: 24
  logtime: "2003-05-28T07:53:35Z"
  props:
    current_moodid: 109
    current_music: peace
    import_source: livejournal.com/fanf/6365
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/6156.html"
format: casual
lj:
  anum: 221
  can_comment: 1
  ditemid: 6365
  event_timestamp: 1054136700
  eventtime: "2003-05-28T15:45:00Z"
  itemid: 24
  logtime: "2003-05-28T07:53:35Z"
  props:
    current_moodid: 109
    current_music: peace
  reply_count: 0
  url: "https://fanf.livejournal.com/6365.html"
title: All talked out
...

Well I finished <a href="https://fanf2.user.srcf.net/hermes/doc/talks/2003-05-techlinks/">the slides and notes for the talk</a> early enough that I could review the whole thing and then have a Thai pub lunch before actually doing it. It seemed to go well -- they laughed in most of the right places, and they seemed reasonably convinced by my answers to their questions.

I now have a slightly sore throat (note for next time: water!) and I'm feeling a little zonked. Going climbing in London this evening, though, so I'm in for a good night's sleep.
