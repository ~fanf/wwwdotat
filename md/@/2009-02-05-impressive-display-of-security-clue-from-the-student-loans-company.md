---
dw:
  anum: 54
  eventtime: "2009-02-05T13:45:00Z"
  itemid: 374
  logtime: "2009-02-05T13:52:04Z"
  props:
    commentalter: 1491292369
    import_source: livejournal.com/fanf/97203
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/95798.html"
format: html
lj:
  anum: 179
  can_comment: 1
  ditemid: 97203
  event_timestamp: 1233841500
  eventtime: "2009-02-05T13:45:00Z"
  itemid: 379
  logtime: "2009-02-05T13:52:04Z"
  props:
    verticals_list: technology
  reply_count: 5
  url: "https://fanf.livejournal.com/97203.html"
title: Impressive display of security clue from the Student Loans Company
...

<p><a href="http://www.slc.co.uk/pdf/FOI%20Exec%20Mgmt%20Board%20Minutes%20-%2018th%20January%20
2008%20(RSJ).pdf">The Student Loan Company executive management board minutes from a meeting just over a year ago</a> says the following in section 6, "update on data security processes":</p>

<blockquote>RSJ provided an update on Data Security and advised that information
which was being received from external sources confirmed that the
transfer of data on removable media devices was now unacceptable. He
stated that there was a need to consult with HEI’s as to the method of
transferring Attendance Confirmation Reports as SLC now had PGP
encryption software available which could replace the previous method of
transferring the data via CD’s. He also stated that the PGP software
which SLC were using should be checked to ensure that it was on the US
Government list of standard encryption as HEI’s are only permitted to
use PGP software from this list.</blockquote>

<p>Not shipping media is good. Using end-to-end encryption is good. (Unlike banks which seem to like SMTP over TLS, which provides no additional security for inter-domain communication.) I wonder why the choice of PGP instead of S/MIME - I believe that PGP usually requires an add-on whereas S/MIME is often built in to MUAs. Perhaps they've been nobbled by a vendor.</p>
