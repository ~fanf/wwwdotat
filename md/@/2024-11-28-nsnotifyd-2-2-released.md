---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/150576.html
  Fediverse: https://mendeddrum.org/@fanf/113560631487821062
...
nsnotifyd-2.2 released
======================

I have made
[a new release of `nsnotifyd`][nsnotifyd],
a tiny DNS server that just listens for NOTIFY messages
and runs a script when one of your zones changes.

[This `nsnotifyd-2.2` release][nsnotifyd] includes a new feature:

  * `nsnotify` can now send NOTIFY messages from a specific source address

Thanks to Adam Augustine for the suggestion. I like receiving messages
that say things like,

> Thanks for making this useful tool available for free.

[nsnotifyd]: https://dotat.at/prog/nsnotifyd/
