---
dw:
  anum: 64
  eventtime: "2016-06-13T12:24:00Z"
  itemid: 460
  logtime: "2016-06-13T11:24:58Z"
  props:
    commentalter: 1491292418
    hasscreened: 1
    import_source: livejournal.com/fanf/145132
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/117824.html"
format: html
lj:
  anum: 236
  can_comment: 1
  ditemid: 145132
  event_timestamp: 1465820640
  eventtime: "2016-06-13T12:24:00Z"
  itemid: 566
  logtime: "2016-06-13T11:24:58Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 31
  url: "https://fanf.livejournal.com/145132.html"
title: Tactile paving
...

<p>In Britain there is a standard for tactile paving at the start and end of shared-use foot / cycling paths. It uses a short section of ridged paving slabs which can be laid with the ridges either along the direction of the path or across the direction of the path, to indicate which side is reserved for which mode of transport.

<h2>Transverse ridges</h2>

<p>If you have small hard wheels, like many pushchairs or the front castors on many wheelchairs, transverse ridges are very bumpy and uncomfortable.

<p>If you have large pneumatic wheels, like a bike, the wheel can ride over the ridges so it doesn't feel the bumps.

<p><i>Transverse ridges are better for bikes</i>

<h2>Longitudinal ridges</h2>

<p>If you have two wheels and the ground is a bit slippery, longitudinal ridges can have a tramline effect which disrupts you steering and therefore balance, so they are less safe.

<p>If you have four wheels, the tramline effect can't disrupt your balance and can be nice and smooth.

<p><i>Longitudinal ridges are better for pushchairs</i>

<h2>The standard</h2>

<p>So <i>obviously</i> the standard is transverse ridges for the footway, and longitudinal ridges for the cycleway.

<p><I><a href="http://fanf.livejournal.com/145211.html">(I have a followup item with a plausible explanation!)</a></I>

<p><img src="http://i.stack.imgur.com/jqW8j.jpg">
