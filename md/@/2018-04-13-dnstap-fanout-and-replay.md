---
dw:
  anum: 180
  eventtime: "2018-04-13T19:08:00Z"
  itemid: 491
  logtime: "2018-04-13T18:09:35Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/125876.html"
format: md
...

dnstap fanout and replay
========================

I have a short wishlist of [`dnstap`](http://dnstap.info/)-related
tools. I haven't managed to find out if anything like this already
exists - if it does exist I'll be grateful for any pointers!

fanout
------

We have a couple of kinds of people who have expressed interest in getting
dnstap feeds from our campus resolvers (though this is not yet happening).

* There are people on site doing information security threat
  intelligence research, who would like a full feed of client queries
  and responses.

* And there are third parties who would like a [passive DNS][] feed of
  outgoing resolver queries, and who aren't allowed a full-fat feed for
  privacy reasons.

[passive DNS]: https://www.farsightsecurity.com/technical/passive-dns/passive-dns-faq/#q11

The [dnstap implementation in BIND][BIND dnstap] only supports one
output stream, so if we are going to satisfy these consumers, we would
need to split the dnstap feed downstream of BIND before feeding the
distributaries onwards.

[BIND dnstap]: https://kb.isc.org/article/AA-01342/0/Using-DNSTAP-with-BIND-9.11.html

replay
------

More recently it occurred to me that it might be useful to generate
queries from a dnstap feed. I have a couple of scenarios:

* Replay client queries against a test server, to verify that it behaves
  OK with real-ish traffic. I have
  [a tool for replaying cache dump files][adns-masterfile],
  but a replaying a cache dump is nothing like real user traffic since
  it doesn't include repeated queries and the queries occur in a
  weirdly lexicographical order.

[adns-masterfile]: https://dotat.at/@/2016-02-18-preheating-a-bind-cache-with-adns-masterfile.html

* Replay outgoing resolver queries from a live server against a
  standby server. These queries are effectively the cache misses, so
  they are less costly to replicate than all the client traffic. This
  keeps the standby cache hot whereas at the moment my standby servers
  have cold caches.

  It might also be worth duplicating this traffic from one live server
  to the other one, in the hope that this increases the cache hit
  rate, since the more users a cache has the higher its hit rate.
  (Some experimentation needed!)

I'm not really insterested in the responses to these queries so it's
OK if the replay just drops the answers. (Though when replaying a full
client query feed it might be useful to compare the replay responses
to the recorded feed of client responses.)

todo?
-----

If anything like this does not exist, I might write it myself.

I have not used protobufs before so I'm keen to hear advice from those who
have already got their hands dirty / fingers burned.

I'm tempted to weld libfstrm to Lua, so you can configure filtering,
replication, and output with a bit of Lua. The number of Lua protobuf
implementations is a bit of a worry - if anyone has a recommendation I'd
like to short-cut the experimental stage. (I should ask this on the Lua
list I guess!)

Alternatively it might be easier to hack around with the golang-dnstap
code, tho then I would have to think harder about how to configure it...
