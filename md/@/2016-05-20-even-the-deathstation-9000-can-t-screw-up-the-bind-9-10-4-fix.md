---
dw:
  anum: 248
  eventtime: "2016-05-20T01:52:00Z"
  itemid: 458
  logtime: "2016-05-20T00:52:37Z"
  props:
    commentalter: 1491292431
    import_source: livejournal.com/fanf/144615
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/117496.html"
format: html
lj:
  anum: 231
  can_comment: 1
  ditemid: 144615
  event_timestamp: 1463709120
  eventtime: "2016-05-20T01:52:00Z"
  itemid: 564
  logtime: "2016-05-20T00:52:37Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/144615.html"
title: "Even the Deathstation 9000 can't screw up the BIND 9.10.4 fix"
...

<p>There was an <a href="https://lists.isc.org/pipermail/bind-users/2016-May/096898.html">interesting and instructive cockup in the BIND 9.10.4
release</a>.
The tl;dr is that they tried to improve performance by changing the
layout of a data structure to use less memory. As a side effect, two
separate sets of flags ended up packed into the same word.
Unfortunately, these different sets of flags were protected by
different locks. This overlap meant that concurrent access to one set
of flags could interfere with access to the other set of flags.
This led to a violation of BIND's self-consistency checks, causing a crash.</p>

<p><a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=commitdiff;h=e2047969decfc0c3fc1a946ccade993cab9c9315">The fix uses an obscure feature of C structure bitfield
syntax</a>.
If you declare a bitfield without a name and with a zero width (in
this case, <code>unsigned int :0</code>) any following bitfields must be placed
in the next "storage unit". This is described in
<a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf">the C standard</a>
section 6.7.2.1p12.</p>

<p>You can see this in action in <a href="https://source.isc.org/cgi-bin/gitweb.cgi?p=bind9.git;a=blob;f=lib/dns/include/dns/rbt.h;h=ef45840d12dbcc6ece1e4ae52f725ecb58fe8a56#l87">the BIND DNS red-black tree declaration</a>.</p>

<h2>A :0 solves the problem, right?</h2>

<p>So a clever idea might be to use this <code>:0</code> bitfield separator so that
the different sets of bitfields will be allocated in different words,
and the different words will be protected by different locks.</p>

<h2>What are the assumptions behind this fix?</h2>

<p>If you only use a <code>:0</code> separator, you are assuming that a "word" (in a
vague hand-wavey sense) corresponds to both a "storage unit", which
the compiler uses for struct layouts, and also to the size of memory
access the CPU uses for bitfields, which matters when we are using
locks to keep some accesses separate from each other.</p>

<h2>What is a storage unit?</h2>

<p><a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf">The C standard</a>
specifies the representation of bitfields in section 6.2.6.1p4:</p>

<blockquote>
  <p>Values stored in bit-fields consist of <em>m</em> bits, where <em>m</em> is the
size specified for the bit-field. The object representation is the
set of <em>m</em> bits the bit-field comprises in the addressable storage
unit holding it.</p>
</blockquote>

<p>Annex J, which covers portability issues, says:</p>

<blockquote>
  <p>The following are unspecified: [... yada yada ...]</p>

<p>The alignment of the addressable storage unit allocated to hold a bit-field</p>
</blockquote>

<h2>What is a storage unit <em>really</em>?</h2>

<p>A "storage unit" is defined by the platform's ABI, which for BIND
usually means the System V Application Binary Interface. <a href="http://www.x86-64.org/documentation/abi.pdf">The amd64
version covers bit fields in section
3.1.2</a>. It says,</p>

<ul>
<li><p>bit-fields must be contained in a storage unit appropriate for its
declared type</p></li>
<li><p>bit-fields may share a storage unit with other struct / union
members</p></li>
</ul>

<p>In this particular case, the storage unit is 4 bytes for a 32 bit
<code>unsigned int</code>. Note that this is less than the architecture's nominal
64 bit width.</p>

<h2>How does a storage unit correspond to memory access widths?</h2>

<p>Who knows?</p>

<h2>What?</h2>

<p>It is unspecified.</p>

<h2>So...</h2>

<p>The broken code declared a bunch of adjacent bit fields and forgot
that they were accessed under different locks.</p>

<p>Now, you might hope that you could just add a <code>:0</code> to split these
bitfields into separate storage units, and the split would be enough
to also separate the CPU's memory accesses to the different storage
units.</p>

<p>But you would be assuming that the code that accesses the bitfields
will be compiled to use <code>unsigned int</code> sized memory accesses to read
and write the <code>unsigned int</code> sized storage units.</p>

<h2>Um, do we have any guarantees about access size?</h2>

<p>Yes! There is <code>sig_atomic_t</code> which has been around since C89, and a
load of very recent atomic stuff. But none of it is used by this part
of BIND's code.</p>

<p>(Also, worryingly, the AMD64 SVABI does not mention "atomic".)</p>

<h2>So what is this "Deathstation 9000" thing then?</h2>

<p>The <a href="http://en.academic.ru/dic.nsf/enwiki/2748465">DS9K</a> is the
canonical epithet for the most awkward possible implementation of C,
which follows the standard to the letter, but also violates common
assumptions about how C works in practice. It is invoked as a horrible
nightmare, but in recent years it has become a disastrous reality as
compilers have started <a href="http://blog.llvm.org/2011/05/what-every-c-programmer-should-know.html">exploiting undefined behaviour to support
advanced
optimizations</a>.</p>

<p>(Sadly the Armed Response Technologies website has died, and
Wikipedia's DS9K page has been deleted. About the only place you can
find information about it is in old <code>comp.lang.c</code> posts...)</p>

<h2>And why is the DS9K relevant?</h2>

<p>Well, in this case we don't need to go deep into DS9K territory. There
are vaguely reasonable ABIs for which a small :0 fix would not
actually work.</p>

<p>For instance, there might be a CPU which can only do 64 bit memory
accesses, but which has a 32 bit <code>int</code> storage unit. This type
representation would probably mean the C compiler has really bad
performance, so it is fairly unlikely. But it is allowed, and there
are (or were) CPUs which can't do sub-word memory accesses, and which
have very little RAM so they want to pack data tightly.</p>

<p>On a CPU like this, the storage unit doesn't match the memory access,
so C's <code>:0</code> syntax for skipping to the next storage unit will fail to
achieve the desired effect of isolating memory accesses that have to
be performed under different concurrent access locks.</p>

<h2>DS9K defence technology</h2>

<p>So the BIND 9.10.4 fix does <em>two</em> things:</p>

<p>The most important change is to move one of the sets of bitfields from
the start of the struct to the end of it. This means there are several
pointers in between the fields protected by one lock and the fields
protected by the other lock, so even a DS9K can't reasonably muddle
them up.</p>

<p>Secondly, they used magical :0 syntax and extensive comments to
(hopefully) prevent the erroneous compaction from happening again.
Even if the bitfields get moved back to the start of the struct (so
the pointers no longer provide insulation) the :0 might help to
prevent the bug from causing crashes.</p>

<h3>(edited to add)</h3>

<p>When I wrapped this article up originally, I forgot to return to
<code>sig_atomic_t</code>. If, as a third fix, these bitfields were
also changed from <code>unsigned int</code> to <code>sig_atomic_t</code>,
it would further help to avoid problems on systems where the natural atomic
access width is wider than an <code>int</code>, like the lesser cousin
of the DS9K I outlined above. However, <code>sig_atomic_t</code> can be
signed or unsigned so it adds a new portability wrinkle.</p>

<h2>Conclusion</h2>

<p>The clever :0 is not enough, and the BIND developers were right not to
rely on it.</p>
