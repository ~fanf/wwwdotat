---
dw:
  anum: 123
  eventtime: "2015-08-21T01:17:00Z"
  itemid: 427
  logtime: "2015-08-21T00:17:51Z"
  props:
    commentalter: 1491292410
    import_source: livejournal.com/fanf/136481
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/109435.html"
format: html
lj:
  anum: 33
  can_comment: 1
  ditemid: 136481
  event_timestamp: 1440119820
  eventtime: "2015-08-21T01:17:00Z"
  itemid: 533
  logtime: "2015-08-21T00:17:51Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 9
  url: "https://fanf.livejournal.com/136481.html"
title: Fare thee well
...

<p>At some point, 10 or 15 years ago, I got into the habit of saying goodbye to people by saying "stay well!"

<p>I like it because it is cheerful, it closes a conversation without introducing a new topic, and it feels meaningful without being stilted (like "farewell") or rote (like "goodbye").

<p>But.

<p>"Stay well" works nicely in a group of healthy people, but it is problematic with people who are ill.

<p>Years ago, before "stay well!" was completely a habit, a colleague got prostate cancer. The treatment was long and brutal. I had to be careful when saying goodbye, but I didn't break the habit.

<p>It is perhaps even worse with people who are chronically ill, because "stay well" (especially when I say it) has a casually privileged assumption that I am saying it to people who are already cheerfully well.

<p>In the last week this phrase has got a new force for me. I really do mean "stay well" more than ever, but I wish I could express it without implying that you are already well or that it is your duty to be well.
