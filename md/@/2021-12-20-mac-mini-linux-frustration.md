---
dw:
  anum: 10
  eventtime: "2021-12-20T12:39:00Z"
  itemid: 534
  logtime: "2021-12-20T12:42:08Z"
  props:
    commentalter: 1640034893
    hasscreened: 1
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1640018157
  url: "https://fanf.dreamwidth.org/136714.html"
format: md
...

Mac Mini Linux frustration
==========================

I have an old Mac Mini which I am repurposing as a Linux box. Last
month [I upgraded its hardware and posted pictures on
Twitter](https://twitter.com/fanf/status/1461410908324904971).
This weekend I upgraded the software.

However there is a compatibility problem that I have not managed to
solve: Linux isn't able to drive the display hardware as well as it
should be able to.

<toc>


the problem
-----------

Linux is only able to drive a display when booted with `radeon.modeset=0`.

The consequences of this setting are:

  * no accelerated graphics

  * unable to drive 4k monitor

Without the `radeon.modeset=0` setting the display goes blank apart
from an unblinking `_`; Linux boots OK but X fails to start.


the setup
---------

  * Mac Mini 5,2 (mid 2011)

  * Boot ROM version MM51.0080.B00

  * Intel i5 2.5GHz

  * AMD Radeon HD 6630M

  * 16 GB RAM

  * 750 GB very slow rotating rust disk, with a Bootcamp setup

      * Mac OS 10.13.6 "High Sierra"

      * Windows of some foul flavour

  * 1 TB SSD, dedicated to Debian 11.2 "bullseye"


causes
------

The Linux kernel radeon driver needs a Video BIOS to support
modesetting; without that the graphics stack is crippled. It logs:

    [drm:radeon_get_bios [radeon]] *ERROR* Unable to locate a BIOS ROM

The Mac has Video BIOS emulation as part of its [UEFI BIOS
Compatibility Support Module (CSM)][UEFI CSM]. The Mac's boot ROM
enables its UEFI CSM when starting an OS in legacy mode (aka BIOS
compatibility mode), e.g. as part of Windows / Boot Camp support.

[UEFI CSM]: https://www.rodsbooks.com/efi-bootloaders/csm-good-bad-ugly.html

There is [a summary of this issue on the Debian Wiki](https://wiki.debian.org/MacMiniIntel)

So I need to get the Mac to boot Linux in legacy mode. How?


partitions
----------

There is [a description of the Mac's boot mode logic on the Arch
forums](https://bbs.archlinux.org/viewtopic.php?pid=1351520#p1351520)
which says that it uses legacy mode if the disk has an MBR, or if it
has a hybrid GPT/MBR with the active boot flag set in one of the MBR
partitions.

I inspected both disks with `fdisk` under Linux, which shows they are
both set up according to this scheme.

Windows boots OK, and I believe that like Linux, it also requires
legacy mode to work properly.


oddly blessed
-------------

MacOS has a `bless` command that configures the boot process,
including options for legacy booting. So maybe I need to `bless`
it with the right incantation?

There is an oddity that the Mac boots by default into Linux
(possibly because the SSD is the "first" disk according to the
hardware), but when I boot while pressing "option" to get the device
selection menu, it only lists the operating systems on the old disk.

This matches the behaviour of `systemsetup -liststartupdisks`, which
only lists Mac OS and Windows. `systemsetup -getstartupdisk` prints
nothing.

There is a (related?) oddity that `bless --info --verbose` complains
that my boot configuration is inconsistent, but the XML plist in the
output is too inscrutable for me to see the problem.

[_eta_] And in Mac OS System Preferences -> Startup Disk, the panel
for choosing a volume remains empty. If I try to interact with System
Preferences any further, it beachballs and seems to require a Force
Quit or a very long wait.


dunno
-----

This is all rather annoying, but I don't want to spend much more time
on it. I'll leave the Mac Mini running linux headless, so I can get
the crappy old monitor and the rest of the debugging clutter off my
desk, and leave the DisplayPort on my 4k monitor unused.


... _later_ ...

more faff
---------

Julian Field suggested resetting the NVRAM, which to be fair is the
Mac equivalent of "have you tried turning it off and on again?".
There is some evidence that I have NVRAM problems so I tried it.

But it did not fix any of the problems, and it made some of them
worse: `bless` became unable to read the `efi-boot-device` NVRAM
variable, and (as I expected) the Mac no longer booted into Linux by
default.

Booting into recovery mode and choosing the Mac OS startup disk (Linux
was not an option) sorted out the NVRAM issue, and `bless` no longer
complained about inconsistencies. After I disabled System Integrity
Protection, `bless` was also able to change things.

But I was still stuck booting into Mac OS not Linux.


workaround
----------

So I installed [the _rEFInd_ boot manager](https://rodsbooks.com/refind/)
which is much more useful than the Mac's built-in boot menu. It
automatically found the Mac OS, Windows, and Recovery partitions on
the old RRD, plus GRUB in the EFI system partition on the SSD, and the
Linux kernel itself.

Booting via `grub` works as before (with modesetting disabled on the
kernel command line). Booting Linux directly from _rEFInd_ works even
better: the UEFI CSM is enabled and the radeon driver is able to do
modesetting, and drive the 4k monitor.

Amusingly, it seems that the Boot ROM (and _rEFInd_) are not able to
drive the 4k monitor, so now it stays blank until Linux is up and
running.

But now it works!
