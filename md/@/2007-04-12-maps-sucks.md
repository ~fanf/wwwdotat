---
dw:
  anum: 54
  eventtime: "2007-04-12T07:56:00Z"
  itemid: 284
  logtime: "2007-04-12T07:14:14Z"
  props:
    commentalter: 1491292350
    import_source: livejournal.com/fanf/73020
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/72758.html"
format: html
lj:
  anum: 60
  can_comment: 1
  ditemid: 73020
  event_timestamp: 1176364560
  eventtime: "2007-04-12T07:56:00Z"
  itemid: 285
  logtime: "2007-04-12T07:14:14Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/73020.html"
title: MAPS sucks
...

<p>We use <a href="http://www.ja.net/services/network-services/mail/mail-abuse/rbl-plus-guide.html">the JANET subscription to the MAPS RBL+</a>. It is a frequent source of false positives which causes a great deal of irritation.
<ul>
<li>Their RSS (relay spam stopper, i.e. list of open relays) does not have any re-testing nor any expiration of old entries, so is full of shockingly stale entries.</li>
<li>Their DUL (dynamic user list, i.e. list of home computer IP addresses) has many errors. They do not track re-assignments of address space which caused them to list gmail's servers earlier this year.</li>
<li>Spamhaus's ZEN list is just as effective but much more trouble-free. Why is JANET paying for the RBL+ and not ZEN?</li>
<li>They do not accept corrections from their paying customers. WTF!</li>
</ul>
Sadly I can't easily ditch the RBL+ since using both it and ZEN adds 20-25% to our block rate compared to either alone, so performance would suffer horribly. Also, I don't want to get into the game of maintaining my own blacklist and/or whitelist - a time sink I do not need.</p>
