---
dw:
  anum: 203
  eventtime: "2006-09-20T15:58:00Z"
  itemid: 254
  logtime: "2006-09-20T15:59:19Z"
  props:
    commentalter: 1491292341
    import_source: livejournal.com/fanf/65409
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/65227.html"
format: casual
lj:
  anum: 129
  can_comment: 1
  ditemid: 65409
  event_timestamp: 1158767880
  eventtime: "2006-09-20T15:58:00Z"
  itemid: 255
  logtime: "2006-09-20T15:59:19Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/65409.html"
title: A new acronym
...

Yes I know this is a day late, but I have come up with a new (email-related) acronym: "Anti-virus and anti-spam tests" = "avast!".
