---
format: casual
lj:
  anum: 90
  can_comment: 1
  ditemid: 111706
  event_timestamp: 1295268780
  eventtime: "2011-01-17T12:53:00Z"
  itemid: 436
  logtime: "2011-01-17T12:53:36Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/111706.html"
title: Debit several clue points from Le Cr&eacute;dit Lyonnais
...

<pre>
; <<>> DiG 9.6.2-P2 <<>> +dnssec mx carte-isic.lcl.fr.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54984
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 3, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 4096
;; QUESTION SECTION:
;carte-isic.lcl.fr.             IN      MX

;; ANSWER SECTION:
carte-isic.lcl.fr.      407     IN      MX      5 82.118.192.159. ; *D'OH!*

;; AUTHORITY SECTION:
lcl.fr.                 407     IN      NS      ramses.credit-agricole.fr.
lcl.fr.                 407     IN      NS      ns0.creditlyonnais.fr.
lcl.fr.                 407     IN      NS      chenar.credit-agricole.fr.

;; Query time: 0 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Mon Jan 17 12:52:13 2011
;; MSG SIZE  rcvd: 167
</pre>
