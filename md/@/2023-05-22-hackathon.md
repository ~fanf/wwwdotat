---
comments:
  Cohost: https://cohost.org/fanf/post/1573033-on-my-blog
  Dreamwidth: https://fanf.dreamwidth.org/142513.html
title: RIPE DNS Hackathon
...

This weekend I was in Rotterdam for the
[RIPE DNS Hackathon](https://github.com/DNS-Hackathon-2023).

About 50 people gathered with several ideas for potential projects: things
like easier DNSSEC provisioning, monitoring DNS activity in the network,
what is the environmental cost of the DNS, ...

At the start of the weekend we were asked to introduce ourselves and
say what our goals were. My goal was to do something different from my
day job working on BIND. I was successful, tho I did help some others
out with advice on a few of BIND's obscurities.

The team I joined was very successful at producing a working prototype
and a cool demo.

<cut>

runner up
---------

The project that was the second most interesting to me was
["DNS OOPS"](https://github.com/DNS-Hackathon-2023/DNS-oops),
out-of-protocol signalling. The idea there was to find out
things like when a zone has been loaded and is ready to serve,
so that it can be added to a BGP route advertisement.

I talked about it with Willem Toorop, and I said OOPS sounded close to
what I had done with [`nsnotifyd`](https://dotat.at/prog/nsnotifyd),
but OOPS would need a little more information added to the NOTIFY
messages, for instance if you want to monitor key rollovers.

The OOPS team ended up using `nsnotifyd` as part of their demo, and
Lars-Johann Liman reported a usability bug to me, which I fixed.

[our project](https://github.com/DNS-Hackathon-2023/diggin-in)
-------------

I joined a team to work on Emile Aben's idea for a scripting language that
could run on [RIPE Atlas measurement probes](https://atlas.ripe.net/).
Instead of collecting voluminous data that later needs to be reduced in a
complicated manner, a script on the probe could strip out unwanted data
before collection.

I suggested that [Starlark](https://github.com/bazelbuild/starlark)
might be a good language to use. It's a simplified dialect of Python
designed for sandboxed scripts embedded in applications. It was
originally created for the Bazel build system, but it's a general
purpose alternative to Tcl, Lua, Guile, etc.

Annika Hennig suggested WASM as an alternative, which sounded cool to
me. But it was too difficult to get a WASM runtime up and running with
functions plugged in for DNS resolution. So we switched to [the Golang
implementation of Starlark](https://github.com/google/starlark-go).

results
-------

We got Starlark to invoke `dig` or the [RIPE Atlas tool `evdig`](https://labs.ripe.net/author/philip_homburg/releasing-ripe-atlas-measurements-source-code/)
I suggested using `dig +yaml` but we ended up using [`jc --dig`](https://github.com/kellyjonbrazil/jc)
which is new to me.

Annika Hennig got us started with Starlark-go, and made an awesome
front end based on [Codepen](https://codepen.io/) that could
dynamically deploy scripts to probes. Raffaele Sommese wrote a bunch
of Starlark scripts, and got it working with `evdig`. Jonas Andersson
set up a RIPE Atlas probe onto which we could install our prototype
software, and sketched out some security considerations. Emile
gathered use-cases and wrote the presentation. I hooked more functions
up to Starlark and examined security of the Starlark implementation
and our prototype code.

The demo was deploying a Starlark script from the Codepen front-end to
a couple of probes; the script monitored changes to the `.com` SOA
serial number across `gtld-servers.net`.

conclusion
----------

Starlark turned out to be nice to work with, and Golang is a good
hackathon language. I have not used Golang much before so I didn't
write much code; I was much more successful at coming up with ideas
and suggestions.

For the context of this project, it looks like Starlark will be more
accessible for data scientists than the possible alternatives, as I
hoped.
