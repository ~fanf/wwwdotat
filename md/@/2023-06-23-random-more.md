More random floating point numbers
==================================

I got some interesting comments about [my previous notes on random
floating point numbers](https://dotat.at/@/2023-06-23-random-double.html)
on [Lobsters](https://lobste.rs/s/kojwb9/random_floating_point_numbers),
[Dreamwidth](https://fanf.dreamwidth.org/142922.html), and from [Pete
Cawley on Twitter](https://twitter.com/fanf/status/1672237358266064897).

Here's an addendum about an alternative model of uniformity.

There are 2^62 double precision floats between 0.0 and 1.0, but as I
described before under _"the problem"_, [they are not distributed
uniformly](https://dotat.at/@/2023-06-23-random-double.html#the-problem):
the smaller ones are much denser. Because of this, there are two ways
to model a uniform distribution using floating point numbers.

Both algorithms in my previous note use a discrete model: the
functions return one of 2^52 or 2^53 evenly spaced numbers.

You can also use a continuous model, where you imagine a uniformly
random real number with unbounded precision, and return the closest
floating point result. This can have better behaviour if you go on to
transform the result to model different distrbutions (normal, poisson,
exponential, etc.)

Taylor Campbell explains [how to generate uniform random
double-precision floating point numbers][mumble-howto] with [source
code][mumble-source]. Allen Downey has [an older description of
generating pseudo-random floating-point values][downey].

[mumble-howto]: https://mumble.net/~campbell/2014/04/28/uniform-random-float
[mumble-source]: https://mumble.net/~campbell/2014/04/28/random_real.c
[downey]: https://allendowney.com/research/rand/downey07randfloat.pdf

In practice, the probability of entering the arbitrary-precision loop
in Campbell's code is vanishingly tiny, so with some small adjustments
it can be omitted entirely. Marc Reynolds explains how to generate
[higher density uniform floats][reynolds-dense] this way, and Pete
Cawley has [terse implementations that use one or two random integers
per double][corsix-gist]. (Reynolds also has a note about [adjusting the
range and fenceposts of discrete random floating point
numbers][reynolds-bounds].)

[reynolds-dense]: http://marc-b-reynolds.github.io/distribution/2017/01/17/DenseFloat.html
[reynolds-bounds]: http://marc-b-reynolds.github.io/math/2020/06/16/UniformFloat.html
[corsix-gist]: https://gist.github.com/corsix/c21d715238e32ce6d44a7fc1b44f427d
