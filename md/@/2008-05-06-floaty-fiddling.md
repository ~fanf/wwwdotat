---
dw:
  anum: 82
  eventtime: "2008-05-06T20:24:00Z"
  itemid: 339
  logtime: "2008-05-06T19:24:57Z"
  props:
    commentalter: 1491292353
    import_source: livejournal.com/fanf/87612
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/86866.html"
format: html
lj:
  anum: 60
  can_comment: 1
  ditemid: 87612
  event_timestamp: 1210105440
  eventtime: "2008-05-06T20:24:00Z"
  itemid: 342
  logtime: "2008-05-06T19:24:57Z"
  props: {}
  reply_count: 8
  url: "https://fanf.livejournal.com/87612.html"
title: floaty fiddling
...

<p>So I needed some Lua functions to extract bit fields from an integral value, specifically <tt>struct stat.st_mode</tt>. Lua only has floating point numbers, and its standard library doesn't extend beyond ANSI C. So I wrote the following. (Note that <tt>a % b == a - floor(a/b)*b</tt>.)</p>
<pre>
   function mask(lobit, hibit, num)
      local toolo = num % 2^lobit
      return (num - toolo) % 2^hibit
   end

   function bit(bit, num)
      return num / 2^bit % 2 >= 1
   end
</pre>
