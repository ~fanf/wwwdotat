---
dw:
  anum: 89
  eventtime: "2015-06-15T17:12:00Z"
  itemid: 421
  logtime: "2015-06-15T16:12:31Z"
  props:
    commentalter: 1501088647
    import_source: livejournal.com/fanf/134988
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/107865.html"
format: casual
lj:
  anum: 76
  can_comment: 1
  ditemid: 134988
  event_timestamp: 1434388320
  eventtime: "2015-06-15T17:12:00Z"
  itemid: 527
  logtime: "2015-06-15T16:12:31Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/134988.html"
title: "nsnotifyd: handle DNS NOTIFY messages by running a command"
...

About ten days ago, I was wondering how I could automatically and promptly record changes to a zone in git. The situation I had in mind was a special zone which was modified by <tt>nsupdate</tt> but hosted on a server whose DR plan is "rebuild from Git using Ansible". So I thought it would be a good idea to record updates in git so they would not be lost.

My <a href="https://twitter.com/fanf/status/606570587255881728">initial idea</a> was to use BIND's <tt>update-policy external</tt> feature, which allows you to hook into its dynamic update handling. However that would have problems with race conditions, since the update handler doesn't know how much time to allow for BIND to record the update.

So I thought it might be better to write <a href="https://twitter.com/fanf/status/606775591233286144">a DNS NOTIFY handler</a>, which gets told about updates just after they have been recorded. And I thought that wiring a DNS server daemon would not be much harder than writing an update-policy external daemon.

<a href="https://twitter.com/jpmens">@jpmens</a> responded with enthusiasm, and a some hours later I had something vaguely working.

What I have written is a special-purpose DNS server called <a href="http://dotat.at/prog/nsnotifyd/"><tt>nsnotifyd</tt></a>. It is actually a lot more general than just recording a zone's history in git. You can run any command as soon as a zone is changed - the script for recording changes in git is just one example.

Another example is running <a href="http://dotat.at/prog/nsdiff"><tt>nsdiff</tt></a> to propagate changes from a hidden master to a DNSSEC signer. You can do the same job with BIND's inline-signing mode, but maybe you need to insert some evil zone mangling into the process, say...

Basically, anywhere you currently have a cron job which is monitoring updates to DNS zones, you might want to run it under <tt>nsnotifyd</tt> instead, so your script runs as soon as the zone changes instead of running at fixed intervals.

Since a few people expressed an interest in this program, I have written documentation and packaged it up, so you can download it from <a href="http://dotat.at/prog/nsnotifyd/">the <tt>nsnotifyd</tt> home page</a>, or from one of several git repository servers.

(Epilogue: I realised halfway through this little project that I had a better way of managing my special zone than updating it directly with nsupdate. Oh well, I can still use nsnotifyd to drive <a href="https://twitter.com/diffroot">@diffroot</a>!)
