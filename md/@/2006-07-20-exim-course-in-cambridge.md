---
dw:
  anum: 225
  eventtime: "2006-07-20T16:37:00Z"
  itemid: 247
  logtime: "2006-07-20T15:43:27Z"
  props:
    commentalter: 1491292335
    import_source: livejournal.com/fanf/63670
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/63457.html"
format: casual
lj:
  anum: 182
  can_comment: 1
  ditemid: 63670
  event_timestamp: 1153413420
  eventtime: "2006-07-20T16:37:00Z"
  itemid: 248
  logtime: "2006-07-20T15:43:27Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/63670.html"
title: Exim course in Cambridge
...

Last year I missed the annual Exim course in Cambridge owing to being on honeymoon at the time - though I still did the setup for the practical sessions, which was slightly hairy since it was the first time we had done a practical in Cambridge and I was not going to be there to fix any bugs; fortunately there were none.

This year I am in Cambridge, and as well as doing a slightly updated version of the practical, I have also given a talk. The slides are now on the web at https://fanf2.user.srcf.net/hermes/doc/talks/2006-07-exim-course/
