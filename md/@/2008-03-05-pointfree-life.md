---
dw:
  anum: 115
  eventtime: "2008-03-05T16:51:00Z"
  itemid: 328
  logtime: "2008-03-05T17:51:11Z"
  props:
    import_source: livejournal.com/fanf/84859
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/84083.html"
format: html
lj:
  anum: 123
  can_comment: 1
  ditemid: 84859
  event_timestamp: 1204735860
  eventtime: "2008-03-05T16:51:00Z"
  itemid: 331
  logtime: "2008-03-05T17:51:11Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/84859.html"
title: pointfree life
...

<p>The Game of Life is fairly pointless, but in this case it is also written in "point-free" style which minimizes the use of variables. I think it's quite cute.</p>

<p>It's easy to represent an unbounded quarter-plane in Haskell as <tt>[[Cell]]</tt>, i.e. a list of rows of cells - the lists are lazy, so they can make themselves as long as necessary. You can then define the universe without form and void, such that darkness is upon the face of the screen, as:</p>

<pre>
  data Cell = Dead | Alive
  deadline = <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Arepeat">repeat</a> Dead
  deadspace = <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Arepeat">repeat</a> deadline
</pre>

<p>To create a pattern we add live cells to this substrate. First we need a helper function that changes just the Nth element of a list:</p>

<pre>
  mapNth n f xs = as ++ f b : cs
    where (as,b:cs) = <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Arepeat">splitAt</a> n xs
</pre>

<p>Then to set an individual cell we use mapNth to modify the Xth element of the Yth row. Pointfree style tends to use <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3A%24">the <tt>$</tt> operator</a> a lot, which makes Perl poetry-style programmers feel at home.</p>

<pre>
  setcell (x,y) = mapNth y $ mapNth x $ <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3A%24">const</a> Alive
</pre>

<p>We can apply <tt>setcell</tt> successively starting from dead space to create a pattern. For example, here's a glider set to run off into the unbounded corner of the universe.</p>

<pre>
  pattern = <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Afoldr">foldr</a> setcell deadspace
  glider = pattern [(2,1),(3,2),(3,3),(2,3),(1,3)]
</pre>

<p>When working out the next generation we need to work out the neighbourhood of each cell, i.e. the 3x3 square consisting of the cell and its eight neighbours. We'll end up with the neighbourhood represented as a triple of triples, on which it's straightforward to specify the Game of Life rule:</p>

<pre>
  fromCell Dead = 0
  fromCell Alive = 1

  rule ((a,b,c),(d,e,f),(g,h,i)) =
      if count == 2 then e else
      if count == 3 then Alive else Dead
    where
      count = sum $ map fromCell [a,b,c,d,f,g,h,i]
</pre>

<p>The one-dimensional version of the neighbourhood is quite easy, turning a list into a list of overlapping triples.</p>

<pre>
  by3 (a:b:c:ds) = (a,b,c) : by3 (b:c:ds)
  by3     _      = []
</pre>

<p>We're going to apply <tt>by3</tt> to each row to make horizontal triples, and to the list of rows itself to make triple rows of triples. However what we want is rows of triple triples. Turning a triple of lists into a list of triples is almost what <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Azip3">zip3</a> does, except that zip3 is inconveniently curried.</p>

<pre>
  uncurry3 f (a,b,c) = f a b c
  zip3' = uncurry3 zip3
</pre>

<p>Then the code to turn a list of rows of cells into a list of rows of neighbourhoods is just as I outlined in the previous paragraph.</p>

<pre>
  by3by3 = map zip3' <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3A.">.</a> by3 <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3A.">.</a> map by3
</pre>

<p>When calculating the next generation, we want to add a strip of empty space to each edge of the universe, so that all our cells have neighbours. If we did not do this the universe would unravel at one cell per generation, a bit like in Greg Egan's book "Schildt's ladder".</p>

<pre>
  grow = <a href="http://haskell.org/onlinereport/exps.html#sections">(deadline:)</a> . map (deadcell<a href="http://haskell.org/onlinereport/exps.html#sections">:</a>)
</pre>

<p>Now we can actually calculate the next generation! Grow the universe, work out the neighbourhoods, then apply the rule to each of them.</p>

<pre>
  generate = map (map rule) . by3by3 . grow
</pre>

<p>To display the universe, we need to convert cells to strings</p>

<pre>
  instance Show Cell where
    show Dead = "  "
    show Alive = "[]"
</pre>

<p>To display the whole universe, we need to truncate it to the size of the screen by taking only the necessary elements from the lists, concatenate the string representation of the cells in each row, and join the rows with newlines.</p>

<pre>
  display (x,y) = <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Aunlines">unlines</a> . map (<a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3AconcatMap">concatMap</a> show . <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Atake">take</a> x) . <a href="http://haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html#v%3Atake">take</a> y
</pre>

<p>Then the main program simply loops displaying the universe and calculating its next generation.</p>

<pre>
  loop u = do
    putStrLn $ display (10,10) u
    _ <- getLine
    loop $ generate u

  main :: IO ()
  main = loop glider
</pre>
