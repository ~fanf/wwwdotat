---
dw:
  anum: 128
  eventtime: "2008-01-28T12:36:00Z"
  itemid: 318
  logtime: "2008-01-28T12:37:14Z"
  props:
    import_source: livejournal.com/fanf/82165
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/81536.html"
format: html
lj:
  anum: 245
  can_comment: 1
  ditemid: 82165
  event_timestamp: 1201523760
  eventtime: "2008-01-28T12:36:00Z"
  itemid: 320
  logtime: "2008-01-28T12:37:14Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/82165.html"
title: More about Bloom filters
...

<p>While writing up <a href="http://fanf.livejournal.com/81696.html">my notes on Bloom filters</a>, I wanted a better idea of the relationship between the false positive rate and the size multiplier.</p>

<blockquote><table>
<tr><td><i>sm</i></td><td>= <i>size</i> / <i>pop</i></td></tr>
<tr><td><i>pz</i></td><td>= exp(&minus;<i>nh</i> &lowast; <i>pop</i> / <i>size</i>)</td></tr>
<tr><td></td><td>= exp(&minus;<i>nh</i> / <i>sm</i>)</td></tr>
<tr><td><i>fpr</i></td><td>= (1 &minus; <i>pz</i>) <sup><i>nh</i></sup></td></tr>
</table></blockquote>

<p>Given a fixed number of hash functions, what size multiplier do we need to make enough space in the Bloom filter to produce the desired false positive rate? How does the size multiplier change as <i>nh</i> and <i>fpr</i> change? I wrote a program to solve this numerically, and the output is below. The entries for higher <i>fpr</i>s show quite nicely that there's an optimal <i>nh</i> for a given <i>fpr</i>.</p>

<table>
<tr>
<th><i>size/pop</i></th>
<th><i>nh</i>=1</th>
<th><i>nh</i>=2</th>
<th><i>nh</i>=3</th>
<th><i>nh</i>=4</th>
<th><i>nh</i>=5</th>
<th><i>nh</i>=6</th>
<th><i>nh</i>=7</th>
<th><i>nh</i>=8</th>
<th><i>nh</i>=9</th>
<th><i>nh</i>=10</th>
<th><i>nh</i>=11</th>
<th><i>nh</i>=12</th>
<th><i>nh</i>=13</th>
<th><i>nh</i>=14</th>
<th><i>nh</i>=15</th>
<th><i>nh</i>=16</th>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-1</sup></th>
<td>     1.4</td>
<td>     1.6</td>
<td>     1.9</td>
<td>     2.2</td>
<td>     2.4</td>
<td>     2.7</td>
<td>     3.0</td>
<td>     3.2</td>
<td>     3.5</td>
<td>     3.7</td>
<td>     3.9</td>
<td>     4.2</td>
<td>     4.4</td>
<td>     4.6</td>
<td>     4.8</td>
<td>     5.1</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-2</sup></th>
<td>     3.5</td>
<td>     2.9</td>
<td>     3.0</td>
<td>     3.3</td>
<td>     3.5</td>
<td>     3.8</td>
<td>     4.1</td>
<td>     4.4</td>
<td>     4.6</td>
<td>     4.9</td>
<td>     5.2</td>
<td>     5.4</td>
<td>     5.7</td>
<td>     5.9</td>
<td>     6.2</td>
<td>     6.4</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-3</sup></th>
<td>     7.5</td>
<td>     4.6</td>
<td>     4.3</td>
<td>     4.4</td>
<td>     4.6</td>
<td>     4.9</td>
<td>     5.2</td>
<td>     5.4</td>
<td>     5.7</td>
<td>     6.0</td>
<td>     6.3</td>
<td>     6.5</td>
<td>     6.8</td>
<td>     7.1</td>
<td>     7.3</td>
<td>     7.6</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-4</sup></th>
<td>    15.5</td>
<td>     7.0</td>
<td>     5.9</td>
<td>     5.8</td>
<td>     5.9</td>
<td>     6.0</td>
<td>     6.3</td>
<td>     6.5</td>
<td>     6.8</td>
<td>     7.1</td>
<td>     7.3</td>
<td>     7.6</td>
<td>     7.9</td>
<td>     8.2</td>
<td>     8.4</td>
<td>     8.7</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-5</sup></th>
<td>    31.5</td>
<td>    10.3</td>
<td>     7.9</td>
<td>     7.3</td>
<td>     7.2</td>
<td>     7.3</td>
<td>     7.4</td>
<td>     7.7</td>
<td>     7.9</td>
<td>     8.1</td>
<td>     8.4</td>
<td>     8.7</td>
<td>     9.0</td>
<td>     9.2</td>
<td>     9.5</td>
<td>     9.8</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-6</sup></th>
<td>    63.5</td>
<td>    15.0</td>
<td>    10.4</td>
<td>     9.2</td>
<td>     8.8</td>
<td>     8.7</td>
<td>     8.7</td>
<td>     8.9</td>
<td>     9.1</td>
<td>     9.3</td>
<td>     9.5</td>
<td>     9.8</td>
<td>    10.0</td>
<td>    10.3</td>
<td>    10.6</td>
<td>    10.9</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-7</sup></th>
<td>   127.5</td>
<td>    21.6</td>
<td>    13.6</td>
<td>    11.3</td>
<td>    10.5</td>
<td>    10.2</td>
<td>    10.1</td>
<td>    10.2</td>
<td>    10.3</td>
<td>    10.5</td>
<td>    10.7</td>
<td>    10.9</td>
<td>    11.1</td>
<td>    11.4</td>
<td>    11.7</td>
<td>    11.9</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-8</sup></th>
<td>   255.5</td>
<td>    31.0</td>
<td>    17.5</td>
<td>    13.9</td>
<td>    12.5</td>
<td>    11.9</td>
<td>    11.6</td>
<td>    11.5</td>
<td>    11.6</td>
<td>    11.7</td>
<td>    11.9</td>
<td>    12.1</td>
<td>    12.3</td>
<td>    12.5</td>
<td>    12.8</td>
<td>    13.0</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-9</sup></th>
<td>   511.5</td>
<td>    44.2</td>
<td>    22.5</td>
<td>    16.9</td>
<td>    14.8</td>
<td>    13.8</td>
<td>    13.3</td>
<td>    13.0</td>
<td>    13.0</td>
<td>    13.0</td>
<td>    13.1</td>
<td>    13.3</td>
<td>    13.5</td>
<td>    13.7</td>
<td>    13.9</td>
<td>    14.2</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-10</sup></th>
<td>  1023.5</td>
<td>    63.0</td>
<td>    28.7</td>
<td>    20.6</td>
<td>    17.4</td>
<td>    15.9</td>
<td>    15.1</td>
<td>    14.7</td>
<td>    14.5</td>
<td>    14.4</td>
<td>    14.5</td>
<td>    14.6</td>
<td>    14.7</td>
<td>    14.9</td>
<td>    15.1</td>
<td>    15.3</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-11</sup></th>
<td>  2047.5</td>
<td>    89.5</td>
<td>    36.6</td>
<td>    24.9</td>
<td>    20.4</td>
<td>    18.2</td>
<td>    17.1</td>
<td>    16.4</td>
<td>    16.1</td>
<td>    15.9</td>
<td>    15.9</td>
<td>    15.9</td>
<td>    16.0</td>
<td>    16.1</td>
<td>    16.3</td>
<td>    16.5</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-12</sup></th>
<td>  4095.5</td>
<td>   127.0</td>
<td>    46.5</td>
<td>    30.0</td>
<td>    23.8</td>
<td>    20.9</td>
<td>    19.3</td>
<td>    18.3</td>
<td>    17.8</td>
<td>    17.5</td>
<td>    17.4</td>
<td>    17.3</td>
<td>    17.3</td>
<td>    17.4</td>
<td>    17.6</td>
<td>    17.7</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-13</sup></th>
<td>  8191.5</td>
<td>   180.0</td>
<td>    59.0</td>
<td>    36.0</td>
<td>    27.7</td>
<td>    23.8</td>
<td>    21.7</td>
<td>    20.4</td>
<td>    19.7</td>
<td>    19.2</td>
<td>    18.9</td>
<td>    18.8</td>
<td>    18.8</td>
<td>    18.8</td>
<td>    18.9</td>
<td>    19.0</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-14</sup></th>
<td> 16383.5</td>
<td>   255.0</td>
<td>    74.7</td>
<td>    43.2</td>
<td>    32.3</td>
<td>    27.1</td>
<td>    24.3</td>
<td>    22.7</td>
<td>    21.6</td>
<td>    21.0</td>
<td>    20.6</td>
<td>    20.4</td>
<td>    20.2</td>
<td>    20.2</td>
<td>    20.2</td>
<td>    20.3</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-15</sup></th>
<td> 32767.5</td>
<td>   361.0</td>
<td>    94.5</td>
<td>    51.8</td>
<td>    37.4</td>
<td>    30.8</td>
<td>    27.3</td>
<td>    25.1</td>
<td>    23.8</td>
<td>    22.9</td>
<td>    22.4</td>
<td>    22.0</td>
<td>    21.8</td>
<td>    21.7</td>
<td>    21.6</td>
<td>    21.7</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-16</sup></th>
<td> 65535.5</td>
<td>   511.0</td>
<td>   119.4</td>
<td>    62.0</td>
<td>    43.4</td>
<td>    35.0</td>
<td>    30.5</td>
<td>    27.8</td>
<td>    26.1</td>
<td>    25.0</td>
<td>    24.2</td>
<td>    23.7</td>
<td>    23.4</td>
<td>    23.2</td>
<td>    23.1</td>
<td>    23.1</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-17</sup></th>
<td>131071.5</td>
<td>   723.1</td>
<td>   150.9</td>
<td>    74.1</td>
<td>    50.2</td>
<td>    39.7</td>
<td>    34.1</td>
<td>    30.7</td>
<td>    28.6</td>
<td>    27.2</td>
<td>    26.2</td>
<td>    25.6</td>
<td>    25.1</td>
<td>    24.8</td>
<td>    24.6</td>
<td>    24.6</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-18</sup></th>
<td>262143.5</td>
<td>  1023.0</td>
<td>   190.5</td>
<td>    88.5</td>
<td>    58.1</td>
<td>    44.9</td>
<td>    38.0</td>
<td>    33.9</td>
<td>    31.3</td>
<td>    29.5</td>
<td>    28.3</td>
<td>    27.5</td>
<td>    26.9</td>
<td>    26.5</td>
<td>    26.3</td>
<td>    26.1</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-19</sup></th>
<td>524287.5</td>
<td>  1447.2</td>
<td>   240.4</td>
<td>   105.6</td>
<td>    67.1</td>
<td>    50.8</td>
<td>    42.3</td>
<td>    37.4</td>
<td>    34.2</td>
<td>    32.1</td>
<td>    30.6</td>
<td>    29.6</td>
<td>    28.8</td>
<td>    28.3</td>
<td>    27.9</td>
<td>    27.7</td>
</tr>
<tr>
<th><i>fpr</i>=2<sup>-20</sup></th>
<td>1048575.5</td>
<td>  2047.0</td>
<td>   303.3</td>
<td>   126.0</td>
<td>    77.5</td>
<td>    57.4</td>
<td>    47.1</td>
<td>    41.1</td>
<td>    37.3</td>
<td>    34.8</td>
<td>    33.0</td>
<td>    31.7</td>
<td>    30.8</td>
<td>    30.1</td>
<td>    29.7</td>
<td>    29.3</td>
</tr>
</table>
