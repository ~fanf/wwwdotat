Addenbrooke's cataract clinic
=============================

Following [the cataract clinic referral I got in
September](2021-09-22-my-cataract.html) I spent this afternoon at
Addenbrooke's having my eyes examined. It was about as useful and
informative as I hoped, though it took a long time. (left the house at
13:00, got back at 17:00)

_This post has a sequel with some notes on my [cataract surgery][]._

[cataract surgery]: https://dotat.at/@/2023-01-14-cataract-op.html


the cataract
------------

I have a large dense cataract, which means the whole lens must be
replaced. This is a more difficult operation than usual: cataract
surgery commonly deals with softer/smaller cataracts, which only need
to replace the inner part of the lens; a simpler and less invasive
procedure.


outcomes
--------

The new lens will be fixed focus.

It is difficult to say how much vision I will have. I am going to get
a copy of my opticians records which should give the surgeons a better
idea of how my vision might compare to my benchmark, i.e. the way it
was before the lens clouded over.

(To be honest I was hoping for better than that! But I'll be happy if
I get some of my left-side peripheral vision back.)

There is a small risk that my visual cortex might have problems
integrating the vision from both eyes; I got the impression that this
was explained to prepare me for the possible range of outcomes.

There was (curiously) less discussion about surgical complications,
though they are covered in the patient handouts.


anaesthesia
-----------

In many cases cataract surgery is done under local anaesthetic, but
because I am younger (so a general is less risky) and because it will
be a longer procedure, we decided to book me in for a general.

Afterwards I will need someone (Rachel!) to escort me home.

And there is a course of (I think antibiotic?) eye drops 4x each day
for (IIRC) 28 days, and a protective patch for the first few nights.


waiting time
------------

probably about six months


machine that goes _ping_
------------------------

There were some curious optometry devices that I haven't seen before.

To get the prescription approximately right for the replacement lens,
they measure the shape of the eye. (Longer eyes means more short
sighted.) The current technology for this is optical, but it didn't
work on my eye (cataract too dense) so they used an older ultrasound
thing that reminded me of a CRT light pen. It did, indeed, go _ping_
when making a measurement.

Later on one of the surgeons used a more typical ultrasound device: I
closed my eye and probed it through my eyelid and a generous smear
of KY jelly.

There were various kinds of eye drops; my vision is still a bit blurry
from dilated pupils!
