---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/147932.html
  Fediverse: https://mendeddrum.org/@fanf/112604618072064414
...
nsnotifyd-2.1 released
======================

I have made
[a new release of `nsnotifyd`][nsnotifyd],
a tiny DNS server that just listens for NOTIFY messages
and runs a script when one of your zones changes.

[This `nsnotifyd-2.1` release][nsnotifyd] includes a few bugfixes:

  * more lenient handling of trailing `.` in domain names on the command line
  * avoid dividing by zero when the refresh interval is less than 10 seconds
  * do not error out when in TCP mode and debug mode and the refresh timer expires
  * explain how to fix incomplete github forks

Many thanks to Lars-Johann Liman, JP Mens, and Jonathan Hewlett for
the bug reports. I like receiving messages that say things like,

> thanks for nsnotifyd, is a great little program, and a good
> example of a linux program, does one thing well.

(There's more like that in the [nsnotifyd-2.0][] release annoucement.)

I have also included a little `dumpaxfr` program, which I wrote when
fiddling around with binary wire format DNS zone transfers. I used the
`nsnotifyd` infrastructure as a short cut, though `dumpaxfr` doesn't
logically belong here. But it's part of the family, so I wrote a
[dumpaxfr(1) man page][dumpaxfr] and included it in this release.

I will be surprised if anyone else finds `dumpaxfr` useful!

[nsnotifyd]: https://dotat.at/prog/nsnotifyd/
[dumpaxfr]: https://dotat.at/prog/nsnotifyd/dumpaxfr.1.html
[nsnotifyd-2.0]: https://dotat.at/@/2022-01-25-nsnotifyd-2-0-released.html
