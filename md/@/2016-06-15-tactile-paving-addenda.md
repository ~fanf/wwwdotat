---
dw:
  anum: 67
  eventtime: "2016-06-15T09:38:00Z"
  itemid: 461
  logtime: "2016-06-15T08:39:18Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/145211
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/118083.html"
format: html
lj:
  anum: 59
  can_comment: 1
  ditemid: 145211
  event_timestamp: 1465983480
  eventtime: "2016-06-15T09:38:00Z"
  itemid: 567
  logtime: "2016-06-15T08:39:18Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/145211.html"
title: Tactile paving addenda
...

<p>I have found out the likely reason that <a href="http://fanf.livejournal.com/145132.html">tactile paving markers on dual-use paths are the wrong way round</a>!

<p>A page on the UX StackExchange discusses <a href="http://ux.stackexchange.com/questions/42859/what-is-the-psychology-behind-laying-out-slabs-at-different-angles-for-cyclists">tactile paving on dual-use paths</a> and the best explanation is that it can be uncomfortable to walk on longitudinal ridges, whereas transverse ones are OK. 

<p>(<a href="http://therantyhighwayman.blogspot.co.uk/2015/02/tricky-tactiles.html?m=1">Apparently the usual terms are "tram" vs "ladder".</a>)

<p>I occasionally get a painful twinge in my ankle when I tread on the edge of a badly laid paving stone, so I can sympathise. But surely tactile paving is <I>supposed</I> to be felt by the feet, so it should not hurt people who tread on it even inadvertently!
