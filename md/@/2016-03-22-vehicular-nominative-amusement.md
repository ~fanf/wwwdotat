---
dw:
  anum: 171
  eventtime: "2016-03-22T21:37:00Z"
  itemid: 449
  logtime: "2016-03-22T21:37:30Z"
  props:
    commentalter: 1491292421
    import_source: livejournal.com/fanf/142116
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/115115.html"
format: html
lj:
  anum: 36
  can_comment: 1
  ditemid: 142116
  event_timestamp: 1458682620
  eventtime: "2016-03-22T21:37:00Z"
  itemid: 555
  logtime: "2016-03-22T21:37:30Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 3
  url: "https://fanf.livejournal.com/142116.html"
title: Vehicular nominative amusement
...

<p>Today I have been amused by a conversation on Twitter about car names.</p>

<p>It started with <a href="http://brilliantmaps.com/european-surnames/">a map of common occupational surnames in
Europe</a> which prompted me
to say that I thought <a href="https://twitter.com/fanf/status/712211005359714304">Smith's Cars doesn't sound quite as romantic as
Ferrari</a>.
<a href="https://twitter.com/pseudomonas/status/712211276068429824">Adam</a> and
<a href="https://twitter.com/richwareham/status/712250746243309568">Rich</a>
pointed out that "Farrier's Cars" might be a slightly better
translation since it keeps the meaning and still sounds almost as
good. And it made me think that perhaps <a href="https://en.wikipedia.org/wiki/File:Ferrari-Logo.svg">the horse is
prancing</a> because
it has new shoes.</p>

<p>In response I got <a href="https://twitter.com/bmcnett/status/712213502153216000">a lot of agricultural translations from
@bmcnett</a>.
Later there followed several more or less apocryphal stories about car
names: why <a href="https://twitter.com/StripeyCaptain/status/712345925822320640">the Toyota MR2 sold like shit in
France</a>,
why <a href="https://twitter.com/jpmens/status/712365703433687040">the Rolls Royce Silver Shadow was nearly silver-plated crap in
Germany</a>, or
(<a href="https://twitter.com/PhilRodgers/status/712359206083502084">one</a>,
<a href="https://twitter.com/alanrmb/status/712362832264863748">two</a>,
<a href="https://twitter.com/jpmens/status/712365303620104194">three</a>) why the
<a href="https://en.wikipedia.org/wiki/Mitsubishi_Pajero">Mitsubishi Pajero</a>
was only bought by wankers in Spain, and finally why <a href="http://www.snopes.com/business/misxlate/nova.asp">the Chevy Nova
did, in fact, <i>go</i> in Latin
America</a>.</p>

<p>(And, tangentially relevant, Mike Pitt also mentioned that <a href="https://en.wikipedia.org/wiki/Giuseppe_Verdi">Joe
Green</a>'s name is also
more romantic in his native Italian.)</p>

<p>But I was puzzled for a while when <a href="https://twitter.com/mathew/status/712309980242665472">the uniquely named mathew
mentioned the Lamborghini
Countach</a>,
before all the other silliness, when I had only remarked about
unromantic translations. I wasn't able to find an etymology of
Lamborghini, but <a href="https://en.wikipedia.org/wiki/File:Lamborghini_Logo.svg">their logo is a charging
bull</a>, and
<a href="http://www.car-addicts.com/feature/bulls-inspired-lamborghini-names">many of their cars are named after famous fighting
bulls</a>.
However, the Countach is not; the story goes that <a href="http://nancyfriedman.typepad.com/away_with_words/2012/03/how-the-countach-got-its-name.html">when Nuccio Bertoni
first saw a prototype he swore in
amazement</a>
- "countach" or "cuntacc" is apparently the kind of general-purpose
profanity a 1970s Piedmontese man might utter appreciatively upon
seeing a beautiful woman or a beautiful car.</p>

<p>So, maybe, at a stretch, it would not be completely wrong to translate
"Lamborghini Countach" to "bull shit!"</p>
