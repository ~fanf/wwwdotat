---
dw:
  anum: 6
  eventtime: "2004-04-09T19:11:00Z"
  itemid: 79
  logtime: "2004-04-09T11:11:32Z"
  props:
    commentalter: 1491292310
    import_source: livejournal.com/fanf/20273
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/20230.html"
format: casual
lj:
  anum: 49
  can_comment: 1
  ditemid: 20273
  event_timestamp: 1081537860
  eventtime: "2004-04-09T19:11:00Z"
  itemid: 79
  logtime: "2004-04-09T11:11:32Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/20273.html"
title: Sean of the dead
...

Enjoyed the trailer which I saw before the remake of Dawn of the Dead last week, so I'm going to see Sean this evening at the Village^WVUE cinema at 21:10.
