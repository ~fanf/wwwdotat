---
dw:
  anum: 35
  eventtime: "2015-11-17T22:59:00Z"
  itemid: 437
  logtime: "2015-11-17T22:59:12Z"
  props:
    commentalter: 1491292414
    hasscreened: 1
    import_source: livejournal.com/fanf/139158
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/111907.html"
format: html
lj:
  anum: 150
  can_comment: 1
  ditemid: 139158
  event_timestamp: 1447801140
  eventtime: "2015-11-17T22:59:00Z"
  itemid: 543
  logtime: "2015-11-17T22:59:12Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/139158.html"
title: C preprocessor expressions
...

<blockquote>
  <p><em>With reference to the C standard, describe the syntax of
controlling expressions in C preprocessor conditional directives.</em></p>
</blockquote>

<p>I've started work on a "C preprocessor partial evaluator". My aim is
to re-do <a href="http://dotat.at/prog/unifdef/">unifdef</a> with better
infrastructure than 1980s line-at-a-time <code>stdio</code>, so that it's easier
to implement a more complete C preprocessor. The downside, of course,
is that the infrastructure (Lua and Lpeg) is more than ten times
bigger than the whole of unifdef.</p>

<p>The main feature I want is macro expansion; the second feature I want
is <code>#if</code> expression simplification. The latter leads to the question
above: exactly what is allowed in a C preprocessor conditional
directive controlling expression? This turns out to be more tricky
than I expected.</p>

<p>What actually triggered the question was that I "know" that <code>sizeof</code>
doesn't work in preprocessor expressions because "obviously" the
preprocessor doesn't know about details of the target architecture,
but I couldn't find where in the standard it says so.</p>

<p>My reference is
<abbr style="border-bottom: 1px dotted;" title="international organization for standardization">ISO</abbr>
<abbr style="border-bottom: 1px dotted;" title="joint technical committee one">JTC1</abbr>
<abbr style="border-bottom: 1px dotted;" title="subcommittee twenty-two - programming languages">SC22</abbr>
<abbr style="border-bottom: 1px dotted;" title="working group fourteen - C">WG14</abbr> document
<a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf">n1570</a>
which is very close to the final committee draft of ISO 9899:2011,
the C11 standard.</p>

<p>Preprocessor expressions are specified in section 6.10.1 "Conditional
inclusion". Paragraph 1 says:</p>

<blockquote>
  <p>The expression that controls conditional inclusion shall be an
integer constant expression except that: identifiers (including
those lexically identical to keywords) are interpreted as described
below;<sup>166)</sup> and it may contain [<code>defined</code>] unary operator
expressions [...]</p>

<p><sup>166)</sup> Because the controlling constant expression is
evaluated during translation phase 4, all identifiers either are or
are not macro names — there simply are no keywords, enumeration
constants, etc.</p>
</blockquote>

<p>The crucial part that I missed is the parenthetical "including
[identifiers] lexically identical to keywords" - this applies to the
<code>sizeof</code> keyword, as footnote 166 obliquely explains.</p>

<p>... A brief digression on "translation phases". These are specified in
section 5.1.1.2, which lists 8 phases. Now, if you have done an
undergraduate course in compilers or read the dragon book, you might
expect this list to include things like lexing, parsing, symbol
tables, something about translation to and optimization of object
code, and something about linking separately compiled units. And it
does, sort of. But whereas compilers are heavily weighted towards the
middle and back end, C standard translation phases focus on lexical
and preprocessor matters, to a ridiculous extent. I find this
imbalance quite funny (in a rather dry way) - after such a lengthy and
detailed build-up, the last two items in the list are almost, "and
then a miracle occurs", especially the last sentence in phase 7 which
is a bit LOL WTF.</p>

<blockquote>
  <p><em>6</em>. Adjacent string literal tokens are concatenated.</p>

<p><em>7</em>. White-space characters separating tokens are no longer
significant. Each preprocessing token is converted into a token.
The resulting tokens are syntactically and semantically analyzed
and translated as a translation unit.</p>

<p><em>8</em>. All external object and function references are resolved. Library
components are linked to satisfy external references to functions
and objects not defined in the current translation. All such
translator output is collected into a program image which contains
information needed for execution in its execution environment.</p>
</blockquote>

<p>So, anyway, what footnote 166 is saying is that in the preprocessor
there is no such thing as a keyword - the preprocessor has a sketchy
lexer that produces "<em>preprocessing-token</em>"s (as specified in section
6.4) which are a simplified subset of the compiler's "<em>token</em>"s which
mostly don't turn up until translation phase 7.</p>

<p>Paragraph 1 said identifiers are interpreted as described below, which
refers to this sentence in section 6.10.1 paragraph 4:</p>

<blockquote>
  <p>After all replacements due to macro expansion and the defined unary
operator have been performed, all remaining identifiers (including
those lexically identical to keywords) are replaced with the
<em>pp-number</em> 0, and then each preprocessing token is converted into a
token. The resulting tokens compose the controlling constant
expression which is evaluated according to the rules of 6.6.</p>
</blockquote>

<p>This means that if you try to use a keyword (such as <code>sizeof</code>) in a
preprocessor expression, it gets replaced by zero and (usually) turns
into a syntax error. And this is why compilers produce
less-than-straightforward error messages like <code>error: missing binary
operator before token "("</code> if you try to use <code>sizeof</code>.</p>

<p>Smash-keyword-to-zero has another big implication which is a bit more
subtle. Section 6.6 specifies constant expressions, and paragraphs 3
and 6 are particularly relevant to the preprocessor.</p>

<blockquote>
  <p><small>3</small> Constant expressions shall not contain assignment,
increment, decrement, function-call, or comma operators, except when
they are contained within a subexpression that is not evaluated.</p>
</blockquote>

<p>It is normal for real preprocessor expression parsers to implement a
simplified subset of the C expression syntax which simply lacks
support for these forbidden operators. So, if you put <code>sizeof(int)</code> in
a preprocessor expression, that gets turned into <code>0(0)</code> before it is
evaluated, and you get an error about a missing binary operator. If
you write something similar where the <em>compiler</em> expects an integer
constant expression, you will get errors complaining that integers are
not functions or that function calls are not allowed in integer
constant expressions.</p>

<blockquote>
  <p><small>6</small> An integer constant expression shall have integer
type and shall only have operands that are integer constants,
enumeration constants, character constants, <code>sizeof</code> expressions
whose results are integer constants, <code>_Alignof</code> expressions, and
floating constants that are the immediate operands of casts.</p>
</blockquote>

<p>Re-read this sentence from the point of view of the preprocessor,
after identifiers and keywords have been smashed to zero. There aren't
any enumeration constants, because they are
<strike>identifiers</strike> zero. Similarly there aren't any <code>sizeof</code>
or <code>_Alignof</code> expressions. And there can't be any casts because you
can't write a type without at least one identifier. (One situation
where smash-keyword-to-zero does <em>not</em> cause a syntax error is an
expression like <code>(unsigned)-1</code> and I bet that turns up in real-world
preprocessor expressions.) And since there can't be any casts, there
can't be any floating constants.</p>

<p>And therefore the preprocessor does not need any floating point
support at all.</p>

<p>I am slightly surprised that such a fundamental simplification
requires such a long chain of reasoning to obtain it from the
standard. Perhaps (like my original question about <code>sizeof</code>)
I have overlooked the relevant text.</p>

<p>Finally, my thanks to Mark Wooding and <a href="https://twitter.com/bmastenbrook/status/666660315187314688">Brian
Mastenbrook</a>
for pointing me at the crucial words in the standard.</p>
