---
format: casual
lj:
  anum: 41
  can_comment: 1
  ditemid: 106537
  event_timestamp: 1273660320
  eventtime: "2010-05-12T10:32:00Z"
  itemid: 416
  logtime: "2010-05-12T10:32:40Z"
  props:
    personifi_tags: "17:75,18:12,42:12,16:75,3:25,nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/106537.html"
title: The Red Flag no longer flies over Downing Street
...

The Tory flag is Cambridge blue
no longer of the purest hue
It's all because we made a pact
with yellow Lib'ral Democrat
We'll brief against the other side
if you don't like what we decide
so next time they'll lose thoroughly
and give us a majority!

<i>Suggestions for alternate lines welcome :-)</i>
