---
dw:
  anum: 250
  eventtime: "2008-04-15T16:42:00Z"
  itemid: 335
  logtime: "2008-04-15T15:42:51Z"
  props:
    import_source: livejournal.com/fanf/86599
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/86010.html"
format: html
lj:
  anum: 71
  can_comment: 1
  ditemid: 86599
  event_timestamp: 1208277720
  eventtime: "2008-04-15T16:42:00Z"
  itemid: 338
  logtime: "2008-04-15T15:42:51Z"
  props:
    verticals_list: computers_and_software
  reply_count: 0
  url: "https://fanf.livejournal.com/86599.html"
title: A little more Hashlife
...

<p>In <a href="http://fanf.livejournal.com/83709.html">my post about Hashlife</a> I said that calc1() "is not memoized, but instead relies on calc2() for efficiency". This is wrong.</p>

<p><small>[calc1() is the function that calculates an arbitrary number of generations into the future; calc2() is the function that calculates 2^(n-2) generations into the future for a square 2^n cells on a side, and its result is cached in the main quadtree.]</small></p>

<p>Calculating Life efficiently requires skipping over the easy bits as fast as possible, which in most implementations is blank space, but in Hashlife it ought to include any other kind of previously-computed space too. However calc1() can only skip large amounts of space if it is also skipping large amounts of time - a multiple of a large power of two, to be exact. This means that if you are single-stepping, my Hashlife code would separately examine every 4x4 square in the universe. This is particularly dumb when there is inevitably lots of blank space around the edges.</p>

<p>The solution is to memoize the calculation for arbitrary numbers of generations (or at least for 1 <= g <= n^(n-2) for squares 2^n on a side). If you do that, it no longer makes sense to store the result pointers in the squares themselves: instead, bung them all in another hash table that is indexed by the pair of a square and a generation count. Having done that, it also makes sense to evict the population counts from the squares into a third hash table, since they are rarely needed so just waste memory.</p>

<p>The other Hashlife implementations that I was cribbing off do not have this extra cacheing. I wonder if that is why Hashlife has a poor reputation for efficiency? I'll have to do some more implementation to find out.</p>

<p>While I'm here, I should say that David Bell's equivalent of calc1() is interesting because it only examines the past light cone of the region that is being displayed, which saves working on irrelevant far-away areas - though he spoils it by re-doing the recursion for every pixel to be displayed...</p>
