---
dw:
  anum: 100
  eventtime: "2007-08-23T22:44:00Z"
  itemid: 300
  logtime: "2007-08-23T21:45:48Z"
  props:
    commentalter: 1491292344
    import_source: livejournal.com/fanf/77178
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/76900.html"
format: html
lj:
  anum: 122
  can_comment: 1
  ditemid: 77178
  event_timestamp: 1187909040
  eventtime: "2007-08-23T22:44:00Z"
  itemid: 301
  logtime: "2007-08-23T21:45:48Z"
  props: {}
  reply_count: 8
  url: "https://fanf.livejournal.com/77178.html"
title: best SOA evar!
...

<pre>
; <<>> DiG 9.2.4 <<>> soa figure53.com. @ns1.dreamhost.com.
;; global options:  printcmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 26927
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;figure53.com.                  IN      SOA

;; ANSWER SECTION:
figure53.com.           14400   IN      SOA     2007081700. 21487. 1800 1814400 14400 604800 3600

;; Query time: 153 msec
;; SERVER: 66.33.206.206#53(ns1.dreamhost.com.)
;; WHEN: Thu Aug 23 22:42:10 2007
;; MSG SIZE  rcvd: 81
</pre>
