---
dw:
  anum: 179
  eventtime: "2003-05-02T13:16:00Z"
  itemid: 19
  logtime: "2003-05-02T08:28:55Z"
  props:
    commentalter: 1491292304
    current_moodid: 98
    current_music: Daft Punk
    import_source: livejournal.com/fanf/5087
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/5043.html"
format: casual
lj:
  anum: 223
  can_comment: 1
  ditemid: 5087
  event_timestamp: 1051881360
  eventtime: "2003-05-02T13:16:00Z"
  itemid: 19
  logtime: "2003-05-02T08:28:55Z"
  props:
    current_moodid: 98
    current_music: Daft Punk
  reply_count: 2
  url: "https://fanf.livejournal.com/5087.html"
title: another ykybhtlw dream
...

I had another odd dream last Saturday night after Rachel & Keith's housewarming party. Someone asked me over for some advice. I looked over their shoulder to see a book containing maths in some kind of lemma, lemma, proof arrangement (with some parts indented) -- except that the maths was actually chocolate animal biscuits. I looked at this for a bit trying to puzzle it out, until some talking woke me up. Rachel was looking at me quizzically, because I had been talking in my sleep, explaining Python's scoping to her. (It seemed that the object vs. function scoping of variables somehow related to the indentation of the chocolate biscuit maths.) We quickly went back to sleep.

I asked Rachel about this in the morning and it turned out that the whole thing had been a dream. I don't remember dreaming inside a dream before...
