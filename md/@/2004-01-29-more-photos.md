---
dw:
  anum: 202
  eventtime: "2004-01-29T12:35:00Z"
  itemid: 58
  logtime: "2004-01-29T12:37:27Z"
  props:
    commentalter: 1491292308
    current_moodid: 91
    import_source: livejournal.com/fanf/14896
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/15050.html"
format: casual
lj:
  anum: 48
  can_comment: 1
  ditemid: 14896
  event_timestamp: 1075379700
  eventtime: "2004-01-29T12:35:00Z"
  itemid: 58
  logtime: "2004-01-29T12:37:27Z"
  props:
    current_moodid: 91
  reply_count: 2
  url: "https://fanf.livejournal.com/14896.html"
title: More photos
...

I've updated the <a href="https://fanf2.user.srcf.net/photos.html">Hermes pictures</a> page with a photo of the backup server (which has shiny blue and white LEDs).

I've also added a page about <a href="https://fanf2.user.srcf.net/hermes/doc/misc/shed/">the shed in the machine room</a>.
