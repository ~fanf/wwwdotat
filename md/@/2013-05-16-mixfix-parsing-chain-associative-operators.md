---
format: html
lj:
  anum: 252
  can_comment: 1
  ditemid: 127228
  event_timestamp: 1368663900
  eventtime: "2013-05-16T00:25:00Z"
  itemid: 496
  logtime: "2013-05-15T23:25:38Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 12
  url: "https://fanf.livejournal.com/127228.html"
title: Mixfix parsing / chain-associative operators
...

<meta charset="utf-8">

<p>Earlier this evening I was reading about <a
href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.157.7899">parsing
user-defined mixfix operators in Agda</a>.

<p>Mixfix operators are actually quite common though they are usually
called something else. The Danielsson and Norell paper describes
operators using a notation in which underscores are placeholders for
the operands, so (using examples from C) we have

<ul><li>postfix operators
    <ul><li>_ ++
        <li>_ .member
    </ul>
    <li>prefix operators
    <ul><li>! _
        <li>& _
    </ul>
    <li>infix operators
    <ul><li>_ + _
	<li>_ == _
    </ul>
</ul>

<p>There are also circumfix operators, of which the most common example
is (_) which boringly does nothing. Mathematical notation has some
more fun examples, such as ceiling ⎡_⎤ and floor ⎣_⎦.

<p>Mixfix operators have a combination of fixities. C has a few examples:
<ul>
    <li>_ [ _ ]
    <li>_ ? _ : _
</ul>

You can also regard function call syntax as a variable-arity mixfix
operator :-)

<p>The clever part of the paper is how it handles precedence in a way
that is reasonably easy for a programmer to understand when defining
operators, and which allows for composing libraries which might have
overlapping sets of operator definitions.

<p>One thing that their mixfix parser does not get funky about is
associativity: it supports the usual left-, right-, and
non-associative operators. One of my favourite syntactic features is
chained relational operators, as found in BCPL and Python. (For fun I
added the feature to Lua -
see <a href="https://github.com/fanf2/lua-5/commit/3cf0d2">this and
the following two patches</a>.) You can write an expression like
<pre>
    a OP b OP c OP d
</pre>
which is equivalent to
<pre>
    a OP b && b OP c && c OP d
</pre>
except that each operand is evaluated at most once. (Though
unfortunately BCPL may evaluate inner operands twice.) This is not
just short-cutting variable-arity comparison because the operators can
differ.

<p>So I wonder, are there other examples of chain-associative
operators? They might have a different underlying reduction operator
instead of &&, perhaps, which would imply different short-cut behaviour.

<p>Perhaps an answer would come to mind if I understood more of the
category-theory algebraic functional programming stuff like bananas
and lenses and what have you...
