---
dw:
  anum: 208
  eventtime: "2007-08-09T16:06:00Z"
  itemid: 296
  logtime: "2007-08-09T19:11:45Z"
  props:
    commentalter: 1491292389
    import_source: livejournal.com/fanf/76075
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/75984.html"
format: casual
lj:
  anum: 43
  can_comment: 1
  ditemid: 76075
  event_timestamp: 1186675560
  eventtime: "2007-08-09T16:06:00Z"
  itemid: 297
  logtime: "2007-08-09T19:11:45Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/76075.html"
title: The scalability of Erlang-style message passing
...

Erlang is an excellent language for implementing distributed systems. One of the reasons for this is the semantics of its message-passing primitives. (Other reasons include the way it isolates processes and its distributed failure handling.)

Erlang's <tt>send</tt> operation is non-blocking, and sends a message to a particular process. Its <tt>receive</tt> operation can block (with an optional timeout) and can selectively pick messages from the process's mailbox out-of-order, which is useful for avoiding state explosion or tedious book-keeping.

Some languages, including <a href="http://www.wotug.org/occam/">occam</a> and <a href="http://cml.cs.uchicago.edu/">CML</a>, have a <tt>send</tt> which can block until another process is ready to <tt>receive</tt>. This kind of rendezvous doesn't make sense in a distributed setting when there is significant latency between the two processes: does the synchronization happen at the sender or the receiver? With asynchronous message passing you have to be explicit about it, typically by making the sender wait for an ack so synchronization happens at the receiver.

Instead of messages being sent to processes, many concurrent languages have a notion of a channel. The problem with channels in a distributed setting is dealing with message routing when channel endpoints can be passed from machine to machine. It's unfeasibly tricky if (as with CML and <a href="http://haskell.org/haskellwiki/GHC/Concurrency">Concurrent Haskell</a>) multiple processes (potentially on multiple machines) can be waiting to <tt>receive</tt> from a channel.

It's interesting to observe that, as well as scaling up gracefully, Erlang-style message passing also scales down to very simple forms of concurrency, i.e. coroutines, co-operative multi-tasking, or "<a href="http://en.wikipedia.org/wiki/Fiber_(computer_science)">fibres</a>". Communication between coroutines is usually synchronous (since you pass the flow of control as well as some values) and addressed by process (not by channel).

Although coroutines can be dead simple, there are some variations, chiefly between asymmetric/symmetric (distinguishing resume and yield or not), and kinds of hobbled coroutines that typically can't yield from a nested function call. The paper about <a href="http://www.inf.puc-rio.br/~roberto/docs/corosblp.pdf">coroutines in Lua</a> has a good survey, plus an example of how you can implement symmetric coroutines on top of asymmetric coroutines by adding a scheduler. (The dual implementation is similar.) You also need a scheduler if you are going to add features like message passing. <a href="http://members.verizon.net/olsongt/stackless/why_stackless.html#enter-coroutines">Stackless Python</a> is a good example of an elaborate coroutine implementation.

Erlang-style message passing between coroutines is dead simple. The <tt>send</tt> routine just appends the message to the end of a queue, and <tt>receive</tt> just pulls a message off the start of the queue and dispatches it to the destination process. For selective receive, each process can have a mailbox of pending messages which it checks for a match before dispatching on the main queue; when a process is resumed with a message that doesn't match its current <tt>receive</tt> statement it adds the message to its mailbox and dispatches the next message from the main queue.

Thinking about this reminds me a lot of my larval stage hacking around with the Acorn Archimedes RISC OS WIMP, especially the UserMessage feature.
