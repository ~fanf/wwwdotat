---
dw:
  anum: 179
  eventtime: "2016-10-19T14:25:00Z"
  itemid: 471
  logtime: "2016-10-19T13:25:15Z"
  props:
    commentalter: 1522648303
    hasscreened: 1
    import_source: livejournal.com/fanf/147756
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/120755.html"
format: html
lj:
  anum: 44
  can_comment: 1
  ditemid: 147756
  event_timestamp: 1476887100
  eventtime: "2016-10-19T14:25:00Z"
  itemid: 577
  logtime: "2016-10-19T13:25:15Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/147756.html"
title: Domain transfers are shocking
...

<p>I have a bit of a bee in my bonnet about using domain names
consistently as part of an organization's branding and communications.
I don't much like the proliferation of special-purpose or short-term
vanity domains.</p>

<p>They are particularly vexing when I am doing something
security-sensitive. For example, domain name transfers. I'd like to be
sure that someone is not trying to race with my transfer and steal the
domain name, say.</p>

<p>Let's have a look at a practical example: transfering a domain from
Gandi to Mythic Beasts.</p>

<p>(I like Gandi, but getting the University to pay their domain fees is
a massive chore. So I'm moving to Mythic Beasts, who are local,
friendly, accommodating, and able to invoice us.)</p>

<p><i>Edited to add:</i> The following is more ranty and critical than is entirely fair. I should make it clear that both Mythic Beasts and Gandi are right at the top of my list of companies that it is good to work with.</p>

</p>This just happens to be an example where I get to see both ends of the transfer. In most cases I am transferring to or from someone else, so I don't get to see the whole process, and the technicalities are trivial compared to the human co-ordination!</p>

<h2>First communication</h2>

<pre><code>    Return-Path: &lt;opensrs-bounce@registrarmail.net&gt;
    Message-Id: &lt;DIGITS.DATE-osrs-transfers-DIGITS@cron01.osrs.prod.tucows.net&gt;
    From: "Transfer" &lt;do_not_reply@ns-not-in-service.com&gt;
    Subject: Transfer Request for EXAMPLE.ORG

    https://approve.domainadmin.com/transfer/?domain=EXAMPLE.ORG
</code></pre>

<p>A classic! Four different domain names, none of which identify either
of our suppliers! But I know Mythic Beasts are an OpenSRS reseller,
and OpenSRS is a Tucows service.</p>

<p>Let's see what <code>whois</code> has to say about the others...</p>

<pre><code>    Domain Name: REGISTRARMAIL.NET
    Registrant Name: Domain Admin
    Registrant Organization: Yummynames.com
    Registrant Street: 96 Mowat Avenue
    Registrant City: Toronto
    Registrant Email: whois@yummynames.com
</code></pre>

<p>"Yummynames". Oh kaaaay.</p>

<pre><code>    Domain Name: YUMMYNAMES.COM
    Registrant Name: Domain Admin
    Registrant Organization: Tucows.com Co.
    Registrant Street: 96 Mowat Ave.
    Registrant City: Toronto
    Registrant Email: tucowspark@tucows.com
</code></pre>

<p>Well I suppose that's OK, but it's a bit of a rabbit hole.</p>

<p>Also,</p>

<pre><code>    $ dig +short mx registrarmail.net
    10 mx.registrarmail.net.cust.a.hostedemail.com.
</code></pre>

<p>Even more generic than Fastmail's <code>messagingengine.com</code>
infrastructure domain :-)</p>

<pre><code>    Domain Name: HOSTEDEMAIL.COM
    Registrant Name: Domain Admin
    Registrant Organization: Tucows Inc
    Registrant Street: 96 Mowat Ave.
    Registrant City: Toronto
    Registrant Email: domain_management@tucows.com
</code></pre>

<p>The domain in the <code>From:</code> address, <code>ns-not-in-service.com</code> is an odd
one. I have seen it in whois records before, in an obscure context.
When a domain needs to be cancelled, there can sometimes be glue
records inside the domain which also need to be cancelled. But they
can't be cancelled if other domains depend on those glue records. So,
the registrar renames the glue records into a place-holder domain,
allowing the original domain to be cancelled.</p>

<p>So it's weird to see one of these cancellation workaround placeholder
domains used for customer communications.</p>

<pre><code>    Domain Name: NS-NOT-IN-SERVICE.COM
    Registrant Name: Tucows Inc.
    Registrant Organization: Tucows Inc.
    Registrant Street: 96 Mowat Ave
    Registrant City: Toronto
    Registrant Email: corpnames@tucows.com
</code></pre>

<p>Tucows could do better at keeping their whois records consistent!</p>

<p>Finally,</p>

<pre><code>    Domain Name: DOMAINADMIN.COM
    Registrant Name: Tucows.com Co. Tucows.com Co.
    Registrant Organization: Tucows.com Co.
    Registrant Street: 96 Mowat Ave
    Registrant City: Toronto
    Registrant Email: corpnames@tucows.com
</code></pre>

<p>So good they named it twice!</p>

<h2>Second communication</h2>

<pre><code>    Return-Path: &lt;bounce+VERP@bounce.gandi.net&gt;
    Message-ID: &lt;DATE.DIGITS@brgbnd28.bi1.0x35.net&gt;
    From: "&lt;noreply"@domainnameverification.net
    Subject: [GANDI] IMPORTANT: Outbound transfer of EXAMPLE.ORG to another provider

    http://domainnameverification.net/transferout_foa/?fqdn=EXAMPLE.ORG
</code></pre>

<p>The syntactic anomaly in the <code>From:</code> line is a nice touch.</p>

<p>Both <code>0x35.net</code> and <code>domainnameverification.net</code> belong to Gandi.</p>

<pre><code>    Registrant Name: NOC GANDI
    Registrant Organization: GANDI SAS
    Registrant Street: 63-65 Boulevard MASSENA
    Registrant City: Paris
    Registrant Email: noc@gandi.net
</code></pre>

<p>Impressively consistent whois :-)</p>

<h2>Third communication</h2>

<pre><code>    Return-Path: &lt;opensrs-bounce@registrarmail.net&gt;
    Message-Id: &lt;DIGITS.DATE-osrs-transfers-DIGITS@cron01.osrs.prod.tucows.net&gt;
    From: "Transfers" &lt;dns@mythic-beasts.com&gt;
    Subject: Domain EXAMPLE.ORG successfully transferred
</code></pre>

<p>OK, so this message has the reseller's branding, but the first one didn't?!</p>

<h2>The web sites</h2>

<p>To confirm a transfer, you have to paste an EPP authorization code
into the old and new registrars' confirmation web sites.</p>

<p>The first site <code>https://approve.domainadmin.com/transfer/</code> has very
bare-bones OpenSRS branding. It's a bit of a pity they don't allow
resellers to add their own branding.</p>

<p>The second site <code>http://domainnameverification.net/transferout_foa/</code>
is unbranded; it isn't clear to me why it isn't part of Gandi's normal
web site and user interface. Also, it is plain HTTP without TLS!</p>

<h2>Conclusion</h2>

<p>What I would like from this kind of process is an impression that it is
reassuringly simple - not involving loads of unexpected organizations
and web sites, difficult to screw up by being inattentive. The actual
experience is shambolic.</p>

<p>And remember that basically all Internet security rests on domain name
ownership, and this is part of the process of maintaining that
ownership.</p>

<p>Here endeth the rant.</p>
