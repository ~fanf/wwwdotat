---
dw:
  anum: 191
  eventtime: "2020-11-13T22:44:00Z"
  itemid: 522
  logtime: "2020-11-13T22:59:09Z"
  props:
    commentalter: 1605349577
    hasscreened: 1
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1605308761
  url: "https://fanf.dreamwidth.org/133823.html"
format: md
...

Leap second hiatus
==================

[ adapted from [a Twitter thread](https://twitter.com/fanf/status/1327353787548393472) ]

Normally I only pay attention to leap seconds every 6 months when the
IERS publishes [Bulletin C][IERS bulletins], the leap second yes or no
announcement. But this week brings news from [Michael Deckers via the
LEAPSECS mailing list][LEAPSECS], and it relates to Bulletin A, which
is why it’s off my usual 6 month schedule.

[LEAPSECS]: https://pairlist6.pair.net/pipermail/leapsecs/2020-November/007269.html

<cut>

Leap seconds exist because the Earth takes (_very_ roughly) about a
millisecond more than 24 * 60 * 60 seconds to rotate each day; when we
have accumulated enough extra milliseconds, a leap second is inserted
into UTC to keep it in sync with the Earth. At the moment the Earth is
rotating faster than in recent decades: these shorter days, with a
lower length-of-day, means the milliseconds accumulate more slowly,
and we get fewer leap seconds.

The news from Bulletin A is not unexpected: [in January][old1] I noted
that we are currently in a long gap between leap seconds, and [in
July][old2] I observed that the length of day chart is still very low.

[IERS bulletins]: https://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html
[old1]: https://twitter.com/fanf/status/1221882993549156352
[old2]: https://twitter.com/fanf/status/1280621594616967171

[Bulletin A][] is a weekly summary of predictions from the latest
Earth Orientation model. It includes the projected difference between
Earth rotation angle (UT1) and UTC, which in turn determines when a
leap second is needed. The notable thing about the latest Bulletin A
is the factor 0.00000 in the equation

        UT1-UTC = -0.1746 - 0.00000 (MJD - 59173) - (UT2-UT1)

[Bulletin A]: https://datacenter.iers.org/data/latestVersion/6_BULLETIN_A_V2013_016.txt

> _[ Tangential aside: I feel the need to describe 0.00000 as all
> balls, because in PostgreSQL you can use [“allballs” in a time
> literal][allballs] as an alias for midnight :-) ]_

[allballs]: https://postgresql.org/docs/current/datatype-datetime.html#DATATYPE-DATETIME-SPECIAL-TABLE

The zero factor is multiplied by the date, which is writtem
MJD - 59173. This is the time in days after 20th November; the product
indicates how much UT1 and UTC are expected to differ in the near
future, i.e. the rate of divergence is now zero.

Michael Deckers said in [his LEAPSECS message][LEAPSECS] that we
haven’t seen a rate difference as low as zero since 1961!

This implies that unless something wild happens, we are very unlikely
to have a leap second in the next few years.

Time for some pictures! (click on the charts to visit interactive
versions on the IERS web site)

Here's a chart of Bulletin A UT1-UTC values, in which you can
see the vertical cliffs when leap seconds occurred. See how flat it is
towards the end of the chart! Past values are in red, and projected
future values in blue.

<a href="https://datacenter.iers.org/singlePlot.php?plotname=FinalsDataIAU2000A-UT1-UTC-BULA&id=10">
<img src="https://datacenter.iers.org/data/pl/FinalsDataIAU2000A-UT1-UTC-BULA.png"
     alt="chart of Bulletin A UT1-UTC"
	 width="100%">
</a>

And here's the LOD, length of day, the time it takes for the Earth to
rotate, measured in milliseconds longer than 24 * 60 * 60 seconds.
This is the gradient of the UT1-UTC chart; you can see that it's a
difficult mixture of short-term noise, seasonal variations, and
unpredictable longer-term changes.

<a href="https://datacenter.iers.org/singlePlot.php?plotname=FinalsDataIAU2000A-LOD-BULA&id=10">
<img src="https://datacenter.iers.org/data/pl/FinalsDataIAU2000A-LOD-BULA.png"
     alt="chart of length of day"
	 width="100%">
</a>

In the charts, compare the recent values to the period 1999 - 2006,
which was the longest gap between leap seconds. The numbers suggest
the current gap will be even longer.

The absence of leap seconds has the advantage that leap second bugs
don’t get tickled, but it has the disadvantage that timekeeping code
might rot and new bugs or regressions can be introduced without anyone
noticing. _Even worse_ is the risk of the length of day getting
_shorter_ which could in theory mean we might need a **negative leap
second**. There has never been a negative leap second, and if there is
one, everyone who deals with NTP or kernel timekeeping code expects
that it will be an appalling shitshow.

So, I guess, let’s all hope that the Bulletin A UT1-UTC chart remains
nice and flat :-)
