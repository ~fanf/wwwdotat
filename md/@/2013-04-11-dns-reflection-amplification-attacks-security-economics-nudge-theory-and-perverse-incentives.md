---
format: html
lj:
  anum: 104
  can_comment: 1
  ditemid: 126568
  event_timestamp: 1365704040
  eventtime: "2013-04-11T18:14:00Z"
  itemid: 494
  logtime: "2013-04-11T17:14:02Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 7
  url: "https://fanf.livejournal.com/126568.html"
title: "DNS reflection / amplification attacks: security economics, nudge theory, and perverse incentives."
...

<p>In recent months DNS amplification attacks have grown to become a
serious problem, the most recent peak being the 300 Gbit/s
<a href="http://www.spamhaus.org/news/article/695/answers-about-recent-ddos-attack-on-spamhaus">Spamhaus DDoS attack</a>
which received widespread publicity partly due to effective PR by
<a href="http://blog.cloudflare.com/the-ddos-that-almost-broke-the-internet">CloudFlare</a>
and a series of articles in the New York Times
(<a href="http://www.nytimes.com/2013/03/27/technology/internet/online-dispute-becomes-internet-snarling-attack.html?pagewanted=all">one</a>
<a href="http://www.nytimes.com/2013/03/28/technology/attacks-on-spamhaus-used-internet-against-itself.html?pagewanted=all">two</a>
<a href="http://www.nytimes.com/interactive/2013/03/30/technology/how-the-cyberattack-on-spamhaus-unfolded.html">three</a>).

<p>Amplification attacks are <i>not</i> specific to the DNS: any service
that responds to a single datagram with a greater number and/or size
of reply datagrams can be used to magnify the size of an attack. Other
examples are ICMP (as in the <a href="http://en.wikipedia.org/wiki/Smurf_attack">smurf attacks</a>
that caused problems about 15 years ago) and SNMP (which has not yet
been abused on a large scale).

<p>The other key ingredient is the attacker's ability to spoof the source
address on packets, in order to direct the amplified response towards
the ultimate victim instead of back to the attacker. The attacks can
be defeated either by preventing amplification or by preventing
spoofed source addresses.

<p>Smurf attacks were stopped by disabling ICMP to broadcast addresses,
both in routers (dropping ICMP to local broadcast addresses) and hosts
(only responding to directly-addressed ICMP). This fix was possible
since there is very little legitimate use for broadcast ICMP. The fix
was successfully deployed mainly because vendor defaults changed and
equipment was upgraded. Nudge theory in action.

<p>If you can't simply turn off an amplifier, you may be able to restrict
it to authorized users, either by IP address (as in recursive DNS
servers) and/or by application level credentials (such as SNMP
communities). It is easier to police any abuse if the potential
abusers are local. Note that if you are restricting UDP services by IP
address, you also need to deploy spoofed source address filtering to
prevent remote attacks which have both amplifier and victim on your
network. There are still a lot of vendors shipping recursive DNS servers that
are open by default; this is improving slowly.

<p>But some amplifiers, such as authoritative DNS serves, are hard to
close because they exist to serve anonymous clients across the
Internet. It may be possible to quash the abuse by <a href="http://www.redbarn.org/dns/ratelimits">suitably clever
rate-limiting</a> which, if you are lucky, can be as easy to use as
DNS RRL; without sufficiently advanced technology you have to rely on
diligent expert operators.

<p>There is a large variety of potential amplifiers which each require
specific mitigation; but all these attacks depend on spoofed source
addresses, so they can all be stopped by preventing spoofing. This has
been recommended for over ten years (see
<a href="http://tools.ietf.org/html/bcp38">BCP 38</a> and
<a href="http://tools.ietf.org/html/bcp84">BCP 84</a>) but it still
has not been deployed widely enough to stop the attacks. The problem
is that there are not many direct incentives to do so: there is the
reputation for having a well-run network, and perhaps a reduced risk
of being sued or prosecuted - though the risk is nearly negligible
even if you don't filter. Malicious traffic does not usually cause
operational problems for the originating network in the way it does
for the victims and often also the amplifiers.
There is a lack of indirect incentives too:
routers do not filter spoofed packets by default.

<p>There are a number of disincentives. There is a risk of accidental
disruption due to more complicated configuration. Some networks view
transparency to be more desirable than policing the security of their
customers. And many networks do not want to risk losing money if the
filters cause problems for their customers.

<p>As well as <a href="https://freedom-to-tinker.com/blog/felten/security-lessons-from-the-big-ddos-attacks/">unhelpful externalities</a>
there are perverse incentives: source address spoofing has to be
policed by an edge network provider that is acting as an agent of the
attacker - perhaps unwittingly, but they are still being paid to
provide insecure connectivity. There is a positive incentive to
corrupt network service providers that allow criminals to evade
spoofing filters. The networks that feel the pain are unable to fix
the problem.

<p>More speculatively, if we can't realistically guarantee security near
the edge, it might be possible to police spoofing throughout the
network. In order for this to be possible, we need a comprehensive
registry of which addresses are allocated to which networks (a secure
whois), and a solid idea of which paths can be used to reach each
network (secure BGP). This might be enough to configure useful packet
filters, though it will have <a href="http://fanf.livejournal.com/113608.html">similar scalability problems as
address-based packet forwarding</a>.

<p>So we will probably never be able to eliminate amplification attacks,
though we ought to be able to reduce them to a tolerable level. To do
so we will have to reduce both datagram amplifiers and source address
spoofing as much as possible, but neither of these tasks will ever be
complete.
