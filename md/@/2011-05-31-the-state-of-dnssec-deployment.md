---
format: html
lj:
  anum: 68
  can_comment: 1
  ditemid: 113732
  event_timestamp: 1306885440
  eventtime: "2011-05-31T23:44:00Z"
  itemid: 444
  logtime: "2011-05-31T22:44:15Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/113732.html"
title: The state of DNSSEC deployment
...

<p>DNSSEC comprises a couple of fairly independent sets of protocol extensions:
<ul>
<li>Transaction authentication</li>
<li>Data authentication</li>
</ul>
</p>
<p>Transaction authentication has a few varieties: TSIG, GSS-TSIG, and SIG(0). It is mainly used to authenticate zone transfers between master and slave authoritative servers, and to authenticate DNS UPDATE messages (in Microsoft Active Directory domains, for instance). It has been widely deployed for years now. It's fairly unglamorous because it doesn't require a massive worldwide campaign to get it deployed: instead a network of clients and servers can deploy it and benefit from improved security without liaising with anyone else.</p>
<p>Data authentication is where all the noise is at the moment. Unlike TSIG, this is a change to the DNS data model to add fine-grained cryptographic signatures to the data in the DNS, so that users can verify that it is authentic. The deployment campaign is well under way: many of the largest TLDs are signed and accepting secure delegations. There are still barriers to deployment: many registrars are not yet ready to handle secure delegations, and many DNS hosting services don't support DNSSEC. You can work around the former to some extent using <a href="http://dlv.isc.org/">the ISC's DLV service</a>. You can work around the latter by doing it yourself, but the tools are still a bit rough so you have to enjoy being on the bleeding edge.</p>
<p>The counterpart to signing the zones is deploying validating resolvers that actually check the signatures. It's harder to tell how widespread this is yet. It's fairly clear that the <tt>.gov</tt> requirement to deploy DNSSEC was interpreted as a requirement to sign the zones, but not to deploy validation. Hence they seem to have been slow to become aware of broken signatures and to fix them. If you turn validation on, you have to be willing to deal with a little more operational pain than usual for the DNS. While it is common for smaller domains to screw up their DNS, DNSSEC has new exciting failure modes that have led to occasional breakage in more important domains. But in practice for us it doesn't seem to have noticeably increased the support load - but we don't deal with <tt>.gov</tt> much :-)</p>
<p>What's next? There are a couple of prongs developing in parallel:
<ul>
<li>Applications built on DNSSEC</li>
<li>End-user security</li>
</ul>
</p>
<p>Once you can depend on the authenticity / validity of data obtained from the DNS, you can use it to bootstrap trust in higher-level protocols. For example, you can use <a href="http://tools.ietf.org/search/rfc4255">SSHFP</a> records to avoid manual host key validation or leap-of-faith authentication. Or you can use <a href="http://tools.ietf.org/wg/dane/">TLSA</a> records to mitigate <a href="http://www.google.co.uk/search?q=a+mighty+fortress+is+our+pki">the weaknesses of the X.509 certificate authority system</a>.</p>
<p>But in order for these applications to be secure, you have to push the secure area all the way out to the end user. Here the roadmap is a bit murky because some important parts are missing.</p>
<p>The traditional Unix stub resolver is based on the assumption that you can just do a simple request/response with your recursive resolver which does all the hard work for you. The recursive server is usually remote, and the stub resolver usually doesn't have DNS hardening features (port randomization, query ID randomization), and the link between the two is the most likely to be attacked. Given that, a validating remote resolver is not actually much use - it adds failure modes without meaningfully improving security.</p>
<p>A fix that is sometimes suggested is to deploy some kind of channel security between stubs and resolvers (something like TSIG). This isn't attractive for a couple of reasons. It probably requires new protocol development to specify the key management, so it'll be a long time before it is usable. Also the stubs continue to trust their resolvers, which is not desirable for mobile devices roaming on untrustworthy networks.</p>
<p>Really, what you want to do is run a validating cache on your local machine, which the stub resolvers can talk to over a private socket. A shared validator keeps the stub resolvers simple, and a cache avoids repeated fetches of the same signatures and keys.</p>
<p>BIND 9 can in fact be run in exactly this mode, called a "lightweight resolver daemon". I think this means "lightweight configuration" and "lightweight protocol" but not "lightweight implementation" since <tt>lwresd</tt> is in fact just an alias for the full BIND <tt>named</tt>. It was originally intended to offload the complexity of dealing with IPv6 bit-labels and A6 records, but it seems to have been neglected since they were obsoleted by the simpler AAAA records. It could really do with being resurrected for DNSSEC.</p>
<p>There are a few improvements that could be made to <tt>lwresd</tt>: it should reconfigure itself automatically when <tt>/etc/resolv.conf</tt> is updated; it should have a command-line option to turn on validation using the built-in trust anchors; it should try bypassing the forwarding server if it doesn't support DNSSEC; and (more speculatively) the lightweight resolver protocol could be extended to provide better DNSSEC error reporting - the DNS itself just gives an uninformative "server failure" reply if validation fails (or for any of several other errors).</p>
<p>The advantage of this setup is that it doesn't change the relationship between the end host and the network: you continue to use the resolvers you get from DHCP (<tt>lwresd</tt> forwards queries to them) so caches remain effective and strict firewalls don't break things (as they would if you ran a full resolver).</p>
