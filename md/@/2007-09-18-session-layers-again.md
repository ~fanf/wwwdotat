---
dw:
  anum: 148
  eventtime: "2007-09-18T16:46:00Z"
  itemid: 307
  logtime: "2007-09-18T19:06:52Z"
  props:
    import_source: livejournal.com/fanf/78954
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/78740.html"
format: casual
lj:
  anum: 106
  can_comment: 1
  ditemid: 78954
  event_timestamp: 1190133960
  eventtime: "2007-09-18T16:46:00Z"
  itemid: 308
  logtime: "2007-09-18T19:06:52Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/78954.html"
title: "Session layers, again"
...

There's been a moderately interesting thread on the main IETF list recently, especially the part starting with <a href="http://www1.ietf.org/mail-archive/web/ietf/current/msg47823.html">a message from Karl Auerbach</a> which reminded me of <a href="http://fanf.livejournal.com/53662.html">a post I wrote last year</a>. Especially interesting is the link posted by Lars Eggert to <a href="http://www.brynosaurus.com/pub/net/sst-abs.html">a paper by Bryan Ford</a> (of <a href="http://fanf.livejournal.com/74193.html">PEG parsers</a> fame). I need a less long-winded summary of my session layer idea, so this is it.

I'm starting from the observation that lots of application protocols have some idea of a session, but the Internet architecture provides no support for sessions so each application that needs one has re-invented the idea differently. The earliest example is the FTP control connection which manages multiple data connections. HTTP requests are mostly independent of each other, but it has had various feeble attempts at sessions built on top of it. TLS has a session cache to slightly reduce reconnect latency. BEEP and ssh use a single TCP connection per session and multiplex concurrent activities down the same pipe. Etc. There's obviously a need to fulfill.

A session is essentially a user's login to an application, therefore they should have the same lifetime. Login implies that a session's core service is a cryptographic association between the client and server, which has to provide the usual integrity, confidentiality, as well as authenticity features. (As such it should be able to replace IPsec, TLS, SASL, etc.)

On top of this is built a layer for multiplexing streams (and perhaps datagrams). Since it already has a security association, it can avoid many performance problems caused by multiplexing using concurrent TCP connections:

* it can omit the TCP three-way handshake and the TCP+TLS 6-way handshake, to get <a href="http://www.ietf.org/rfc/rfc1644.txt">T/TCP</a> startup performance without its security problems;

* it can avoid slow-start restart delays and <a href="http://www.w3.org/Protocols/HTTP/1.0/HTTPPerformance.html">related problems</a> by doing congestion control at the session layer instead of per-stream (see also <a href="http://www.ietf.org/rfc/rfc2140.txt">RFC 2140</a>);

* it can avoid the problems that ssh and BEEP have with <a href="http://sites.inka.de/~W1011/devel/tcp-tcp.html">multiple layers of windowing</a> and head-of-line blocking on packet loss, by making each stream run fully independently on top of the datagram layer;

* if it supports both reliable streams and best-effort datagrams in the same session then it'll support media apps (SIP, Jingle) well even when there is asymmetrical connectivity (NATs or firewalls - assuming they allow a session to be established in the first place);

* the crypto provides the endpoints with a secure identity for the session that's independent of lower-level addressing or routing, so they can re-establish the session if either end moves and can re-locate the other, without interrupting the higher-level data streams - mobility for free.

This is all very similar to Bryan Ford's SST design, so I'm really pleased that the lazyweb has turned my dreaming into something real! However SST's channels are lower-level than my sessions: all apps communicating between the same pair of hosts use one channel. I'm not sure how this affects higher-level authentication (SASL and perhaps X.509) - you would at least need some way to cryptographically bind app-level auth to the SST channel, but can other apps sharing the channel spoof this binding, and does it matter?

Last year's article also had some wild speculation about fixing the problems of NAT and global routing scalability, but that part is not relevant to the current discussion and too vague to be summarized more concisely, so I won't try (even though I still quite like the idea).
