---
dw:
  anum: 16
  eventtime: "2007-01-05T01:42:00Z"
  itemid: 272
  logtime: "2007-01-05T01:42:40Z"
  props:
    commentalter: 1491292378
    import_source: livejournal.com/fanf/70049
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/69648.html"
format: html
lj:
  anum: 161
  can_comment: 1
  ditemid: 70049
  event_timestamp: 1167961320
  eventtime: "2007-01-05T01:42:00Z"
  itemid: 273
  logtime: "2007-01-05T01:42:40Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/70049.html"
title: Greenwich meridian
...

<p>I recently read the official transcript of <a href="http://www.gutenberg.org/files/17759/17759-h/17759-h.htm">the international conference held at Washington DC in October 1884 for the purpose of fixing a prime meridian and a universal day</a>. This conference was called by the US government to (in effect) ratify at the political level a decision that had been made at the technical level by the International Geodetical Association in Rome in 1883.</p>

<p>I expected it to be deathly boring, but it turned out to be entertaining at a number of levels.</p>

<p>The result was that the Greenwich meridian would be considered the zero of longitude, and that it would be counted from -180&deg; in the West to +180&deg; in the East. Universal time would be counted from 0h to 24h such that noon at Greenwich would be 12h UT and the date line would be at 180&deg;. All of these details could have been different.</p>

<p>There is a lot of procedural guff in the transcript, and it was starting to look like a meeting of the People's Front of Judea (but less funny) when it turned into something like a skit from 1066 And All That. The three principal protagonists are the USA, Britain, and France, all of whom act like stereotypes of themselves.</p>

<ul>
<li>USA: brash, impatient, sure that they already have the correct answer and that this international diplomacy is just an irritatingly slow way of rubber-stamping a decision that has already been made, but nevertheless keen on openness and democracy in the most practical sense.</li>
<li>Britain: Top Nation and knows it, therefore can be smugly quiet with the knowledge that the result will be in their favour, with the occassional insufferably magnanimous and sporting comment that they "had no desire to advocate any one of the places enumerated" ect.</li>
<li>France: Utterly affronted by the Americans and pissed off that they aren't Top Nation any more, arguing that if they can't have the meridian then no-one can, and that the result should hold to the philosophical ideals of the metric system, the French Revolutionary Calendar, and decimal clocks.</li>
</ul>

<p>In the end, the main result was determined to a large extent by the fact that about 70% of the world's shipping used charts based on the Greenwich meridian, and this had also been the decision in Rome. However, they did not agree to all of the previous year's choices; in particular, because the people in Rome were astronomers and navigators, they preferred UT days to be counted noon-to-noon so that a night's observing could all be logged with the same date, whereas the people in Washington preferred to count them midnight-to-midnight following the usual practice amongst normal people. Also, in Rome they preferred to count longitude from 0&deg; to 360&deg;, but in Washington they decided on -180&deg; to +180&deg; because this was closer to the markings on existing charts. (The west/negative to east/positive choice was kept.)</p>

<p>On the way some interesting arguments were made, often including fascinating facts. A significant one was that the prime meridian should be based on a geographical feature, such as the centre of the Atlantic (the Azores) or the Pacific (the Bering strait). It's lucky that both Greenwich and Paris are close enough to being 180&deg; from the Bering strait for practical purposes - in particular that the International Date Line could (later) be mostly drawn along 180&deg; without undue pain. By the 1880s, sailors already had an informal date line through the Pacific; I guess that this is because of the size of the ocean and because America was colonized from Europe - would the date line go down the Atlantic if it had been colonized from the Far East? One of the wackier points was that the Gregorian calendar was a <em>Roman</em> calendar and that Rome was the centre of the Christian world, so it should obviously be the centre of universal time. A more sensible suggestion (but still too radical to be accepted) was that the world should be divided into hourly timezones, very similar to the ones we have now and to the ones that had already been established on the North American railways, and based on GMT.</p>

<p>The history subsequent to the Washington conference is somewhat ironic. One requirement at the time was that the prime meridian passed through the transit instrument of a good observatory which could thereby keep the time on which longitude depended (or that the meridian was some fixed offset from such an observatory, though they decided against that option to save on maritime charts). By the start of WWI, people were developing long-distance precision time transfer using radio telegraphy, so the dependence on a single observatory and master clock was no longer necessary or desirable. The French established the Bureau International de l'Heure (BIH) which defined and maintained time and longitude based on observations from multiple observatories, and thereby took back what they had lost three decades previously!</p>

<p>This should have made no difference, but owing to longstanding inaccuracies in the official longitudes of the various observatories, the zero longitude of the BIH's geodetic model no longer matched the Airy transit circle at Greenwich. Further errors (of about 10m) were introduced when the Royal Observatory moved from Greenwich to Herstmonceaux after WWII. The biggest change, however, happened when the standard geodetic model was fixed to be geocentric, i.e. to have a common centre of revolution with the Earth. Although the new model matched the old fairly well at the equator, it ended up being off by about 100m at Greenwich, and this discrepancy is still preserved by WGS84, the world geodetic system used by GPS. If you go to Greenwich you'll find that not only does your GPS receiver disagree with the markings on the ground, but also your Ordnance Survey maps - because they use a datum established before the construction of the Airy transit circle.</p>

<p>Nowadays the Greenwich Observatory is just a museum and irrelevant to time and longitude, despite what HMG thinks. For example, BBC Radio's six-pip time signal is now derived from GPS and synchronized to UTC (despite the fact that legal UK time is still GMT, i.e. UT1) - there's even a seventh pip to mark a leap second correctly! More absurdly, the government made a fuss about Greenwich time in the run-up to the millennium under the brand name "Greenwich Electronic Time", including promotion of NTP and installation of atomic clocks at the LINX (which is based at Telehouse and is coincidentally almost exactly on the meridian).</p>
