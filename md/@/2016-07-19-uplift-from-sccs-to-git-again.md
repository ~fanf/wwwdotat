---
dw:
  anum: 102
  eventtime: "2016-07-19T16:32:00Z"
  itemid: 462
  logtime: "2016-07-19T15:32:12Z"
  props:
    import_source: livejournal.com/fanf/145441
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/118374.html"
format: html
lj:
  anum: 33
  can_comment: 1
  ditemid: 145441
  event_timestamp: 1468945920
  eventtime: "2016-07-19T16:32:00Z"
  itemid: 568
  logtime: "2016-07-19T15:32:12Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/145441.html"
title: "Uplift from SCCS to git, again"
...

<p>Nearly two years ago I wrote about my project to convert the 25-year history of our DNS infrastructure from SCCS to git -
"<a href="http://fanf.livejournal.com/132656.html">uplift from SCCS to git</a>"</p>

<p>In May I got <a href="https://twitter.com/fanf/status/734099299248803840">email from Akrem ANAYA asking my SCCS to git scripts</a>. I was pleased that they might be of use to someone else!

<p>And now I have started work on moving our <a href="http://www.ucs.cam.ac.uk/managed-zone-service/">Managed Zone Service</a> to a new server. The MZS is our vanity domain system, for research groups who want domain names not under <tt>cam.ac.uk</tt>.

<p>The MZS also uses SCCS for revision control, so I have resurrected my uplift scripts for another outing. This time the job only took a couple of days, instead of a couple of months :-)

<p>The most amusing thing I found was the cron job which stores a daily dump of the MySQL database in SCCS...
