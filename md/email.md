Tony Finch's email address
==========================

I have a strange email address: <dot@dotat.at>. (The logo at the top
of this page is a dotat.)

When I first investigated buying my domain in early 1997 the Austrian
domain registry organised subdomains under `.co.at` and `.ac.at` etc.
so it was not available. Some time later that year the policy was
relaxed, so when I next looked in October 1997 I was able to buy
`dotat.at`. I'm still slightly surprised that no-one got there before
me!

Some of my other email addresses, past and present, are:

    fanf@isc.org
    fanf2@cam.ac.uk
    fanf@cb4.eu
    fanf@chiark.greenend.org.uk
    fanf@apache.org
    fanf@covalent.net
    fanf@demon.net
    fanf@exim.org
    fanf@FreeBSD.org
    fanf@inmos.co.uk
    fanf@lspace.org
