use crate::prelude::*;
use chrono::prelude::*;

pub fn year3339(rfc3339: &str) -> String {
    let yyyy = regex!(r#"^(\d\d\d\d)-"#);
    yyyy.captures(rfc3339).unwrap()[1].to_owned()
}

pub fn months3339(rfc3339: &str) -> u32 {
    let yyyy_mm = regex!(r#"^(\d\d\d\d)-(\d\d)-"#);
    let caps = yyyy_mm.captures(rfc3339).unwrap();
    let yyyy: u32 = caps[1].parse().unwrap();
    let mm: u32 = caps[2].parse().unwrap();
    12 * yyyy + mm
}

fn format3339(time: DateTime<Utc>) -> String {
    time.format("%FT%TZ").to_string()
}

pub fn now3339() -> String {
    format3339(Utc::now())
}

pub fn systime3339(time: std::time::SystemTime) -> String {
    format3339(DateTime::<Utc>::from(time))
}

pub fn unix3339(unix_time: i64) -> Result<String> {
    let date_time = DateTime::<Utc>::from_timestamp(unix_time, 0)
        .ok_or_else(|| anyhow!("could not convert unix time {unix_time}"))?;
    Ok(format3339(date_time))
}
