pub const WHAT_IDENT: &str = what_ident::what_ident!();

pub fn what_ident_options(opts: &[&str]) {
    if opts.is_empty()
        || std::env::args().any(|arg| opts.iter().any(|opt| arg == *opt))
    {
        print!("{WHAT_IDENT}");
        std::process::exit(0);
    }
}
