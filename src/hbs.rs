use crate::prelude::*;

pub use handlebars::Handlebars;
use handlebars::JsonRender as _;
use handlebars::Renderable as _;

pub fn html_cut(body: String, url: &str) -> (String, String) {
    let cut = regex!(r"(?s)^(.*?)\n<cut>\n(.*)$");
    if let Some(split) = cut.captures(&body) {
        let short = format!(
            "{}\n\n<p><i><a href='{}'>read more ...</a></i></p>\n",
            &split[1], url
        );
        (short, split[1].to_string() + &split[2])
    } else {
        (body.clone(), body)
    }
}

fn md_helper(
    h: &handlebars::Helper,
    _: &Handlebars,
    _: &handlebars::Context,
    _: &mut handlebars::RenderContext,
    out: &mut dyn handlebars::Output,
) -> handlebars::HelperResult {
    if let Some(param) = h.param(0) {
        out.write(&md::html(&param.value().render()))?
    } else if let Some(block) = h.template() {
        let mut md = handlebars::StringOutput::new();
        block.render(
            &Handlebars::new(),
            &handlebars::Context::null(),
            &mut handlebars::RenderContext::new(None),
            &mut md,
        )?;
        out.write(&md::html(&md.into_string()?))?
    } else {
        dbg!(h.is_block());
        dbg!(h.has_block_param());
        return Err(handlebars::RenderError::new("missing md param"));
    }
    Ok(())
}

fn svg_helper(
    h: &handlebars::Helper,
    _: &Handlebars,
    _: &handlebars::Context,
    _: &mut handlebars::RenderContext,
    out: &mut dyn handlebars::Output,
) -> handlebars::HelperResult {
    if let Some(param) = h.param(0) {
        let mut path = site_source_directory("other")?;
        path.push("svg");
        let name = param.value().render();
        if name.is_empty() {
            return Err(handlebars::RenderError::new("svg name is empty"));
        }
        path.push(&name);
        path.set_extension("svg");
        let svg = std::fs::read_to_string(&path).map_err(|e| {
            handlebars::RenderError::from_error(
                &format!("loading {}", path.display()),
                e,
            )
        })?;
        out.write(&svg)?;
    } else {
        dbg!(h.is_block());
        dbg!(h.has_block_param());
        return Err(handlebars::RenderError::new("missing svg name"));
    }
    Ok(())
}

pub fn read_all_handlebars() -> Result<Handlebars<'static>> {
    let path = site_source_directory("hbs")?;
    let mut hbs = Handlebars::new();
    hbs.set_strict_mode(true);
    hbs.register_templates_directory(".hbs", path)?;
    hbs.register_helper("md", Box::new(md_helper));
    hbs.register_helper("svg", Box::new(svg_helper));
    Ok(hbs)
}

pub trait Render {
    fn render_to_file<T, P>(
        &self,
        name: &str,
        data: &T,
        file: &P,
    ) -> Result<()>
    where
        P: AsRef<Path>,
        T: Serialize;

    fn render_template_to_file<T, P>(
        &self,
        template: &str,
        data: &T,
        file: &P,
    ) -> Result<()>
    where
        P: AsRef<Path>,
        T: Serialize;

    fn render_to_string<T>(&self, name: &str, data: &T) -> Result<String>
    where
        T: Serialize;
}

fn creat<P>(path: &P) -> Result<File>
where
    P: AsRef<Path>,
{
    std::fs::create_dir_all(path.as_ref().parent().unwrap())?;
    Ok(File::create(path)?)
}

impl Render for Handlebars<'_> {
    fn render_to_file<T, P>(&self, name: &str, data: &T, path: &P) -> Result<()>
    where
        P: AsRef<Path>,
        T: Serialize,
    {
        let ctx = || {
            let path = path.as_ref().display();
            anyhow!("failed to render_to_file(\"{}\", \"{}\")", name, path)
        };
        let file = creat(path).with_context(ctx)?;
        self.render_to_write(name, data, file).with_context(ctx)
    }

    fn render_template_to_file<T, P>(
        &self,
        template: &str,
        data: &T,
        path: &P,
    ) -> Result<()>
    where
        P: AsRef<Path>,
        T: Serialize,
    {
        let ctx = || {
            let path = path.as_ref().display();
            anyhow!("failed to render_template_to_file(\"{}\")", path)
        };
        let file = creat(path).with_context(ctx)?;
        self.render_template_to_write(template, data, file).with_context(ctx)
    }

    fn render_to_string<T>(&self, name: &str, data: &T) -> Result<String>
    where
        T: Serialize,
    {
        let ctx = || anyhow!("failed to render_to_string(\"{}\")", name);
        self.render(name, data).with_context(ctx)
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct Titles {
    head_title: String,
    body_title: String,
}

impl Titles {
    pub fn new(title: &str) -> Titles {
        Titles { head_title: title.to_string(), body_title: title.to_string() }
    }
}
