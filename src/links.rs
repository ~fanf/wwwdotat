use crate::prelude::*;

pub type TagsLinks = BTreeMap<String, RawLink>;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RawLink {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<String>,
    pub date: String,
    pub link: String,
    pub text: String,
}

impl RawLink {
    pub fn age(&self) -> u32 {
        months3339(&now3339()) - months3339(&self.date)
    }

    pub fn format(&self) -> String {
        if self.age() > 12 {
            format!(
                "{} retro-link! {} - {}",
                year3339(&self.date),
                self.link,
                self.text
            )
        } else {
            format!("{} - {}", self.link, self.text)
        }
    }

    pub fn format_newsy(&self) -> String {
        let mut text = String::new();
        for c in self.text.chars() {
            if text.len() < 80 {
                text.push(c);
            }
        }
        text
    }
}
