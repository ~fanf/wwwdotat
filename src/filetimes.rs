use crate::prelude::*;

const TIMES: &str = "times.yml";

pub struct Cache {
    times: BTreeMap<String, FileTimes>,
    status: BTreeMap<String, GitStatus>,
}

// this is kind of stupidly stringly typed
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct FileTimes {
    updated: String,
    published: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum GitStatus {
    Clean,
    Dirty,
    Unknown,
}

impl Default for FileTimes {
    fn default() -> FileTimes {
        let updated = "0000-00-00T00:00:00Z".to_string();
        let published = "9999-12-31T23:59:59Z".to_string();
        FileTimes { updated, published }
    }
}

impl FileTimes {
    pub fn update(&mut self, date: &str) {
        if self.published.as_str() > date {
            self.published = date.to_string();
        }
        if self.updated.as_str() < date {
            self.updated = date.to_string();
        }
    }

    fn new_from_file(path: &str) -> Result<FileTimes> {
        let meta = std::fs::metadata(path)?;
        let published = systime3339(meta.created()?);
        let updated = systime3339(meta.modified()?);
        Ok(FileTimes { published, updated })
    }

    fn new_from_git(path: &str) -> Result<FileTimes> {
        // print date in local time with strftime, and set local time to UTC
        let git = std::process::Command::new("git")
            .args(["log", "--format=%ad %s", "--date=format-local:%FT%TZ"])
            .arg(path)
            .env("TZ", "UTC")
            .output()?;
        std::io::stderr().write_all(&git.stderr).unwrap();
        if !git.status.success() {
            bail!("git log {} failed", path);
        }
        let log = String::from_utf8(git.stdout).unwrap();
        let line = regex!(
            r#"(?mx)^
                ([0-9-]{10}T[0-9:]{8}Z)
                \s+
                ([^\n]+)
            $"#
        );
        let mut oldest = None;
        let mut newest = None;
        let mut publish = None;
        for cap in line.captures_iter(&log) {
            if cap[2].contains("publish") {
                publish = Some(cap[1].to_string());
            }
            if newest.is_none() {
                newest = Some(cap[1].to_string());
            }
            oldest = Some(cap[1].to_string());
        }
        if publish.is_none() {
            publish = oldest;
        }
        if let (Some(published), Some(updated)) = (publish, newest) {
            Ok(FileTimes { published, updated })
        } else {
            FileTimes::new_from_file(path)
        }
    }
}

impl Cache {
    pub fn new() -> Result<Cache> {
        let git = std::process::Command::new("git")
            .args(["ls-files", "-cmot", "--stage"])
            .output()?;
        std::io::stderr().write_all(&git.stderr)?;
        if !git.status.success() {
            bail!("git ls-files failed: exit status {}", git.status);
        }

        let list = String::from_utf8(git.stdout)?;
        let line = regex!(
            r#"(?mx)^
                ([CHKMRS?])\ (?:[^\t]+\t)?(.*)
            $"#
        );
        let mut status = BTreeMap::new();
        for cap in line.captures_iter(&list) {
            status.insert(
                cap[2].to_string(),
                match &cap[1] {
                    "?" => GitStatus::Unknown,
                    "H" => GitStatus::Clean,
                    _ => GitStatus::Dirty,
                },
            );
        }

        let times = if Path::new(TIMES).is_file() {
            yaml::from_reader(
                File::open(TIMES)
                    .with_context(|| format!("could not read {}", TIMES))?,
            )?
        } else {
            BTreeMap::new()
        };

        Ok(Cache { times, status })
    }

    pub fn write(&self) -> Result<()> {
        Ok(yaml::to_writer(File::create(TIMES)?, &self.times)?)
    }

    fn git(&self, path: &str) -> Result<FileTimes> {
        if let Some(times) = self.times.get(path).cloned() {
            Ok(times)
        } else {
            FileTimes::new_from_git(path)
        }
    }

    pub fn get(&self, path: &str) -> Result<FileTimes> {
        match self.status.get(path) {
            Some(GitStatus::Clean) => self.git(path),
            Some(GitStatus::Dirty) => {
                let git_times = self.git(path)?;
                let file_times = FileTimes::new_from_file(path)?;
                Ok(FileTimes {
                    published: git_times.published,
                    updated: file_times.updated,
                })
            }
            _ => FileTimes::new_from_file(path),
        }
    }

    pub fn set(&mut self, path: &str, times: &FileTimes) {
        if Some(&GitStatus::Clean) == self.status.get(path) {
            self.times.insert(path.to_string(), times.clone());
        } else {
            self.times.remove(path);
        }
    }
}
